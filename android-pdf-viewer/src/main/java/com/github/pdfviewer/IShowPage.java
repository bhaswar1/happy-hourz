package com.github.pdfviewer;

import android.graphics.Bitmap;

public interface IShowPage {

    public Bitmap showPage(int index);
    public  void showToolBar(String string);
}
