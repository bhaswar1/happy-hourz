/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.admin.happyhours;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.admin.happyhours";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "1.3";
  // Fields from build type: debug
  public static final String API_KEY = "ABCXYZ123TEST";
  public static final String BASE_URL = "http://www.mocky.io/v2";
}
