// Generated by data binding compiler. Do not edit!
package com.admin.happyhours.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.admin.happyhours.R;
import com.admin.happyhours.customview.BookTextView;
import com.admin.happyhours.ui.transparentrestaurantaddtocollection.TransparentViewModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivityTransparentBinding extends ViewDataBinding {
  @NonNull
  public final CardView cardViewTransparent;

  @NonNull
  public final ImageView imgClose;

  @NonNull
  public final ConstraintLayout parentLayout;

  @NonNull
  public final RecyclerView recyclerView;

  @NonNull
  public final BookTextView tvTitle;

  @Bindable
  protected TransparentViewModel mViewModel;

  protected ActivityTransparentBinding(Object _bindingComponent, View _root, int _localFieldCount,
      CardView cardViewTransparent, ImageView imgClose, ConstraintLayout parentLayout,
      RecyclerView recyclerView, BookTextView tvTitle) {
    super(_bindingComponent, _root, _localFieldCount);
    this.cardViewTransparent = cardViewTransparent;
    this.imgClose = imgClose;
    this.parentLayout = parentLayout;
    this.recyclerView = recyclerView;
    this.tvTitle = tvTitle;
  }

  public abstract void setViewModel(@Nullable TransparentViewModel viewModel);

  @Nullable
  public TransparentViewModel getViewModel() {
    return mViewModel;
  }

  @NonNull
  public static ActivityTransparentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_transparent, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivityTransparentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivityTransparentBinding>inflateInternal(inflater, R.layout.activity_transparent, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityTransparentBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_transparent, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivityTransparentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivityTransparentBinding>inflateInternal(inflater, R.layout.activity_transparent, null, false, component);
  }

  public static ActivityTransparentBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivityTransparentBinding bind(@NonNull View view, @Nullable Object component) {
    return (ActivityTransparentBinding)bind(component, view, R.layout.activity_transparent);
  }
}
