// Generated by data binding compiler. Do not edit!
package com.admin.happyhours.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.admin.happyhours.R;
import com.admin.happyhours.customview.BookEditText;
import com.admin.happyhours.customview.BookTextView;
import com.admin.happyhours.ui.signup.SignupViewModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivitySignupBinding extends ViewDataBinding {
  @NonNull
  public final BookEditText etCpassword;

  @NonNull
  public final BookEditText etEmail;

  @NonNull
  public final BookEditText etName;

  @NonNull
  public final BookEditText etNumber;

  @NonNull
  public final BookEditText etPassword;

  @NonNull
  public final ImageView imvBack;

  @NonNull
  public final ImageView iv1;

  @NonNull
  public final ImageView iv2;

  @NonNull
  public final ConstraintLayout parentLl;

  @NonNull
  public final RelativeLayout rlFemal;

  @NonNull
  public final RelativeLayout rlMail;

  @NonNull
  public final BookTextView titleDob;

  @NonNull
  public final RelativeLayout tvCal;

  @NonNull
  public final BookTextView tvDob;

  @NonNull
  public final BookTextView tvGender;

  @NonNull
  public final BookTextView tvSignup;

  @NonNull
  public final View v1;

  @Bindable
  protected SignupViewModel mViewModel;

  protected ActivitySignupBinding(Object _bindingComponent, View _root, int _localFieldCount,
      BookEditText etCpassword, BookEditText etEmail, BookEditText etName, BookEditText etNumber,
      BookEditText etPassword, ImageView imvBack, ImageView iv1, ImageView iv2,
      ConstraintLayout parentLl, RelativeLayout rlFemal, RelativeLayout rlMail,
      BookTextView titleDob, RelativeLayout tvCal, BookTextView tvDob, BookTextView tvGender,
      BookTextView tvSignup, View v1) {
    super(_bindingComponent, _root, _localFieldCount);
    this.etCpassword = etCpassword;
    this.etEmail = etEmail;
    this.etName = etName;
    this.etNumber = etNumber;
    this.etPassword = etPassword;
    this.imvBack = imvBack;
    this.iv1 = iv1;
    this.iv2 = iv2;
    this.parentLl = parentLl;
    this.rlFemal = rlFemal;
    this.rlMail = rlMail;
    this.titleDob = titleDob;
    this.tvCal = tvCal;
    this.tvDob = tvDob;
    this.tvGender = tvGender;
    this.tvSignup = tvSignup;
    this.v1 = v1;
  }

  public abstract void setViewModel(@Nullable SignupViewModel viewModel);

  @Nullable
  public SignupViewModel getViewModel() {
    return mViewModel;
  }

  @NonNull
  public static ActivitySignupBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_signup, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivitySignupBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivitySignupBinding>inflateInternal(inflater, R.layout.activity_signup, root, attachToRoot, component);
  }

  @NonNull
  public static ActivitySignupBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_signup, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivitySignupBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivitySignupBinding>inflateInternal(inflater, R.layout.activity_signup, null, false, component);
  }

  public static ActivitySignupBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivitySignupBinding bind(@NonNull View view, @Nullable Object component) {
    return (ActivitySignupBinding)bind(component, view, R.layout.activity_signup);
  }
}
