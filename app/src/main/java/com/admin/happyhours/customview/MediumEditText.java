package com.admin.happyhours.customview;

import android.content.Context;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;


/**

 */
public class MediumEditText extends AppCompatEditText {

    public MediumEditText(Context context) {
        super(context);
        init(context);
    }

    public MediumEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public MediumEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context) {
        super.setTypeface(FontCache.get("AvenirLTStd-Medium.otf", context));
    }
}
