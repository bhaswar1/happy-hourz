package com.admin.happyhours.customview;

import android.content.Context;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by Bodhidipta on 13/06/16.
 */
public class RegularTextView extends AppCompatTextView {

    public RegularTextView(Context context) {
        super(context);
        init(context);
    }

    public RegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public RegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context) {
        super.setTypeface(FontCache.get("Avenir.ttf", context));
    }
}