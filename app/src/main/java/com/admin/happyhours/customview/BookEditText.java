package com.admin.happyhours.customview;

import android.content.Context;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;


public class BookEditText extends AppCompatEditText {

    public BookEditText(Context context) {
        super(context);
        init(context);
    }

    public BookEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public BookEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context) {
        super.setTypeface(FontCache.get("AvenirLTStd-Book.otf", context));
    }
}
