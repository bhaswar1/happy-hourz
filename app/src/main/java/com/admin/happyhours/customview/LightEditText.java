package com.admin.happyhours.customview;

import android.content.Context;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;


public class LightEditText extends AppCompatEditText {

    public LightEditText(Context context) {
        super(context);
        init(context);
    }

    public LightEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public LightEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context) {
        super.setTypeface(FontCache.get("AvenirLTStd-Light.otf", context));
    }

}
