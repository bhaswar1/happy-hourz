package com.admin.happyhours.customview;

import android.content.Context;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

/**

 */
public class HeavyTextView extends AppCompatTextView {

    public HeavyTextView(Context context) {
        super(context);
        init(context);
    }

    public HeavyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public HeavyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context) {
        super.setTypeface(FontCache.get("AvenirLTStd-Heavy.otf", context));
    }
}