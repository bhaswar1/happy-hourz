package com.admin.happyhours.datatypecommon;

import com.admin.happyhours.data.model.api.response.EventListingResponse;
import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;

public class EventBusForEventListingLike {

    String button_tyoe;
    int position;
    EventListingResponse.Details detail;

    public EventBusForEventListingLike(String button_tyoe, int position, EventListingResponse.Details detail) {
        this.button_tyoe = button_tyoe;
        this.position = position;
        this.detail = detail;
    }

    public String getButton_tyoe() {
        return button_tyoe;
    }

    public int getPosition() {
        return position;
    }

    public EventListingResponse.Details getDetail() {
        return detail;
    }
}
