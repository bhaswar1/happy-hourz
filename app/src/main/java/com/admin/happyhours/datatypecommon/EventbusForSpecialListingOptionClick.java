package com.admin.happyhours.datatypecommon;

import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;

public class EventbusForSpecialListingOptionClick {

     String check_option_click;
     int position;
    SpecialPageRestuarantListingResponse.Detail detail;

    public EventbusForSpecialListingOptionClick(String check_option_click, int position, SpecialPageRestuarantListingResponse.Detail detail) {
        this.check_option_click = check_option_click;
        this.position = position;
        this.detail = detail;
    }

    public String getCheck_option_click() {
        return check_option_click;
    }

    public int getPosition() {
        return position;
    }

    public SpecialPageRestuarantListingResponse.Detail getDetail() {
        return detail;
    }
}
