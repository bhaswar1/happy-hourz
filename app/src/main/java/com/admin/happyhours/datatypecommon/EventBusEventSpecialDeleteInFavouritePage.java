package com.admin.happyhours.datatypecommon;

public class EventBusEventSpecialDeleteInFavouritePage {

    /* False for Event and True for Special*/
    boolean aBoolean_event_or_special;

    int position;

    public EventBusEventSpecialDeleteInFavouritePage(boolean aBoolean_event_or_special, int position) {
        this.aBoolean_event_or_special = aBoolean_event_or_special;
        this.position = position;
    }

    public boolean isaBoolean_event_or_special() {
        return aBoolean_event_or_special;
    }

    public int getPosition() {
        return position;
    }
}
