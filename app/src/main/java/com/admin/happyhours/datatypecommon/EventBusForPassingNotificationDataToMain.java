package com.admin.happyhours.datatypecommon;

public class EventBusForPassingNotificationDataToMain {

    String message;

    public EventBusForPassingNotificationDataToMain(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
