package com.admin.happyhours.datatypecommon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationDatatype {

    @SerializedName("body")
    @Expose
    String body;

    @SerializedName("title")
    @Expose
    String title;

    @SerializedName("notification")
    @Expose
    String notification;
    @SerializedName("eventid")
    @Expose
    String eventid;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @SerializedName("badge")
    @Expose
    String badge;

    public String getNotification() {
        return notification;
    }

    public String getEventid() {
        return eventid;
    }

    public String getBadgecount() {
        return badge;
    }
}
