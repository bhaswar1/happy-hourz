package com.admin.happyhours.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.admin.happyhours.R;
import com.admin.happyhours.datatypecommon.EventBusForPassingNotificationDataToMain;
import com.admin.happyhours.datatypecommon.NotificationDatatype;
import com.admin.happyhours.ui.main.HomeActivity;
import com.admin.happyhours.utils.AppConstants;
import com.admin.happyhours.utils.SharedPrefManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "PushNotificationService";
    private static int NOTIFICATION_ID = 1;

    private SharedPrefManager sharedPrefManager;
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        if (s != null && !TextUtils.isEmpty(s.trim())) {
            Log.d(TAG, "Refreshed_token: " + s);
            SharedPrefManager.getInstance(getApplicationContext()).saveDeviceToken(s);
        } else {
            //String str_token = FirebaseInstanceId.getInstance().getToken();
            //Log.d(TAG, "Refreshed_token: " + str_token);

            FirebaseMessaging.getInstance().getToken()
                    .addOnCompleteListener(new OnCompleteListener<String>() {
                        @Override
                        public void onComplete(@NonNull Task<String> task) {
                            if (!task.isSuccessful()) {
                                Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                                return;
                            }

                            // Get new FCM registration token
                            String str_token = task.getResult();

                            SharedPrefManager.getInstance(getApplicationContext()).saveDeviceToken(str_token);
                        }
                    });
        }

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(s);
    }


    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        Log.d(TAG, "Message_data_push: " + remoteMessage.getNotification());
//        if (remoteMessage.getData()!=null){
//            for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
//                String key = entry.getKey();
//                String value = entry.getValue();
//                Log.d(TAG, "key, " + key + " value " + value);
//            }
//        }
///
        sharedPrefManager = SharedPrefManager.getInstance(getApplicationContext());
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0  && sharedPrefManager.getBooleanForIsUserAlreadyLoggedIn()) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (remoteMessage.getData().get("message") != null) {
                Log.d(TAG, "Message  " + remoteMessage.getData().get("message"));
                sendNotification(remoteMessage.getData().get("message"), remoteMessage);


            }

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                scheduleJob();
            } else {
                // Handle message within 10 seconds
//                handleNow();
            }
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + new Gson().toJson(remoteMessage.getNotification()));
        }
    }


    public void sendNotification(String msg, RemoteMessage extras) {

        NotificationDatatype notificationDatatype = new Gson().fromJson(msg, NotificationDatatype.class);
        Intent intent=new Intent();
        intent.putExtra("badge",notificationDatatype.getBadgecount());
        intent.setAction("TestAction1");
        sendBroadcast(intent);
        //notificationDatatype.getBadgecount()


        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Intent i;
        PendingIntent contentIntent = null;

        sharedPrefManager.saveNotification(msg);

        if (isAppRunning(getApplicationContext())) {
            Log.i("isAppRunning ", "true");
            EventBus.getDefault().post(new EventBusForPassingNotificationDataToMain(msg));
//            sharedPrefManager.saveBooleanForNotificationRedirection(true);

            i = new Intent(HomeActivity.newIntent(getApplicationContext(), msg));



        } else {

            Log.i("isAppRunning ", "false");
//            i = new Intent(this, Splash.class);
//            sharedPrefManager.saveBooleanForNotificationRedirection(true);
            i = new Intent(HomeActivity.newIntent(getApplicationContext(), msg));


         //   i = new Intent(getApplicationContext().getPackageName()+"."+SplashActivity.newIntent(getApplicationContext(), msg));
        }




        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_push);
//        contentView.setImageViewResource(R.id.image, R.mipmap.ic_launcher);
        contentView.setImageViewResource(R.id.image, R.mipmap.ic_launcher);
        contentView.setTextViewText(R.id.title, notificationDatatype.getTitle());
        contentView.setTextViewText(R.id.text, notificationDatatype.getBody()
                 );
//        new String(Character.toChars(    0x1F440)
//        new String(Character.toChars())
//Re
        contentIntent = PendingIntent.getActivity(this, 0, i, 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher_round);

        String channelId = AppConstants.channelId;
        CharSequence channelName = AppConstants.channelname;
        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);

            notificationChannel.setName(getString(R.string.happy_hourz_notification));
              notificationChannel.setDescription(notificationDatatype.getBadgecount());
          //  notificationChannel.setDescription("Notification : " + notificationDatatype.getNotification());
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notificationChannel.setSound(alarmSound, new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build());
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mNotificationManager.createNotificationChannel(notificationChannel);
        }



Log.d("notificationDatatype","--"+ new Gson().toJson(notificationDatatype).toString());
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, AppConstants.channelId)



//                .setSmallIcon(R.mipmap.ic_launcher_round)


            //    .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentTitle(notificationDatatype.getTitle())
                .setAutoCancel(true)
                .setNumber(Integer.parseInt(notificationDatatype.getBadgecount()))
                .setContentText(notificationDatatype.getBody())
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(
                                notificationDatatype.getBody())
                        );

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setSmallIcon(R.drawable.icon_transperent);
            mBuilder.setLargeIcon(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),
                    R.mipmap.ic_launcher_round), 128, 128, false));
            mBuilder.setColor(getResources().getColor(R.color.light_blue));
        } else {
            mBuilder.setSmallIcon(R.mipmap.ic_launcher_round);
            mBuilder.setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false));

        }


//        " "+new String(Character.toChars(    0x1F440)
        //     .setStyle(new NotificationCompat. DecoratedCustomViewStyle());

//      .  setStyle(NotificationCompat.BigTextStyle()
//                .bigText(emailObject.getSubjectAndSnippet()));


      //  mBuilder.setCustomContentView(contentView);
      //  mBuilder.setCustomBigContentView(contentView);
//                .setContentText(notificationDatatype.getBody());

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);
        mBuilder.setDefaults(-1);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setAutoCancel(true);


        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        //notification.defaults |= Notification.DEFAULT_SOUND;
       // notification.defaults |= Notification.DEFAULT_VIBRATE;


        mNotificationManager.notify(Integer.parseInt(notificationDatatype.getBadgecount()), mBuilder.build());
    }

    public static boolean isAppRunning(Context context) {
        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();

        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(context.getPackageName())) {
                if (appProcess.importance != ActivityManager.RunningAppProcessInfo.IMPORTANCE_PERCEPTIBLE) {
                    return true;
                }
            }
        }
        return false;
    }

}
