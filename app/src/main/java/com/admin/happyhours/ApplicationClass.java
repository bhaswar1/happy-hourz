package com.admin.happyhours;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

import com.admin.happyhours.data.remote.retrofitnetwork.ApiFactory;
import com.admin.happyhours.data.remote.retrofitnetwork.UsersService;
import com.admin.happyhours.di.component.DaggerAppComponent;

public class ApplicationClass extends Application implements HasActivityInjector {


    private static UsersService usersService;
    private static ApplicationClass applicationClass;

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this);

        usersService = ApiFactory.create();
        applicationClass = this;



        /*Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)  // Enables Crashlytics debugger
                .build();
        Fabric.with(fabric);*/
    }

    public static synchronized ApplicationClass getInstance(){
        Log.e("===check===","4");
        return applicationClass;
    }

    public static UsersService getRetrofitService(){
        Log.e("===check===","5");
        return usersService;
    }
}
