package com.admin.happyhours.interfaces;

import android.view.View;

public interface ItemLongClickListener {

    void onItemLongClick(int position, View view);
}
