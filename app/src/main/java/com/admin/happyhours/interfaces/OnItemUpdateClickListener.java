package com.admin.happyhours.interfaces;


public interface OnItemUpdateClickListener {
    void onItemClick(int position, int delete);
}
