package com.admin.happyhours.interfaces;


import android.view.View;

public interface OnItemClickListener {
    void onItemClick(int position, View view);
}
