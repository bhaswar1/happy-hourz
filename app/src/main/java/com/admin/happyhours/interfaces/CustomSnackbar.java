package com.admin.happyhours.interfaces;

import android.graphics.Typeface;

import com.admin.happyhours.utils.SSnackbar;


public interface CustomSnackbar {
    SSnackbar.Builder setTextColor(int textColor);

    SSnackbar.Builder setBackgroundColor(int backgroundColor);

    SSnackbar.Builder setDuration(int duration);

    SSnackbar.Builder setStyle(SnackbarStyle style);

    SSnackbar.Builder setPosition(SnackbarPosition position);

    SSnackbar.Builder setTypeface(Typeface typeface);

    SSnackbar.Builder setText(String text);

    public enum SnackbarStyle {
        NORMAL(Typeface.NORMAL),
        BOLD(Typeface.BOLD),
        ITALIC(Typeface.ITALIC),
        BOLD_ITALIC(Typeface.BOLD_ITALIC);

        private final int value;

        private SnackbarStyle(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

    public enum SnackbarPosition {
        BOTTOM, TOP
    }
}
