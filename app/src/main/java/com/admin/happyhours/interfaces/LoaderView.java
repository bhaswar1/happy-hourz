package com.admin.happyhours.interfaces;

import android.graphics.Paint;


public interface LoaderView {
    void setRectColor(Paint rectPaint);

    void invalidate();

    boolean valueSet();


}
