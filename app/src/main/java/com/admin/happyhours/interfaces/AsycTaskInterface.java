package com.admin.happyhours.interfaces;

import org.json.JSONObject;

public interface AsycTaskInterface {
    void  onSuccess(String response, String msg, JSONObject jsonObject);
    void onError(String msg);
}
