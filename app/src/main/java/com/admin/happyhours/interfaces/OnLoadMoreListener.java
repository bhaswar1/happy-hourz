package com.admin.happyhours.interfaces;

public interface OnLoadMoreListener {
    void onLoadMore();
}
