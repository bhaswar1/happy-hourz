package com.admin.happyhours.interfaces;


public interface OnConnectionTimeoutListener {
    void onConnectionTimeout();

}
