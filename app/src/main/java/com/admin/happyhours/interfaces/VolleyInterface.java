package com.admin.happyhours.interfaces;

import org.json.JSONObject;

public interface VolleyInterface {
   void  onSuccess(Integer code, String msg, JSONObject jsonObject);
    void onError(String msg);
}
