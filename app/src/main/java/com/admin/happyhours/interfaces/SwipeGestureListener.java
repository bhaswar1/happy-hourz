package com.admin.happyhours.interfaces;

public interface SwipeGestureListener {

    void onLeftFling();

    void onRightFling();

    void onUpFling();

    void onDownFling();

    void onLeftScroll();

    void onRightScroll();

    void onUpScroll();

    void onDownScroll();
}
