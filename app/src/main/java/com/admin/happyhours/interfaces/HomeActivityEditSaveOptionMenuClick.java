package com.admin.happyhours.interfaces;

public interface HomeActivityEditSaveOptionMenuClick {

    void onSave();

    void onEdit();

}
