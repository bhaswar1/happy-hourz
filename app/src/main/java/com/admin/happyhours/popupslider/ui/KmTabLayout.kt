package com.kodmap.app.library.ui

import android.content.Context
import com.google.android.material.tabs.TabLayout
import androidx.core.content.ContextCompat
import android.util.AttributeSet

class KmTabLayout : TabLayout {
    constructor(context: Context) : super(context) {

    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {

    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {

    }


}