package com.kodmap.app.library.ui

import android.annotation.SuppressLint
import android.content.Context
import androidx.viewpager.widget.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent


class KmViewPager : androidx.viewpager.widget.ViewPager {

    private var swipeLocked: Boolean = false

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return !swipeLocked && super.onTouchEvent(event)
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
//        if(event.pointerCount>=0)
//        return !swipeLocked && super.onInterceptTouchEvent(event)
//        return false
        try {
            return super.onInterceptTouchEvent(event)
        } catch (ex: IllegalArgumentException) {
            ex.printStackTrace()
        }

        return false
    }

    override fun canScrollHorizontally(direction: Int): Boolean {

        return !swipeLocked && super.canScrollHorizontally(direction)
    }


    fun setSwipeLocked(bool: Boolean) {
        swipeLocked = bool
    }

}
