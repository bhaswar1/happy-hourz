package com.admin.happyhours.utils;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

import com.admin.happyhours.interfaces.SwipeGestureListener;


public class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {

    private static final int SLIDE_THRESHOLD = 100;
    final static String LOGTAG = SwipeGestureDetector.class.getSimpleName();
    private SwipeGestureListener swipeGestureListener;

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        try {
            float deltaY = e2.getY() - e1.getY();
            float deltaX = e2.getX() - e1.getX();

            if (Math.abs(deltaX) > Math.abs(deltaY)) {
                if (Math.abs(deltaX) > SLIDE_THRESHOLD) {
                    if (deltaX > 0) {
                        // the user made a sliding right gesture
                        swipeGestureListener.onRightScroll();
                    } else {
                        // the user made a sliding left gesture
                        swipeGestureListener.onLeftScroll();
                    }
                }
            } else {
                if (Math.abs(deltaY) > SLIDE_THRESHOLD) {
                    if (deltaY > 0) {
                        // the user made a sliding down gesture
                        swipeGestureListener.onDownScroll();
                    } else {
                        // the user made a sliding up gesture
                        swipeGestureListener.onUpScroll();
                    }
                }
            }
        } catch (Exception exception) {
            Log.e(LOGTAG, exception.getMessage());
        }

        return false;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2,
                           float velocityX, float velocityY) {

        switch (getSlope(e1.getX(), e1.getY(), e2.getX(), e2.getY())) {
            case 1:
                Log.d(LOGTAG, "top");
                if (swipeGestureListener!=null){
                    swipeGestureListener.onUpFling();
                }
                return true;
            case 2:
                Log.d(LOGTAG, "left");
                if (swipeGestureListener!=null){
                    swipeGestureListener.onLeftFling();
                }
                return true;
            case 3:
                Log.d(LOGTAG, "down");
                if (swipeGestureListener!=null){
                    swipeGestureListener.onDownFling();
                }
                return true;
            case 4:
                Log.d(LOGTAG, "right");
                if (swipeGestureListener!=null){
                    swipeGestureListener.onRightFling();
                }
                return true;
        }
        return false;
    }

    private int getSlope(float x1, float y1, float x2, float y2) {
        Double angle = Math.toDegrees(Math.atan2(y1 - y2, x2 - x1));
        if (angle > 45 && angle <= 135)
            // top
            return 1;
        if (angle >= 135 && angle < 180 || angle < -135 && angle > -180)
            // left
            return 2;
        if (angle < -45 && angle >= -135)
            // down
            return 3;
        if (angle > -45 && angle <= 45)
            // right
            return 4;
        return 0;
    }

    public void setSwipeGestureListener(SwipeGestureListener swipeGestureListener) {
        this.swipeGestureListener = swipeGestureListener;
    }
}