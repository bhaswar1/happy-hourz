package com.admin.happyhours.utils.GoogleMapHelper;

import org.json.JSONObject;

public interface ReturnAddressDetailsAlongLatLong {

    void returnData(JSONObject result, String latitude, String longitude, String country, String administrativeAreaLevel1,
                    String administrativeAreaLevel2, String locality);

}
