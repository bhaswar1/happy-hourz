package com.admin.happyhours.utils;

import android.text.InputFilter;
import android.text.Spanned;

public class EdittextInputFilters {


    public static InputFilter getFilterWithoutSpace(){
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (Character.isWhitespace(source.charAt(i))) {
                        return "";
                    }
                }
                return null;
            }

        };
        return filter;
    }

    public static InputFilter getFilterForLettersOnly(){
        return new InputFilter() {
            public CharSequence filter(CharSequence src, int start,
                                       int end, Spanned dst, int dstart, int dend) {
                if (src.toString().matches("[a-zA-Z ]+")) {
                    return src;
                }
                return "";
            }
        };
    }

}
