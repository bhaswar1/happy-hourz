package com.admin.happyhours.utils;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by su on 9/1/17.
 */

public class CustomJSONParser {

    public static String ImageParam;


    public void fireAPIForGetMethod(Context mcontext, final String URL, final HashMap<String, String> setGetData,
                                    final CustomJSONResponse customJSONResponse) {


        if (NetworkUtils.isNetworkConnected(mcontext)) {

            new AsyncTask<Void, Void, Void>() {

                String PARAMS = "?";
                private String stringResponse = null;
                private Exception exception = null;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

                    customJSONResponse.onStart();

                    if (setGetData != null && setGetData.size() > 0) {
//                        PARAMS = "&";
                        for (Map.Entry<String, String> entry : setGetData.entrySet()) {
//                            System.out.println(entry.getKey() + " = " + entry.getValue());
                            PARAMS = PARAMS + entry.getKey() + "=" + entry.getValue() + "&";
                        }

                    }
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        if (!isCancelled()) {

                            OkHttpClient client = new OkHttpClient.Builder().retryOnConnectionFailure(true).connectTimeout(10000, TimeUnit.MILLISECONDS).build();
                            Request request = new Request.Builder().url(URL + PARAMS).build();
                            Response response = client.newCall(request).execute();

                            stringResponse = response.body().string();
                            new JSONObject(stringResponse);


//                       Logger.printMessage("response", "respose_ww_body::" + response.body().string());
                        }
                    } catch (Exception e) {
                        this.exception = e;
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (!isCancelled() && exception == null) {
                        try {
                            if (new JSONObject(stringResponse).getString("status").equalsIgnoreCase("success")) {
                                customJSONResponse.onSuccess(stringResponse);
                            } else {
                                customJSONResponse.onError(new JSONObject(stringResponse).getString("message") + "", stringResponse);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        customJSONResponse.onError(exception.getMessage() + "");
                    }
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            customJSONResponse.onError("No internet connection found. Please check your internet connection.");
        }
    }


    public interface CustomJSONResponse {
        void onSuccess(String result);

        void onError(String error, String response);

        void onError(String error);

        void onStart();
    }

}
