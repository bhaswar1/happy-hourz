package com.admin.happyhours.utils;

import android.content.Context;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class UuidFactory {

	/**
	 *
	 * @param cnt
	 * @return
	 */
	public static String getUuId(Context cnt) {
		String result;

        final String androidId = Secure.getString(cnt.getContentResolver(), Secure.ANDROID_ID);
		System.out.println("ANDROID_ID="+androidId);

		System.out.println("UUID="+ UUID.randomUUID().toString());

        // get device id
       	final String deviceId = ((TelephonyManager) cnt.getSystemService( Context.TELEPHONY_SERVICE )).getDeviceId();
		//final String deviceId = Installation.id(cnt);
		System.out.println("deviceId="+deviceId);
        // split android ID in to two parts
        String androidIdPart1 = androidId.substring(0, androidId.length() / 2);
        String androidIdPart2 = androidId.substring(androidId.length() / 2);

        // split device ID in to two parts
        String deviceIdPart1 = deviceId.substring(0, deviceId.length() / 2);
        String deviceIdPart2 = deviceId.substring(deviceId.length() / 2);
        
        // mix parts
        String finalIdString = androidIdPart1 + deviceIdPart2 + deviceIdPart1 + androidIdPart2;
        
        // encode string to md5
        //result = "d896d51e453358743550670e1835aea";
        result = encodeToMD5(finalIdString);


        return result;
	}
	
	/**
	 * 
	 * @param source
	 * @return
	 */
	private static String encodeToMD5(String source) {
	    String result = null;
		MessageDigest m;
		try {
			m = MessageDigest.getInstance("MD5");
		    m.update(source.getBytes(), 0, source.length());
		    result = new BigInteger(1, m.digest()).toString(16);
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		}
		return result;
	}
}
