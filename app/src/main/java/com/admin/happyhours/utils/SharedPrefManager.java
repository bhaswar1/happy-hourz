package com.admin.happyhours.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {



    private static SharedPrefManager mInstance;
    private static Context mCtx;
    private static final String TAG_TOKEN = "tagtoken";
    private static final String TAG_NOTIFICATION = "tagnotification";
    private static final String TAG_BOOLEAN_FOR_NOTIFICATION_REDIRECTION = "notificationreirection";
    private static final String TAG_BOOLEAN_IS_NOTIFICATION_SHOWABLE = "notificationshowable";
    private static final String SHARED_PREF_NAME = "FCMSharedPref";
    private SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }




    public boolean saveDeviceToken(String token){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TAG_TOKEN, token);

        editor.apply();
        return true;
    }
    public String getDeviceToken(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return  sharedPreferences.getString(TAG_TOKEN, null);
    }


    public boolean saveNotification(String notification){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TAG_NOTIFICATION, notification);
        editor.apply();
        return true;
    }
    public String getNotification(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return  sharedPreferences.getString(TAG_NOTIFICATION, null);
    }
    public void deleteNotification(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(TAG_NOTIFICATION).apply();
    }



    public boolean saveBooleanForIsUserAlreadyLoggedIn(boolean notification_redirection){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(TAG_BOOLEAN_IS_NOTIFICATION_SHOWABLE, notification_redirection);
        editor.apply();
        return true;
    }
    public boolean getBooleanForIsUserAlreadyLoggedIn(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return  sharedPreferences.getBoolean(TAG_BOOLEAN_IS_NOTIFICATION_SHOWABLE, false);
    }


    /*public boolean saveBooleanForNotificationRedirection(boolean notification_redirection){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(TAG_BOOLEAN_FOR_NOTIFICATION_REDIRECTION, notification_redirection);
        editor.apply();
        return true;
    }
    public boolean getBooleanForNotificationRedirection(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return  sharedPreferences.getBoolean(TAG_BOOLEAN_FOR_NOTIFICATION_REDIRECTION, false);
    }*/
    /*public void deleteBooleanForNotificationRedirection(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(TAG_BOOLEAN_FOR_NOTIFICATION_REDIRECTION).apply();
    }*/

}
