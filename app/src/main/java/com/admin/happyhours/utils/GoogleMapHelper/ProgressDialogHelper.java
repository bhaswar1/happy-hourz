package com.admin.happyhours.utils.GoogleMapHelper;

import android.app.ProgressDialog;
import android.content.Context;

import com.admin.happyhours.R;


public class ProgressDialogHelper {

    static ProgressDialog progressDialog;

    public static void showProgressDialogPleaseWait(Context mContext) {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(mContext);
        }
        progressDialog.setMessage(mContext.getResources().getString(R.string.wait));
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public static void hideProgressDialogPleaseWait() {
        if (progressDialog == null) {
            return;
        }
        progressDialog.dismiss();
        progressDialog = null;
    }

}
