package com.admin.happyhours.utils;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import android.widget.DatePicker;

public class CustomDatePickerDialog extends DatePickerDialog {

    private int maxYear;
    private int maxMonth;
    private int maxDay;

    public CustomDatePickerDialog(Context context, OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth) {
        super(context, callBack, year, monthOfYear, dayOfMonth);
    }

    public void setMaxDate(long maxDate) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getDatePicker().setMinDate(maxDate);
        } else {

        }
    }

    public void setMinDate(long maxDate) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            getDatePicker().setMaxDate(maxDate);
        } else {

        }
    }

    @Override
    public void onDateChanged(@NonNull DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            super.onDateChanged(view, year, monthOfYear, dayOfMonth);
        } else {
            if (year < maxYear)
                view.updateDate(maxYear, maxMonth, maxDay);

            if (monthOfYear < maxMonth && year == maxYear)
                view.updateDate(maxYear, maxMonth, maxDay);

            if (dayOfMonth < maxDay && year == maxYear && monthOfYear == maxMonth)
                view.updateDate(maxYear, maxMonth, maxDay);
        }
    }

}
