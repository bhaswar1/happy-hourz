package com.admin.happyhours.utils.GoogleMapHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;

import androidx.core.widget.NestedScrollView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.admin.happyhours.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.admin.happyhours.utils.GoogleMapHelper.HelperClassGoogleMapConstants.API_KEY;
import static com.admin.happyhours.utils.GoogleMapHelper.HelperClassGoogleMapConstants.GET_ADDRESS_FROM_LATLONG;
import static com.admin.happyhours.utils.GoogleMapHelper.HelperClassGoogleMapConstants.OUT_JSON;
import static com.admin.happyhours.utils.GoogleMapHelper.HelperClassGoogleMapConstants.OUT_JSON2;
import static com.admin.happyhours.utils.GoogleMapHelper.HelperClassGoogleMapConstants.PLACES_API_BASE;
import static com.admin.happyhours.utils.GoogleMapHelper.HelperClassGoogleMapConstants.PLACES_API_BASE_DETAILS;
import static com.admin.happyhours.utils.GoogleMapHelper.HelperClassGoogleMapConstants.TYPE_AUTOCOMPLETE;

public class HelperClassGoogleMap {


    public static GoogleMap mMap;
    public static Marker myMarker;
    public static Marker myMarker1[]=new Marker[1000];
    static URL url, addressurl;
    static JSONArray predictionsJsonArr;
    public static Marker selectedMarkerForNearby = null;
    public static Marker selectedMarkerForEvents = null;
    public static Marker selectedMarkerForHotels = null;
    public static  boolean check=true;
   public static int count=1;
    public static void customZoomIntoMapAfterShowingMarkerOnMap(LatLng latLng, float zoomLevel) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoomLevel).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.moveCamera(cameraUpdate);
    }

    public static void showMarkerOnMap(SupportMapFragment mapFragment, final ArrayList<Double> latitudes, final ArrayList<Double> longitudes,
                                       final ArrayList<String> markerTitles) {
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {
                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
//                        latitude=String.valueOf(marker.getPosition().latitude);
//                        longitude= String.valueOf(marker.getPosition().longitude);
//
//                        Log.v("latitude", String.valueOf(marker.getPosition().latitude));
//                        Log.v("longitude", String.valueOf(marker.getPosition().longitude));
//                        getAddressFromLocation((marker.getPosition().latitude), marker.getPosition().longitude);
                    }
                });
                for (int i = 0; i < latitudes.size(); i++) {
                    myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(i), longitudes.get(i))).title(markerTitles.get(i)).
                            icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_icon)).draggable(false));
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudes.get(0), longitudes.get(0)), 13f));

            }
        });
    }

    public static void showMarkerOnMapwithuser(SupportMapFragment mapFragment, final ArrayList<Double> latitudes, final ArrayList<Double> longitudes,
                                               final ArrayList<String> markerTitles) {
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {
                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
//                        latitude=String.valueOf(marker.getPosition().latitude);
//                        longitude= String.valueOf(marker.getPosition().longitude);
//
//                        Log.v("latitude", String.valueOf(marker.getPosition().latitude));
//                        Log.v("longitude", String.valueOf(marker.getPosition().longitude));
//                        getAddressFromLocation((marker.getPosition().latitude), marker.getPosition().longitude);
                    }
                });
                myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(0), longitudes.get(0))).title(markerTitles.get(0)).
                        icon(BitmapDescriptorFactory.fromResource(R.drawable.locetorcurrent)).draggable(false));
                for (int i = 1; i < latitudes.size(); i++) {
                    myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(i), longitudes.get(i))).title(markerTitles.get(i)).
                            icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_icon)).draggable(false));
                    Log.d("check_map_lat ", i + ": " + latitudes.get(i));
                    Log.d("check_map_long ", i + ": " + longitudes.get(i));
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudes.get(0), longitudes.get(0)), 13f));

            }
        });
    }

    public static void showMarkerOnMapWithMapReadyInterface(SupportMapFragment mapFragment, final ArrayList<Double> latitudes, final ArrayList<Double> longitudes,
                                                            final ArrayList<String> markerTitles, final MapIsReady mapIsReady) {
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {

                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
//                        latitude=String.valueOf(marker.getPosition().latitude);
//                        longitude= String.valueOf(marker.getPosition().longitude);
//
//                        Log.v("latitude", String.valueOf(marker.getPosition().latitude));
//                        Log.v("longitude", String.valueOf(marker.getPosition().longitude));
//                        getAddressFromLocation((marker.getPosition().latitude), marker.getPosition().longitude);
                    }
                });
                for (int i = 0; i < latitudes.size(); i++) {
                    myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(i), longitudes.get(i))).title(markerTitles.get(i)).
                            icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).draggable(false));
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudes.get(0), longitudes.get(0)), 1f));
                mapIsReady.isReadyStatus(true);

            }
        });
    }

    public static void showMarkerOnMapInsideNestedScrollView(SupportMapFragment mapFragment, final ArrayList<Double> latitudes, final ArrayList<Double> longitudes,
                                                             final ArrayList<String> markerTitles, final View transparentOverlayOverMapFragment,
                                                             final NestedScrollView sv) {


        showMarkerOnMap(mapFragment, latitudes, longitudes, markerTitles);

        transparentOverlayOverMapFragment.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        sv.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        sv.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        sv.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });
    }

    public static void getAddressPredictions(String input, final ReturnPredictions returnPredictions) {
        StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
        sb.append("?key=" + API_KEY);
        //sb.append("&types=(cities)");
        try {
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            url = new URL(sb.toString());
            Log.v("url", url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        AsyncTask<Void, Void, Void> allEvnets = new AsyncTask<Void, Void, Void>() {

            String urlResponseApi = "", asyncTaskExceptionStr = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    OkHttpClient okHttpClient = new OkHttpClient();

                    Request request = new Request.Builder()
                            .url(url)
                            .get()
                            .build();
                    Response response = okHttpClient.newCall(request).execute();
                    urlResponseApi = response.body().string();
                    Log.v("response", urlResponseApi);
                    JSONObject results = new JSONObject(urlResponseApi);
                    predictionsJsonArr = results.getJSONArray("predictions");
                } catch (Exception e) {
                    asyncTaskExceptionStr = e.toString();
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                returnPredictions.jsonArrayResult(predictionsJsonArr);
            }
        };
        allEvnets.execute();
    }

    public static void getAddressDetails(String placeid, Context mcontext, final ReturnAddressDetailsAlongLatLong returnAddressDetailsAlongLatLong) {
        StringBuilder sb = new StringBuilder(PLACES_API_BASE_DETAILS + OUT_JSON2);
        try {
            sb.append("placeid=" + URLEncoder.encode(placeid, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        sb.append("&key=" + API_KEY);

        try {
            addressurl = new URL(sb.toString());
            Log.v("url", addressurl.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        ProgressDialogHelper.showProgressDialogPleaseWait(mcontext);
        AsyncTask<Void, Void, Void> allEvents = new AsyncTask<Void, Void, Void>() {

            String urlResponseApi;
            JSONObject result;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    OkHttpClient okHttpClient = new OkHttpClient();

                    Request request = new Request.Builder()
                            .url(addressurl)
                            .get()
                            .build();
                    Response response = okHttpClient.newCall(request).execute();
                    urlResponseApi = response.body().string();
                    Log.v("response", urlResponseApi);

                    JSONObject obj = new JSONObject(urlResponseApi);
                    result = obj.getJSONObject("result");

                    //updateMapWithNewAddress(latitudeNew,longitudeNew);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                ProgressDialogHelper.hideProgressDialogPleaseWait();
                try {
                    JSONArray addresscomponent = result.getJSONArray("address_components");
                    Log.v("addresscomponentLength", String.valueOf(addresscomponent.length()));
                    JSONObject geometry = result.getJSONObject("geometry");
                    JSONObject locationdetail = geometry.getJSONObject("location");
                    String latitude = locationdetail.getString("lat");
                    String longitude = locationdetail.getString("lng");
                    Log.v("latitude", ":" + latitude);
                    Log.v("longitude", ":" + longitude);
                    String country = "", administrativeAreaLevel1 = "", administrativeAreaLevel2 = "", locality = "";
                    for (int i = 0; i < addresscomponent.length(); i++) {
                        JSONObject detail = addresscomponent.getJSONObject(i);
                        JSONArray typesarray = detail.getJSONArray("types");
                        for (int j = 0; j < typesarray.length(); j++) {
                            if (typesarray.getString(j).equalsIgnoreCase("country")) {
                                country = detail.getString("long_name");
                            }
                            if (typesarray.getString(j).equalsIgnoreCase("administrative_area_level_1")) {
                                administrativeAreaLevel1 = detail.getString("long_name");
                            }
                            if (typesarray.getString(j).equalsIgnoreCase("administrative_area_level_2")) {
                                administrativeAreaLevel2 = detail.getString("long_name");
                            }
                            if (typesarray.getString(j).equalsIgnoreCase("locality")) {
                                locality = detail.getString("long_name");
                            }
                        }

                    }
                    returnAddressDetailsAlongLatLong.returnData(result, latitude, longitude, country, administrativeAreaLevel1,
                            administrativeAreaLevel2, locality);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        };
        allEvents.execute();


    }

    public static void getAddressFromLatLong(Double latitude, Double longitude, final ReturnString returnString) {
        ArrayList<String> latlng = new ArrayList<>();
        String address = "";
        StringBuilder sb = new StringBuilder(GET_ADDRESS_FROM_LATLONG);
        sb.append("?key=" + API_KEY);
        //sb.append("&types=(cities)");
        try {
            latlng.add(latitude.toString());
            latlng.add(longitude.toString());
            sb.append("&latlng=" + URLEncoder.encode(TextUtils.join(",", latlng), "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            url = new URL(sb.toString());
            Log.v("url", url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        AsyncTask<Void, Void, Void> allEvnets = new AsyncTask<Void, Void, Void>() {

            String urlResponseApi = "", asyncTaskExceptionStr = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    OkHttpClient okHttpClient = new OkHttpClient();
                    //  LogHelper.logReadyMadeString("fffff"+url.toString());
                    Request request = new Request.Builder()
                            .url(url)
                            .get()
                            .build();
                    Response response = okHttpClient.newCall(request).execute();
                    urlResponseApi = response.body().string();
                    Log.v("responsesdfsdf", urlResponseApi);
                    JSONObject results = new JSONObject(urlResponseApi);
                    returnString.returnStrMethod(results.toString());
                } catch (Exception e) {
                    asyncTaskExceptionStr = e.toString();
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        };
        allEvnets.execute();
    }

    public static void getAddressFromPlaceid(String placeid, final ReturnString returnString) {
        String address = "";
        if (placeid.equalsIgnoreCase("")) {
            returnString.returnStrMethod("");
        }
        StringBuilder sb = new StringBuilder(PLACES_API_BASE_DETAILS);
        sb.append(OUT_JSON2);
        sb.append("key=" + API_KEY);
        //sb.append("&types=(cities)");
        try {
            sb.append("&placeid=" + URLEncoder.encode(placeid, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            url = new URL(sb.toString());
            Log.v("url", url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        AsyncTask<Void, Void, Void> allEvnets = new AsyncTask<Void, Void, Void>() {

            String urlResponseApi = "", asyncTaskExceptionStr = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    OkHttpClient okHttpClient = new OkHttpClient();
                    //  LogHelper.logReadyMadeString("fffff"+url.toString());
                    Request request = new Request.Builder()
                            .url(url)
                            .get()
                            .build();
                    Response response = okHttpClient.newCall(request).execute();
                    urlResponseApi = response.body().string();
                    Log.v("responsesdfsdfaaa", urlResponseApi);
                    JSONObject results = new JSONObject(urlResponseApi);
                    returnString.returnStrMethod(results.toString());
                } catch (Exception e) {
                    asyncTaskExceptionStr = e.toString();
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        };
        allEvnets.execute();
    }

    public static void getLatLongFromAddress(Context mContext, final String strAddress, final GetLatLngFromAddressString getLatLngFromAddressString) {

        ProgressDialogHelper.showProgressDialogPleaseWait(mContext);
        StringBuilder sb = new StringBuilder(GET_ADDRESS_FROM_LATLONG);
        sb.append("key=" + API_KEY);
        sb.append("&address=" + strAddress);
        try {
            url = new URL(sb.toString());
            Log.v("url", url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        AsyncTask<Void, Void, LatLng> allEvnets = new AsyncTask<Void, Void, LatLng>() {

            String urlResponseApi = "", asyncTaskExceptionStr = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected LatLng doInBackground(Void... voids) {
                try {
                    OkHttpClient okHttpClient = new OkHttpClient();
                    // LogHelper.logReadyMadeString("fffff"+url.toString());
                    Request request = new Request.Builder()
                            .url(url)
                            .get()
                            .build();
                    Response response = okHttpClient.newCall(request).execute();
                    urlResponseApi = response.body().string();
                    Log.v("responsesdfsdfaaa", urlResponseApi);
                    JSONObject results = new JSONObject(urlResponseApi);
                    JSONArray resultsJsonArr = results.getJSONArray("results");
                    JSONObject resultsJsonObj = resultsJsonArr.getJSONObject(0);
                    JSONObject geometryJson = resultsJsonObj.getJSONObject("geometry");
                    JSONObject locationJson = geometryJson.getJSONObject("location");
                    LatLng customLatLng = new LatLng(locationJson.getDouble("lat"),
                            locationJson.getDouble("lng"));
                    Log.v("aaaaaaaaddddda", customLatLng.toString());
                    return customLatLng;
                } catch (Exception e) {
                    asyncTaskExceptionStr = e.toString();
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(LatLng customLatLng) {
                ProgressDialogHelper.hideProgressDialogPleaseWait();
                getLatLngFromAddressString.getLatLnt(customLatLng);
            }
        };
        allEvnets.execute();
    }

    public static void getAddressPredictionsCountryRestriction(String input, final ReturnPredictions returnPredictions) {
        /**
         * https://developers.google.com/places/web-service/autocomplete
         * don't use as the country filter can filter only upto 5 countries till now
         */
        StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
        sb.append("?key=" + API_KEY);
        sb.append("&components=country:in|country:ar");
        try {
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            url = new URL(sb.toString());
            Log.v("url", url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        AsyncTask<Void, Void, Void> allEvnets = new AsyncTask<Void, Void, Void>() {

            String urlResponseApi = "", asyncTaskExceptionStr = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    OkHttpClient okHttpClient = new OkHttpClient();

                    Request request = new Request.Builder()
                            .url(url)
                            .get()
                            .build();
                    Response response = okHttpClient.newCall(request).execute();
                    urlResponseApi = response.body().string();
                    Log.v("response", urlResponseApi);
                    JSONObject results = new JSONObject(urlResponseApi);
                    predictionsJsonArr = results.getJSONArray("predictions");
                } catch (Exception e) {
                    asyncTaskExceptionStr = e.toString();
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                returnPredictions.jsonArrayResult(predictionsJsonArr);
            }
        };
        allEvnets.execute();
    }
// final onItemClickReturnString onItemClickReturnString
    public static void showMarkerOnMapForNearby(final Context mContext, final SupportMapFragment mapFragment, final ArrayList<Double> latitudes, final ArrayList<Double> longitudes,
                                         final ArrayList<String> mapAddressIds) {

        selectedMarkerForNearby = null;

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mMap.clear();
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudes.get(0), longitudes.get(0)), 12f));

//                mMap.addCircle(new CircleOptions()
//                        .center(new LatLng(mMap.getCameraPosition().target.latitude,mMap.getCameraPosition().target.longitude))
//                        .radius(700)
//                        .strokeColor(Color.RED)
//                        .fillColor(R.color.light_blue));

                myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(0), longitudes.get(0))).title(mapAddressIds.get(0)).
                        icon(BitmapDescriptorFactory.fromResource(R.drawable.locetorcurrent)).draggable(false));
//                final Circle[] circle = { mMap.addCircle(new CircleOptions()
//                        .center(new LatLng(mMap.getCameraPosition().target.latitude,mMap.getCameraPosition().target.longitude))
//                        .radius(900)
//                        .strokeColor(R.color.transparent)
//                        .fillColor(R.color.light_blue))};

                mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                   @Override
                   public void onCameraIdle() {
//                       circle[0].remove();
//                        circle[0] = mMap.addCircle(new CircleOptions()
//                               .center(new LatLng(mMap.getCameraPosition().target.latitude,mMap.getCameraPosition().target.longitude))
//                               .radius(900)
//                               .strokeColor(R.color.transparent)
//                               .fillColor(R.color.light_blue));

                       //  Double distance_lat = circle[0].getCenter().latitude + circle[0].getRadius();

                      //   Double distance_long = circle[0].getCenter().longitude + circle[0].getRadius();

                       if(myMarker1[1]!=null){
                          // myMarker1.remove();
                          // Log.d("inside","-sjs-"+circle[0].getCenter().latitude);
                           check=false;
                        //   distance_lat=distance_lat-1;
                           for(int i=1;i<count;i++) {

                             //  Log.d("inside","-j-"+circle[0].getCenter().latitude);
                               if(myMarker1[i]!=null) {
                               //    Log.d("inside","-j-"+circle[0].getCenter().latitude);
                                   myMarker1[i].remove();
                               }
                           }
                           count=1;
                       }

                      // myMarker.remove();


                        Log.d("inside","--"+myMarker1[1]);
                        int item=1;

                            for (int i = 1; i < latitudes.size(); i++) {

                                //   Log.d("circlelat","--"+distance_lat);
                              //  Double resturant_lat = latitudes.get(i) + circle[0].getRadius();

                               // Log.d("circlelat1", "-----radious----" + resturant_lat+ "---" + distance_lat );
                             //   Log.d("circlelat1", "-----normal----" + latitudes.get(i) + "---" + circle[0].getCenter().latitude);

                             //   Double r=circle[0].getCenter().latitude-900;
                               // Double l1=latitudes.get(i)+900;
                              //  Double centerLat=circle[0].getCenter().latitude+circle[0].getRadius();
                               // Double l=latitudes.get(i)+circle[0].getCenter().latitude;
                                 LatLng latLng1 =new LatLng(mMap.getCameraPosition().target.latitude,mMap.getCameraPosition().target.longitude);
                                LatLng latLng2 =new LatLng(latitudes.get(i),longitudes.get(i));
                                Log.d("dis",""+ CalculationByDistance(latLng1,latLng2)) ;
                                //Log.d("dis2",latLng1.d)
                                float[] results = new float[1];
                                Location.distanceBetween(latLng1.latitude, latLng1.longitude,
                                        latLng2.latitude, latLng2.longitude,
                                        results);

                              //Log.d("dis",""+ CalculationByDistance(latLng1,latLng2)) ;
                                if (Double.compare(CalculationByDistance(latLng1,latLng2),5000)<=0) {
                                    //Log.d("circlelat1", "----" + distance_lat);
                                    //  myMarker.remove();
                                    myMarker1[item] = mMap.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(i), longitudes.get(i))).title(mapAddressIds.get(i)).
                                            icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_icon)).draggable(false));
                                    count++;
                                    item++;

                                }
                            }



                   }
               });
                mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {


                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
//                        latitude=String.valueOf(marker.getPosition().latitude);
//                        longitude= String.valueOf(marker.getPosition().longitude);
//
//                        Log.v("latitude", String.valueOf(marker.getPosition().latitude));
//                        Log.v("longitude", String.valueOf(marker.getPosition().longitude));
//                        getAddressFromLocation((marker.getPosition().latitude), marker.getPosition().longitude);
                    }
                });

//                for (int i = 0; i < latitudes.size(); i++) {
//                    myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(i), longitudes.get(i))).title(mapAddressIds.get(i)).
//                            icon(BitmapDescriptorFactory.fromResource(R.drawable.locator)).draggable(false));
//                }

//                if (latitudes.size() != 0) {
//                  //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudes.get(0), longitudes.get(0)), 13f));
//
//                    try {
//                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//                            @Override
//                            public boolean onMarkerClick(Marker marker) {
//
//                                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.loc));
//                                if (selectedMarkerForNearby != null) {
//                                    selectedMarkerForNearby.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.locator));
//                                }
//                                selectedMarkerForNearby = marker;
//                                String markerId = marker.getTitle();
//                               // onItemClickReturnString.onItemClick(markerId);
//                                return true;
//                            }
//                        });
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
                //mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(mContext, nearbyProductJsonArr));

            }
        });
    }

    public static double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));

        double meter = (valueResult % 1000)+(kmInDec*1000);
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return meter;
    }
    public static void showMarkerOnMapForEvents(final Context mContext, SupportMapFragment mapFragment, final ArrayList<Double> latitudes, final ArrayList<Double> longitudes,
                                                final ArrayList<String> mapAddressIds, final onItemClickReturnString onItemClickReturnString) {

        selectedMarkerForEvents = null;
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mMap.clear();
                mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {
                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
//                        latitude=String.valueOf(marker.getPosition().latitude);
//                        longitude= String.valueOf(marker.getPosition().longitude);
//
//                        Log.v("latitude", String.valueOf(marker.getPosition().latitude));
//                        Log.v("longitude", String.valueOf(marker.getPosition().longitude));
//                        getAddressFromLocation((marker.getPosition().latitude), marker.getPosition().longitude);
                    }
                });

                for (int i = 0; i < latitudes.size(); i++) {
                    myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(i), longitudes.get(i))).title(mapAddressIds.get(i)).
                            icon(BitmapDescriptorFactory.fromResource(R.drawable.locator)).draggable(false));
                }
                if (latitudes.size() != 0) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudes.get(0), longitudes.get(0)), 1f));

                    try {
                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {

                                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.loc));
                                if (selectedMarkerForEvents != null) {
                                    selectedMarkerForEvents.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.locator));
                                }
                                selectedMarkerForEvents = marker;
                                String markerId = marker.getTitle();
                                onItemClickReturnString.onItemClick(markerId);
                                return true;
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(mContext, nearbyProductJsonArr));

            }
        });
    }

    public static void showMarkerOnMapForHotels(final Context mContext, SupportMapFragment mapFragment, final ArrayList<Double> latitudes, final ArrayList<Double> longitudes,
                                                final ArrayList<String> mapAddressIds, final onItemClickReturnString onItemClickReturnString) {

        selectedMarkerForHotels = null;
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mMap.clear();
                mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {
                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
//                        latitude=String.valueOf(marker.getPosition().latitude);
//                        longitude= String.valueOf(marker.getPosition().longitude);
//
//                        Log.v("latitude", String.valueOf(marker.getPosition().latitude));
//                        Log.v("longitude", String.valueOf(marker.getPosition().longitude));
//                        getAddressFromLocation((marker.getPosition().latitude), marker.getPosition().longitude);
                    }
                });

                for (int i = 0; i < latitudes.size(); i++) {
                    myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(i), longitudes.get(i))).title(mapAddressIds.get(i)).
                            icon(BitmapDescriptorFactory.fromResource(R.drawable.locator)).draggable(false));
                }
                if (latitudes.size() != 0) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudes.get(0), longitudes.get(0)), 1f));

                    try {
                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {

                                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.loc));
                                if (selectedMarkerForHotels != null) {
                                    selectedMarkerForHotels.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.locator));
                                }
                                selectedMarkerForHotels = marker;
                                String markerId = marker.getTitle();
                                onItemClickReturnString.onItemClick(markerId);
                                return true;
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(mContext, nearbyProductJsonArr));

            }
        });
    }

    public static void showDirectionBetweenTwoPlacesInGoogleMapUsingLatiLong(Activity activity,
                                                                             String current_lat,
                                                                             String current_longi,
                                                                             String destination_lat,
                                                                             String destinatione_longi) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr="+current_lat+","+current_longi+"&daddr="+destination_lat+","+destinatione_longi));
        activity.startActivity(intent);
    }

    public static void showDirectionFromCurrentToDestinationPlaceInGoogleMapUsingLatiLong(Activity activity,
                                                                             String destination_lat,
                                                                             String destinatione_longi) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr="+destination_lat+","+destinatione_longi));
        activity.startActivity(intent);
    }
}
