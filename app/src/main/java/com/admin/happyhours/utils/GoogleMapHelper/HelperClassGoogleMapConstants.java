package com.admin.happyhours.utils.GoogleMapHelper;

public class HelperClassGoogleMapConstants {

    public static final String API_KEY="AIzaSyDZWpqIfDAPjlBqhVi9Famz3LB0SJnz8po";
    public static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    public static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    public static final String OUT_JSON = "/json";
    public static final String OUT_JSON2 = "json?";
    public static final String PLACES_API_BASE_DETAILS="https://maps.googleapis.com/maps/api/place/details/";
    public static final String GET_ADDRESS_FROM_LATLONG = "https://maps.googleapis.com/maps/api/geocode/json?";

}
