/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.admin.happyhours.utils;

/**
 * Created by amitshekhar on 07/07/17.
 */

public final class AppConstants {

    public static final String adapter_img_share = "adapter_img_share";
    public static final String adapter_img_bookmark = "adapter_img_bookmark";
    public static final String adapter_img_like_in_event_listing = "adapter_img_like_in_event_listing";
    public static final String adapter_img_like = "adapter_img_like";

    public static final double lattitude=0.0;
    public static final double longtitude=0.0;

    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;

    public static final String DB_NAME = "happyhours.db";

    public static final long NULL_INDEX = -1L;

    public static final String PREF_NAME = "mindorks_pref";

    public static final String SEED_DATABASE_OPTIONS = "seed/options.json";

    public static final String SEED_DATABASE_QUESTIONS = "seed/questions.json";

    public static final String STATUS_CODE_FAILED = "failed";

    public static final String STATUS_CODE_SUCCESS = "success";

    public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";
//    public static final String BASE_URL = "http://ec2-13-52-216-108.us-west-1.compute.amazonaws.com/";
    public static final String BASE_URL = "https://happyhourzapp.com/";


   // public static final String BASE_URL = "https://esolz.co.in/lab6/happyhours/";
    public static final int REQUEST_CODE_STORAGE_PERMS = 321;
    public static final String splash_screen = "splash_screen";
    public static final String all_screen = "all_screen";
    public static final String device_type = "1";

    public static final String FB_LOGIN = "fb";
    public static final String GOOGLE_LOGIN = "google";

    public static final int LISTING_FETCH_LIMIT = 6;

    public static String channelname="HappyHourz";
    public static String channelId="happy_hourz";

    public static int create_collection_page_for_create=0;
    public static int create_collection_page_for_update=1;
    public static int create_collection_page_for_create_and_add=2;

    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}
