package com.admin.happyhours.utils.GoogleMapHelper;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class GooglePlaceAddressAdapter extends RecyclerView.Adapter<GooglePlaceAddressAdapter.ViewHolder> {

    ArrayList<AddressDetailModel> placeAddress;
    Context mContext;
    GetPlaceIdAndAddressInterface getPlaceIdAndAddressInterface;

    public GooglePlaceAddressAdapter(ArrayList<AddressDetailModel> placeAddress, Context mContext, GetPlaceIdAndAddressInterface getPlaceIdAndAddressInterface) {
        this.placeAddress = placeAddress;
        this.mContext = mContext;
        this.getPlaceIdAndAddressInterface = getPlaceIdAndAddressInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.place_address,parent,false);
        //
        // return new GooglePlaceAddressAdapter.ViewHolder(view);
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

//        holder.tv_address.setText(placeAddress.get(position).getAddress());
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getPlaceIdAndAddressInterface.getPlaceIdAndAddress(placeAddress.get(position).getPlaceId(),placeAddress.get(position).getAddress());
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return placeAddress.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // TextView tv_address;
        public ViewHolder(View itemView) {
            super(itemView);
            //  tv_address=itemView.findViewById(R.id.tv_address);
        }
    }
}
