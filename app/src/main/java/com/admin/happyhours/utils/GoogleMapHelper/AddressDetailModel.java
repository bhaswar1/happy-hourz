package com.admin.happyhours.utils.GoogleMapHelper;

public class AddressDetailModel {
    String address;
    String placeId;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }
}
