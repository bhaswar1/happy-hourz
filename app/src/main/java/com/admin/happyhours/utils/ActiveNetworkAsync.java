package com.admin.happyhours.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;

public class ActiveNetworkAsync extends AsyncTask<String, Integer, String> {
    private String TAG = ActiveNetworkAsync.class.getSimpleName();
    private boolean aBoolean = false;
    private Context context;
    private CheckHostReachableInterface checkHostReachableInterface;

    public ActiveNetworkAsync(Context context, CheckHostReachableInterface checkHostReachableInterface) {
        this.context = context;
        this.checkHostReachableInterface = checkHostReachableInterface;
        execute();
    }


    @Override
    protected String doInBackground(String... strings) {
        int timeout = 10000;
        ConnectivityManager CManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo NInfo = CManager.getActiveNetworkInfo();
        if (NInfo != null && NInfo.isConnectedOrConnecting()) {

            try{
                URL url = new URL(AppConstants.BASE_URL);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                int respCode = connection.getResponseCode();
                Log.d(TAG, "network_response"+": "+respCode);
                System.out.println(respCode);
                aBoolean = true;
                Log.d(TAG,"check_online_status"+ ": " + "connected ");
            }catch(UnknownHostException e){
                aBoolean = false;
                Log.d(TAG,"check_online_status"+ ": " + "Unknown Host: "+e.getMessage());
            } catch (ProtocolException e) {
                aBoolean = false;
                Log.d(TAG,"check_online_status"+ ": " + "connected but no internet: "+e.getMessage());
            } catch (MalformedURLException e) {
                aBoolean = false;
                Log.d(TAG,"check_online_status"+ ": " + "connected but no internet: "+e.getMessage());
            } catch (IOException e) {
                aBoolean = false;
                Log.d(TAG,"check_online_status"+ ": " + "connected but no internet: "+e.getMessage());
            }

            /*try {
                if (InetAddress.getByName(AppConstants.HOST).isReachable(timeout)) {
                    // host reachable
                    aBoolean = true;
                    Log.d("check_online_status", ": " + "connected ");
                } else {
                    // host not reachable
                    aBoolean = false;
                    Log.d("check_online_status", ": " + "connected but no internet");
                }
            } catch (IOException e) {
                e.printStackTrace();
                aBoolean = false;
                Log.d("check_online_status", ": url is unreachable:- " + e.getMessage());
            }*/
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (checkHostReachableInterface != null) {
            checkHostReachableInterface.isHostReachable(aBoolean);
        }

    }

    public interface CheckHostReachableInterface {
        void isHostReachable(boolean b);
    }
}