package com.admin.happyhours.utils.GoogleMapHelper;

import org.json.JSONArray;

public interface ReturnPredictions {

    void jsonArrayResult(JSONArray predictions);

}
