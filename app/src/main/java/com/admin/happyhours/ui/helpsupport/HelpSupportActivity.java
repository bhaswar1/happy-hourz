package com.admin.happyhours.ui.helpsupport;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.view.View;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.admin.happyhours.databinding.ActivityHelpsupportBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.utils.PermissionUtils;


//import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

public class HelpSupportActivity extends BaseActivity<ActivityHelpsupportBinding, HelpSupportViewModel>
        implements HelpSupportNavigator {

    private static final int CHECK_LOCATION_SETTINGS_REQUEST_CODE = 1;
    private FusedLocationProviderClient mFusedLocationClient;
    private PermissionUtils permissionUtils;
    private ArrayList<String> permissions = new ArrayList<>();
    LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private SpecialPageRestuarantListingResponse.Detail restaurantDetails;
    private File file_cache_dir;
    ArrayList<String> fileList;

    public String arr[]={"","",""};
    int pos=0;
    String restaurantId;

    private AdView mAdView;

    @Inject
    HelpSupportViewModel mHelpSupportViewModel;
    private ActivityHelpsupportBinding activityHelpsupportBinding;

    public static Intent newIntent(Context context) {

            return new Intent(context, HelpSupportActivity.class);


    }


//    public static Intent newIntent(Context context) {
//        return new Intent(context, HelpSupportActivity.class);
//    }
//
//    public static Intent newIntent(Context context, String notification) {
//        Log.d("check_noti_1", ": " + notification);
//        return new Intent(context, HelpSupportActivity.class).putExtra("notification", notification);
//    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_helpsupport;
    }

    @Override
    public HelpSupportViewModel getViewModel() {
        return mHelpSupportViewModel;
    }

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setStatusBarHidden(HelpSupportActivity.this);
        mHelpSupportViewModel.setNavigator(this);
        activityHelpsupportBinding = getViewDataBinding();


        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        Toolbar toolbar = activityHelpsupportBinding.toolbar;
        setSupportActionBar(toolbar);
        activityHelpsupportBinding.toolbar.setNavigationIcon(R.drawable.back_arrow_black);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        activityHelpsupportBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        showLoading();
        mHelpSupportViewModel.setHelpSuppoer("16");



    }




    @Override
    public void success(CommonSimpleResponse response) {
        hideLoading();
        activityHelpsupportBinding.helpTv.setText(Html.fromHtml(response.getResult()));
        //Toast.makeText(HelpSupportActivity.this,getString(R.string.review_posted),Toast.LENGTH_LONG).show();

    }

    @Override
    public void error() {
        hideLoading();
    }

    @Override
    public void onError(Throwable throwable) {

    }
}