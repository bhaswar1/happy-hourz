package com.admin.happyhours.ui.transparentrestaurantlist;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.RestaurantListingCollectionDetailsResponse;
import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class TransparentRestaurantListingViewModel extends BaseViewModel<TransparentRestaurantListingNavigator> {


    public TransparentRestaurantListingViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    void fetchRestuarantListing(String collection_id, int page, int limit) {
        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);

        Disposable disposable = ApplicationClass.getRetrofitService()
                .fetchRestaurantCollection(responseUser.getId(), collection_id, page, limit)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<RestaurantListingCollectionDetailsResponse>() {
                    @Override
                    public void accept(RestaurantListingCollectionDetailsResponse response) throws Exception {
                        if (response != null) {
                            Log.d("check_response", ": "+new Gson().toJson(response));

                            getNavigator().onSuccess(response);

                        } else {
                            Log.d("check_response", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response", ": " + throwable.getMessage());
                        getNavigator().onError(throwable);
                    }
                });

        getCompositeDisposable().add(disposable);
    }

}
