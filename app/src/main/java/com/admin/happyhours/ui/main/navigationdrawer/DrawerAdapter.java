package com.admin.happyhours.ui.main.navigationdrawer;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.admin.happyhours.R;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.utils.SharedPrefManager;

import java.util.LinkedList;

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {

    RecyclerView recyclerView;
    public int expandedPosition = -1;
    private OnItemClickListener onItemClickListener = null;
    private Context context;

   boolean check=false;
    private LinkedList<DrawerDatatype> drawerDatatypes = new LinkedList<>();

    public DrawerAdapter(Context context, LinkedList<DrawerDatatype> drawerDatatypes) {
        this.context = context;
        this.drawerDatatypes = drawerDatatypes;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.navigation_drawer_single_item, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        holder.item_tv.setWidth(layoutManager.getWidth());

        holder.item_parent_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandedPosition >= 0) {
                    int prev = expandedPosition;

                    notifyItemChanged(prev);
                }

                if (position == expandedPosition) {
                    /*expandedPosition = -1;
                    notifyDataSetChanged();*/

                    // For Multitple click on last item
                    if (position == drawerDatatypes.size()-1){
                        if (onItemClickListener != null) {

                            onItemClickListener.onItemClick(position, holder.item_tv);
                        }
                    }
                } else {
                    expandedPosition = position;
                    notifyDataSetChanged();
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(position, holder.item_tv);
                    }
                }
            }
        });

        holder.item_tv.setText(drawerDatatypes.get(position).getString_item());

        /*SpannableString spannableString = new SpannableString(strings.get(position).getString_item());
        spannableString.setSpan(new UnderlineSpan(), 0, strings.get(position).getString_item().length(), 0);
        holder.item_tv.setText(spannableString);*/
        if (position == expandedPosition) {
            holder.tv_noti_count.setVisibility(View.GONE);
            //SharedPrefManager sharedPrefManager = SharedPrefManager.getInstance(context);
      //  sharedPrefManager.saveNotification("0");
          //  if(!check) {
                SharedPrefManager.getInstance(context).deleteNotification();
                check = true;
           // }
            holder.item_tv.setTypeface(null, Typeface.BOLD);
            holder.item_tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_arrow,
                    0);
            holder.view1.setVisibility(View.VISIBLE);
        } else {
            holder.item_tv.setTypeface(null, Typeface.NORMAL);
            holder.item_tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,0);
            holder.view1.setVisibility(View.INVISIBLE);
        }

        if (position==3 && drawerDatatypes.get(position).getNotification_badge_count()!=0){
            if(!check) {

                holder.rl_noti_count.setVisibility(View.VISIBLE);
                //notificationtTextview = holder.tv_noti_count;
                holder.tv_noti_count.setText("" + drawerDatatypes.get(position).getNotification_badge_count());

            }
        }else {
            holder.rl_noti_count.setVisibility(View.GONE);
        }

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public int getItemCount() {
        return drawerDatatypes.size();
    }

    public void hidenotification(boolean check) {
//        if(check)
//           //notificationtTextview.setVisibility(View.GONE);
//        notifyItemChanged(4);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout item_parent_ll, rl_noti_count;
        TextView item_tv, tv_noti_count;
        View view1;
//        ImageView notification_count_img;

        public ViewHolder(View itemView) {
            super(itemView);

            item_parent_ll = itemView.findViewById(R.id.item_parent_ll);
            item_tv = itemView.findViewById(R.id.item_tv);
            view1 = itemView.findViewById(R.id.view1);
            rl_noti_count = itemView.findViewById(R.id.rl_noti_count);
            tv_noti_count = itemView.findViewById(R.id.tv_noti_count);
//            notification_count_img = itemView.findViewById(R.id.notification_count_img);
        }


    }

    public void setonClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public void selectItem(int position) {
        expandedPosition = position;
        notifyDataSetChanged();
    }


    public void changeNotificationBadge(String badge_count,boolean check){
        drawerDatatypes.get(4).setNotification_badge_count(Integer.parseInt(badge_count));
        notifyItemChanged(4);
    }

    public void setDraweritemPositionTobeActivated(int position){
        expandedPosition = position;
        notifyDataSetChanged();
    }



}
