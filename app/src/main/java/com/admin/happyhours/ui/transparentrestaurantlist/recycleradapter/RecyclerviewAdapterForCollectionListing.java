package com.admin.happyhours.ui.transparentrestaurantlist.recycleradapter;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.RestaurantListingCollectionDetailsResponse;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.admin.happyhours.ui.resturantdeatails.ResturantDetailsActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.LinkedList;

public class RecyclerviewAdapterForCollectionListing extends RecyclerView.Adapter<RecyclerviewAdapterForCollectionListing.Viewholder> {

    private Activity context;
    public boolean isLoading;
    private final int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private OnLoadMoreListener mOnLoadMoreListener;
    private OnItemClickListener callback = null;
    private LinkedList<RestaurantListingCollectionDetailsResponse.Details> details;

    public RecyclerviewAdapterForCollectionListing(Activity activity, LinkedList<RestaurantListingCollectionDetailsResponse.Details> details,
                                                   RecyclerView recyclerView, final LinearLayoutManager linearLayoutManager) {
        this.context = activity;
        this.details = details;
//        this.details.add(new CollectionListingResponse.Details());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Viewholder(LayoutInflater.from(context).inflate(R.layout.row_item_transparent_restaurant_listing, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void clearData() {
        details.clear();
        notifyDataSetChanged();
    }

    public LinkedList<RestaurantListingCollectionDetailsResponse.Details> getData() {
        return details;
    }

    public void setAllData(LinkedList<RestaurantListingCollectionDetailsResponse.Details> data) {
        details.addAll(data);
        notifyDataSetChanged();
    }

    public void setData(RestaurantListingCollectionDetailsResponse.Details detail) {
        details.add(detail);
        notifyDataSetChanged();
    }

    public void setLoaded() {
        isLoading = false;
    }



    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView resturantimage;
        TextView resturantName, address;
        CardView cardView;



        public Viewholder(View itemView) {
            super(itemView);

            resturantimage = itemView.findViewById(R.id.resturantimage);
            resturantName = itemView.findViewById(R.id.resturantName);
            address = itemView.findViewById(R.id.address);
            cardView=itemView.findViewById(R.id.card_collect);
        }

        public void bind(final int position) {
//            Glide.with(context).load(details.get(position).getCoverimage()).into(resturantimage);\
            Glide.with(context).load(details.get(position).getCoverimage()).apply(RequestOptions.centerInsideTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(resturantimage);

//            Glide.with(context)
//                    .load(details.get(position).getCoverimage())
//                    .apply(RequestOptions.placeholderOf(R.drawable.no_image_3x).error(R.drawable.no_image_3x))
//                    .into(resturantimage);

            if (details.get(position).getName()!=null) {
                resturantName.setText(details.get(position).getName());
            }
            if (details.get(position).getDescription()!=null) {
                address.setText(details.get(position).getDescription());
            }
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, ResturantDetailsActivity.class).putExtra("resturantid",details.get(position).getRestaurent_id()));
                }
            });
        }
    }
}
