package com.admin.happyhours.ui.main.special;

import androidx.lifecycle.ViewModelProvider;

import com.admin.happyhours.ViewModelProviderFactory;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class SpecialModule {


    @Provides
    SpecialViewModel specialViewModel(DataManager dataManager,
                                      SchedulerProvider schedulerProvider) {
        return new SpecialViewModel(dataManager, schedulerProvider);
    }


    @Provides
    ViewModelProvider.Factory provideSpecialViewModel(SpecialViewModel profilePageViewModel) {
        return new ViewModelProviderFactory<>(profilePageViewModel);
    }

}
