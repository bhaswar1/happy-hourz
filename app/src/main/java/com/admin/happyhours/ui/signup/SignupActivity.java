package com.admin.happyhours.ui.signup;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;


import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.databinding.ActivitySignupBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.ui.login.LoginActivity;
import com.admin.happyhours.ui.main.HomeActivity;
import com.admin.happyhours.utils.AppConstants;
import com.admin.happyhours.utils.CommonUtils;
import com.admin.happyhours.utils.NetworkUtils;
import com.admin.happyhours.utils.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

public class SignupActivity extends BaseActivity<ActivitySignupBinding, SignupViewModel> implements SignupNavigator {

    private String TAG = SignupActivity.this.getClass().getSimpleName();
    ActivitySignupBinding activitySignupBinding;
    @Inject
    SignupViewModel signupViewModel;
    String gender = "Male";
    DatePickerDialog datePickerDialog;
    private FirebaseAnalytics mFirebaseAnalytics;

    public static Intent newIntent(Context context) {
        return new Intent(context, SignupActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_signup;
    }

    @Override
    public SignupViewModel getViewModel() {
        return signupViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStatusBarHidden(SignupActivity.this);
        signupViewModel.setNavigator(this);
        activitySignupBinding = getViewDataBinding();
        activitySignupBinding.rlMail.setBackgroundResource(R.drawable.textbackgroundchoose);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        hideSoftKeyboardWithTouchOutsideEdittext(activitySignupBinding.parentLl);

        gaBack();

        selectMaleFemale();

        chooseDate();
       // activitySignupBinding.etName.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        activitySignupBinding.tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkConnected(SignupActivity.this)) {
                    String name = activitySignupBinding.etName.getText().toString().trim();
                    String phone = activitySignupBinding.etNumber.getText().toString().trim();
                    String email = activitySignupBinding.etEmail.getText().toString().trim();
                    String password = activitySignupBinding.etPassword.getText().toString().trim();
                    String confirm_password = activitySignupBinding.etCpassword.getText().toString().trim();
                    String dob = activitySignupBinding.tvDob.getText().toString().trim();
                    if (checkValidation(name, phone, email, password, confirm_password, gender, dob)) {
                        Log.d("check_dob", ": " + dob);
                        String device_token = SharedPrefManager.getInstance(SignupActivity.this).getDeviceToken();
                        showLoading();
                        signupViewModel.apiHitForSignup(name, phone, email, confirm_password, gender, dob, device_token, AppConstants.device_type);
                    }
                }else {
                    Toast.makeText(SignupActivity.this, "Please Check Network", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void gaBack() {
        activitySignupBinding.imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void selectMaleFemale() {
        activitySignupBinding.rlMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activitySignupBinding.rlMail.setBackgroundResource(R.drawable.textbackgroundchoose);
                activitySignupBinding.rlFemal.setBackgroundResource(R.drawable.textviewbackground);
                gender = "Male";
            }
        });
        activitySignupBinding.rlFemal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activitySignupBinding.rlFemal.setBackgroundResource(R.drawable.textbackgroundchoose);
                activitySignupBinding.rlMail.setBackgroundResource(R.drawable.textviewbackground);
                gender = "Female";
            }
        });
    }

    private void chooseDate() {
        activitySignupBinding.tvDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                datePickerDialog = new DatePickerDialog(SignupActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                if ((monthOfYear + 1)<10){
                                    activitySignupBinding.tvDob.setText(year + "/0" + (monthOfYear + 1) + "/" + dayOfMonth);
                                }else {
                                    activitySignupBinding.tvDob.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
                                }
                            }
                        }, year, month, day);

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.getDatePicker().getTouchables().get(0).performClick();

                datePickerDialog.show();

            }
        });
    }

    private boolean checkValidation(String name, String phone, String email, String password, String confirm_password,
                                    String gender_this, String dob) {
        if (TextUtils.isEmpty(name.trim())) {
            activitySignupBinding.etName.requestFocus();
            activitySignupBinding.etName.setError("Please enter name");
            return false;
        }
        if (TextUtils.isEmpty(phone.trim())) {
            activitySignupBinding.etNumber.requestFocus();
            activitySignupBinding.etNumber.setError(getString(R.string.can_not_be_blank));
            return false;
        }
        if (phone.length() != 10) {
            activitySignupBinding.etNumber.requestFocus();
            activitySignupBinding.etNumber.setError(getString(R.string.invalid_phone_number));
            return false;
        }
        if (TextUtils.isEmpty(email.trim())) {
            activitySignupBinding.etEmail.requestFocus();
            activitySignupBinding.etEmail.setError("Please enter your email id ");
            return false;
        }

        if (!CommonUtils.isEmailValid(email.trim())) {
            activitySignupBinding.etEmail.requestFocus();
            activitySignupBinding.etEmail.setError(getString(R.string.invalid_email));
            return false;
        }

        if (TextUtils.isEmpty(password.trim())) {
            activitySignupBinding.etPassword.requestFocus();
            activitySignupBinding.etPassword.setError(getString(R.string.can_not_be_blank));
            return false;
        }
        if ((password.trim().length() != 6)){
            activitySignupBinding.etPassword.requestFocus();
            activitySignupBinding.etPassword.setError(getString(R.string.password_atleast));
            return false;
        }
        if (TextUtils.isEmpty(confirm_password.trim())) {
            activitySignupBinding.etCpassword.requestFocus();
            activitySignupBinding.etCpassword.setError(getString(R.string.can_not_be_blank));
            return false;
        }

        if (!password.equals(confirm_password)) {
            activitySignupBinding.etCpassword.requestFocus();
            activitySignupBinding.etCpassword.setError(getString(R.string.do_no_match_password));
            return  false;
        }
        if (TextUtils.isEmpty(gender_this.trim())) {
            activitySignupBinding.tvGender.requestFocus();
            activitySignupBinding.tvGender.setError("Please select gender");
            Toast.makeText(this, "Please select gender", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            activitySignupBinding.tvGender.setError(null);
        }
        if (TextUtils.isEmpty(dob.trim()) || dob.trim().equals(getResources().getString(R.string.dob_placeholder))) {
            activitySignupBinding.titleDob.requestFocus();
            activitySignupBinding.titleDob.setError("Please choose date of birth");
            Toast.makeText(this, "Please choose date of birth", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            activitySignupBinding.titleDob.setError(null);
        }
        return true;
    }

    @Override
    public void onSuccess(NormalLoginResponse normalLoginResponse) {
        if (normalLoginResponse!=null){
            hideLoading();
            Toast.makeText(this, normalLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();


            if(normalLoginResponse.getStatus().equals("success")) {
                Bundle bundle = new Bundle();
                bundle.putString("userId", normalLoginResponse.getDetails().getId());
                bundle.putString("userName", normalLoginResponse.getDetails().getUserName());
                // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
                //  bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, resturantName);
                bundle.putString("userAdress",normalLoginResponse.getDetails().getAddress());
                bundle.putString("userphoneno",normalLoginResponse.getDetails().getPhone());
                bundle.putString("useremail",normalLoginResponse.getDetails().getEmail());
                bundle.putString("usergender",normalLoginResponse.getDetails().getGender());
                mFirebaseAnalytics.logEvent("registered_as_customer_android", bundle);
                startActivity(HomeActivity.newIntent(SignupActivity.this));
                finish();
            }else{
             //   startActivity(LoginActivity.newIntent(SignupActivity.this));

            }


        }
    }

    @Override
    public void onError(Throwable throwable) {
        hideLoading();
        Log.d(TAG,":-ERROR:-  "+throwable.getMessage());
    }
}
