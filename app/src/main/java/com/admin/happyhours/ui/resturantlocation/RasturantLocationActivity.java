package com.admin.happyhours.ui.resturantlocation;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.databinding.ActivityRasturantLocationBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.utils.GoogleMapHelper.HelperClassGoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.ArrayList;

import javax.inject.Inject;

public class RasturantLocationActivity extends BaseActivity<ActivityRasturantLocationBinding, RasturantLocationViewModel> implements RasturantLocationNavigator {

    private String TAG = RasturantLocationActivity.this.getClass().getSimpleName();
    ActivityRasturantLocationBinding activityRasturantLocationBinding;
    @Inject
    RasturantLocationViewModel rasturantLocationViewModel;
    String gender = "";
    DatePickerDialog datePickerDialog;
    SupportMapFragment mapFragment;


    public static Intent newIntent(Context context) {
        return new Intent(context, RasturantLocationActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_rasturant_location;
    }

    @Override
    public RasturantLocationViewModel getViewModel() {
        return rasturantLocationViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStatusBarHidden(RasturantLocationActivity.this);
        rasturantLocationViewModel.setNavigator(this);
        activityRasturantLocationBinding = getViewDataBinding();
        Toolbar mtoolbar=activityRasturantLocationBinding.toolbar;
        mtoolbar.setNavigationIcon(R.drawable.back_arrow_black);
        mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        ArrayList<Double> latitude = new ArrayList<>();
        ArrayList<Double> longitude = new ArrayList<>();
        ArrayList<String> markerTitles = new ArrayList<>();
        latitude.add(Double.parseDouble(getIntent().getStringExtra("lattitude")));
        longitude.add(Double.parseDouble(getIntent().getStringExtra("longitude")));
        markerTitles.add(getIntent().getStringExtra("rasturantName"));
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        HelperClassGoogleMap.showMarkerOnMap(mapFragment,latitude,longitude,markerTitles);

    }






    @Override
    public void onSuccess(NormalLoginResponse normalLoginResponse) {
    }

    @Override
    public void onError(Throwable throwable) {
        hideLoading();
        Log.d(TAG,":-ERROR:-  "+throwable.getMessage());
    }
}
