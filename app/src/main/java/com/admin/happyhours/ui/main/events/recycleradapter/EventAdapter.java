package com.admin.happyhours.ui.main.events.recycleradapter;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.EventListingResponse;
import com.admin.happyhours.datatypecommon.EventBusForEventListingLike;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.admin.happyhours.ui.eventdetails.EventDetailActivity;
import com.admin.happyhours.utils.AppConstants;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.LinkedList;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    public boolean isLoading;
    private final int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private Activity context;
    private OnLoadMoreListener mOnLoadMoreListener;
    private OnItemClickListener callback = null;
    private LinkedList<EventListingResponse.Details> details;


    public EventAdapter(FragmentActivity activity, LinkedList<EventListingResponse.Details> details,
                        RecyclerView recyclerView, final LinearLayoutManager linearLayoutManager ) {
        this.context = activity;
        this.details = details;
//        this.details.add(new CollectionListingResponse.Details());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.event_list_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void clearData() {
        details.clear();
        notifyDataSetChanged();
    }

    public void changeRowDataAfteLikeOrLike(String check_for_button, int position) {
        if (check_for_button.equals(AppConstants.adapter_img_like_in_event_listing)) {
            if (details.get(position).getFavourite_status() == 0) {
              Toast.makeText(context,context.getString(R.string.add_to_favourite),Toast.LENGTH_LONG).show();
                details.get(position).setFavourite_status(1);
            } else {
                details.get(position).setFavourite_status(0);
                Toast.makeText(context,context.getString(R.string.removed_success_favourite),Toast.LENGTH_LONG).show();

            }
        }
        notifyItemChanged(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView, imv_share, imv_like;
        TextView month_year_tv, date_tv, title_tv, sub_title_tv, tv_content;
        ConstraintLayout constraintLayoutEvent;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            imv_share = itemView.findViewById(R.id.imv_share);
            imv_like = itemView.findViewById(R.id.imv_like);
            month_year_tv = itemView.findViewById(R.id.month_year_tv);
            date_tv = itemView.findViewById(R.id.date_tv);
            title_tv = itemView.findViewById(R.id.title_tv);
            sub_title_tv = itemView.findViewById(R.id.sub_title_tv);
            tv_content = itemView.findViewById(R.id.tv_content);
            constraintLayoutEvent = itemView.findViewById(R.id.constraintEvent);
        }

        public void bind(final int position) {

            if (details.get(position).getEventimage() != null) {
                Glide.with(context).load(details.get(position).getEventimage()).into(imageView);
            }
            if (details.get(position).getFavourite_status() == 1){
                imv_like.setImageResource(R.drawable.like_logo_checked);
            }else {
                imv_like.setImageResource(R.drawable.like_logo);
            }
            imv_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = new Gson().toJson(details.get(position));
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "share_restaurant_details");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Join event "+ details.get(position).getEventname()+" at "+ details.get(position).getVenueName()+ " on "+ details.get(position).getEvent_day()+" "+details.get(position).getEvent_month()+" "+details.get(position).getEvent_year()+". Click on the following link to download Happy Hourz - https://happyhourzapp.com/app/Restaurant_event_deeplink?event_id=" + details.get(position).getEventid());
                    context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });
            imv_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new EventBusForEventListingLike(AppConstants.adapter_img_like_in_event_listing, position, details.get(position)));
                }
            });

            month_year_tv.setText(details.get(position).getEvent_month() + "\n" + details.get(position).getEvent_year());

            date_tv.setText(details.get(position).getEvent_day());

            title_tv.setText(details.get(position).getEventname());

            sub_title_tv.setText(details.get(position).getAddress());

            tv_content.setText(details.get(position).getDescription());
            constraintLayoutEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivityForResult(new Intent(context, EventDetailActivity.class).
                            putExtra("eventId",details.get(position).getEventid()).
                            putExtra("favourite_status",Integer.toString(details.get(position).getFavourite_status())).
                            putExtra("position",Integer.toString(position)),7);

                }
            });

        }

    }
    public void changeValueOfParticularPosition(int position, int isChecked) {
        this.details.get(position).setFavourite_status(isChecked);
        notifyItemChanged(position);
    }

    public LinkedList<EventListingResponse.Details> getData() {
        return details;
    }

    public void setAllData(LinkedList<EventListingResponse.Details> data) {
        details.addAll(data);
        notifyDataSetChanged();
    }

    public void setData(EventListingResponse.Details detail) {
        details.add(detail);
        notifyDataSetChanged();
    }

    public void setLoaded() {
        isLoading = false;
    }
}
