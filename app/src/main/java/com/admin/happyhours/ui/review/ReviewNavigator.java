package com.admin.happyhours.ui.review;

import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.ProfileImageUpdateResponse;

public interface ReviewNavigator {
    void success(CommonSimpleResponse response);

    void error();
}
