package com.admin.happyhours.ui.main.notification;

import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.NotificationListingResponse;

public interface NotificationNavigator {


    void onSuccessAfterGettingListing(NotificationListingResponse notificationListingResponse);

    void onError(Throwable throwable);

    void onSuccessAfterClearBadgeCount(CommonSimpleResponse commonSimpleResponse);

    void noData();

    void noload();
}
