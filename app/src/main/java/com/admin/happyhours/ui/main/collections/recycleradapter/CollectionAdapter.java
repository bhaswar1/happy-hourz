package com.admin.happyhours.ui.main.collections.recycleradapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.admin.happyhours.R;
import com.admin.happyhours.customview.MediumTextView;
import com.admin.happyhours.data.model.api.response.CollectionListingResponse;
import com.admin.happyhours.interfaces.ItemLongClickListener;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.ui.base.recyclerviewbaseadapter.ParentAdapter;
import com.bumptech.glide.Glide;

import java.util.List;

public class CollectionAdapter extends ParentAdapter<CollectionListingResponse.Details> {

    private OnItemClickListener callback = null;
    private ItemLongClickListener callbackLong= null;
    private Context context;

    public CollectionAdapter(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public long getYourItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.collection_list_row_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder fholder = (ViewHolder) holder;
            fholder.bind(position);
        }
    }

    public boolean isPositionFooter(int position) {
        return position == getItemCount() - 1 && showLoader;
    }


    public void setOnItemClickListener(OnItemClickListener callback) {
        this.callback = callback;
    }

    public void setCallbackLong(ItemLongClickListener callbackLong) {
        this.callbackLong = callbackLong;
    }

    public void ClearData() {
        mItems.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_collection, rl_new;
        ImageView imv_collection;
        MediumTextView tv_collectionname;

        public ViewHolder(View itemView) {
            super(itemView);
            rl_collection = (RelativeLayout) itemView.findViewById(R.id.rl_collection);
            rl_new = (RelativeLayout) itemView.findViewById(R.id.rl_new);
            imv_collection = (ImageView) itemView.findViewById(R.id.imv_collection);
            tv_collectionname = (MediumTextView) itemView.findViewById(R.id.tv_collectionname);
        }

        public void bind(final int position) {
            if (position == 0) {
                rl_new.setVisibility(View.VISIBLE);
                rl_collection.setVisibility(View.GONE);
                rl_new.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onItemClick(position, view);
                    }
                });
            } else {
                rl_new.setVisibility(View.GONE);
                rl_collection.setVisibility(View.VISIBLE);

                Glide.with(context).load(mItems.get(position).getCollection_image()).into(imv_collection);

                if(!mItems.get(position).getCollection_name().isEmpty())
                tv_collectionname.setText(mItems.get(position).getCollection_name().trim());
                rl_collection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onItemClick(position, view);
                    }
                });
                rl_collection.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        callbackLong.onItemLongClick(position, v);
                        return true;
                    }
                });

            }
        }
    }

    public List<CollectionListingResponse.Details> getData() {
        return mItems;
    }

    public void setData(List<CollectionListingResponse.Details> data) {
        mItems.addAll(data);
        notifyDataSetChanged();
    }

    /*private Activity activity;
    private LinkedList<CollectionListingResponse.Details> details = new LinkedList<>();

    public CollectionAdapter(Activity activity) {
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(activity).inflate(R.layout.collection_list_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_collection, rl_new;
        ImageView imv_collection;
        HighSemiBoldTextView tv_collectionname;

        public ViewHolder(View itemView) {
            super(itemView);

            rl_collection = (RelativeLayout) itemView.findViewById(R.id.rl_collection);
            rl_new = (RelativeLayout) itemView.findViewById(R.id.rl_new);
            imv_collection = (ImageView) itemView.findViewById(R.id.imv_collection);
            tv_collectionname = (HighSemiBoldTextView) itemView.findViewById(R.id.tv_collectionname);


        }

        public void bind(int position) {
            if (position == 0) {
                rl_new.setVisibility(View.VISIBLE);
                rl_collection.setVisibility(View.GONE);
                rl_new.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
            } else {
                rl_new.setVisibility(View.GONE);
                rl_collection.setVisibility(View.VISIBLE);

                Glide.with(activity).load(details.get(position).getCollection_image()).into(imv_collection);
                tv_collectionname.setText(details.get(position).getCollection_name());

            }
        }
    }

    public void setData(LinkedList<CollectionListingResponse.Details> details){
        this.details.addAll(details);
        notifyDataSetChanged();
    }*/
}
