package com.admin.happyhours.ui.privacytermsbusinesssignup;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.PrivacyPolicyAndTermsAndConditionResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class PrivacyTermsBusinessViewModel extends BaseViewModel<PrivacyTermsBusinessNavigator> {



    public PrivacyTermsBusinessViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }


    public void fetchDataPrivacyPolyAndTermsAndCondition(String mode) {


        Disposable disposable = ApplicationClass.getRetrofitService()
                .privacyPolicyAndTermsCondition(mode)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<PrivacyPolicyAndTermsAndConditionResponse>() {
                    @Override
                    public void accept(PrivacyPolicyAndTermsAndConditionResponse response) throws Exception {
                        if (response != null) {

                            Log.d("check_response", ": " + response.getMessage());
                            getNavigator().onSuccess(response);
                            if (response.getDetails() != null && response.getStatus().equals("success")) {

                            }
                        } else {
                            Log.d("check_response", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response", ": " + throwable.getMessage());
                        getNavigator().onError(throwable);
                    }
                });

        getCompositeDisposable().add(disposable);
    }
}
