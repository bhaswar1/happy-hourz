package com.admin.happyhours.ui.resturantdeatails;

import androidx.databinding.ObservableField;
import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.Details;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.ResturantsAllData;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class ResturantDetailsViewModel extends BaseViewModel<ResturantDetailsNavigator> {

 //   public ObservableField<String> phone = new ObservableField<>();
    public ObservableField<String> designerEng = new ObservableField<>();
    public ObservableField<String> productPrice = new ObservableField<>();
    public ObservableField<String> productSizeUS= new ObservableField<>();


    public ResturantDetailsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }


    void setLikeUnlike(String userId,String restuarantId) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            // Log.d("check_uid", ": " + userId);
            //  Log.d("check_limit", ": " + limit);
            //   Log.d("page", ": " + mode);
            // Log.d("check_latitude", ": " + lattitude);
            //   Log.d("check_longitude", ": " + longitude);

            Disposable disposable = ApplicationClass.getRetrofitService()
                    .speciallikeAndUnlike(userId,restuarantId)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<CommonSimpleResponse>() {
                        @Override
                        public void accept(CommonSimpleResponse response) throws Exception {
                            if(response!=null){
                                //getNavigator().onSuccess(response);
                                getNavigator().likeSucess(response);
                            }
                            Log.d("account","--"+new Gson().toJson(response)+response.getMessage());

                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response", ": " + throwable.getMessage());
                            // getNavigator().onError(throwable);
                        }
                    });

//
            getCompositeDisposable().add(disposable);

        }
    }

    void setResturantDetail(String userId,String mode,String lattitude,String longitude,String resturentId) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("check_uid", ": " + userId);
            Log.d("check_resturentId", ": " + resturentId);
            Log.d("check_mode", ": " + mode);
           Log.d("check_latitude", ": " + lattitude);
            Log.d("check_longitude", ": " + longitude);

//            Disposable disposable = ApplicationClass.getRetrofitService()
//                    .resturantDeatail(userId,resturentId,mode,lattitude,longitude)
//                    .subscribeOn(getSchedulerProvider().io())
//                    .observeOn(getSchedulerProvider().ui())
//                    .subscribe(new Consumer<RestaurantDetailsData>() {
//                        @Override
//                        public void accept(RestaurantDetailsData response) throws Exception {
//                            Log.d("response_check","--"+new Gson().toJson(response));
////                            if (response != null) {
////
////                                if (response.getStatus() != null && response.getStatus().trim().toLowerCase().equals("success")) {
////                                    getNavigator().onSuccess(response);
////
////                                }
////
////                            } else {
////                                Log.d("check_response", ": null response");
////                            }
//                        }
//                    }, new Consumer<Throwable>() {
//                        @Override
//                        public void accept(Throwable throwable) throws Exception {
//                            Log.d("check_response", ":t " + throwable.getMessage());
//                            getNavigator().onError(throwable);
//                        }
//                    });
//
//            getCompositeDisposable().add(disposable);
            Disposable disposable = ApplicationClass.getRetrofitService()
                    .resturantDeatail(userId,resturentId,mode, lattitude, longitude)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<JsonObject>() {
                        @Override
                        public void accept(JsonObject response) throws Exception {
                            if(response!=null ) {


                                // getNavigator().onSuccess(response);
                                ResturantsAllData resturantsAllData = new Gson().fromJson(response.toString(),
                                        ResturantsAllData.class);


                                    Details details = new Gson().fromJson(response.get("Details"),
                                            Details.class);
                                    Log.d("response_check", "--" + new Gson().toJson(details));
                                    if(resturantsAllData!=null)

                                    getNavigator().onSuccess(resturantsAllData, response);


                                // Log.d("response_check","--"+response.getCoverimage());
                                //    Log.d("response_check","--"+response.getDetails().getOtherimages().get(0));
                            }else{
                                getNavigator().noData();
                            }

                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response", ": " + throwable.getMessage());
                            getNavigator().onError(throwable);
                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }
    void bookmarkAndLikeApiHit(  String restaurantId) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

           // Log.d("check_button", ": " + check_for_button);
            Log.d("check_adapter_uid", ": " + responseUser.getId());
            Log.d("check_adapter_restid", ": " +restaurantId);
         //   if (check_for_button.equals(AppConstants.adapter_img_bookmark)) {

                Disposable disposable = ApplicationClass.getRetrofitService()
                        .bookmarkAndUnbookmark(responseUser.getId(), restaurantId)
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(new Consumer<CommonSimpleResponse>() {
                            @Override
                            public void accept(CommonSimpleResponse response) throws Exception {
                                if (response != null) {

                                    if (response.getStatus() != null && response.getStatus().trim().toLowerCase().equals("success")) {
                                        getNavigator().responseAfterBookmarkOrLike( );
                                      }

                                } else {
                                    Log.d("check_response", ": null response");
                                }
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Log.d("check_response", ": " + throwable.getMessage());
                                getNavigator().onError(throwable);
                            }
                        });

                getCompositeDisposable().add(disposable);


        }
    }


    public void setData(String phone) {
         //   this.phone.set(phone);
    }
}
