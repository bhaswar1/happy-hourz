package com.admin.happyhours.ui.createcollection;

import com.admin.happyhours.data.model.api.response.CollectionCreateResponse;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;

public interface CreateCollectionNavigator {

    void onSuccessAfterCreation(CollectionCreateResponse collectionCreateResponse, String type);


    void onSuccessAfterUpdation(CommonSimpleResponse commonSimpleResponse, String type);


    void onSuccessAfterCreationAndAdditonRestaurantToCollection(CommonSimpleResponse commonSimpleResponse, String type);

    void onError(Throwable throwable);

}
