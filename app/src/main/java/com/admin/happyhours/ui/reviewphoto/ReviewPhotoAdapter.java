package com.admin.happyhours.ui.reviewphoto;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.ReviewPhotoDetail;
import com.admin.happyhours.data.model.api.response.SearchDetail;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kodmap.app.library.PopopDialogBuilder;

import java.util.LinkedList;

public class ReviewPhotoAdapter extends  RecyclerView.Adapter<ReviewPhotoAdapter.ViewHolder> {
    public boolean isLoading;
    private final int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private Context context;
    private OnLoadMoreListener mOnLoadMoreListener;
    private OnItemClickListener callback = null;
    private LinkedList<ReviewPhotoDetail> details;
    public ReviewPhotoAdapter.ItemClickListener itemClickListener;
    final  LinkedList<String> list=new LinkedList<>();
    public ReviewPhotoAdapter(Context activity, LinkedList<ReviewPhotoDetail> details,
                             RecyclerView recyclerView, final GridLayoutManager linearLayoutManager) {
        this.context = activity;
        this.details = details;
//        this.details.add(new CollectionListingResponse.Details());
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });


    }

    @NonNull
    @Override
    public ReviewPhotoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReviewPhotoAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_review_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewPhotoAdapter.ViewHolder holder, final int position) {
        holder.bind(position);


    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void clearData() {
        details.clear();

    }

//    public void changeRowDataAfteLikeOrLike(String check_for_button, int position) {
//        if (check_for_button.equals(AppConstants.adapter_img_like_in_event_listing)) {
//            if (details.get(position).getFavourite_status() == 0) {
//                details.get(position).setFavourite_status(1);
//            } else {
//                details.get(position).setFavourite_status(0);
//            }
//        }
//        notifyItemChanged(position);
//    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
      //  TextView resturant_title,resturant_address;

        public ViewHolder(View itemView) {
            super(itemView);
         //   resturant_title=itemView.findViewById(R.id.title);
            imageView=itemView.findViewById(R.id.images);

        }

        public void bind(final int position) {
            Glide.with(context).load(details.get(position).getImage()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(imageView);
          // list.add(details.get(position).getImage());
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  Toast.makeText(context,"position"+position+list.size(),Toast.LENGTH_SHORT).show();

                    Dialog dialog1 = new PopopDialogBuilder(context)
                            // Set list like as option1 or option2 or option3
                            .setList(list,position)
                            // or setList with initial position that like .setList(list,position)
                            // Set dialog header color
                            .setHeaderBackgroundColor(android.R.color.transparent)
                            // Set dialog background color
                            .setDialogBackgroundColor(R.color.black)
                            // Set close icon drawable
                            .setCloseDrawable(R.drawable.ic_close_white_24dp)
                            // Set loading view for pager image and preview image
                            .setLoadingView(R.layout.loader_item_layout)
                            // Set dialog style
                            // .setDialogStyle(R.style.DialogStyle)
                            // Choose selector type, indicator or thumbnail
                            .showThumbSlider(false)

                            // Set image scale type for slider image
                            .setSliderImageScaleType(ImageView.ScaleType.FIT_CENTER)
                            // Set indicator drawable
                         //  .setSelectorIndicator(R.drawable.selected_indicato)
                            // Enable or disable zoomable
                            .setIsZoomable(true)

                            // Build Km Slider Popup Dialog
                            .build();
                    dialog1.show();

                }
            });
        }

//        @Override
//        public void onClick(View view) {
//          //  if (searchItemClickListener != null)
//                searchItemClickListener.onItemClick(view, getAdapterPosition());
//
//        }
    }

    public LinkedList<ReviewPhotoDetail> getData() {
        return details;
    }

    public void setAllData(LinkedList<ReviewPhotoDetail> data) {
        list.clear();
        details.addAll(data);
        for(int i=0;i<details.size();i++){

            list.add(details.get(i).getImage());
        }

        notifyDataSetChanged();
    }

    public void setData(SearchDetail detail) {
      //  details.add(detail);
      //  notifyDataSetChanged();
    }

    public void setLoaded() {
        isLoading = false;
    }

    public interface ItemClickListener {
        void onItemClick(SearchDetail view,int position);

    }
    public void setListener(ReviewPhotoAdapter.ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;

    }
}


