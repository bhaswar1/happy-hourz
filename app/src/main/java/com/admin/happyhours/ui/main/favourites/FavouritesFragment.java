package com.admin.happyhours.ui.main.favourites;


import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.admin.happyhours.BR;
import com.google.android.material.tabs.TabLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.Detail;
import com.admin.happyhours.data.model.api.response.EventDetail;
import com.admin.happyhours.data.model.api.response.FevouriteEventData;
import com.admin.happyhours.data.model.api.response.FevouriteListData;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.databinding.FragmentFavouritesBinding;
import com.admin.happyhours.datatypecommon.EventBusEventSpecialDeleteInFavouritePage;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.admin.happyhours.ui.base.BaseFragment;
import com.admin.happyhours.ui.main.HomeActivity;
import com.admin.happyhours.utils.AppConstants;
import com.admin.happyhours.utils.GlobalBus;
import com.admin.happyhours.utils.SimpleDividerItemDecoration;
import com.google.gson.Gson;

import org.greenrobot.eventbus.Subscribe;

import java.util.LinkedList;

import javax.inject.Inject;

import static androidx.constraintlayout.widget.ConstraintSet.VERTICAL;


public class FavouritesFragment extends BaseFragment<FragmentFavouritesBinding, FavouritesViewModel> implements FavouritesNavigator {

    private static final String TAG = "FavouritesFragment";
    FragmentFavouritesBinding fragmentFavouritesBinding;
    FavouritesViewModel favouritesViewModel;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    TabLayout tabLayout;
    FevouriteListAdapter fevouriteListAdapter;
    FevouriteEventListAdapter fevouriteEventListAdapter;
    private int pagination = 1;
    private boolean hasLoadMore = false;
    int specialitemCount=0;
     int eventitemCount;
    int i=0;

    int totalItem=0;
    boolean setEventBus=false;


    DividerItemDecoration decoration;

    public static FavouritesFragment newInstance() {
        Bundle args = new Bundle();
        FavouritesFragment fragment = new FavouritesFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_favourites;
    }

    @Override
    public FavouritesViewModel getViewModel() {
        favouritesViewModel = ViewModelProviders.of(this, mViewModelFactory).get(FavouritesViewModel.class);
        return favouritesViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        favouritesViewModel.setNavigator(this);
        //fragmentFavouritesBinding = getViewDataBinding();


    }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentFavouritesBinding = getViewDataBinding();
       // Toast.makeText(getActivity(), "No stores", 5000).show();

        tabLayout = fragmentFavouritesBinding.tablayout;
        //initCollapsingToolbar();
        tabLayout.addTab(tabLayout.newTab().setText("Specials"));
        tabLayout.addTab(tabLayout.newTab().setText("Events"));
        fragmentFavouritesBinding.noDataTextView.setVisibility(View.GONE);
        fragmentFavouritesBinding.fevouriteRecycle.setVisibility(View.VISIBLE);
        tabSelset();
        decoration = new DividerItemDecoration(getBaseActivity().getApplicationContext(), VERTICAL);
        Log.d("hii", "-");
        getBaseActivity().showLoading();
        if (tabLayout.getSelectedTabPosition() == 0) {
            favouritesViewModel.setResturantDetail(favouritesViewModel.getDataManager().getCurrentUserId().toString(), pagination,1000,false);

            setRecycleView();
            fragmentFavouritesBinding.fevouriteRecycle.setAdapter(fevouriteListAdapter);
        }
        //  setEventRecycleView();

//        if(tabLayout.getSelectedTabPosition()==1){
//            setEventRecyclerView();
//        }
    }

    private void test() {


        // fragmentFavouritesBinding.fevouriteRecycle.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        fragmentFavouritesBinding.eventRecycle.setLayoutManager(linearLayoutManager);
        // fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(new SpacesItemDecoration(ViewUtils.dpToPx(2)));
        LinkedList<EventDetail> details = new LinkedList<>();
        fevouriteEventListAdapter = new FevouriteEventListAdapter(getActivity(), details, fragmentFavouritesBinding.eventRecycle, linearLayoutManager);
        //  fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(decoration);
        //  fragmentFavouritesBinding.fevouriteRecycle.setAdapter(fevouriteListAdapter);
        fevouriteEventListAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
//                Log.d("checking_steps_loadmore",": "+(++check));
                Log.d("check_bools", ": " + hasLoadMore);
                if (hasLoadMore) {
                    getBaseActivity().showLoading();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadData();
                        }
                    }, 1000);
                }
            }
        });

    }

    private void setEventRecycleView() {


        // fragmentFavouritesBinding.fevouriteRecycle.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        fragmentFavouritesBinding.fevouriteRecycle.setLayoutManager(linearLayoutManager);
        // fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(new SpacesItemDecoration(ViewUtils.dpToPx(2)));
        LinkedList<EventDetail> details = new LinkedList<>();
        fevouriteEventListAdapter = new FevouriteEventListAdapter(getActivity(), details, fragmentFavouritesBinding.fevouriteRecycle, linearLayoutManager);
        //  fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(decoration);
        //  fragmentFavouritesBinding.fevouriteRecycle.setAdapter(fevouriteListAdapter);
        fevouriteEventListAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
//                Log.d("checking_steps_loadmore",": "+(++check));
                Log.d("check_bools", ": " + hasLoadMore);
                if (hasLoadMore) {
                    getBaseActivity().showLoading();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadData();
                        }
                    }, 1000);
                }
            }
        });

    }

    private void loadData() {
        if (favouritesViewModel.getDataManager().getCurrentUserInfo() != null) {
            //  getBaseActivity().showLoading();
            NormalLoginResponse.Details details = new Gson().fromJson(favouritesViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
            favouritesViewModel.setEventDetail(details.getId(), pagination, AppConstants.LISTING_FETCH_LIMIT, true);
        }

    }

    private void setRecycleView() {

        // fragmentFavouritesBinding.fevouriteRecycle.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        fragmentFavouritesBinding.fevouriteRecycle.setLayoutManager(linearLayoutManager);
        fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        fragmentFavouritesBinding.eventRecycle.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        LinkedList<Detail> details = new LinkedList<>();
        fevouriteListAdapter = new FevouriteListAdapter(getActivity(), details, fragmentFavouritesBinding.fevouriteRecycle, linearLayoutManager);
        // DividerItemDecoration decoration = new DividerItemDecoration(getBaseActivity().getApplicationContext(), VERTICAL);
        // fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(decoration);
        //  fragmentFavouritesBinding.fevouriteRecycle.setAdapter(fevouriteListAdapter);

        fevouriteListAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
//                Log.d("checking_steps_loadmore",": "+(++check));
                Log.d("check_bools", ": " + hasLoadMore);
                if (hasLoadMore) {
                    getBaseActivity().showLoading();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadrestaurantData();
                        }
                    }, 1000);
                }
            }
        });

    }

    private void loadrestaurantData() {
        if (favouritesViewModel.getDataManager().getCurrentUserInfo() != null) {
            //  getBaseActivity().showLoading();
            NormalLoginResponse.Details details = new Gson().fromJson(favouritesViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
            favouritesViewModel.setResturantDetail(details.getId(), pagination, AppConstants.LISTING_FETCH_LIMIT,true);
        }

    }

    public void tabSelset() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    specialitemCount=0;

                    // activityCartBinding.group.setVisibility(View.GONE);
                    //setCartList();
                    fragmentFavouritesBinding.noDataTextView.setVisibility(View.GONE);

                    fragmentFavouritesBinding.eventRecycle.setVisibility(View.GONE);

                    fragmentFavouritesBinding.fevouriteRecycle.setVisibility(View.VISIBLE);
                    pagination = 1;
                    hasLoadMore = false;
                    setRecycleView();
                    fragmentFavouritesBinding.fevouriteRecycle.setAdapter(fevouriteListAdapter);

                    if (fevouriteListAdapter != null) {
                        fevouriteListAdapter.clearData();
                    }
                    getBaseActivity().showLoading();
                    // favouritesViewModel.setResturantDetail(favouritesViewModel.getDataManager().getCurrentUserId().toString(), pagination,AppConstants.LISTING_FETCH_LIMIT);
                    if (favouritesViewModel.getDataManager().getCurrentUserInfo() != null) {
                        getBaseActivity().showLoading();
                        NormalLoginResponse.Details details = new Gson().fromJson(favouritesViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                        favouritesViewModel.setResturantDetail(details.getId(), pagination,1000,false);
                    }


                } else if (tab.getPosition() == 1) {

                    eventitemCount=0;
                    //  setWishList();
                    fragmentFavouritesBinding.fevouriteRecycle.setVisibility(View.GONE);
                    fragmentFavouritesBinding.noDataTextView.setVisibility(View.GONE);
                    fragmentFavouritesBinding.eventRecycle.setVisibility(View.VISIBLE);
                    pagination = 1;
                    hasLoadMore = false;
                    test();
                    fragmentFavouritesBinding.eventRecycle.setAdapter(fevouriteEventListAdapter);
                    if (fevouriteEventListAdapter != null) {
                        fevouriteEventListAdapter.clearData();
                    }
                    if (favouritesViewModel.getDataManager().getCurrentUserInfo() != null) {
                        getBaseActivity().showLoading();
                        NormalLoginResponse.Details details = new Gson().fromJson(favouritesViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                        favouritesViewModel.setEventDetail(details.getId(), pagination,1000, false);
                    }

                //    fevouriteEventListAdapter.notifyDataSetChanged();
                    //  getBaseActivity().showLoading();

                    //  favouritesViewModel.setEventDetail(favouritesViewModel.getDataManager().getCurrentUserId().toString(), "1", "100");


                }
            }


            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {


            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==7){
            if (resultCode==8){
                int position = data.getExtras().getInt("item_position");
                boolean isChecked = data.getExtras().getBoolean("checkeFavourite");

                if (fevouriteEventListAdapter!=null){
                    if (!isChecked) {
                        if(eventitemCount>1) {

                            fevouriteEventListAdapter.changeValueOfParticularPosition(position);

                            eventitemCount=eventitemCount-1;
                        }else {
                            noData();;
                        }

                    }
                }
                boolean check_for_collection_open_from_details = data.getExtras().getBoolean("check_for_collection_open_from_details");
                final String restaurant_data = data.getExtras().getString("restaurant_data");
                if (check_for_collection_open_from_details){

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("insidehere","---"+"here1");

                            ((HomeActivity)getActivity()).goFromSpecialToCollection(restaurant_data);
                        }
                    }, 200);
                }
            }
        }
        if (requestCode==9){
            if (resultCode==8){
                int position = data.getExtras().getInt("item_position");
                boolean isChecked = data.getExtras().getBoolean("checkeFavourite");
              //  Toast.makeText(getActivity(),"inside this",Toast.LENGTH_LONG).show();
                if (fevouriteListAdapter!=null){


                    if (!isChecked) {

                        if(specialitemCount>1) {
                            fevouriteListAdapter.changeValueOfParticularPosition(position);


                            specialitemCount=specialitemCount-1;

                        }
                        else
                            noData();;
                    }
                }
                boolean check_for_collection_open_from_details = data.getExtras().getBoolean("check_for_collection_open_from_details");
                final String restaurant_data = data.getExtras().getString("restaurant_data");
                if (check_for_collection_open_from_details){

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("insidehere","---"+"here2");

                            ((HomeActivity)getActivity()).goFromSpecialToCollection(restaurant_data);
                        }
                    }, 200);
                }
            }
        }

    }

    @Override
    public void onSuccess(FevouriteListData detail) {

        //fevouriteListAdapter.setAllData(detail.getDetails());

        // getBaseActivity().hideLoading();
        getBaseActivity().hideLoading();

        if (detail != null) {
            if (detail.getDetails() != null && detail.getDetails().size() > 0) {
                if (detail.getDetails().size() == AppConstants.LISTING_FETCH_LIMIT) {
                    pagination = pagination + 1;
                    hasLoadMore = true;
                } else {
                    hasLoadMore = false;
                }
                totalItem=detail.getDetails().size();
                specialitemCount=specialitemCount+detail.getDetails().size();
               //fevouriteListAdapter.clearData();
               //
                fevouriteListAdapter.setAllData(detail.getDetails());
                fevouriteListAdapter.setLoaded();
//                collectionAdapter.setData(collectionListingResponse.getDetails());
            } else {
                hasLoadMore = false;
            }
        }
    }


    @Override
    public void onEventSuccess(FevouriteEventData response) {
        //  fevouriteEventListAdapter.setAllData(response.getDetails());


        if (response != null) {
            if (response.getDetails() != null && response.getDetails().size() > 0) {
                if (response.getDetails().size() == AppConstants.LISTING_FETCH_LIMIT) {
                    pagination = pagination + 1;

                    hasLoadMore = true;
                    //eventitemCount=eventitemCount+response.getDetails().size();
                } else {
                    hasLoadMore = false;


                }
                eventitemCount=eventitemCount+response.getDetails().size();

                Log.d("eventitemCount","--"+eventitemCount);
                fevouriteListAdapter.clearData();
                fevouriteEventListAdapter.clearData();
                fevouriteEventListAdapter.setAllData(response.getDetails());
                fevouriteEventListAdapter.setLoaded();
//                collectionAdapter.setData(collectionListingResponse.getDetails());
            } else {
                hasLoadMore = false;
            }
        }
        getBaseActivity().hideLoading();
    }

    @Override
    public void noData() {
         if(tabLayout.getSelectedTabPosition()==1) {
             fragmentFavouritesBinding.noDataTextView.setText(getResources().getString(R.string.no_events_available));


         }else{
             fragmentFavouritesBinding.noDataTextView.setText(getResources().getString(R.string.no_special_available));

         }

        fragmentFavouritesBinding.eventRecycle.setVisibility(View.GONE);
        fragmentFavouritesBinding.noDataTextView.setVisibility(View.VISIBLE);
        fragmentFavouritesBinding.fevouriteRecycle.setVisibility(View.GONE);


        //  Toast.makeText(getBaseActivity(),"inside",Toast.LENGTH_LONG).show();
        getBaseActivity().hideLoading();
    }

    @Override
    public void noload() {
        getBaseActivity().hideLoading();
    }

    @Override
    public void onSuccessEventDelete(CommonSimpleResponse commonSimpleResponse, int position) {
        getBaseActivity().hideLoading();
        if (commonSimpleResponse != null) {
            if (commonSimpleResponse.getStatus().trim().toLowerCase().equals("success")) {
                Toast.makeText(getActivity(),getResources().getString(R.string.removed_success_favourite),Toast.LENGTH_SHORT).show();

                if(eventitemCount>1) {
                    fevouriteEventListAdapter.clearParticularData(position);
                    eventitemCount=eventitemCount-1;
                }else{
                    noData();
                }
            }
        }
    }

    @Override
    public void onError(Throwable throwable) {
        getBaseActivity().hideLoading();
        if (throwable != null) {
            Log.d(TAG, "onError: " + throwable.getMessage());
        }
    }

    @Override
    public void onSuccessSpecialDelete(CommonSimpleResponse commonSimpleResponse, int position) {
        getBaseActivity().hideLoading();
        if (commonSimpleResponse != null) {
            if (commonSimpleResponse.getStatus().trim().toLowerCase().equals("success")) {
                Toast.makeText(getActivity(),getResources().getString(R.string.removed_success_favourite),Toast.LENGTH_SHORT).show();
                if(specialitemCount>1) {
                    fevouriteListAdapter.clearParticularData(position);
                    specialitemCount = specialitemCount - 1;

//                  if(totalItem<=6){
//                       pagination=1;
//                  }
                 //  onResume();
                }
                else
                    noData();
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
       GlobalBus.getEventBus().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        GlobalBus.getEventBus().register(this);


    }

    @Subscribe
    public void onEventForDeletionOfSpecialOrEvent(EventBusEventSpecialDeleteInFavouritePage eventBusEventSpecialDeleteInFavouritePage) {

        if (eventBusEventSpecialDeleteInFavouritePage != null) {
            if (eventBusEventSpecialDeleteInFavouritePage.isaBoolean_event_or_special()) {
                /* Special Delete api hit */
                getBaseActivity().showLoading();
                String event_id = fevouriteListAdapter.getData().get(eventBusEventSpecialDeleteInFavouritePage.getPosition()).getId();
                favouritesViewModel.apiHitSpecialLikeUnlike(event_id, eventBusEventSpecialDeleteInFavouritePage.getPosition());

            } else {
                /* Event Delete api hit */
                getBaseActivity().showLoading();
                String event_id = fevouriteEventListAdapter.getData().get(eventBusEventSpecialDeleteInFavouritePage.getPosition()).getId();
                favouritesViewModel.apiHitEventLikeUnlike(event_id, eventBusEventSpecialDeleteInFavouritePage.getPosition());

            }
        }
    }
}
