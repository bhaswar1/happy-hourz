package com.admin.happyhours.ui.main.collections;

import androidx.lifecycle.ViewModelProvider;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class CollectionModule {


    @Provides
    CollectionViewModel collectionViewModel(DataManager dataManager,
                                            SchedulerProvider schedulerProvider) {
        return new CollectionViewModel(dataManager, schedulerProvider);
    }


//    @Provides
//    ViewModelProvider.Factory provideCollectionViewModel(CollectionViewModel profilePageViewModel) {
//        return new ViewModelProviderFactory<>(profilePageViewModel);
//    }

}
