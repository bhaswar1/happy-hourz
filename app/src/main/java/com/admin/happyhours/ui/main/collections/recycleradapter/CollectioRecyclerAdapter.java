package com.admin.happyhours.ui.main.collections.recycleradapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import android.widget.RelativeLayout;

import com.admin.happyhours.R;
import com.admin.happyhours.customview.MediumTextView;
import com.admin.happyhours.data.model.api.response.CollectionListingResponse;
import com.admin.happyhours.interfaces.ItemLongClickListener;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.LinkedList;

public class CollectioRecyclerAdapter extends RecyclerView.Adapter<CollectioRecyclerAdapter.ViewHolder> {

    private int SELECTED_ITEM = -1;
    public boolean isLoading;
    private final int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private Context context;
    private OnLoadMoreListener mOnLoadMoreListener;
    private OnItemClickListener callback = null;
    private ItemLongClickListener onItemLongClick = null;
    private LinkedList<CollectionListingResponse.Details> details;

    public  int favouriteStatus=0;

    public CollectioRecyclerAdapter(FragmentActivity activity, LinkedList<CollectionListingResponse.Details> details,
                                    RecyclerView recyclerView, final GridLayoutManager gridLayoutManager, int favouriteStatus) {
        this.context = activity;
        this.details = details;
        this.favouriteStatus=favouriteStatus;
//        this.details.add(new CollectionListingResponse.Details());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = gridLayoutManager.getItemCount();
                lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    @NonNull
    @Override
    public CollectioRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.collection_list_row_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CollectioRecyclerAdapter.ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public void setOnItemClickListener(OnItemClickListener callback) {
        this.callback = callback;
    }

    public void setCallbackLong(ItemLongClickListener callbackLong) {
        this.onItemLongClick = callbackLong;
    }
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }
    public void clearData() {
        details.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_collection, rl_new, imv_selector;
        ImageView imv_collection,imv_collection1;
        MediumTextView tv_collectionname;

        public ViewHolder(View itemView) {
            super(itemView);
            rl_collection = (RelativeLayout) itemView.findViewById(R.id.rl_collection);
            rl_new = (RelativeLayout) itemView.findViewById(R.id.rl_new);
            imv_collection = (ImageView) itemView.findViewById(R.id.imv_collection);
            imv_collection1 = (ImageView) itemView.findViewById(R.id.imv_collection1);

            tv_collectionname = (MediumTextView) itemView.findViewById(R.id.tv_collectionname);
            imv_selector = itemView.findViewById(R.id.imv_selector);

        }

        public void bind(final int position) {
            if (position == 0) {
                rl_new.setVisibility(View.VISIBLE);
                rl_collection.setVisibility(View.GONE);
                rl_new.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onItemClick(position, view);
                    }
                });
            } else {
                rl_new.setVisibility(View.GONE);
                rl_collection.setVisibility(View.VISIBLE);


                if (!details.get(position).getCollection_name().isEmpty()) {
                    String temp=details.get(position).getCollection_name().trim();
                    Character ch=temp.charAt(0);
                    String title=ch.toString().toUpperCase()+temp.substring(1);
                    Log.d("titlecollect","="+title);
                    tv_collectionname.setText(title);
                    Log.d("check_imgae_adp",": "+details.get(position).getCollection_image());
                }

                if(position==1  && favouriteStatus==1 ){
                    tv_collectionname.setText("Favourites");
//                    imv_collection.setScaleType(ImageView.ScaleType.FIT_XY);
//                    imv_collection.setMaxHeight(140);
                    imv_collection1.setVisibility(View.VISIBLE);
imv_collection.setVisibility(View.GONE);

                    imv_collection1.setImageResource(R.drawable.favimage);
               //   tv_collectionname.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.like_logo_checked, 0);
                }else{
                    imv_collection1.setVisibility(View.INVISIBLE);

                    imv_collection.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.placeholder_image).
                            error(R.drawable.placeholder_image)).load(details.get(position).getCollection_image()).
                            into(imv_collection);

                }


                rl_collection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onItemClick(position, view);
                    }
                });
                rl_collection.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        onItemLongClick.onItemLongClick(position, v);
                        return true;
                    }
                });

            }

            if (position == SELECTED_ITEM){
                imv_selector.setVisibility(View.VISIBLE);
            }else {
                imv_selector.setVisibility(View.GONE);
            }
        }
    }

    public LinkedList<CollectionListingResponse.Details> getData() {
        return details;
    }

    public void setAllData(LinkedList<CollectionListingResponse.Details> data, int favouriteStatus) {
        this.favouriteStatus=favouriteStatus;
        details.addAll(data);
        notifyDataSetChanged();
    }
    public void setData(CollectionListingResponse.Details detail) {
        details.add(detail);
        notifyDataSetChanged();
    }
    public void setLoaded() {
        isLoading = false;
    }

    public void clearParticularData(int position){
        details.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, details.size());
    }

    public void showHideSelector(int position){
        SELECTED_ITEM = position;
        notifyDataSetChanged();
    }



}
