package com.admin.happyhours.ui.main;

import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.CusineRespone;
import com.admin.happyhours.data.model.api.response.ServiceResponse;
import com.admin.happyhours.data.model.api.response.TagResponse;

public interface HomeNavigator {

    void responseAdterLogout(CommonSimpleResponse commonSimpleResponse);

void  badgeSuccess(String badge);
    void onError(Throwable throwable);

    void success(TagResponse response);

    void cusinsuccess(CusineRespone response);

    void serviceSuccess(ServiceResponse response);
}
