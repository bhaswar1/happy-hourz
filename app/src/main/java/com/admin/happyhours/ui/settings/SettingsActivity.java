package com.admin.happyhours.ui.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.databinding.ActivitySettingsBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.google.gson.Gson;

import javax.inject.Inject;

public class SettingsActivity extends BaseActivity<ActivitySettingsBinding, SettingsViewModel> implements SettingsNavigator {


    ActivitySettingsBinding activitySettingsBinding;
    @Inject
    SettingsViewModel settingsViewModel;
   private String password;
    AlertDialog alertDialog;
    String newpass;

    public static Intent newIntent(Context context) {
        return new Intent(context, SettingsActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_settings;
    }

    @Override
    public SettingsViewModel getViewModel() {
        return settingsViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySettingsBinding = getViewDataBinding();
        settingsViewModel.setNavigator(this);
        setStatusBarHidden(SettingsActivity.this);
        Toolbar mtoolbar=activitySettingsBinding.toolbar;
        setSupportActionBar(mtoolbar);
        mtoolbar.setNavigationIcon(R.drawable.back_arrow_black);
        mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        password=settingsViewModel.getDataManager().getPassword();
        if (password!=null){
            activitySettingsBinding.changepassword.setVisibility(View.VISIBLE);
        }else{
            activitySettingsBinding.changepassword.setVisibility(View.GONE);
        }
        Log.d("password","--"+password);


        if (settingsViewModel.getDataManager().getCurrentUserInfo() != null) {
           showLoading();
            NormalLoginResponse.Details details = new Gson().fromJson(settingsViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
            settingsViewModel.pushNotofication(details.getId());
        }


activitySettingsBinding.switch1.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if(activitySettingsBinding.switch1.isChecked()){
          //  Toast.makeText(BusinessClaimActivity.this, "on", Toast.LENGTH_SHORT).show();

            if (settingsViewModel.getDataManager().getCurrentUserInfo() != null) {
                showLoading();
                NormalLoginResponse.Details details = new Gson().fromJson(settingsViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                settingsViewModel.updatepushNotofication(details.getId(),"1");
            }

        }else{
          //  Toast.makeText(BusinessClaimActivity.this, "offf", Toast.LENGTH_SHORT).show();
            if (settingsViewModel.getDataManager().getCurrentUserInfo() != null) {
                showLoading();
                NormalLoginResponse.Details details = new Gson().fromJson(settingsViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                settingsViewModel.updatepushNotofication(details.getId(),"0");
            }


        }
    }
});


        activitySettingsBinding.changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SettingsActivity.this).setCancelable(false);
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.custom_changepasswor_dialog, null);
                dialogBuilder.setView(dialogView);
                TextView cancelicon = (TextView) dialogView.findViewById(R.id.textView29);
                final EditText oldPassWord=  dialogView.findViewById(R.id.oldPassword);
                final EditText newpassword=  dialogView.findViewById(R.id.newPassword) ;
                final EditText confirmPassword=  dialogView.findViewById(R.id.confirmPassword) ;

                Button submit=(Button) dialogView.findViewById(R.id.submit);
                //  Toast.makeText(BusinessClaimActivity.this,"click",Toast.LENGTH_LONG).show();
                  alertDialog = dialogBuilder.create();
                alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                alertDialog.show();

                cancelicon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });



                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String oldpass=oldPassWord.getText().toString().trim();
                         newpass=newpassword.getText().toString().trim();
                        String confirmpass=confirmPassword.getText().toString().trim();
                        //Toast.makeText(BusinessClaimActivity.this,"checked",Toast.LENGTH_LONG).show();

                        if(oldpass.length()==0 || oldpass==null){
                          //  Toast.makeText(BusinessClaimActivity.this,"checked",Toast.LENGTH_LONG).show();
                            Log.d("password","--"+password);
                            oldPassWord.requestFocus();
                            oldPassWord.setError("Please enter your current password ");

                        }else{

                            if(!oldpass.equals(password)){
                                oldPassWord.requestFocus();
                                newpassword.setError(null);
                                confirmPassword.setError(null);
                               // confirmPassword.setText("");
                               // newpassword.setText("");
                              oldPassWord.setSelection(oldpass.length());
                              //  oldPassWord.setSelection(password.length());
                                oldPassWord.setError(getString(R.string.do_no_match_password));
                            }

                            else {
                                if (newpass.length() == 0 || newpass == null) {
                                    newpassword.requestFocus();
                                    newpassword.setSelection(0);

                                    newpassword.setError("Please set a new password");
                                } else {

                                    if (newpass.trim().length() != 6) {

                                        newpassword.requestFocus();
                                       // newpassword.setSelection(password.length());
                                        confirmPassword.setError(null);

                                        newpassword.setError(getResources().getString(R.string.password_atleast));
                                    } else {
                                                newpassword.setError(null);

                                        if (confirmpass.length() == 0 || confirmpass == null) {
                                            confirmPassword.requestFocus();

                                            confirmPassword.setError("Confirm password should not be blank");
                                        } else {
                                            if (!confirmpass.equals(newpass)) {
                                                confirmPassword.requestFocus();

                                                confirmPassword.setError(getString(R.string.confrm_and_new_pass));
                                            } else {
                                                if (settingsViewModel.getDataManager().getCurrentUserInfo() != null) {
                                                    showLoading();
                                                    NormalLoginResponse.Details details = new Gson().fromJson(settingsViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                                                    settingsViewModel.changePassword(details.getId(), password, newpass);
                                                }


                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            }
        });


    }

    @Override
    public void success(String push) {
        hideLoading();
        if (push.equals("1")) {

            activitySettingsBinding.switch1.setChecked(true);
        }
    }

    @Override
    public void updatePushSuccess(CommonSimpleResponse response) {

            if(response.getStatus().toLowerCase().equals("success")){
                hideLoading();
            }
    }

    @Override
    public void onError(Throwable throwable) {
hideLoading();
    }

    @Override
    public void updatePasswordSuccess(CommonSimpleResponse response) {
        hideLoading();
        alertDialog.dismiss();
        Toast.makeText(this, "password updated succesfully", Toast.LENGTH_SHORT).show();
        password=newpass;
        settingsViewModel.getDataManager().setPassword(newpass);
    }
}
