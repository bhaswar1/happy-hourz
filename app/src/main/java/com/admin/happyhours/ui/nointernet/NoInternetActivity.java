package com.admin.happyhours.ui.nointernet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.databinding.ActivityNoInternetBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.bumptech.glide.Glide;

import javax.inject.Inject;

public class NoInternetActivity extends BaseActivity<ActivityNoInternetBinding, NoInternetViewModel> implements
        NoInternetNavigator, BaseActivity.closeOnInternetActivity {

    ActivityNoInternetBinding activityNoInternetBinding;
    @Inject
    NoInternetViewModel noInternetViewModel;

    public static Intent newIntent(Context context) {
        return new Intent(context, NoInternetActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_no_internet;
    }

    @Override
    public NoInternetViewModel getViewModel() {
        return noInternetViewModel;
    }

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        getWindow().setBackgroundDrawableResource(R.raw.no_internet_conenction_image);

        activityNoInternetBinding = getViewDataBinding();
        noInternetViewModel.setNavigator(this);

        setStatusBarHidden(NoInternetActivity.this);

        Glide.with(NoInternetActivity.this).load(R.raw.no_internet_conenction_image).into(activityNoInternetBinding.imageNoInternet);

        setCloseOnInternetActivity(this);

        activityNoInternetBinding.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onCloseActivity() {
        this.finish();
    }

    /*@Override
    protected void closeNoInternetActivityAfterConnectionIsDone() {
        this.finish();
    }*/
}
