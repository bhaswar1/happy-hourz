package com.admin.happyhours.ui.createcollection;


import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateCollectionModule {


    @Provides
    CreateCollectionViewModel provideCreateCollectionViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new CreateCollectionViewModel(dataManager, schedulerProvider);
    }
}
