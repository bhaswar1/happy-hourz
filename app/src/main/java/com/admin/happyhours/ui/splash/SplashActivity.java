package com.admin.happyhours.ui.splash;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.databinding.ActivitySplashBinding;
import com.admin.happyhours.services.MyFirebaseMessagingService;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.ui.login.LoginActivity;
import com.admin.happyhours.utils.AppConstants;
import com.admin.happyhours.utils.PermissionUtils;
import com.admin.happyhours.utils.SharedPrefManager;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity<ActivitySplashBinding, SplashViewModel>
        implements SplashNavigator, PermissionUtils.PermissionResultCallback {

    private static final int CHECK_LOCATION_SETTINGS_REQUEST_CODE = 1;
    private FusedLocationProviderClient mFusedLocationClient;
    private PermissionUtils permissionUtils;
    private ArrayList<String> permissions = new ArrayList<>();
    LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private SharedPrefManager sharedPrefManager;
    @Inject
    SplashViewModel mSplashViewModel;
    private ActivitySplashBinding activitySplashBinding;
    String value;

    String deepLinkId = "";
    String deepLinkType = "";
    String deepLinkLat = "";
    String deepLinkLong = "";


    public static Intent newIntent(Context context) {
        return new Intent(context, SplashActivity.class);
    }

    public static Intent newIntent(Context context, String notification) {
        Log.d("check_noti_1",": "+notification);
        return new Intent(context, SplashActivity.class).putExtra("notification",notification);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public SplashViewModel getViewModel() {
        return mSplashViewModel;
    }

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("screen","=======");

        getWindow().setBackgroundDrawableResource(R.drawable.splash_new);
        MyFirebaseMessagingService firebaseMessagingService=new MyFirebaseMessagingService();

        Intent intent = getIntent();
        this.displayIntentContent(intent.getExtras());

        setStatusBarHidden(SplashActivity.this);
        hideNavBar(AppConstants.splash_screen);
        mSplashViewModel.setNavigator(this);
        activitySplashBinding = getViewDataBinding();


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        permissionUtils = new PermissionUtils(SplashActivity.this);

        PendingIntent contentIntent = null;


//        if (getIntent().getExtras() != null ) {
//            for (String key : getIntent().getExtras().keySet()) {
//
//                Log.d("field", "--" + key);
//                if (key.equals("message")) {
//                     value = getIntent().getExtras().getString(key);
//                     break;
//
//                }
//            }
//            Intent i;
//            Log.d("TAG", "Key: "  + " Value:-- " + value);
//            if (value != null) {
//
//
//                startActivity(new Intent(SplashActivity.this,HomeActivity.class).putExtra("notification",value));
//                // firebaseMessagingService.sendNotification(value,remoteMessage);
//            }
//        }





//        if (getIntent()!=null && getIntent().getExtras()!=null &&
//                getIntent().getExtras().getString("notification")!=null){
//            Log.d("check_noti_1",": "+getIntent().getExtras().getString("notification"));
////            SharedPrefManager.getInstance(HelpSupportActivity.this).saveBooleanForNotificationRedirection(true);
//        }else {
////            SharedPrefManager.getInstance(HelpSupportActivity.this).saveBooleanForNotificationRedirection(false);
//        }



        //
        /*PackageManager pm = getPackageManager();
        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            permissions.add(Manifest.permission.CAMERA);
        }
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);*/
//        permissions
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    // ...
                    Log.d("latitude2", ":" + location.getLatitude());
                    Log.d("longitude2", ":" + location.getLongitude());
                    try {
                        JSONObject locationJson = new JSONObject();
                        locationJson.put("latitude", location.getLatitude());
                        locationJson.put("longitude", location.getLongitude());

                        Log.d("latitude3", ":" + location.getLatitude());
                        Log.d("longitude3", ":" + location.getLongitude());
                        saveLatLongToLocal(location.getLatitude(), location.getLongitude());
                        stopLocationUpdates();
                        //moving to login activity
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        };

        generateKeyhash();

        Uri uri=getIntent().getData();
        try {
            Log.e("data", "" + uri.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
        String data1;
        if (uri!=null){
            data1= uri.getQueryParameter("id");
            deepLinkId = data1;
            Log.e("===data===",data1);
            try {
                Log.e("===path===",uri.getPath());
                deepLinkType = uri.getPath().replace("/","");
                if (uri.getPath().equals("/restaurant")){
                    deepLinkLat= uri.getQueryParameter("latitude");
                    deepLinkLong= uri.getQueryParameter("longitude");
                }else {
                    deepLinkLat = "";
                    deepLinkLong = "";
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    private void displayIntentContent(Bundle bundle) {
        if (bundle == null) {
        return;
        }
        String data = ""; for (String key : bundle.keySet()) {

            if (key.equals("message")) {
                Object value = bundle.get(key);
                data += key + " : " + value.toString() + "\n";
            }
        }
//
    }

    private void saveLatLongToLocal(double latitude, double longitude) {
        mSplashViewModel.getDataManager().setLatitude(String.valueOf(latitude));
        mSplashViewModel.getDataManager().setLongitude(String.valueOf(longitude));

        goToNextActivity();

    }
    private void goToNextActivity() {
        if (isNetworkConnected()){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = LoginActivity.newIntent(SplashActivity.this);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            if (!deepLinkId.isEmpty()){
                                intent.putExtra("deepLinkType",deepLinkType);
                                intent.putExtra("deepLinkId",deepLinkId);
                                intent.putExtra("deepLinkLat",deepLinkLat);
                                intent.putExtra("deepLinkLong",deepLinkLong);
                            }
                            startActivity(intent);
                            SplashActivity.this.finish();
                        }
                    });
                }
            }, 100);
        }else {
            Toast.makeText(this, "Please connect to Internet first", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void PermissionGranted(int request_code) {
        Log.i("request_code", "" + request_code);
        GoToNext();
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        permissions = new ArrayList<>();
        for (int i = 0; i < granted_permissions.size(); i++) {

            switch (granted_permissions.get(i)) {
                case Manifest.permission.ACCESS_FINE_LOCATION:
                    permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);

            }

        }
        permissionUtils.check_permission(permissions, "Need These permissions", AppConstants.REQUEST_CODE_STORAGE_PERMS);
    }

    @Override
    public void PermissionDenied(int request_code) {
        Log.i("PermissionDenied", "" + request_code);
    }

    @Override
    public void NeverAskAgain(int request_code) {

        GoToNext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        CheckPermissionAndGo();
    }

    private void CheckPermissionAndGo() {

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            GoToNext();
        } else {
            getPermissionTo();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void getPermissionTo() {
        permissionUtils.check_permission(permissions, "Need These permissions", AppConstants.REQUEST_CODE_STORAGE_PERMS);

    }

    private void GoToNext() {
        gettingLocationAfterPermissionGranted();
    }


    protected void createLocationRequest() {
        if (mLocationRequest == null) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        }
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @SuppressLint("MissingPermission")
    public void gettingLocationAfterPermissionGranted() {
        Log.e("===/===","1");
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {

                            // Logic to handle location object
                            try {
                                JSONObject locationJson = new JSONObject();
                                locationJson.put("latitude", location.getLatitude());
                                locationJson.put("longitude", location.getLongitude());
                                Log.d("latitude1", ":" + location.getLatitude());
                                Log.d("longitude1", ":" + location.getLongitude());
                                saveLatLongToLocal(location.getLatitude(), location.getLongitude());

                                //moving to login activity
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            createLocationRequest();
                            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                                    .addLocationRequest(mLocationRequest);
                            SettingsClient client = LocationServices.getSettingsClient(SplashActivity.this);
                            Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
                            task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                                @Override
                                public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                                    // All location settings are satisfied. The client can initialize
                                    // location requests here.
                                    // ...
                                    startLocationUpdates();
                                }
                            });

                            task.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    if (e instanceof ResolvableApiException) {
                                        // Location settings are not satisfied, but this can be fixed
                                        // by showing the user a dialog.
                                        try {
                                            // Show the dialog by calling startResolutionForResult(),
                                            // and check the result in onActivityResult().
                                            ResolvableApiException resolvable = (ResolvableApiException) e;
                                            resolvable.startResolutionForResult(((Activity) SplashActivity.this),
                                                    CHECK_LOCATION_SETTINGS_REQUEST_CODE);
                                        } catch (IntentSender.SendIntentException sendEx) {
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHECK_LOCATION_SETTINGS_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                startLocationUpdates();
            } else {
                Toast.makeText(this, "Please turn on location settings", Toast.LENGTH_SHORT).show();
//                finish();
            }
        }
    }

    private void generateKeyhash(){
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.admin.happyhours",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

}
