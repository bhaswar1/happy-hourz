package com.admin.happyhours.ui.main.special.recycleradapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.admin.happyhours.R;

import java.util.ArrayList;

public class Offeradapter extends RecyclerView.Adapter<Offeradapter.customview> {


    private Activity mcontext;
    ArrayList<String> offers = new ArrayList<>();
    private int fposition=0;


    public Offeradapter(Activity mcontext) {

        this.mcontext = mcontext;
    }

    @Override
    public customview onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Offeradapter.customview(LayoutInflater.from(mcontext).inflate(R.layout.offersdetails, parent, false));
    }

    @Override
    public void onBindViewHolder(customview holder, int position) {

        holder.onBind(position);

    }

    @Override
    public int getItemCount() {
        Log.d("size","---"+offers.size());


        return offers.size();
    }

    public class customview extends RecyclerView.ViewHolder {
        TextView tv_offerdata,item;
        View v2;

        public customview(View itemView) {
            super(itemView);
            tv_offerdata =  itemView.findViewById(R.id.food);
            item=itemView.findViewById(R.id.item);
            v2 =   itemView.findViewById(R.id.v2);
        }

        public void onBind(int position) {
            tv_offerdata.setText(offers.get(position));
            if(position==0 && fposition!=0){
                tv_offerdata.setVisibility(View.VISIBLE);

                tv_offerdata.setText("Food");
            }else if(position==fposition){
                tv_offerdata.setVisibility(View.VISIBLE);

                tv_offerdata.setText("Drinks");
            }else{
                tv_offerdata.setVisibility(View.GONE);
            }
            item.setText(offers.get(position));
//            if (position == (offers.size() - 1)){
//                v2.setVisibility(View.GONE);
//            }
        }
    }

    public void addOfferData(ArrayList<String> offers,int fposition1){
        this.fposition=fposition1;
        this.offers.clear();
        this.offers.addAll(offers);
        notifyDataSetChanged();
    }

    public void clearData(){
        this.offers.clear();
        notifyDataSetChanged();
    }
}