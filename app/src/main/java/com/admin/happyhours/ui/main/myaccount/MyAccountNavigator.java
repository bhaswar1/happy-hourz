package com.admin.happyhours.ui.main.myaccount;

import com.admin.happyhours.data.model.api.response.AccountResponse;
import com.admin.happyhours.data.model.api.response.ProfileImageUpdateResponse;

public interface MyAccountNavigator {
    void onSuccess(AccountResponse response);

    void success(ProfileImageUpdateResponse profileImageUpdateResponse);

    void updateProfileSuccess(AccountResponse response);

    void noData();

    void phoneExist();
}
