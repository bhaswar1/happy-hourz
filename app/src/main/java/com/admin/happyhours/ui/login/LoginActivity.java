package com.admin.happyhours.ui.login;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import androidx.annotation.NonNull;

import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.databinding.ActivityLoginBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.ui.forgotpassword.ForgotPasswordActivity;
import com.admin.happyhours.ui.main.HomeActivity;
import com.admin.happyhours.ui.signup.SignupActivity;
import com.admin.happyhours.ui.splash.SplashActivity;
import com.admin.happyhours.utils.AppConstants;
import com.admin.happyhours.utils.AppUpdateChecker;
import com.admin.happyhours.utils.AsteriskPasswordTransformationMethod;
import com.admin.happyhours.utils.CommonUtils;
import com.admin.happyhours.utils.EdittextInputFilters;
import com.admin.happyhours.utils.NetworkUtils;
import com.admin.happyhours.utils.SharedPrefManager;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.SignInMethodQueryResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.inject.Inject;

import static com.admin.happyhours.utils.AppConstants.FB_LOGIN;
import static com.admin.happyhours.utils.AppConstants.GOOGLE_LOGIN;

public class LoginActivity extends BaseActivity<ActivityLoginBinding, LoginViewModel> implements LoginNavigator {

    private static final int RC_SIGN_IN = 9001;
    private FirebaseAuth firebaseAuth;
    private String google_id = "";
//    private String google_id_token = "";
    private String g_name, g_email;
    private String fb_connection_id, fb_name, fb_email, fb_imageurl;
    private CallbackManager callbackManager;
    private boolean aBoolean_check_for_social_login;
    private String TAG = LoginActivity.this.getClass().getSimpleName();
    ActivityLoginBinding activityLoginBinding;
    @Inject
    LoginViewModel loginViewModel;
    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInOptions gso;
    private FirebaseAnalytics mFirebaseAnalytics;
    public static Intent newIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public LoginViewModel getViewModel() {
        return loginViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(LoginActivity.this);
        callbackManager = CallbackManager.Factory.create();
        setStatusBarHidden(LoginActivity.this);
//        hideNavBar(AppConstants.all_screen);
        activityLoginBinding = getViewDataBinding();
        loginViewModel.setNavigator(this);
      //  AdRequest adRequest = new AdRequest.Builder().build();
        hideSoftKeyboardWithTouchOutsideEdittext(activityLoginBinding.layoutParent);

        if (loginViewModel.getDataManager().getCurrentUserInfo()!=null){
            gotoMainActivity();
        }/*else {
            SharedPrefManager.getInstance(LoginActivity.this).saveBooleanForNotificationRedirection(false);
        }*/
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        activityLoginBinding.etEmail.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        activityLoginBinding.etPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);


        activityLoginBinding.etEmail.setFilters(new InputFilter[]
                {EdittextInputFilters.getFilterWithoutSpace()});
        activityLoginBinding.etPassword.setFilters(new InputFilter[]
                {EdittextInputFilters.getFilterWithoutSpace()});
        activityLoginBinding.etPassword.setTransformationMethod(new AsteriskPasswordTransformationMethod());

        /*activityLoginBinding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s.toString().trim())) {
                    activityLoginBinding.textInputLayoutEmail.setErrorEnabled(false);
                    if (CommonUtils.isEmailValid(s.toString().trim())) {
                        activityLoginBinding.textInputLayoutEmail.setErrorEnabled(false);
                    } else {
                        activityLoginBinding.textInputLayoutEmail.setErrorEnabled(true);
                        activityLoginBinding.textInputLayoutEmail.setError(getResources().getString(R.string.invalid_email));
                    }
                } else {

                    activityLoginBinding.textInputLayoutEmail.setErrorEnabled(true);
                    activityLoginBinding.textInputLayoutEmail.setError(getResources().getString(R.string.email_can_not_be_blank));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        activityLoginBinding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s.toString().trim())) {
                    activityLoginBinding.textInputLayoutPassword.setErrorEnabled(false);
                } else {
                    activityLoginBinding.textInputLayoutPassword.setErrorEnabled(true);
                    activityLoginBinding.textInputLayoutPassword.setError(getResources().getString(R.string.password_can_not_be_blank));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/
            forGotpassword();
        activityLoginBinding.tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = activityLoginBinding.etEmail.getText().toString();
                String password = activityLoginBinding.etPassword.getText().toString();

                if (NetworkUtils.isNetworkConnected(LoginActivity.this)) {
                    if (checkValidation(email, password)) {
                        hideKeyboard();
                        if (NetworkUtils.isNetworkConnected(LoginActivity.this)) {
                            showLoading();
                            String device_token = SharedPrefManager.getInstance(LoginActivity.this).getDeviceToken();
                            loginViewModel.login(email, password, AppConstants.device_type, device_token);
                        } else {
                            Toast.makeText(LoginActivity.this, getResources().getString(R.string.check_internet), Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Please Check Network", Toast.LENGTH_SHORT).show();
                }
            }
        });

        activityLoginBinding.tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SignupActivity.newIntent(LoginActivity.this));
            }
        });



        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        hideKeyboard();
                        hideLoading();
                        fb_connection_id = loginResult.getAccessToken().getUserId();

                        if (AccessToken.getCurrentAccessToken() != null) {
                            Log.d("accesstoken","---"+AccessToken.getCurrentAccessToken());

                            RequestData();

                        }
                    }

                    @Override
                    public void onCancel() {
                        hideKeyboard();
                        hideLoading();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d("facebookerror","--"+error.getMessage());
                        hideKeyboard();
                        hideLoading();
                    }
                });
        getHash();
        //get release hash key from play console SHA-1
        hashFromSHA1("AE:70:FC:7C:8B:F0:14:35:88:2B:88:A2:3A:2A:6E:BE:89:A4:9B:26");
        fbLogin();
        googleLogin();

        //Checking App Version..............
        AppUpdateChecker appUpdateChecker = new AppUpdateChecker(this); //pass the activity in constructure
        appUpdateChecker.checkForUpdate(false); //mannual check false here
    }

    private void forGotpassword() {
        activityLoginBinding.forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,ForgotPasswordActivity.class));
            }
        });
    }
    /* End onCreate Method */


    private boolean checkValidation(String email, String password) {
        if (TextUtils.isEmpty(email.trim())) {
//            activityLoginBinding.textInputLayoutEmail.setErrorEnabled(true);
//            activityLoginBinding.textInputLayoutEmail.setError(getString(R.string.email_can_not_be_blank));
            activityLoginBinding.etEmail.requestFocus();
            activityLoginBinding.etEmail.setError(getString(R.string.email_can_not_be_blank));
            return false;
        }

        if (!CommonUtils.isEmailValid(email.trim())) {
//            activityLoginBinding.textInputLayoutEmail.setErrorEnabled(true);
//            activityLoginBinding.textInputLayoutEmail.setError(getString(R.string.invalid_email));
            activityLoginBinding.etEmail.requestFocus();
            activityLoginBinding.etEmail.setError(getString(R.string.invalid_email));
            return false;
        }

        if (TextUtils.isEmpty(password.trim())) {
//            activityLoginBinding.textInputLayoutPassword.setErrorEnabled(true);
//            activityLoginBinding.textInputLayoutPassword.setError(getString(R.string.password_can_not_be_blank));
            activityLoginBinding.etPassword.requestFocus();
            activityLoginBinding.etPassword.setError(getString(R.string.password_can_not_be_blank));
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess(NormalLoginResponse normalLoginResponse) {
        hideKeyboard();
        hideLoading();
        if (normalLoginResponse != null) {
            Toast.makeText(this, normalLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();


            if (normalLoginResponse.getDetails() != null) {
                Bundle bundle = new Bundle();
                bundle.putString("userId", normalLoginResponse.getDetails().getId());
                bundle.putString("userName", normalLoginResponse.getDetails().getUserName());
                // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
                //  bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, resturantName);
                bundle.putString("userAdress",normalLoginResponse.getDetails().getAddress());
                bundle.putString("userphoneno",normalLoginResponse.getDetails().getPhone());
                bundle.putString("useremail",normalLoginResponse.getDetails().getEmail());
                bundle.putString("usergender",normalLoginResponse.getDetails().getGender());
                mFirebaseAnalytics.logEvent("logged_in_android", bundle);
                gotoMainActivity();
            }
        }
    }

    private void gotoMainActivity(){
        Intent intent = HomeActivity.newIntent(LoginActivity.this);
        if (getIntent().hasExtra("deepLinkId")){
            intent.putExtra("deepLinkId",getIntent().getStringExtra("deepLinkId"));
            intent.putExtra("deepLinkType",getIntent().getStringExtra("deepLinkType"));
            intent.putExtra("deepLinkLat",getIntent().getStringExtra("deepLinkLat"));
            intent.putExtra("deepLinkLong",getIntent().getStringExtra("deepLinkLong"));
        }
        startActivity(intent);
        //startActivity(HomeActivity.newIntent(LoginActivity.this));
        finishAffinity();
    }

    @Override
    public void onError(Throwable throwable) {
        hideLoading();
        Log.d(TAG, ":-ERROR:-  " + throwable.getMessage());
    }





    /*@Override
    public void fbSignupRequired() {
        hideLoading();
        SocialSignupDialog.newInstance(FB_LOGIN, fb_name, fb_email, fb_connection_id, fb_imageurl).show(getSupportFragmentManager());
    }

    @Override
    public void googleSignupRequired() {
        hideLoading();
        SocialSignupDialog.newInstance(GOOGLE_LOGIN, g_name, g_email, google_id).show(getSupportFragmentManager());
    }*/


    /* facebook Start */
    private void fbLogin() {
        activityLoginBinding.facebookTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkConnected(LoginActivity.this)) {
                    aBoolean_check_for_social_login = false;
                    LoginManager.getInstance().logOut();
                    LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email"));
                } else {
                    Toast.makeText(LoginActivity.this, "Please Check Network", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                JSONObject json = response.getJSONObject();
                if (json != null) {
                    System.out.println("User fb response!" + json.toString());
                    try {
                        fb_name = json.getString("name");
                        /*fb_fname = json.getString("first_name");
                        fb_lname = json.getString("last_name");*/
                        if (json.has("email"))
                            fb_email = json.getString("email");
                        else
                            fb_email = "";
                        fb_imageurl = "https://graph.facebook.com/" + json.getString("id") + "/picture?type=large";

                        Log.d("name_fb", ": " + fb_name);
                        /*Log.d("fname_fb", ": " + fb_fname);
                        Log.d("lname_fb", ": " + fb_lname);*/
                        Log.d("image_fb", ": " + fb_imageurl);
                        Log.d("email_fb", ": " + fb_email);
                        Log.d("connectionId_fb", ": " + fb_connection_id);

                        hideKeyboard();
                        showLoading();

                        loginViewModel.loginWithFbOrGplus(FB_LOGIN, fb_name, fb_email, fb_connection_id, fb_imageurl,
                                AppConstants.device_type, getDeviceToken());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,first_name,last_name,email,picture");
      //  parameters.putString("fields", "feed");
        request.setParameters(parameters);
        request.executeAsync();
    }
    /* facebook End */


    /* Google Start */
    private void googleLogin() {
        firebaseAuth = FirebaseAuth.getInstance();
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .requestProfile()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(LoginActivity.this, gso);

        activityLoginBinding.googleTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkConnected(LoginActivity.this)) {
                    aBoolean_check_for_social_login = true;
                    //logOutGoogle();
                    firebaseAuth.signOut();
                    mGoogleSignInClient.signOut();

                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                } else {
                    Toast.makeText(LoginActivity.this, "Please Check Network", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {

//        google_id_token = acct.getIdToken();
        google_id = acct.getId();
        Log.d(TAG, "google_id:" + google_id);
        Log.d(TAG, "email_chk_gle:" + acct.getEmail());
        Log.d(TAG, "google_id_token:" + acct.getIdToken());

        firebaseAuth.fetchSignInMethodsForEmail(acct.getEmail()).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
            @Override
            public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {

                AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
                firebaseAuth.signInWithCredential(credential)
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {


                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    Log.d(TAG, "signInWith_plus:success");
                                    FirebaseUser user = firebaseAuth.getCurrentUser();

                                    Log.d("fireBase_uuid", ": " + user.getUid());
                                    Log.d("google_email", ": " + user.getEmail());
                                    Log.d("google_name", ": " + user.getDisplayName());

                                    g_name = user.getDisplayName();
                                    g_email = user.getEmail();

                                    hideKeyboard();
                                    showLoading();
                                    loginViewModel.loginWithFbOrGplus(GOOGLE_LOGIN, g_name, g_email, google_id,
                                            user.getPhotoUrl().toString(), AppConstants.device_type, getDeviceToken());

                                } else {
                                    // If sign in fails, display a message to the user.
                                    Log.w(TAG, "signInWith_plus:failure", task.getException());

                                }
                            }
                        });
            }
        });
    }

    public void logOutGoogle() {
//        FirebaseAuth.getInstance().signOut();
        mGoogleSignInClient.signOut().addOnCompleteListener(LoginActivity.this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
    }
    /* Google End */


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!aBoolean_check_for_social_login) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else {
            if (requestCode == RC_SIGN_IN && resultCode==RESULT_OK) {

                showLoading();
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    firebaseAuthWithGoogle(account);
                } catch (ApiException e) {
                    // Google Sign In failed, update UI appropriately
                    Log.w(TAG, "Google sign in failed", e);
                }
            }
        }
    }

    private void getHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    public void hashFromSHA1(String sha1) {
        String[] arr = sha1.split(":");
        byte[] byteArr = new  byte[arr.length];

        for (int i = 0; i< arr.length; i++) {
            byteArr[i] = Integer.decode("0x" + arr[i]).byteValue();
        }

        Log.e("hash_released : ", Base64.encodeToString(byteArr, Base64.NO_WRAP));
    }

    private String getDeviceToken(){

        String device_token = SharedPrefManager.getInstance(LoginActivity.this).getDeviceToken();
        Log.d("check_fcm_device_token",": "+device_token);
        return device_token;
    }

}
