package com.admin.happyhours.ui.main.notification;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.NotificationListingResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class NotificationViewModel extends BaseViewModel<NotificationNavigator> {

    private static final String TAG = "NotificationViewModel";

    public NotificationViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }


    void getNotificationListing(int limit, int page,final boolean check) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("check_uid", ": " + responseUser.getId());

            final boolean[] b_hit_for_clearing_badge_count = {false};

            Disposable disposable = ApplicationClass.getRetrofitService()
                    .notificationListing(responseUser.getId(), limit, page)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<NotificationListingResponse>() {
                        @Override
                        public void accept(NotificationListingResponse response) throws Exception {
                            if (response != null) {

                                Log.d(TAG, "_check_response: " + new Gson().toJson(response));

                                getNavigator().onSuccessAfterGettingListing(response);

                                if (response.getStatus() != null && response.getStatus().trim().toLowerCase().equals("success")) {

                                    if (!b_hit_for_clearing_badge_count[0]){
                                        b_hit_for_clearing_badge_count[0] = true;
                                        clearNotificationBadgeCount();
                                    }

                                }else {
                                    if (!check) {
                                        getNavigator().noData();
                                    } else {
                                        getNavigator().noload();
                                    }

                                   // getNavigator().noData();
                                }

                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d(TAG, "_check_response: " + ": " + throwable.getMessage());
                            getNavigator().onError(throwable);

                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }

    private void clearNotificationBadgeCount() {
        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("check_uid", ": " + responseUser.getId());

            Disposable disposable = ApplicationClass.getRetrofitService()
                    .clearNotificationBadgeCount(responseUser.getId())
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<CommonSimpleResponse>() {
                        @Override
                        public void accept(CommonSimpleResponse response) throws Exception {
                            if (response != null) {

                                Log.d(TAG, "_check_response: " + new Gson().toJson(response));

                                getNavigator().onSuccessAfterClearBadgeCount(response);

                                if (response.getStatus() != null && response.getStatus().trim().toLowerCase().equals("success")) {

                                }

                            } else {
                                Log.d(TAG, "_check_response: " + ": null response");
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d(TAG, "_check_response: " + ": " + throwable.getMessage());
                            getNavigator().onError(throwable);

                        }
                    });

            getCompositeDisposable().add(disposable);
        }
    }


}
