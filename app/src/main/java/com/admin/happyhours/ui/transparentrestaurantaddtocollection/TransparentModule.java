package com.admin.happyhours.ui.transparentrestaurantaddtocollection;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class TransparentModule {


    @Provides
    TransparentViewModel provideTransparentViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new TransparentViewModel(dataManager, schedulerProvider);
    }


}
