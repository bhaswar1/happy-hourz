package com.admin.happyhours.ui.resturantlocation;

import com.admin.happyhours.data.model.api.response.NormalLoginResponse;

public interface RasturantLocationNavigator {

    void onSuccess(NormalLoginResponse normalLoginResponse);

    void onError(Throwable throwable);
}
