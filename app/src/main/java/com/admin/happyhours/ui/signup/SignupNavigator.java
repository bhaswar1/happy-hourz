package com.admin.happyhours.ui.signup;

import com.admin.happyhours.data.model.api.response.NormalLoginResponse;

public interface SignupNavigator {

    void onSuccess(NormalLoginResponse normalLoginResponse);

    void onError(Throwable throwable);
}
