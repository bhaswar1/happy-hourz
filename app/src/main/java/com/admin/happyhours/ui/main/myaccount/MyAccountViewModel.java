package com.admin.happyhours.ui.main.myaccount;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.AccountResponse;
import com.admin.happyhours.data.model.api.response.CollectionCreateResponse;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.FevouriteListData;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.NotificationListingResponse;
import com.admin.happyhours.data.model.api.response.ProfileImageUpdateResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import java.io.File;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MyAccountViewModel extends BaseViewModel<MyAccountNavigator> {


    public MyAccountViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
    void setAcountDetail(String userId) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

           // Log.d("check_uid", ": " + userId);
          //  Log.d("check_limit", ": " + limit);
            //   Log.d("page", ": " + mode);
            // Log.d("check_latitude", ": " + lattitude);
            //   Log.d("check_longitude", ": " + longitude);

            Disposable disposable = ApplicationClass.getRetrofitService()
                    .accountDetail(userId)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<AccountResponse>() {
                        @Override
                        public void accept(AccountResponse response) throws Exception {
                            if(response.getStatus().equals("success")){
                                getNavigator().onSuccess(response);
                            }
                            Log.d("account","--"+new Gson().toJson(response)+response.getMessage());

                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response", ": " + throwable.getMessage());
                            // getNavigator().onError(throwable);
                        }
                    });

//
            getCompositeDisposable().add(disposable);

        }
    }
    public void updateCollection(String userid, File collectionimage
                               ) {

        MultipartBody.Part body = null;
        if (collectionimage != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), collectionimage);
            body = MultipartBody.Part.createFormData("profileimage", collectionimage.getName(), reqFile);
        }
        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), userid);
      //  RequestBody collec_name = RequestBody.create(MediaType.parse("text/plain"), collection_name);
       // RequestBody TYPE = RequestBody.create(MediaType.parse("text/plain"), type);
      //  RequestBody COLLECTION_ID = RequestBody.create(MediaType.parse("text/plain"), collection_id);

        Disposable disposable = ApplicationClass.getRetrofitService()
                .updateProfileImage(user_id, body)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<ProfileImageUpdateResponse>() {
                    @Override
                    public void accept(ProfileImageUpdateResponse response) throws Exception {
                        if (response != null && response.getStatus().toLowerCase().equals("success")) {

                            getNavigator().success(response);

                        } else {
                            Log.d("check_response_update", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response_update", ": " + throwable.getMessage());


                    }
                });

        getCompositeDisposable().add(disposable);
    }
    void updateProfile(String uid,String name,String address,String lattitude ,String longitude,String dob,String gender,String phone ) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("uid", ": " + uid);
            Log.d("lattitude", ": " + lattitude);
            Log.d("longitude", ": " + longitude);
            Log.d("name", ": " + name);
            Log.d("dob", ": " + dob);

            Disposable disposable = ApplicationClass.getRetrofitService()
                    .updateProfileData(uid,address,getDataManager().getLatitude(),getDataManager().getLongitude(),name,dob,gender,phone)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<AccountResponse>() {
                        @Override
                        public void accept(AccountResponse response) throws Exception {
                            if (response != null && response.getStatus().toLowerCase().equals("success")) {
                            getNavigator().updateProfileSuccess(response);


                            } else {
                                if(response.getMessage().equals("Phone number already exists")){
                                    getNavigator().phoneExist();
                                }
                                getNavigator().noData();
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response","---"+throwable.toString());
                          //  getNavigator().onError(throwable);

                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }



}
