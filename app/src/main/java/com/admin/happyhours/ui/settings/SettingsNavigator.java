package com.admin.happyhours.ui.settings;

import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;

import retrofit2.Response;

public interface SettingsNavigator {
    void success(String push);

    void updatePushSuccess(CommonSimpleResponse response);

    void onError(Throwable throwable);

    void updatePasswordSuccess(CommonSimpleResponse response);
}
