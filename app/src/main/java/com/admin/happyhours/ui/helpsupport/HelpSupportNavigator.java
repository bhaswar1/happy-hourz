package com.admin.happyhours.ui.helpsupport;

import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;

public interface HelpSupportNavigator {
    void success(CommonSimpleResponse response);

    void error();

    void onError(Throwable throwable);
}
