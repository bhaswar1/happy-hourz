package com.admin.happyhours.ui.claim;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.databinding.ActivityBusinessClaimBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.ui.nointernet.NoInternetActivity;
import com.admin.happyhours.utils.AppConstants;

import javax.inject.Inject;

public class BusinessClaimActivity extends BaseActivity<ActivityBusinessClaimBinding, BusinessClaimViewModel> implements BusinessClaimNavigator {


    ActivityBusinessClaimBinding activityBusinessClaimBinding;
    @Inject
    BusinessClaimViewModel businessClaimViewModel;
   private String password;
   String restaurantid;
    AlertDialog alertDialog;
    String newpass;

    public static Intent newIntent(Context context) {
        return new Intent(context, BusinessClaimActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_business_claim;
    }

    @Override
    public BusinessClaimViewModel getViewModel() {
        return businessClaimViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBusinessClaimBinding = getViewDataBinding();
        businessClaimViewModel.setNavigator(this);
        setStatusBarHidden(BusinessClaimActivity.this);
        Toolbar mtoolbar=activityBusinessClaimBinding.toolbar;
        setSupportActionBar(mtoolbar);
        mtoolbar.setNavigationIcon(R.drawable.back_arrow_black);
        mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        restaurantid=getIntent().getStringExtra("restaurantid");
        setWebView();



    //    WebView wv=new WebView(this);

    }

    private void setWebView() {
      final   WebView mWebView=activityBusinessClaimBinding.webView;

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                //Required functionality here
                return super.onJsAlert(view, url, message, result);
            }
        });

        activityBusinessClaimBinding.webView.setWebViewClient(new WebViewClient());
      //  activityBusinessClaimBinding.webView.getSettings().setJavaScriptEnabled(true);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

            String Url;
        Url= AppConstants.BASE_URL+"app/addbusiness/add/"+restaurantid+"/1";

            if (isNetworkConnected())
            {
                showLoading();
                Log.d("Url", "" + Url);
                activityBusinessClaimBinding.webView.loadUrl(Url);

            }else
            {

                startActivity(new Intent(BusinessClaimActivity.this, NoInternetActivity.class));
               // Toast.makeText(AddressWebviewActivity.this, getString(R.string.network_error),
                     //   Toast.LENGTH_LONG).show();
            }
//                            activityAddressWebivewBinding.webviewPhone.loadUrl(Url);

            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    System.out.println("hello");
                    Log.d("hello", ""+url);
                    return true;
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                  //  showLoading();
                    mWebView.loadUrl("javascript:HtmlViewer.showHTML" +
                            "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");


                    super.onPageStarted(view, url, favicon);
                }

                @Override
                public void onPageFinished(WebView view, String url) {


                    hideLoading();
                }
            });

//        mWebView.loadUrl("https://www.boomn.net/paypal-webview/0.01");

        }


    @Override
    public void success(String push) {

    }

    @Override
    public void updatePushSuccess(CommonSimpleResponse response) {


    }

    @Override
    public void onError(Throwable throwable) {
hideLoading();
    }

    @Override
    public void updatePasswordSuccess(CommonSimpleResponse response) {
    }
}
