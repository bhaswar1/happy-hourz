package com.admin.happyhours.ui.main.search;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.SearchDataResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class SearchViewModel extends BaseViewModel<SearchNavigator> {


    public SearchViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }


    public void searchDetail(String uid, String type,String search,String limit,String page,String timezone) {
        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("userid",uid);
            Log.e("type",type);
            Log.e("search",search);
            Log.e("limit",limit);
            Log.e("page",page);
            Log.e("timezone",timezone);


            Disposable disposable = ApplicationClass.getRetrofitService()
                    .searchList(uid,search,type,limit,page)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<SearchDataResponse>() {
                        @Override
                        public void accept(SearchDataResponse response) throws Exception {
                            if (response != null && response.getStatus().toLowerCase().equals("success") ) {
                                if(response.getStatus().toLowerCase().equals("success"))

                                   getNavigator().searchSuccess(response);
                            }else{
                                getNavigator().noData();
                            }




                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response","---"+throwable.toString());
                            getNavigator().onError(throwable);

                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }
    public void searchHistoryDetail(String uid, String type) {
        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("uid", ": " + uid);


            Disposable disposable = ApplicationClass.getRetrofitService()
                    .searchHistoryList(uid,type)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<SearchDataResponse>() {
                        @Override
                        public void accept(SearchDataResponse response) throws Exception {
                            if (response != null && response.getStatus().toLowerCase().equals("success") ) {
                                if(response.getStatus().toLowerCase().equals("success"))

                                  //  getNavigator().searchSuccess(response);
                                    getNavigator().searchHistorySuccess(response);
                            }else{
                                getNavigator().noData();
                            }




                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response","---"+throwable.toString());
                            getNavigator().onError(throwable);

                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }
    public void clearSearchHistoryDetail(String uid) {
        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("uid", ": " + uid);


            Disposable disposable = ApplicationClass.getRetrofitService()
                    .clearSearchHistoryList(uid)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<CommonSimpleResponse>() {
                        @Override
                        public void accept(CommonSimpleResponse response) throws Exception {
                            if (response != null && response.getStatus().toLowerCase().equals("success") ) {
                                if(response.getStatus().toLowerCase().equals("success"))
                                    getNavigator().noData();
                            }else{

                            }




                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response","---"+throwable.toString());
                            getNavigator().onError(throwable);

                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }

}
