package com.admin.happyhours.ui.nointernet;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;

public class NoInternetViewModel extends BaseViewModel<NoInternetNavigator> {



    public NoInternetViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
}
