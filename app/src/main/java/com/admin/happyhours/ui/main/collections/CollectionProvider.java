package com.admin.happyhours.ui.main.collections;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class CollectionProvider {

    @ContributesAndroidInjector(modules = CollectionModule.class)
    abstract CollectionFragment provideCollectionFragmentFactory();


}
