package com.admin.happyhours.ui.main.events;


import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.EventListingResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.databinding.FragmentEventBinding;
import com.admin.happyhours.datatypecommon.EventBusForEventListingLike;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.admin.happyhours.ui.base.BaseFragment;
import com.admin.happyhours.ui.main.events.recycleradapter.EventAdapter;
import com.admin.happyhours.utils.AppConstants;
import com.admin.happyhours.utils.GlobalBus;
import com.admin.happyhours.utils.SpacesItemDecoration;
import com.admin.happyhours.utils.ViewUtils;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import org.greenrobot.eventbus.Subscribe;

import java.util.LinkedList;

import javax.inject.Inject;


public class EventFragment extends BaseFragment<FragmentEventBinding, EventViewModel> implements EventNavigator,AdListener {

    private @Nullable AdView bannerAdView;

    FragmentEventBinding fragmentEventBinding;
    EventViewModel eventViewModel;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    EventAdapter eventAdapter;
    private int pagination = 1;
    private boolean hasLoadMore = false;
SupportMapFragment mapFragment;

    public FirebaseAnalytics mFirebaseAnalytics;
    public static EventFragment newInstance() {
        Bundle args = new Bundle();
        EventFragment fragment = new EventFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_event;
    }

    @Override
    public EventViewModel getViewModel() {
        eventViewModel = ViewModelProviders.of(this, mViewModelFactory).get(EventViewModel.class);
        return eventViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventViewModel.setNavigator(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        GlobalBus.getEventBus().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        GlobalBus.getEventBus().register(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentEventBinding = getViewDataBinding();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        setupRecyclerview();

        reloadOrLoadData();

      //setAdview();
    }

    private void setAdview() {
        boolean isTablet=false;
        bannerAdView = new AdView(this.getActivity(), "IMG_16_9_APP_INSTALL#",
                isTablet ? AdSize.BANNER_HEIGHT_90 : AdSize.BANNER_HEIGHT_50);

        // Reposition the ad and add it to the view hierarchy.
        fragmentEventBinding.constraintLayout.addView(bannerAdView);

        // Set a listener to get notified on changes or when the user interact with the ad.
//        bannerAdView.setAdListener(this);

        // Initiate a request to load an ad.
        bannerAdView.loadAd();
        bannerAdView.loadAd(bannerAdView.buildLoadAdConfig().withAdListener(this).build());

        fragmentEventBinding.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragmentEventBinding.constraintLayout.removeView(bannerAdView);
            }
        });
    }


    private void reloadOrLoadData(){
        if (eventAdapter!=null){
            eventAdapter.clearData();
            hasLoadMore = false;
            pagination = 1;
//            eventAdapter.setData(new EventListingResponse.Details());
            loadData(false);
        }
    }

    private void loadData(boolean check){
        if (eventViewModel.getDataManager().getCurrentUserInfo() != null) {
            getBaseActivity().showLoading();
            NormalLoginResponse.Details details = new Gson().fromJson(eventViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);

            Bundle bundle = new Bundle();
            bundle.putString("userName",details.getUserName());
            bundle.putString("userid",details.getId());
//            Event listing visited
//            bundle.putString("resturantid",details.getRestaurentId());
//            bundle.putString("collectionName",collectionName);
//            bundle.putString("collectionId",collectionid);
//        bundle.putString("resturantrating",passed_detail_data.getRating());
//            bundle.putString("resturantName",passed_detail_data.getRestaurentName());

            mFirebaseAnalytics.logEvent("event_listing_visited_android", bundle);




            eventViewModel.fetchEventListing(details.getId(), pagination, AppConstants.LISTING_FETCH_LIMIT,check);
        }
    }

    private void setupRecyclerview() {
        fragmentEventBinding.recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        fragmentEventBinding.recyclerView.setLayoutManager(linearLayoutManager);
        fragmentEventBinding.recyclerView.addItemDecoration(new SpacesItemDecoration(ViewUtils.dpToPx(2)));
        LinkedList<EventListingResponse.Details> details = new LinkedList<>();

        eventAdapter = new EventAdapter(getActivity(), details, fragmentEventBinding.recyclerView, linearLayoutManager);

        fragmentEventBinding.recyclerView.setAdapter(eventAdapter);

        eventAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
//                Log.d("checking_steps_loadmore",": "+(++check));
                Log.d("check_bools", ": " + hasLoadMore);
                if (hasLoadMore) {
                    getBaseActivity().showLoading();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadData(true);
                        }
                    }, 1000);
                }
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==7){
            if (resultCode==8){
                int position = data.getExtras().getInt("item_position");
                boolean isChecked = data.getExtras().getBoolean("checkeFavourite");

                if (eventAdapter!=null){
                    if (isChecked) {
                        eventAdapter.changeValueOfParticularPosition(position, 1);
                    }else {
                        eventAdapter.changeValueOfParticularPosition(position, 0);
                    }
                }

            }
        }
    }

    @Override
    public void onSuccess(EventListingResponse eventListingResponse) {
        getBaseActivity().hideLoading();
        if (eventListingResponse != null) {
            if (eventListingResponse.getDetails() != null && eventListingResponse.getDetails().size()>0) {
                if (eventListingResponse.getDetails().size() == AppConstants.LISTING_FETCH_LIMIT) {
                    pagination = pagination + 1;
                    hasLoadMore = true;
                } else {
                    hasLoadMore = false;
                }

                eventAdapter.setAllData(eventListingResponse.getDetails());
                eventAdapter.setLoaded();
//                collectionAdapter.setData(collectionListingResponse.getDetails());
            } else {
                hasLoadMore = false;
            }
        }
        Log.d("check_response_frag", ": " + hasLoadMore);
    }

    @Override
    public void onError(Throwable throwable) {
        getBaseActivity().hideLoading();
//        collectionAdapter.showLoading(false);
        Log.d("check_error", ": " + throwable.getMessage());
    }

    @Override
    public void likeUnlike(String check_for_button, int position) {
        getBaseActivity().hideLoading();
        eventAdapter.changeRowDataAfteLikeOrLike(check_for_button, position);
    }

    @Override
    public void noData() {
        fragmentEventBinding.nodataText.setVisibility(View.VISIBLE);
        fragmentEventBinding.recyclerView.setVisibility(View.GONE);
                getBaseActivity().hideLoading();
    }

    @Subscribe
    public void onEvent(EventBusForEventListingLike eventBusForEventListingLike) {
        if (eventBusForEventListingLike.getButton_tyoe().equals(AppConstants.adapter_img_like_in_event_listing)) {
            NormalLoginResponse.Details responseUser = new Gson().fromJson(eventViewModel.getDataManager().getCurrentUserInfo(),
                    NormalLoginResponse.Details.class);

            Bundle bundle = new Bundle();
            bundle.putString("userName",responseUser.getUserName());
            bundle.putString("userid",responseUser.getId());
            bundle.putString("evenName",eventBusForEventListingLike.getDetail().getEventname());
            bundle.putString("evenAddress",eventBusForEventListingLike.getDetail().getAddress());
            bundle.putString("eventId",eventBusForEventListingLike.getDetail().getEventid());
//            Event listing visited
//            bundle.putString("resturantid",details.getRestaurentId());
//            bundle.putString("collectionName",collectionName);
//            bundle.putString("collectionId",collectionid);
//        bundle.putString("resturantrating",passed_detail_data.getRating());
//            bundle.putString("resturantName",passed_detail_data.getRestaurentName());

            mFirebaseAnalytics.logEvent("event_added_to_favourites", bundle);


            getBaseActivity().showLoading();
            eventViewModel.apiHitLikeUnlike(eventBusForEventListingLike.getButton_tyoe(),
                    eventBusForEventListingLike.getPosition(), eventBusForEventListingLike.getDetail());
        }
    }

    @Override
    public void onError(Ad ad, AdError adError) {

    }

    @Override
    public void onAdLoaded(Ad ad) {

    }

    @Override
    public void onAdClicked(Ad ad) {

    }

    @Override
    public void onLoggingImpression(Ad ad) {

    }
}
