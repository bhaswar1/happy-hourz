package com.admin.happyhours.ui.main.search;

import androidx.lifecycle.ViewModelProvider;

import com.admin.happyhours.ViewModelProviderFactory;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class SearchModule {


    @Provides
    SearchViewModel searchViewModel(DataManager dataManager,
                                    SchedulerProvider schedulerProvider) {
        return new SearchViewModel(dataManager, schedulerProvider);
    }


    @Provides
    ViewModelProvider.Factory provideSearchViewModel(SearchViewModel profilePageViewModel) {
        return new ViewModelProviderFactory<>(profilePageViewModel);
    }

}
