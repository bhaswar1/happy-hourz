package com.admin.happyhours.ui.main.search;


import android.animation.LayoutTransition;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import com.admin.happyhours.BR;
import com.google.android.material.tabs.TabLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.SearchDataResponse;
import com.admin.happyhours.data.model.api.response.SearchDetail;
import com.admin.happyhours.databinding.FragmentSearchBinding;
import com.admin.happyhours.ui.base.BaseFragment;
import com.admin.happyhours.ui.eventdetails.EventDetailActivity;
import com.admin.happyhours.ui.main.HomeActivity;
import com.admin.happyhours.ui.resturantdeatails.ResturantDetailsActivity;
import com.admin.happyhours.utils.ViewUtils;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;

import static android.widget.LinearLayout.VERTICAL;


public class SearchFragment extends BaseFragment<FragmentSearchBinding, SearchViewModel> implements SearchNavigator {

    FragmentSearchBinding fragmentSearchBinding;
    SearchViewModel searchViewModel;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;
SearchListAdapter searchListAdapter;
    TabLayout tabLayout;
    Boolean specialSearchClick=false;
    int i=1;
    TextView tv ;
    public static SearchFragment newInstance() {
        Bundle args = new Bundle();
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_search;
    }

    @Override
    public SearchViewModel getViewModel() {
        searchViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SearchViewModel.class);
        return searchViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchViewModel.setNavigator(this);
    }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentSearchBinding = getViewDataBinding();

        animationOfSearchEdittextExpandAndCollapse();
         tabLayout=fragmentSearchBinding.tablayout;
        //initCollapsingToolbar();

        tabLayout.addTab(tabLayout.newTab().setText("Specials"));
        tabLayout.addTab(tabLayout.newTab().setText("Events"));


//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//            statusButton.backgroundTintList = ColorStateList.valueOf(MY_COLOR)
//
//        else  statusButton.setBackgroundColor(MY_COLOR)
//        for (int i = 0; i < tabLayout.getTabCount(); i++) {
//            //noinspection ConstantConditions
//        //    tv.setTypeface(Typeface);
//            tv = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab,null);
//
//
//            tabLayout.getTabAt(i).setCustomView(tv);
//
//        }

      //  tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FF0000"));
      //  tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
      //  tabLayout.setTabTextColors(Color.parseColor("#727272"), Color.parseColor("#ffffff"));
        setSearchRecycleView();
        fragmentSearchBinding.searchRecycle.setVisibility(View.VISIBLE);

            serachList();
            setTabSelect();
            clearSearchHistory();


            if (searchViewModel.getDataManager().getCurrentUserInfo() != null) {
                getBaseActivity().showLoading();
                NormalLoginResponse.Details details = new Gson().fromJson(searchViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                searchViewModel.searchHistoryDetail(details.getId(), "1");

        }
        searchListAdapter.setListener(new SearchListAdapter.ItemClickListener() {
    @Override
    public void onItemClick(SearchDetail detail, int position) {

        if(!specialSearchClick) {
            Log.d("clicked", "---" + detail.getName());
            fragmentSearchBinding.edittextSearch.setText(detail.getName().trim());
            fragmentSearchBinding.edittextSearch.requestFocus();
//            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.showSoftInput( fragmentSearchBinding.edittextSearch, InputMethodManager.SHOW_FORCED);

            fragmentSearchBinding.edittextSearch.setSelection(detail.getName().length());
            //  fragmentSearchBinding.edittextSearch.setClic
            // Toast.makeText(getActivity(),"clicked"+detail.getName(),Toast.LENGTH_LONG).show();
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(fragmentSearchBinding.layoutSearch);
            fragmentSearchBinding.edittextSearch.setLayoutParams(new ConstraintLayout.LayoutParams(0, 0));
            constraintSet.connect(fragmentSearchBinding.edittextSearch.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, ViewUtils.dpToPx(10));
            constraintSet.connect(fragmentSearchBinding.edittextSearch.getId(), ConstraintSet.END, fragmentSearchBinding.tvSubmit.getId(), ConstraintSet.START, ViewUtils.dpToPx(5));
            constraintSet.applyTo(fragmentSearchBinding.layoutSearch);
            fragmentSearchBinding.tvSubmit.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                fragmentSearchBinding.layoutSearch.getLayoutTransition()
                        .enableTransitionType(LayoutTransition.CHANGING);
            }



            tapSearch(detail.getName().trim());





        }else{
            if(tabLayout.getSelectedTabPosition()==0){
                getActivity().startActivityForResult(new Intent(getActivity(), ResturantDetailsActivity.class).

                                 putExtra("lattitude", detail.getLatitude()).
                                 putExtra("longitude", detail.getLongitude()).
                                putExtra("checklike","ok").
                                putExtra("item_position", position).
                                    putExtra("details",new Gson().toJson(detail)).
                                putExtra("resturantid", detail.getId()),7);
                               // putExtra("details", shareBody),9);

            }else if(tabLayout.getSelectedTabPosition()==1){

                        getActivity().startActivity(new Intent(getActivity(), EventDetailActivity.class).
                                putExtra("eventId",detail.getId()).
                                putExtra("favourite_status","0").
                                putExtra("position",Integer.toString(position)));



            }

        }
    }

            private void tapSearch(String search) {
                if(!search.isEmpty()){

                    if(tabLayout.getSelectedTabPosition()==0){
                        // if (search != null && search.length() != 0) {
                        specialSearchClick=true;
                        if (searchViewModel.getDataManager().getCurrentUserInfo() != null) {
                            getBaseActivity().showLoading();
                            NormalLoginResponse.Details details = new Gson().fromJson(searchViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                            searchViewModel.searchDetail(details.getId(), "1", search, "6", "1",getTimeZone());
                        }


                    }else if(tabLayout.getSelectedTabPosition()==1) {
                        //  if (search != null && search.length() != 0)

                        specialSearchClick = true;
                        if (searchViewModel.getDataManager().getCurrentUserInfo() != null) {
                            getBaseActivity().showLoading();
                            NormalLoginResponse.Details details = new Gson().fromJson(searchViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                            searchViewModel.searchDetail(details.getId(), "2", search, "6", "1",getTimeZone());
                        }

                    }


                }

            }
        });

    }

    private void clearSearchHistory() {
        fragmentSearchBinding.clearsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchViewModel.getDataManager().getCurrentUserInfo() != null) {
                    getBaseActivity().showLoading();
                    NormalLoginResponse.Details details = new Gson().fromJson(searchViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                    searchViewModel.clearSearchHistoryDetail(details.getId());

                }

            }
        });
    }

    private void setTabSelect() {

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition()==0) {
                    specialSearchClick=false;
              //      tv.setTextColor(getResources().getColor(R.color.default_line_indicator_selected_color));

                    if (searchViewModel.getDataManager().getCurrentUserInfo() != null) {
                        getBaseActivity().showLoading();
                        NormalLoginResponse.Details details = new Gson().fromJson(searchViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                        searchViewModel.searchHistoryDetail(details.getId(), "1");

                    }
                }else if(tab.getPosition()==1){
                  //  tv.setTextColor(getResources().getColor(R.color.default_line_indicator_selected_color));

                    specialSearchClick=false;
                    if (searchViewModel.getDataManager().getCurrentUserInfo() != null) {
                        getBaseActivity().showLoading();
                        NormalLoginResponse.Details details = new Gson().fromJson(searchViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                        searchViewModel.searchHistoryDetail(details.getId(), "2");

                    }

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {



            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void serachList() {
        fragmentSearchBinding.edittextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //performSearch();
                    String search = fragmentSearchBinding.edittextSearch.getText().toString().trim();
                    Log.d("search", "--" + search);
                    if(!search.isEmpty()){

                        if(tabLayout.getSelectedTabPosition()==0){
                           // if (search != null && search.length() != 0) {
                                specialSearchClick=true;
                                if (searchViewModel.getDataManager().getCurrentUserInfo() != null) {
                                    getBaseActivity().showLoading();
                                    NormalLoginResponse.Details details = new Gson().fromJson(searchViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                                    searchViewModel.searchDetail(details.getId(), "1", search, "6", "1",getTimeZone());
                                }


                        }else if(tabLayout.getSelectedTabPosition()==1) {
                          //  if (search != null && search.length() != 0)
                                specialSearchClick = true;
                                if (searchViewModel.getDataManager().getCurrentUserInfo() != null) {
                                    getBaseActivity().showLoading();
                                    NormalLoginResponse.Details details = new Gson().fromJson(searchViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                                    searchViewModel.searchDetail(details.getId(), "2", search, "6", "1",getTimeZone());
                                }

                        }


                        }
                    return true;
                }
                return false;
            }
        });







        fragmentSearchBinding.tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                fragmentSearchBinding.edittextSearch.setText(null);

//                String search = fragmentSearchBinding.edittextSearch.getText().toString();
//                if(tabLayout.getSelectedTabPosition()==0){
//                    if (search != null && search.length() != 0) {
//                        specialSearchClick=true;
//                        if (searchViewModel.getDataManager().getCurrentUserInfo() != null) {
//                            getBaseActivity().showLoading();
//                            NormalLoginResponse.Details details = new Gson().fromJson(searchViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
//
////            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
//                            searchViewModel.searchDetail(details.getId(), "1", search, "6", "1");
//                        }
//                    }
//
//                }else if(tabLayout.getSelectedTabPosition()==1){
//                    if (search != null && search.length() != 0) {
//                        specialSearchClick=true;
//                        if (searchViewModel.getDataManager().getCurrentUserInfo() != null) {
//                            getBaseActivity().showLoading();
//                            NormalLoginResponse.Details details = new Gson().fromJson(searchViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
//
////            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
//                            searchViewModel.searchDetail(details.getId(), "2", search, "6", "1");
//                        }
//                    }
//
//                }




            }
        });

    }

    private void setSearchRecycleView() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        fragmentSearchBinding.searchRecycle.setLayoutManager(linearLayoutManager);
        //  fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(new SpacesItemDecoration(ViewUtils.dpToPx(2)));
        LinkedList<SearchDetail> details = new LinkedList<>();
        searchListAdapter = new SearchListAdapter(getActivity(), details, fragmentSearchBinding.searchRecycle, linearLayoutManager);
         DividerItemDecoration decoration = new DividerItemDecoration(getBaseActivity().getApplicationContext(), VERTICAL);
         fragmentSearchBinding.searchRecycle.addItemDecoration(decoration);
       // fragmentSearchBinding.searchRecycle.setAdapter(fevouriteListAdapter);
fragmentSearchBinding.searchRecycle.setAdapter(searchListAdapter);
//        fevouriteListAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore() {
////                Log.d("checking_steps_loadmore",": "+(++check));
//                Log.d("check_bools", ": " + hasLoadMore);
//                if (hasLoadMore) {
//                    getBaseActivity().showLoading();
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            loadrestaurantData();
//                        }
//                    }, 1000);
//                }
//            }
//        });
    }


    private void animationOfSearchEdittextExpandAndCollapse(){
        fragmentSearchBinding.edittextSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ConstraintSet constraintSet = new ConstraintSet();
                    constraintSet.clone(fragmentSearchBinding.layoutSearch);
                    fragmentSearchBinding.edittextSearch.setLayoutParams(new ConstraintLayout.LayoutParams(0, 0));
                    constraintSet.connect(fragmentSearchBinding.edittextSearch.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, ViewUtils.dpToPx(10));
                    constraintSet.connect(fragmentSearchBinding.edittextSearch.getId(), ConstraintSet.END, fragmentSearchBinding.tvSubmit.getId(), ConstraintSet.START, ViewUtils.dpToPx(5));
                    constraintSet.applyTo(fragmentSearchBinding.layoutSearch);
                    fragmentSearchBinding.tvSubmit.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        fragmentSearchBinding.layoutSearch.getLayoutTransition()
                                .enableTransitionType(LayoutTransition.CHANGING);
                    }

                }
                return false;
            }
        });

        fragmentSearchBinding.parentLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    getBaseActivity().hideKeyboard();
                    if (TextUtils.isEmpty(fragmentSearchBinding.edittextSearch.getText().toString().trim())) {
                        ConstraintSet constraintSet = new ConstraintSet();
                        constraintSet.clone(fragmentSearchBinding.layoutSearch);
                        fragmentSearchBinding.edittextSearch.setLayoutParams(new ConstraintLayout.LayoutParams(0, 0));
                        constraintSet.connect(fragmentSearchBinding.edittextSearch.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, ViewUtils.dpToPx(10));
                        constraintSet.connect(fragmentSearchBinding.edittextSearch.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, ViewUtils.dpToPx(10));
                        constraintSet.applyTo(fragmentSearchBinding.layoutSearch);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            fragmentSearchBinding.layoutSearch.getLayoutTransition()
                                    .enableTransitionType(LayoutTransition.CHANGING);
                        }
                        fragmentSearchBinding.tvSubmit.setVisibility(View.GONE);
                    }
                }
                return false;
            }
        });
    }


    @Override
    public void searchSuccess(SearchDataResponse response) {
        getBaseActivity().hideLoading();
        getBaseActivity().hideKeyboard();
        fragmentSearchBinding.searchRecycle.setVisibility(View.VISIBLE);
        fragmentSearchBinding.noDataTextView.setVisibility(View.GONE);
        LinkedList<SearchDetail> searchDetails=new LinkedList<>();
        searchListAdapter.clearData();
        if(response.getDetails()!=null){
            searchDetails.addAll(response.getDetails());
            searchListAdapter.setAllData(searchDetails);
        }
        fragmentSearchBinding.clearsearch.setVisibility(View.VISIBLE);
        Log.d("tessst",""+response.getDetails().get(0).getName());
        //fragmentSearchBinding.edittextSearch.setText(response.getDetails().indexOf(response.getDetails().size()));
i=1;
    }

    @Override
    public void searchHistorySuccess(SearchDataResponse response) {
        getBaseActivity().hideLoading();

        fragmentSearchBinding.searchRecycle.setVisibility(View.VISIBLE);
        fragmentSearchBinding.noDataTextView.setVisibility(View.GONE);
        LinkedList<SearchDetail> searchDetails=new LinkedList<>();
        searchListAdapter.clearData();
        if(response.getDetails()!=null){
            searchDetails.addAll(response.getDetails());
            searchListAdapter.setAllData(searchDetails);
        }
        fragmentSearchBinding.clearsearch.setVisibility(View.VISIBLE);
        Log.d("tessst",""+response.getDetails().get(0).getName());

        if(i==1) {

            fragmentSearchBinding.edittextSearch.requestFocus();
            InputMethodManager inputMethodManager =
                    (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInputFromWindow(
                    fragmentSearchBinding.parentLayout.getApplicationWindowToken(),
                    InputMethodManager.SHOW_FORCED, 0);
        }
    i++;

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(fragmentSearchBinding.layoutSearch);
        fragmentSearchBinding.edittextSearch.setLayoutParams(new ConstraintLayout.LayoutParams(0, 0));
        constraintSet.connect(fragmentSearchBinding.edittextSearch.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, ViewUtils.dpToPx(10));
        constraintSet.connect(fragmentSearchBinding.edittextSearch.getId(), ConstraintSet.END, fragmentSearchBinding.tvSubmit.getId(), ConstraintSet.START, ViewUtils.dpToPx(5));
        constraintSet.applyTo(fragmentSearchBinding.layoutSearch);
        fragmentSearchBinding.tvSubmit.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            fragmentSearchBinding.layoutSearch.getLayoutTransition()
                    .enableTransitionType(LayoutTransition.CHANGING);
        }
//        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.showSoftInput( fragmentSearchBinding.edittextSearch, InputMethodManager.SHOW_FORCED);
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void noData() {
        i=1;
        getBaseActivity().hideKeyboard();
            getBaseActivity().hideLoading();
            fragmentSearchBinding.searchRecycle.setVisibility(View.GONE);
            fragmentSearchBinding.noDataTextView.setVisibility(View.VISIBLE);
            fragmentSearchBinding.clearsearch.setVisibility(View.GONE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==7){
            if (resultCode==8){
                int position = data.getExtras().getInt("item_position");
                boolean isChecked = data.getExtras().getBoolean("checkeFavourite");
                //  Toast.makeText(getActivity(),"inside this",Toast.LENGTH_LONG).show();
                boolean check_for_collection_open_from_details = data.getExtras().getBoolean("check_for_collection_open_from_details");
                final String restaurant_data = data.getExtras().getString("restaurant_data");
                if (check_for_collection_open_from_details){

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("insidehere","---"+"here2"+new Gson().toJson(restaurant_data));

                            ((HomeActivity)getActivity()).goFromSpecialToCollection(restaurant_data);
                        }
                    }, 200);
                }
            }
        }
    }

    public String getTimeZone (){
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"),
                Locale.getDefault());
        Date currentLocalTime = calendar.getTime();
        DateFormat date = new SimpleDateFormat("Z");
        String localTime = date.format(currentLocalTime);
        try {
            return URLEncoder.encode(localTime,"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }


}
