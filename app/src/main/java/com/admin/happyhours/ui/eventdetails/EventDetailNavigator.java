package com.admin.happyhours.ui.eventdetails;

import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.EventDetail;
import com.admin.happyhours.data.model.api.response.EventDetails;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;

public interface EventDetailNavigator {

    void onSuccess(EventDetails eventDetails);

    void onError(Throwable throwable);

    void likeUnlike(CommonSimpleResponse response);

    void noData();
}
