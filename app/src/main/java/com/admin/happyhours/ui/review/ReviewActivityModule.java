package com.admin.happyhours.ui.review;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class ReviewActivityModule {

    @Provides
    ReviewViewModel provideSplashViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new ReviewViewModel(dataManager, schedulerProvider);
    }

}
