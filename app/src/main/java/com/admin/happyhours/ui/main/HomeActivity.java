package com.admin.happyhours.ui.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import com.admin.happyhours.BR;
import com.admin.happyhours.data.model.api.response.ServiceDetail;
import com.admin.happyhours.data.model.api.response.ServiceResponse;
import com.admin.happyhours.ui.eventdetails.EventDetailActivity;
import com.admin.happyhours.ui.main.adapter.ServicesAdapter;
import com.admin.happyhours.ui.resturantdeatails.ResturantDetailsActivity;
import com.admin.happyhours.utils.AppUpdateChecker;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.happyhours.R;
import com.admin.happyhours.customview.ExpandableLayout;
import com.admin.happyhours.customview.BookTextView;
import com.admin.happyhours.customview.HeavyTextView;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.CusineDetail;
import com.admin.happyhours.data.model.api.response.CusineRespone;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.TagDetail;
import com.admin.happyhours.data.model.api.response.TagResponse;
import com.admin.happyhours.databinding.ActivityHomeBinding;
import com.admin.happyhours.datatypecommon.EventBusForPassingNotificationDataToMain;
import com.admin.happyhours.datatypecommon.NotificationDatatype;
import com.admin.happyhours.interfaces.HomeActivityEditSaveOptionMenuClick;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.ui.feedback.FeedbackActivity;
import com.admin.happyhours.ui.login.LoginActivity;
import com.admin.happyhours.ui.main.adapter.TagDrinksAdapter;
import com.admin.happyhours.ui.main.collections.CollectionFragment;
import com.admin.happyhours.ui.main.events.EventFragment;
import com.admin.happyhours.ui.main.favourites.FavouritesFragment;
import com.admin.happyhours.ui.main.myaccount.MyAccountFragment;
import com.admin.happyhours.ui.main.navigationdrawer.DrawerAdapter;
import com.admin.happyhours.ui.main.navigationdrawer.DrawerDatatype;
import com.admin.happyhours.ui.main.notification.NotificationFragment;
import com.admin.happyhours.ui.main.search.SearchFragment;
import com.admin.happyhours.ui.main.special.SpecialFragment;
import com.admin.happyhours.ui.privacytermsbusinesssignup.PrivacyTermsBusinessActivity;
import com.admin.happyhours.ui.settings.SettingsActivity;
import com.admin.happyhours.utils.GlobalBus;
import com.admin.happyhours.utils.SharedPrefManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.pdfviewer.PDFView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;

import static android.widget.LinearLayout.VERTICAL;

public class HomeActivity extends BaseActivity<ActivityHomeBinding, HomeViewModel> implements HomeNavigator, HasSupportFragmentInjector {

    private TabLayout tabLayout_for_filter;
    private HomeActivityEditSaveOptionMenuClick homeActivityEditSaveOptionMenuClick;
    NotificationDatatype notificationDatatype = null;
    private boolean aBoolean_for_notification_page = false;
    private boolean set_notficationCont = false;
    LoginActivity loginActivity = new LoginActivity();
    TagAdapter tagAdapter;
    TagDrinksAdapter tagDrinksAdapter;
    ServicesAdapter servicesAdapter;
    CusinsAdapter cusinsAdapter;
    public CardView filter_layout;
    public ConstraintLayout constraintLayout;
    private boolean background_check = false;
    int i = 0;
    int flag = 0;
    BadgeDrawable badge;
    private SharedPrefManager sharedPrefManager;
    int j = 0;
    public int count = 0;
    String tagDetails, tagDrinkDetails;
    String cusinesDetails;
    String serviceDetails;
    public ImageView imageView_filter;
    SharedPreferences.Editor editor;
    private boolean forground_check = false;
    String temp = null;
    public int distance_value = 50;
    public int deals = 0;
    boolean notification_flag = true;
    public SeekBar mSeekbar;
    public TextView tv_tags, foodTags, drinkTags, serviceTags;

    public ExpandableLayout expandable_tags, expandblefoodTags, expandbleDrinkTags, expandbleServiceTags;
    public TextView distance_tv;
    SharedPreferences sharedPref1;
    public boolean ischecked_tags = false;
    public String tag_id = null;
    //
    public TextView daily_tv;
    public TextView happy_tv;
    public TextView special_tv;
    Menu menuItem;
    MenuItem item_filter;
    boolean ischecked_daily_hour = false;
    boolean ischecked_happy_hour = false;
    boolean ischecked_special_hour = false;
    public NotificaionBadge notificaionBadge;
    //
    public ExpandableLayout expandable_cuisine;
    public FirebaseAnalytics mFirebaseAnalytics;
    boolean apply_click = true;
    private final String TAG = HomeActivity.this.getClass().getSimpleName();
    private DrawerAdapter drawerAdapter;
    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    ActivityHomeBinding activityHomeBinding;
    @Inject
    HomeViewModel homeViewModel;
    boolean saveButton;

    public static Intent newIntent(Context context) {
        return new Intent(context, HomeActivity.class);
    }

    public static Intent newIntent(Context context, String notification) {
        return new Intent(context, HomeActivity.class).putExtra("notification", notification);
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    public HomeViewModel getViewModel() {
        return homeViewModel;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GlobalBus.getEventBus().unregister(this);
    }

    public class NotificaionBadge extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("badgecount", "-----" + intent.getStringExtra("badge"));
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layout_container);

            if (!(fragment instanceof NotificationFragment)) {
                badge.setVisible(true);
                badge.setNumber(Integer.parseInt(intent.getStringExtra("badge")));

//                homeViewModel.setCusinsListing();
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityHomeBinding = getViewDataBinding();
        homeViewModel.setNavigator(this);
        homeViewModel.setTagListing();

        homeViewModel.setServiceListing();
        homeViewModel.setCusinsListing();
        notificaionBadge = new NotificaionBadge();
        badge = activityHomeBinding.navigation.getOrCreateBadge(R.id.navigation_favourites);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //sharepref of filter
        //  notification_flag=false;
        String notifcation = null;
        if (getIntent().hasExtra("notification"))
            notifcation = getIntent().getExtras().getString("notification");
        Log.d("test", "=-----" + new Gson().toJson(notifcation));

        if (notifcation != null) {
            Log.d("test", "=---" + new Gson().toJson(notifcation));
            notification_flag = false;
        }
        distance_tv = activityHomeBinding.getRoot().findViewById(R.id.textView_distance);
        tv_tags = activityHomeBinding.getRoot().findViewById(R.id.tv_tags);
        foodTags = activityHomeBinding.getRoot().findViewById(R.id.food_tagsTv);
        drinkTags = activityHomeBinding.getRoot().findViewById(R.id.drinkTags_tv);
        serviceTags = activityHomeBinding.getRoot().findViewById(R.id.serviceText);
        expandbleDrinkTags = activityHomeBinding.getRoot().findViewById(R.id.expandble_drink_tv);

        expandbleServiceTags = activityHomeBinding.getRoot().findViewById(R.id.expandable_layout_service);

        expandable_cuisine = activityHomeBinding.getRoot().findViewById(R.id.expandable_layout_cuisine);

        daily_tv = activityHomeBinding.getRoot().findViewById(R.id.daily_tv);
        happy_tv = activityHomeBinding.getRoot().findViewById(R.id.happy_tv);
        special_tv = activityHomeBinding.getRoot().findViewById(R.id.special_tv);

        mSeekbar = (SeekBar) activityHomeBinding.getRoot().findViewById(R.id.seekbar_distance);
        sharedPref1 = getSharedPreferences("MySharedPreference", Context.MODE_PRIVATE);
        editor = sharedPref1.edit();
        //expandble tag of filter
        expandblefoodTags = activityHomeBinding.getRoot().findViewById(R.id.expandble_food_tag);
        expandable_tags = activityHomeBinding.getRoot().findViewById(R.id.expandable_layout_tags);
        constraintLayout = activityHomeBinding.getRoot().findViewById(R.id.back_black_transparent_layout);
        filter_layout = activityHomeBinding.getRoot().findViewById(R.id.filter_menu_container_card_view);

        // This means user is already logged in, only then notification will be shown when any notification comes to device
        SharedPrefManager.getInstance(HomeActivity.this).saveBooleanForIsUserAlreadyLoggedIn(true);

        GlobalBus.getEventBus().register(this);

        setStatusBarHidden(HomeActivity.this);
        Log.d("onCreate", "onCreate");
//        BottomNavigationViewHelper.removeShiftMode(activityHomeBinding.navigation);
        activityHomeBinding.navigation.setItemIconTintList(null);

        homeViewModel.setNotificationBadge();

        sharedPrefManager = SharedPrefManager.getInstance(getApplicationContext());
        ;
        NotificationDatatype notificationDatatype = new Gson().fromJson(sharedPrefManager.getNotification(), NotificationDatatype.class);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layout_container);
        if (notificationDatatype != null) {
            if (!notificationDatatype.getBadgecount().isEmpty() && !(fragment instanceof NotificationFragment)) {
                Log.d("getBadgecount", "---" + notificationDatatype.getBadgecount());
                badge.setVisible(true);
                badge.setNumber(Integer.parseInt(notificationDatatype.getBadgecount()));
            } else {
                badge.setVisible(false);

            }
        } else {
            badge.setVisible(false);

        }

//        var badge = bottomNavigation.getOrCreateBadge(menuItemId)
        activityHomeBinding.navigation.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);

//        activityHomeBinding.navigation.enableAnimation(false);
//        activityHomeBinding.navigation.enableShiftingMode(false);
//        activityHomeBinding.navigation.enableItemShiftingMode(false);

        activityHomeBinding.navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        activityHomeBinding.navigation.setSelectedItemId(R.id.navigation_home);


//       activityHomeBinding.navigation.()
        drawerNavigationMenu();

        setDataAndSelectOptionInDrawerNavigation();

        filterMenuFunctionality();

        // imageView_filter.setVisibility(View.GONE);
        //gotoNotificationPage();
        //    Crashlytics.getInstance().crash();

//        configureToolbar();

        setClickOnimage();

        //throw new RuntimeException("Test Crash");

        goToDeepLinkPages();

        //Checking App Version..............
        AppUpdateChecker appUpdateChecker = new AppUpdateChecker(this); //pass the activity in constructure
        appUpdateChecker.checkForUpdate(false); //mannual check false here

    }

    private void goToDeepLinkPages(){
        if (getIntent().hasExtra("deepLinkId")){
            if (getIntent().getStringExtra("deepLinkType").equals("restaurant")){

                startActivity(new Intent(this, ResturantDetailsActivity.class).
                        putExtra("lattitude", getIntent().getStringExtra("deepLinkLat")).
                        putExtra("longitude", getIntent().getStringExtra("deepLinkLong")).
                        putExtra("item_position", 0).
                        putExtra("checklike", "notok").
                        putExtra("resturantid", getIntent().getStringExtra("deepLinkId")));

            }else if (getIntent().getStringExtra("deepLinkType").equals("event")){

                startActivity(new Intent(this, EventDetailActivity.class).
                        putExtra("eventId",getIntent().getStringExtra("deepLinkId")).
                        putExtra("favourite_status","0").
                        putExtra("position","0"));

            }
        }
    }

    private void setClickOnimage() {
        activityHomeBinding.userimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Menu menu = activityHomeBinding.navigation.getMenu();


                if (activityHomeBinding.drawerLayout.isDrawerVisible(GravityCompat.START)) {
                    activityHomeBinding.drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    activityHomeBinding.drawerLayout.openDrawer(GravityCompat.START);
                }
                checkFragmentInBackstackAndOpen(MyAccountFragment.newInstance());

            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        gotoNotificationPage(intent);
        set_notficationCont = true;
        //  notification_flag=false;
        Log.d("onCreate", "onnewintent");
        //item_filter=menuItem.findItem(R.id.apply_tv);

        // drawerAdapter.hidenotification(true);
        //  Toast.makeText(HomeActivity.this,"inside",Toast.LENGTH_LONG).show();

    }

    private void gotoNotificationPage(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            Log.d("check_noti_1", ": " + intent.getExtras().getString("notification"));


        }
        if (aBoolean_for_notification_page) {
//            Toast.makeText(this, "Redirect To Notification Page", Toast.LENGTH_SHORT).show();
            checkFragmentInBackstackAndOpen(NotificationFragment.newInstance());
            showSelectionOfBottomNavigationItem();
            Log.d("check_noti", ": " + notificationDatatype.getNotification());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    aBoolean_for_notification_page = false;
                }
            }, 500);
        }
    }

    /*private void gotoNotificationPage(){
        if (getIntent()!=null && getIntent().getExtras()!=null &&
                getIntent().getExtras().getString("notification")!=null){
            SharedPrefManager.getInstance(HomeActivity.this).saveBooleanForNotificationRedirection(true);
        }
        SharedPrefManager sharedPrefManager = SharedPrefManager.getInstance(HomeActivity.this);
        if (sharedPrefManager.getBooleanForNotificationRedirection()){
            Toast.makeText(this, "Redirect To Notification Page", Toast.LENGTH_SHORT).show();
        }
    }*/


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    count = 0;
                    if (flag == 1) {
                        Log.d("hii", "");
                        item_filter.setVisible(true);
                        item_filter.setIcon(R.drawable.filter);
                        isChecked = false;
                        apply_click = true;

                    }
                    if (!isHidden && apply_click) {
                        item_filter.setIcon(R.drawable.filter);
                        isChecked = false;
                        hidefilterMenu();
                    }
                    homeViewModel.setNotificationBadge();
                    ///  notification_flag=true;
                    editor.clear();
                    editor.commit();
                    checkFragmentInBackstackAndOpen(SpecialFragment.newInstance());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //     SharedPreferences.Editor editor = sharedPref1.edit();


                            showSelectionOfBottomNavigationItem();
                        }
                    }, 100);
                    return true;
                case R.id.navigation_collection:
                    item_filter.setVisible(false);
                    //isChecked=true;
                    homeViewModel.setNotificationBadge();
                    flag = 1;
                    checkFragmentInBackstackAndOpen(CollectionFragment.newInstance());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            showSelectionOfBottomNavigationItem();
                        }
                    }, 100);
                    return true;
                case R.id.navigation_search:
                    item_filter.setVisible(false);
                    //  isChecked=true;
                    flag = 1;
                    homeViewModel.setNotificationBadge();
                    checkFragmentInBackstackAndOpen(SearchFragment.newInstance());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            showSelectionOfBottomNavigationItem();
                        }
                    }, 100);
                    return true;
                case R.id.navigation_events:
                    item_filter.setVisible(false);
                    //   isChecked=true;
                    flag = 1;
                    homeViewModel.setNotificationBadge();
                    checkFragmentInBackstackAndOpen(EventFragment.newInstance());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showSelectionOfBottomNavigationItem();
                        }
                    }, 100);
                    return true;
                case R.id.navigation_favourites:
                    item_filter.setVisible(false);
                    //   isChecked=true;
                    flag = 1;


                    badge.setVisible(false);
                    checkFragmentInBackstackAndOpen(NotificationFragment.newInstance());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showSelectionOfBottomNavigationItem();
                        }
                    }, 100);
                    return true;
            }

            return false;
        }
    };

    private boolean check_for_close = false;

    @Override
    public void onBackPressed() {
        if (flag == 1) {
            item_filter.setVisible(true);
            item_filter.setIcon(R.drawable.filter);
            isChecked = false;
            hidefilterMenu();
        }
        if (activityHomeBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            activityHomeBinding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1 ||
                    getSupportFragmentManager().findFragmentById(R.id.layout_container) instanceof SpecialFragment) {
                if (check_for_close) {
                    finish();
                }
                check_for_close = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        check_for_close = false;
                    }
                }, 2000);
            } else {
                super.onBackPressed();
                showSelectionOfBottomNavigationItem();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showSelectionOfBottomNavigationItem();

        String notifcation = null;
        if (getIntent().hasExtra("notification"))
            notifcation = getIntent().getExtras().getString("notification");

        registerReceiver(notificaionBadge, new IntentFilter("TestAction1"));
        Log.d("onCreate", "onResume");


        if (notifcation != null) {
//            Toast.makeText(this, "Redirect To Notification Page", Toast.LENGTH_SHORT).show();

            try {
                // if(value!=null) {
                // notification_flag=false;
                // Toast.makeText(HomeActivity.this,"notfication-1",Toast.LENGTH_LONG).show();
                JSONObject jsonObject = new JSONObject(notifcation);

                String notificationid = jsonObject.getString("notification");


                Log.d("notificationid", "--" + notificationid);

                if (j == 0) {
                    temp = jsonObject.getString("notification");
                } else {
                    if (temp == jsonObject.getString("notification")) {
                        background_check = true;
                    } else {

                        temp = jsonObject.getString("notification");
                        j = 0;
                    }

                }


                //    }
            } catch (JSONException err) {
                Log.d("Error", err.toString());
            }
            if (!forground_check) {
                //   showHideFilterOptionMenu(false);

                checkFragmentInBackstackAndOpen(NotificationFragment.newInstance());
                showSelectionOfBottomNavigationItem();
//            Log.d("check_noti", ": " + notificationDatatype.getNotification());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        aBoolean_for_notification_page = false;
                        forground_check = true;
                    }
                }, 500);
            }
            j++;
        }
        //   String badge=getIntent().getExtras().getString("badge");

        // Log.d("badge", ": " + badge);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //bundle must contain all info sent in "data" field of the notification
            //   Log.d("notification",""+new Gson().toJson(bundle));
        }
        String value = null;
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {

                Log.d("field", "--" + key);
                if (key.equals("message")) {
                    value = getIntent().getExtras().getString(key);
                    break;

                }
            }

            Log.d("TAG", "Key: " + " Value:-- " + value);

            try {
                if (value != null) {
                    JSONObject jsonObject = new JSONObject(value);
                    //     notification_flag=false;

                    String notificationid = jsonObject.getString("notification");
                    ///  Toast.makeText(HomeActivity.this,"notfication-2",Toast.LENGTH_LONG).show();
                    notification_flag = false;

                    Log.d("notificationid", "--" + notificationid);

                    if (i == 0) {
                        temp = jsonObject.getString("notification");
                    } else {
                        if (temp == jsonObject.getString("notification")) {
                            background_check = true;
                        } else {
                            temp = jsonObject.getString("notification");
                            i = 0;
                        }

                    }


                }
            } catch (JSONException err) {
                Log.d("Error", err.toString());
            }
            if (value != null && !background_check) {
                //   showHideFilterOptionMenu(false);

                checkFragmentInBackstackAndOpen(NotificationFragment.newInstance());
                background_check = true;
//
                //   startActivity(new Intent(SplashActivity.this,HomeActivity.class).putExtra("notification",value));
                // firebaseMessagingService.sendNotification(value,remoteMessage);
            }
            i++;
        }

    }

    private void showSelectionOfBottomNavigationItem() {

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layout_container);

        // Uncheck all menu item
        Menu menu = activityHomeBinding.navigation.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            /*boolean isChecked = menuItem.getItemId() == item.getItemId();*/
            menuItem.setCheckable(false);
            menuItem.setChecked(false);
        }


        // Check only desired item and select the item and unselect other items
        if (fragment instanceof SpecialFragment) {
            menu.findItem(R.id.navigation_home).setChecked(true);
            menu.findItem(R.id.navigation_home).setIcon(R.drawable.specials_icon_selected);
            menu.findItem(R.id.navigation_collection).setIcon(R.drawable.collections_icon);
            menu.findItem(R.id.navigation_search).setIcon(R.drawable.search_icon);
            menu.findItem(R.id.navigation_events).setIcon(R.drawable.event_icon);
            menu.findItem(R.id.navigation_favourites).setIcon(R.drawable.notificatin_icon);
        } else if (fragment instanceof CollectionFragment) {
            //    menu.findItem(R.id.navigation_home).setChecked(false);
            Log.d("insidehere", "---" + "collection");

            //   activityHomeBinding.navigation.setSelectedItemId(R.id.navigation_collection);


            menu.findItem(R.id.navigation_collection).setChecked(true);
            menu.findItem(R.id.navigation_home).setIcon(R.drawable.special_logo);
            menu.findItem(R.id.navigation_collection).setIcon(R.drawable.collections_icon_selected);
            menu.findItem(R.id.navigation_search).setIcon(R.drawable.search_icon);
            menu.findItem(R.id.navigation_events).setIcon(R.drawable.event_icon);
            menu.findItem(R.id.navigation_favourites).setIcon(R.drawable.notificatin_icon);
            hidefilterMenu();
        } else if (fragment instanceof SearchFragment) {
            menu.findItem(R.id.navigation_search).setChecked(true);
            menu.findItem(R.id.navigation_home).setIcon(R.drawable.special_logo);
            menu.findItem(R.id.navigation_collection).setIcon(R.drawable.collections_icon);
            menu.findItem(R.id.navigation_search).setIcon(R.drawable.search_icon_selected);
            menu.findItem(R.id.navigation_events).setIcon(R.drawable.event_icon);
            menu.findItem(R.id.navigation_favourites).setIcon(R.drawable.notificatin_icon);

            hidefilterMenu();
        } else if (fragment instanceof EventFragment) {
            menu.findItem(R.id.navigation_events).setChecked(true);
            menu.findItem(R.id.navigation_home).setIcon(R.drawable.special_logo);
            menu.findItem(R.id.navigation_collection).setIcon(R.drawable.collections_icon);
            menu.findItem(R.id.navigation_search).setIcon(R.drawable.search_icon);
            menu.findItem(R.id.navigation_events).setIcon(R.drawable.event_icon_selected);
            menu.findItem(R.id.navigation_favourites).setIcon(R.drawable.notificatin_icon);
            hidefilterMenu();
        } else if (fragment instanceof NotificationFragment) {
            menu.findItem(R.id.navigation_favourites).setChecked(true);
            menu.findItem(R.id.navigation_home).setIcon(R.drawable.special_logo);
            menu.findItem(R.id.navigation_collection).setIcon(R.drawable.collections_icon);
            menu.findItem(R.id.navigation_search).setIcon(R.drawable.search_icon);
            menu.findItem(R.id.navigation_events).setIcon(R.drawable.event_icon);
            menu.findItem(R.id.navigation_favourites).setIcon(R.drawable.notification_icon_selected);
            badge.setVisible(false);
            hidefilterMenu();
        } else {

            menu.findItem(R.id.navigation_home).setIcon(R.drawable.special_logo);
            menu.findItem(R.id.navigation_collection).setIcon(R.drawable.collections_icon);
            menu.findItem(R.id.navigation_search).setIcon(R.drawable.search_icon);
            menu.findItem(R.id.navigation_events).setIcon(R.drawable.event_icon);
            menu.findItem(R.id.navigation_favourites).setIcon(R.drawable.notificatin_icon);
        }
        // For select or unselect the item in drawer navigation menu
        if (fragment instanceof MyAccountFragment) {
            drawerAdapter.setDraweritemPositionTobeActivated(0);
        } else if (fragment instanceof NotificationFragment) {
            // drawerAdapter.setDraweritemPositionTobeActivated(3);
        } else {
            drawerAdapter.setDraweritemPositionTobeActivated(-1);
        }
        showTextInToolbarRelativeToFragment(fragment);
    }

    private void hidefilterMenu() {
        isHidden = false;
        if (Build.VERSION.SDK_INT < 22) {
            showMenuBelowLollipop();
        } else {
            showMenu();
        }

//        filter_layout.setVisibility(View.GONE);
//        isHidden = true;
//        constraintLayout.setVisibility(View.GONE);
//        isChecked = false;
//        Glide.with(HomeActivity.this).load(getResources().getDrawable(R.drawable.filter)).into(imageView_filter);
    }


    private void checkFragmentInBackstackAndOpen(Fragment fragment) {
        String name_fragment_in_backstack = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        /*boolean fragmentPopped = manager.popBackStackImmediate(name_fragment_in_backstack, 0);

        if (!fragmentPopped && manager.findFragmentByTag(name_fragment_in_backstack) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.layout_container, fragment, name_fragment_in_backstack);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(name_fragment_in_backstack);
            ft.commit();
        }*/


//        Bundle bundle = new Bundle();
//        bundle.putString("edttext", "From Activity");
//                  SpecialFragment specialFragment=new SpecialFragment();
//                    specialFragment.setArguments(bundle);
        // SpecialFragment.newInstance().setArguments(bundle);
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.layout_container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(name_fragment_in_backstack);
        ft.commit();
        showTextInToolbarRelativeToFragment(fragment);
    }


    public void updateProfile(boolean bool) {
        Toolbar toolbar = activityHomeBinding.getRoot().findViewById(R.id.toolbar);
        CardView cardView = activityHomeBinding.getRoot().findViewById(R.id.appBarCardview);
        AppBarLayout appBarLayout = activityHomeBinding.getRoot().findViewById(R.id.appbar);
        //sudip
        View test1View = activityHomeBinding.getRoot().findViewById(R.id.include);
        ;

        if (bool) {
            toolbar.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
            appBarLayout.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
            cardView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));

            ConstraintLayout constraintLayout3 = (ConstraintLayout) findViewById(R.id.constraint);

            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(constraintLayout3);

            constraintSet.connect(test1View.getId(), ConstraintSet.TOP, constraintLayout3.getId(), ConstraintSet.TOP, 0);
            constraintSet.connect(test1View.getId(), ConstraintSet.LEFT, constraintLayout3.getId(), ConstraintSet.LEFT, 0);

            constraintSet.applyTo(constraintLayout3);
        } else {
            // Toast.makeText(HomeActivity.this,"hii",Toast.LENGTH_LONG).show();
            toolbar.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
            appBarLayout.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
            cardView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
            ConstraintLayout constraintLayout3 = (ConstraintLayout) findViewById(R.id.constraint);

//

            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(constraintLayout3);

            // constraintSet.connect(test1View.getId(), constraintLayout3.getId(), constraintLayout3.getId(), constraintLayout3.getId(), 0);
            // constraintSet.constrainDefaultHeight(test1View.getId(), 200);
            constraintSet.connect(test1View.getId(), ConstraintSet.TOP, cardView.getId(), ConstraintSet.BOTTOM, 0);
            constraintSet.connect(test1View.getId(), ConstraintSet.START, constraintLayout3.getId(), ConstraintSet.START, 0);

            constraintSet.applyTo(constraintLayout3);
        }

    }

    private void showTextInToolbarRelativeToFragment(Fragment fragment) {


        TextView tootbar_text = activityHomeBinding.getRoot().findViewById(R.id.toolbar_title_txt);
        ImageView toolbar_image = activityHomeBinding.getRoot().findViewById(R.id.title_image);
        //  updateProfile();
        // test2View.setVisibility(View.GONE);
        //ViewStub stub = (ViewStub) findViewById(R.id.layout_stub);

//        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layout_container);

        if (fragment instanceof SpecialFragment) {
            updateProfile(false);
            toolbar_image.setVisibility(View.VISIBLE);
            tootbar_text.setVisibility(View.GONE);
            tootbar_text.setText(getResources().getString(R.string.title_special));
            tootbar_text.setTextColor(getResources().getColor(android.R.color.black));
            myAccountEditSaveButton(false);
            showHideFilterOptionMenu(true);
        } else if (fragment instanceof CollectionFragment) {
            updateProfile(false);
            toolbar_image.setVisibility(View.VISIBLE);
            tootbar_text.setVisibility(View.GONE);

            tootbar_text.setText(getResources().getString(R.string.title_collections));
            tootbar_text.setTextColor(getResources().getColor(android.R.color.black));
            myAccountEditSaveButton(false);
            showHideFilterOptionMenu(false);
        } else if (fragment instanceof SearchFragment) {
            toolbar_image.setVisibility(View.VISIBLE);
            tootbar_text.setVisibility(View.GONE);

            updateProfile(false);
            tootbar_text.setText(getResources().getString(R.string.title_search));
            tootbar_text.setTextColor(getResources().getColor(android.R.color.black));
            myAccountEditSaveButton(false);
            showHideFilterOptionMenu(false);
        } else if (fragment instanceof EventFragment) {
            toolbar_image.setVisibility(View.VISIBLE);
            tootbar_text.setVisibility(View.GONE);

            updateProfile(false);
            // tootbar_text.setText(getResources().getString(R.string.title_events));
            tootbar_text.setTextColor(getResources().getColor(android.R.color.black));
            myAccountEditSaveButton(false);
            showHideFilterOptionMenu(false);


        } else if (fragment instanceof FavouritesFragment) {
            toolbar_image.setVisibility(View.GONE);
            tootbar_text.setVisibility(View.VISIBLE);
            tootbar_text.setText("Specials and Events");
            // tootbar_text.setTextColor(getResources().getColor(android.R.color.black));

            updateProfile(false);
            // tootbar_text.setText(getResources().getString(R.string.title_favourites));
            tootbar_text.setTextColor(getResources().getColor(android.R.color.black));
            myAccountEditSaveButton(false);
            showHideFilterOptionMenu(false);
        } else if (fragment instanceof MyAccountFragment) {
            item_filter.setVisible(false);
            flag = 1;
            toolbar_image.setVisibility(View.GONE);
            tootbar_text.setVisibility(View.VISIBLE);
            tootbar_text.setText(getResources().getString(R.string.my_account));
            tootbar_text.setTextColor(getResources().getColor(android.R.color.white));
            myAccountEditSaveButton(true);
            showHideFilterOptionMenu(false);
            updateProfile(true);

        } else if (fragment instanceof NotificationFragment) {
            // if(flag==1) {

            //   }
            tootbar_text.setVisibility(View.VISIBLE);

            toolbar_image.setVisibility(View.GONE);

            if (notification_flag) {
                if (count < 1) {
                    item_filter.setVisible(false);
                    flag = 1;
                    count = 0;
                }
            } else {
                count = count + 1;
                notification_flag = true;
            }
            //
            updateProfile(false);
            tootbar_text.setText(getResources().getString(R.string.notification));
            tootbar_text.setTextColor(getResources().getColor(android.R.color.black));
            myAccountEditSaveButton(false);
            showHideFilterOptionMenu(false);
        }
//        invalidateOptionsMenu();
        //  notification_flag=true;
    }


    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    /*private void configureToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(false);
    }*/

    public void goFromSpecialToCollection(String special_to_collection_restaurent_data) {
        Log.d("insidehere", "---" + "here");

        checkFragmentInBackstackAndOpen(CollectionFragment.newInstance(special_to_collection_restaurent_data));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showSelectionOfBottomNavigationItem();
            }
        }, 300);
    }


    private void drawerNavigationMenu() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(false);

        final ConstraintLayout constraintLayout = (ConstraintLayout) findViewById(R.id.parent_layout);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, activityHomeBinding.drawerLayout,
                toolbar, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);

                hideKeyboard();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
                hideKeyboard();
            }

            @SuppressLint("NewApi")
            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = (activityHomeBinding.navView.getWidth() * slideOffset);
                constraintLayout.setTranslationX(moveFactor);
            }
        };

        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.nav_toggle_icon, HomeActivity.this.getTheme());
        actionBarDrawerToggle.setHomeAsUpIndicator(drawable);
        activityHomeBinding.drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();


        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activityHomeBinding.drawerLayout.isDrawerVisible(GravityCompat.START)) {
                    activityHomeBinding.drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    activityHomeBinding.drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    private void setDataAndSelectOptionInDrawerNavigation() {
        updateNavigationDrawerprofile();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeActivity.this);
        activityHomeBinding.navigationDrawerRecyclerview.setLayoutManager(linearLayoutManager);
        activityHomeBinding.navigationDrawerRecyclerview.setHasFixedSize(true);
        SharedPrefManager sharedPrefManager = SharedPrefManager.getInstance(HomeActivity.this);
        NotificationDatatype notificationDatatype = new Gson().fromJson(sharedPrefManager.getNotification(), NotificationDatatype.class);
        LinkedList<DrawerDatatype> strings = new LinkedList<>();
        strings.add(new DrawerDatatype(getResources().getString(R.string.my_account), 0, 0));
        strings.add(new DrawerDatatype(getResources().getString(R.string.settings), 8, 0));

        strings.add(new DrawerDatatype(getResources().getString(R.string.feedback), 2, 0));
//        strings.add(new DrawerDatatype(getResources().getString(R.string.history), 3, 0));
//        if (notificationDatatype != null && notificationDatatype.getBadgecount() != null &&
//                !notificationDatatype.getBadgecount().equals("0")) {
//
//            Log.d("badge","==="+notificationDatatype.getBadgecount());
//            strings.add(new DrawerDatatype(getResources().getString(R.string.notification), 4, Integer.parseInt(notificationDatatype.getBadgecount())));
//        } else {
//            strings.add(new DrawerDatatype(getResources().getString(R.string.notification), 4, 0));
//        }
        strings.add(new DrawerDatatype(getResources().getString(R.string.help_support), 5, 0));
        strings.add(new DrawerDatatype(getResources().getString(R.string.privacy_policy), 1, 0));

//        strings.add(new DrawerDatatype(getResources().getString(R.string.report_a_problem), 6, 0));
        strings.add(new DrawerDatatype(getResources().getString(R.string.terms_of_use), 7, 0));
        strings.add(new DrawerDatatype(getResources().getString(R.string.sign_up_for_buisness_profile), 9, 0));
        strings.add(new DrawerDatatype(getResources().getString(R.string.log_out), 10, 0));
        drawerAdapter = new DrawerAdapter(HomeActivity.this, strings);
        activityHomeBinding.navigationDrawerRecyclerview.setAdapter(drawerAdapter);
        drawerAdapter.setonClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                // Uncheck all menu item
                Menu menu = activityHomeBinding.navigation.getMenu();
                for (int i = 0; i < menu.size(); i++) {
                    MenuItem menuItem = menu.getItem(i);
                    /*boolean isChecked = menuItem.getItemId() == item.getItemId();*/
                    menuItem.setCheckable(false);
                    menuItem.setChecked(false);
                }
                menu.findItem(R.id.navigation_home).setIcon(R.drawable.special_logo);
                menu.findItem(R.id.navigation_collection).setIcon(R.drawable.collections_icon);
                menu.findItem(R.id.navigation_search).setIcon(R.drawable.search_icon);
                menu.findItem(R.id.navigation_events).setIcon(R.drawable.event_icon);
                menu.findItem(R.id.navigation_favourites).setIcon(R.drawable.notification_icon_selected);


                if (activityHomeBinding.drawerLayout.isDrawerVisible(GravityCompat.START)) {
                    activityHomeBinding.drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    activityHomeBinding.drawerLayout.openDrawer(GravityCompat.START);
                }
                switch (position) {
                    case 0:
                        checkFragmentInBackstackAndOpen(MyAccountFragment.newInstance());
                        break;
                    case 1:

//                        File filen = new File(getCacheDir(), "privacypolicy.pdf");
//                        if (!filen.exists()) {
//
//                            try {
//                                InputStream asset = getAssets().open("privacypolicy.pdf");
//                                FileOutputStream output = null;
//                                output = new FileOutputStream(filen);
//                                final byte[] buffer = new byte[1024];
//                                int size;
//                                while ((size = asset.read(buffer)) != -1) {
//                                    output.write(buffer, 0, size);
//                                }
//                                asset.close();
//                                output.close();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//
//                        }
                        // Intent intent1 = new Intent(getApplicationContext(), HomeActivity.class);
//                        Intent intent1                                                                                                                           =new Intent();
//                        intent1.putExtra("EXTRA_SESSION_ID", "test");
                        //   HomeActivity homeActivity=HomeActivity.this;

                        // homeActivity.onNewIntent(intent1);
                        // HomeActivity.newIntent(HomeActivity.this,"kkk");

//                        PDFView.with(HomeActivity.this).fromfilepath(filen.getAbsolutePath(),"1"
//
//                        ).swipeHorizontal(false).
//
//                                start();

                        startActivity(SettingsActivity.newIntent(HomeActivity.this));


                        break;
                    case 2:
                        startActivity(FeedbackActivity.newIntent(HomeActivity.this));
                        break;
                    /*case 3:


                        break;*/

                    case 3:
                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        intent.setData(Uri.parse("mailto:"));
                        // intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"help@happyhourzapp.com"});
                        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@happyhourzapp.com"});
                        //   intent.putExtra(Intent.EXTRA_SUBJECT, "dmkmd");
                        // intent.putExtra(Intent.EXTRA_TEXT, "dmdm");
                        startActivity(intent);
//                        drawerAdapter.hidenotification(true);
//                        checkFragmentInBackstackAndOpen(NotificationFragment.newInstance());

                        break;
                    case 4:
                        File file = new File(getCacheDir(), "Terms_of_use.pdf");
                        if (!file.exists()) {

                            try {
                                InputStream asset = getAssets().open("Terms_of_use.pdf");
                                FileOutputStream output = null;
                                output = new FileOutputStream(file);
                                final byte[] buffer = new byte[1024];
                                int size;
                                while ((size = asset.read(buffer)) != -1) {
                                    output.write(buffer, 0, size);
                                }
                                asset.close();
                                output.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }


//                        PDFView.with(HomeActivity.this).fromfilepath(file.getAbsolutePath(),"0"
//
//                        ).swipeHorizontal(false).
//
//                                start();


                        startActivity(PrivacyTermsBusinessActivity.newIntent(HomeActivity.this, getResources().getString(R.string.privacy_policy)));

                        // startActivity(HelpSupportActivity.newIntent(HomeActivity.this));
//                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
//                        emailIntent.setType("text/plain");
//                        startActivity(emailIntent);

                        break;
                    /*case 6:

                        break;*/
                    case 5:
                        //  startActivity(PrivacyTermsBusinessActivity.newIntent(HomeActivity.this, getResources().getString(R.string.term_and_confition)));

                        startActivity(PrivacyTermsBusinessActivity.newIntent(HomeActivity.this, getResources().getString(R.string.term_and_confition)));

                        break;
                    case 6:
                        startActivity(PrivacyTermsBusinessActivity.newIntent(HomeActivity.this, getResources().getString(R.string.sign_up_for_buisness_profile)));


                        break;

                    case 7:
                        showDialogForLogout();
                        break;
                    case 8:

                        break;
                    default:
                }
            }
        });
    }

    public void updateNavigationDrawerprofile() {
        NormalLoginResponse.Details details = new Gson().fromJson(homeViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
        if (details != null) {
            if (details.getName() != null) {
                //  Toast.makeText(this, "succesfully"+details.getName(), Toast.LENGTH_SHORT).show();

                activityHomeBinding.tvName.setText(details.getName());
            }
            if (details.getEmail() != null) {
                //  activityHomeBinding.userEmail.setText(details.getEmail());
            }
            if (details.getProfileImage() != null) {
                //    Glide.with(HomeActivity.this).load(details.getProfileImage()).into(activityHomeBinding.userimage);
                Glide.with(HomeActivity.this).load(details.getProfileImage()).apply(RequestOptions.circleCropTransform().placeholder(R.drawable.profile_image).error(R.drawable.profile_image)).into(activityHomeBinding.userimage);

            }
        }
    }


    @Subscribe
    public void getNotification(EventBusForPassingNotificationDataToMain eventBusForPassingNotificationDataToMain) {
        if (eventBusForPassingNotificationDataToMain != null && eventBusForPassingNotificationDataToMain.getMessage() != null) {
            Log.d(TAG, " Notification Data : " + eventBusForPassingNotificationDataToMain.getMessage());
            notificationDatatype = new Gson().fromJson(eventBusForPassingNotificationDataToMain.
                    getMessage(), NotificationDatatype.class);
            if (drawerAdapter != null && notificationDatatype.getBadgecount() != null) {
                drawerAdapter.changeNotificationBadge(notificationDatatype.getBadgecount().trim(), true);
                //  sharedPrefManager.saveNotification("0");
                aBoolean_for_notification_page = true;

            }
            Log.d(TAG, " Notification Count : " + notificationDatatype.getBadgecount());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layout_container);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void myAccountEditSaveButton(boolean check_for_show_hide_this_button) {
        final BookTextView tv_my_account_edit_save = activityHomeBinding.getRoot().findViewById(R.id.toolbar_title_option_menu);
        if (check_for_show_hide_this_button) {
            tv_my_account_edit_save.setVisibility(View.VISIBLE);
            tv_my_account_edit_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optionMenuClick();
                }
            });
        } else {
            tv_my_account_edit_save.setVisibility(View.GONE);
        }
    }

    private void optionMenuClick() {
        final BookTextView tv_my_account_edit_save = activityHomeBinding.getRoot().findViewById(R.id.toolbar_title_option_menu);
        if (tv_my_account_edit_save.getText().toString().trim().equals(getResources().getString(R.string.edit))) {
//            Toast.makeText(HomeActivity.this, "Edit will be done", Toast.LENGTH_SHORT).show();
            tv_my_account_edit_save.setText(getResources().getString(R.string.save));
            if (homeActivityEditSaveOptionMenuClick != null) {
                homeActivityEditSaveOptionMenuClick.onEdit();
            }
        } else {
//            Toast.makeText(HomeActivity.this, "Save will be done", Toast.LENGTH_SHORT).show();
//            tv_my_account_edit_save.setText(getResources().getString(R.string.edit));
            if (homeActivityEditSaveOptionMenuClick != null) {
                homeActivityEditSaveOptionMenuClick.onSave();
            }
        }
    }

    public void changeOptionMenuTextToEditAgain() {
        BookTextView tv_my_account_edit_save = activityHomeBinding.getRoot().findViewById(R.id.toolbar_title_option_menu);
        tv_my_account_edit_save.setText(getResources().getString(R.string.edit));
    }

    boolean isChecked = false;

    public void showHideFilterOptionMenu(boolean check_for_filter_menu_show_hide) {
        imageView_filter = activityHomeBinding.getRoot().findViewById(R.id.toolbar_filter_menu);
        if (check_for_filter_menu_show_hide) {
            imageView_filter.setVisibility(View.GONE);
        } else {
            imageView_filter.setVisibility(View.GONE);
        }


        imageView_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChecked) {
                    isChecked = false;
                    //   if(tag_id!=null)
                    checkFragmentInBackstackAndOpen(SpecialFragment.newInstance());

                    editor.putInt("progress", distance_value);  // Pick a key here.
                    editor.putString("tag_id", tag_id);
                    editor.putString("tagDetails", tagDetails);
                    editor.putString("tagDrinkDetails", tagDrinkDetails);
                    editor.putInt("deals", deals);
                    editor.commit();
                    // tag_id=null;
                    Log.d("filled", "1" + isChecked);
                    // imageView_filter.setImageResource(R.drawable.filter);
                    //   Toast.makeText(HomeActivity.this, "clciked right", Toast.LENGTH_SHORT).show();
                    Glide.with(HomeActivity.this).load(getResources().getDrawable(R.drawable.filter)).into(imageView_filter);
                } else {
                    isChecked = true;
                    //   imageView_filter.setImageResource(R.drawable.filter);
//                    expandable_tags.collapse(false);
//                    ischecked_tags=false;
//                    mSeekbar.setProgress(0);
//                    distance_tv.setText("0 miles");
//                    tv_tags.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.down_arrow,0);
//
//                    //   filterMenuFunctionality();
//                    daily_tv.setSelected(false);
//                    ischecked_daily_hour = false;
//                    special_tv.setSelected(false);
//                    ischecked_special_hour = false;
//                    happy_tv.setSelected(false);
//                    ischecked_happy_hour = false;
                    // Toast.makeText(HomeActivity.this, "clciked wrong", Toast.LENGTH_SHORT).show();
                    Log.d("filled", "2" + isChecked);
                    Glide.with(HomeActivity.this).load(getResources().getDrawable(R.drawable.filter_checked)).into(imageView_filter);
                }

                if (Build.VERSION.SDK_INT < 22) {
                    showMenuBelowLollipop();
                } else {
                    showMenu();
                }
            }
        });
        activityHomeBinding.getRoot().findViewById(R.id.back_black_transparent_layout).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //     showSelectionOfBottomNavigationItem();
                        if (apply_click) {
                            isChecked = false;
                            item_filter.setIcon(R.drawable.filter);
                        }
                        //Glide.with(HomeActivity.this).load(getResources().getDrawable(R.drawable.filter)).into(imageView_filter);

                        if (Build.VERSION.SDK_INT < 22) {
                            showMenuBelowLollipop();
                        } else {
                            showMenu();
                        }
                    }
                });
    }

    private boolean isHidden = true;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showMenu() {


        int cx = (filter_layout.getLeft() + filter_layout.getRight());
        int cy = filter_layout.getTop();
        int radius = Math.max(filter_layout.getWidth(), filter_layout.getHeight());

        if (isHidden) {
            Animator anim = android.view.ViewAnimationUtils.createCircularReveal(filter_layout, cx, cy, 0, radius);
            anim.addListener(new AnimatorListenerAdapter() {

                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    filter_layout.setVisibility(View.VISIBLE);
                    isHidden = false;
                    constraintLayout.setVisibility(View.VISIBLE);
                }

            });
            anim.start();
        } else {
            Animator anim = android.view.ViewAnimationUtils.createCircularReveal(filter_layout, cx, cy, radius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    filter_layout.setVisibility(View.GONE);
                    isHidden = true;
                    constraintLayout.setVisibility(View.GONE);
                }
            });
            anim.start();
        }
    }


    private void showMenuBelowLollipop() {
        final CardView filter_layout = activityHomeBinding.getRoot().findViewById(R.id.filter_menu_container_card_view);
        int cx = (filter_layout.getLeft() + filter_layout.getRight());
        int cy = filter_layout.getTop();
        int radius = Math.max(filter_layout.getWidth(), filter_layout.getHeight());

        try {
            SupportAnimator animator = ViewAnimationUtils.createCircularReveal(filter_layout, cx, cy, 0, radius);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(300);

            if (isHidden) {
                filter_layout.setVisibility(View.VISIBLE);
                animator.start();
                isHidden = false;
            } else {
                SupportAnimator animatorReverse = animator.reverse();
                animatorReverse.start();
                animatorReverse.addListener(new SupportAnimator.AnimatorListener() {
                    @Override
                    public void onAnimationStart() {
                    }

                    @Override
                    public void onAnimationEnd() {
                        isHidden = true;
                        filter_layout.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel() {
                    }

                    @Override
                    public void onAnimationRepeat() {
                    }
                });
            }
        } catch (Exception e) {
            isHidden = true;
            filter_layout.setVisibility(View.GONE);
        }
    }


    boolean ischecked_cuisine = false;
    boolean isCheckedFoodTags = false;
    boolean isCheckedDrinkTags = false;
    boolean isCheckedServicekTags = false;
    // boolean ischecked_tags = false;

    private void filterMenuFunctionality() {
        /*tabLayout_for_filter = activityHomeBinding.getRoot().findViewById(R.id.tablayout);
        tabLayout_for_filter.addTab(tabLayout_for_filter.newTab().setText(getResources().getString(R.string.daily_special)));
        tabLayout_for_filter.addTab(tabLayout_for_filter.newTab().setText(getResources().getString(R.string.happy_hour)));
        tabLayout_for_filter.addTab(tabLayout_for_filter.newTab().setText(getResources().getString(R.string.special_hour)));

        tabLayout_for_filter.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Toast.makeText(HomeActivity.this, tab.getText()+" Selected "+tab.getPosition(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });*/
        HeavyTextView applyTv = activityHomeBinding.getRoot().findViewById(R.id.apply_tv);
        HeavyTextView cancelTv = activityHomeBinding.getRoot().findViewById(R.id.cancel_tv);
        cancelTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandable_tags.collapse(false);
                item_filter.setIcon(R.drawable.filter);
                isChecked = false;
                if (Build.VERSION.SDK_INT < 22) {
                    showMenuBelowLollipop();
                } else {
                    showMenu();
                }
                daily_tv.setSelected(false);
                ischecked_daily_hour = false;
                special_tv.setSelected(false);
                ischecked_special_hour = false;
                happy_tv.setSelected(true);
                ischecked_happy_hour = true;
                ischecked_cuisine = false;
                deals = 2;
                expandable_cuisine.collapse(false);
                apply_click = true;
                ischecked_tags = false;
                mSeekbar.setProgress(50);
                distance_tv.setText("50 miles");
                homeViewModel.setTagListing();
                homeViewModel.setCusinsListing();
                homeViewModel.setServiceListing();
                tagDetails = null;
                tagDrinkDetails = null;
                cusinesDetails = null;
                serviceDetails = null;
                tv_tags.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                editor.clear();
                tag_id = null;
                editor.commit();
                checkFragmentInBackstackAndOpen(SpecialFragment.newInstance());

                //   filterMenuFunctionality();
                //      daily_tv.setSelected(false);
            }
        });
        applyTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  filter_layout.setVisibility(View.GONE);
                // item_filter.setIcon(R.drawable.filter);
                apply_click = false;
                flag = 1;
                isChecked = false;
                if (Build.VERSION.SDK_INT < 22) {
                    showMenuBelowLollipop();
                } else {
                    showMenu();
                }
                checkFragmentInBackstackAndOpen(SpecialFragment.newInstance());
                editor.putInt("progress", distance_value);  // Pick a key here.
                editor.putString("tag_id", tag_id);
                editor.putInt("deals", deals);
                editor.putString("tagDetails", tagDetails);
                editor.putString("tagDrinkDetails", tagDrinkDetails);
                editor.putString("cusinsDetails", cusinesDetails);
                editor.putString("serviceDetails", serviceDetails);
                editor.commit();
            }
        });


        mSeekbar.setProgress(50);
        mSeekbar.setMax(100);

        //  mSeekbar.setMin(1);
        //  mSeekbar.(1);
        //  mSeekbar.setProgress(  mSeekbar.getMax()/2);
        distance_tv.setText(50 + " miles");
        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                distance_tv.setText(progress + " miles");
                distance_value = progress;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        deals = 2;
        happy_tv.setSelected(true);
        ischecked_happy_hour = true;
        daily_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                happy_tv.setSelected(false);
                special_tv.setSelected(false);
                ischecked_happy_hour = false;
                ischecked_special_hour = false;
                if (ischecked_daily_hour) {

                    daily_tv.setSelected(false);
                    ischecked_daily_hour = false;
                } else {
                    deals = 1;
                    daily_tv.setSelected(true);
                    ischecked_daily_hour = true;
                }
            }
        });
        happy_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daily_tv.setSelected(false);
                special_tv.setSelected(false);
                ischecked_daily_hour = false;
                ischecked_special_hour = false;
                if (ischecked_happy_hour) {

                    happy_tv.setSelected(false);
                    ischecked_happy_hour = false;
                } else {
                    deals = 2;
                    happy_tv.setSelected(true);
                    ischecked_happy_hour = true;
                }
            }
        });
        special_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daily_tv.setSelected(false);
                happy_tv.setSelected(false);
                ischecked_happy_hour = false;
                ischecked_daily_hour = false;
                if (ischecked_special_hour) {

                    special_tv.setSelected(false);
                    ischecked_special_hour = false;
                } else {
                    deals = 3;
                    //Toast.makeText(this,"cli")
                    special_tv.setSelected(true);
                    ischecked_special_hour = true;
                }
            }
        });
        setCusinerRecyclerView();

        setTagRecyclerView();
        setTagDrinkRecyclerView();
        setServicesRecyclerView();
        final TextView tv_cuisine = activityHomeBinding.getRoot().findViewById(R.id.tv_cuisine);
        //final TextView tv_tags = activityHomeBinding.getRoot().findViewById(R.id.tv_tags);

        //expandable_tags make it global
        //  final ExpandableLayout expandable_tags = activityHomeBinding.getRoot().findViewById(R.id.expandable_layout_tags);
        tv_cuisine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ischecked_cuisine) {
                    expandable_cuisine.expand(false);
                    tv_cuisine.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    ischecked_cuisine = true;
                } else {
                    expandable_cuisine.collapse(false);
                    tv_cuisine.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    ischecked_cuisine = false;
                }
            }
        });
        tv_tags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ischecked_tags) {
                    expandable_tags.expand(false);

                    tv_tags.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    ischecked_tags = true;
                } else {
                    expandable_tags.collapse(false);
                    tv_tags.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    ischecked_tags = false;
                }
            }
        });

        foodTags.setOnClickListener(v -> {
            if (!isCheckedFoodTags) {
                expandblefoodTags.expand(false);
                foodTags.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                isCheckedFoodTags = true;
            } else {
                expandblefoodTags.collapse(false);
                foodTags.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                isCheckedFoodTags = false;
            }


        });

        drinkTags.setOnClickListener(v -> {
            if (!isCheckedDrinkTags) {
                expandbleDrinkTags.expand(false);
                drinkTags.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                isCheckedDrinkTags = true;
            } else {
                expandbleDrinkTags.collapse(false);
                drinkTags.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                isCheckedDrinkTags = false;
            }


        });


        serviceTags.setOnClickListener(v -> {
            if (!isCheckedServicekTags) {
                expandbleServiceTags.expand(false);
                serviceTags.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                isCheckedServicekTags = true;
            } else {
                expandbleServiceTags.collapse(false);
                serviceTags.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                isCheckedServicekTags = false;
            }

        });


        tagDrinksAdapter.setListener((view, position) -> {
            //   Log.d("valueofselecttag","--"+view.getTagid()+view.getTagname()+position);
//          for(int i=0;i<view.size();i++){
//              if(view.get(i).isCheck()){
            tag_id = view.get(i).getTagid();

            tagDrinkDetails = new Gson().toJson(view);
//              }
//
//          }

        });
        tagAdapter.setListener(new TagAdapter.ItemClickListener() {
            @Override
            public void onItemClick(LinkedList<TagDetail> view, int position) {
                //   Log.d("valueofselecttag","--"+view.getTagid()+view.getTagname()+position);
//          for(int i=0;i<view.size();i++){
//              if(view.get(i).isCheck()){
                tag_id = view.get(i).getTagid();

                tagDetails = new Gson().toJson(view);
//              }
//
//          }

            }
        });
        cusinsAdapter.setListener(new CusinsAdapter.ItemClickListener() {
            @Override
            public void onItemClick(LinkedList<CusineDetail> view, int position) {
                cusinesDetails = new Gson().toJson(view);
                tag_id = view.get(i).getCusinsname();
//             for(int i=0;i<view.size();i++){
//                 if(view.get(i).isCheck()){
//                     tag_id=view.get(i).getCusinsname();
//
//
//                 }
//             }
            }
        });


        servicesAdapter.setListener((view, position) -> {
            Log.d("testclick", "---" + position);
            serviceDetails = new Gson().toJson(view);
            tag_id = view.get(position).getServiceName();
        });

    }

    private void setCusinerRecyclerView() {

        RecyclerView cusinRecylerview = activityHomeBinding.getRoot().findViewById(R.id.cusinsRecycler);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeActivity.this);
        cusinRecylerview.setLayoutManager(linearLayoutManager);
        //  fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(new SpacesItemDecoration(ViewUtils.dpToPx(2)));
        LinkedList<CusineDetail> details = new LinkedList<>();

        cusinsAdapter = new CusinsAdapter(HomeActivity.this, details);
//        DividerItemDecoration decoration = new DividerItemDecoration(HomeActivity.this, VERTICAL);
//        cusinRecylerview.addItemDecoration(decoration);
        // fragmentSearchBinding.searchRecycle.setAdapter(fevouriteListAdapter);
        cusinRecylerview.setAdapter(cusinsAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_option, menu);
        menuItem = menu;
        item_filter = menu.findItem(R.id.Apply);
        item_filter.setVisible(true);
        Log.d("onCreate", "onCreateOptionsMenu");

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layout_container);

        if (fragment instanceof NotificationFragment) {
            item_filter.setVisible(false);
            flag = 1;
        }
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();

        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {
            case R.id.Apply:
                if (isChecked) {
                    isChecked = false;

                    item.setIcon(R.drawable.filter);

                } else {
                    if (apply_click) {
                        isChecked = true;
                        item.setIcon(R.drawable.filter_checked);
                    }
                }
                if (Build.VERSION.SDK_INT < 22) {
                    showMenuBelowLollipop();
                } else {
                    showMenu();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setTagDrinkRecyclerView() {

        RecyclerView tagRecyclerview = activityHomeBinding.getRoot().findViewById(R.id.recyclerDrinktag);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeActivity.this);
        tagRecyclerview.setLayoutManager(linearLayoutManager);
        //  fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(new SpacesItemDecoration(ViewUtils.dpToPx(2)));
        LinkedList<TagDetail> details = new LinkedList<>();

        tagDrinksAdapter = new TagDrinksAdapter(HomeActivity.this, details);
//        DividerItemDecoration decoration = new DividerItemDecoration(HomeActivity.this, VERTICAL);
//        tagRecyclerview.addItemDecoration(decoration);
        // fragmentSearchBinding.searchRecycle.setAdapter(fevouriteListAdapter);
        tagRecyclerview.setAdapter(tagDrinksAdapter);
    }

    private void setServicesRecyclerView() {

        RecyclerView servicesRecyclerview = activityHomeBinding.getRoot().findViewById(R.id.service_recycle);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeActivity.this);
        servicesRecyclerview.setLayoutManager(linearLayoutManager);
        //  fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(new SpacesItemDecoration(ViewUtils.dpToPx(2)));
        LinkedList<ServiceDetail> details = new LinkedList<>();

        servicesAdapter = new ServicesAdapter(HomeActivity.this, details);
//        DividerItemDecoration decoration = new DividerItemDecoration(HomeActivity.this, VERTICAL);
        //  servicesRecyclerview.addItemDecoration(decoration);
        // fragmentSearchBinding.searchRecycle.setAdapter(fevouriteListAdapter);
        servicesRecyclerview.setAdapter(servicesAdapter);
    }

    private void setTagRecyclerView() {

        RecyclerView tagRecyclerview = activityHomeBinding.getRoot().findViewById(R.id.recyclerTag);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeActivity.this);
        tagRecyclerview.setLayoutManager(linearLayoutManager);
        //  fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(new SpacesItemDecoration(ViewUtils.dpToPx(2)));
        LinkedList<TagDetail> details = new LinkedList<>();

        tagAdapter = new TagAdapter(HomeActivity.this, details);
        //DividerItemDecoration decoration = new DividerItemDecoration(HomeActivity.this, VERTICAL);
        // tagRecyclerview.addItemDecoration(decoration);
        // fragmentSearchBinding.searchRecycle.setAdapter(fevouriteListAdapter);
        tagRecyclerview.setAdapter(tagAdapter);
    }


    public void setListenerForHomeActivityEditSaveOptionMenuClick(HomeActivityEditSaveOptionMenuClick
                                                                          listenerForHomeActivityEditSaveOptionMenuClick) {
        this.homeActivityEditSaveOptionMenuClick = listenerForHomeActivityEditSaveOptionMenuClick;
    }

    private void logOut() {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(homeViewModel.getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);

        Bundle bundle = new Bundle();
        bundle.putString("userName", responseUser.getUserName());
        bundle.putString("userid", responseUser.getId());

        mFirebaseAnalytics.logEvent("loggedout_android", bundle);
        showLoading();
        // loginActivity.logOutGoogle();
        homeViewModel.userLogout();


    }


    @Override
    public void responseAdterLogout(CommonSimpleResponse commonSimpleResponse) {
        hideLoading();
        Toast.makeText(HomeActivity.this, getString(R.string.successful_logged_out), Toast.LENGTH_LONG).show();
        //  SharedPrefManager.getInstance(HomeActivity.this).deleteNotification();
        SharedPrefManager.getInstance(HomeActivity.this).saveBooleanForIsUserAlreadyLoggedIn(false);
        //  startActivity(new Intent(this,LoginActivity.class));
        startActivity(LoginActivity.newIntent(HomeActivity.this));
        finish();
    }

    @Override
    public void badgeSuccess(String badge) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layout_container);
        if (!badge.equals("0") && !(fragment instanceof NotificationFragment)) {
            this.badge.setVisible(true);
//        badge.setVisible(true);
            this.badge.setNumber(Integer.parseInt(badge));
        }


    }

    @Override
    public void onError(Throwable throwable) {

        hideLoading();
        if (throwable != null) {
            Log.d(TAG, "_ERROR_: " + throwable.getMessage());
        }
    }

    @Override
    public void success(TagResponse response) {
        if (response != null) {

            tagDrinksAdapter.setAllData(response.getDetailsDrink());
            tagAdapter.setAllData(response.getDetails());
        }
    }

    @Override
    public void cusinsuccess(CusineRespone response) {
        cusinsAdapter.setAllData(response.getDetails());

    }

    @Override
    public void serviceSuccess(ServiceResponse response) {

        servicesAdapter.setAllData(response.getDetails());

    }

    private void showDialogForLogout() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle("Logout Confirmation");
        builder.setMessage("Do you want to logout?");
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logOut();
            }
        });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public void clearBadgeCountInAdapter() {
        if (drawerAdapter != null) {
            drawerAdapter.changeNotificationBadge("0", true);
        }
    }

}
