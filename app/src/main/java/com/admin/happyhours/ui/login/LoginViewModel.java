package com.admin.happyhours.ui.login;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

import static com.admin.happyhours.utils.AppConstants.FB_LOGIN;

public class LoginViewModel extends BaseViewModel<LoginNavigator> {


    public LoginViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public void login(String email, final String password, String device_type, String device_token) {
        Disposable disposable = ApplicationClass.getRetrofitService()
                .normalSignin(email, password, device_type, device_token)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<NormalLoginResponse>() {
                    @Override
                    public void accept(NormalLoginResponse response) throws Exception {
                        if (response != null) {
                            Log.d("check_response", ": " + response.getMessage());
                            getNavigator().onSuccess(response);
                            if (response.getDetails() != null && response.getStatus().equals("success")) {
                                getDataManager().setCurrentUserInfo(new Gson().toJson(response.getDetails()));
                                getDataManager().setPassword(password);
                            }
                        } else {
                            Log.d("check_response", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response", ": " + throwable.getMessage());
                        getNavigator().onError(throwable);
                    }
                });

        getCompositeDisposable().add(disposable);
    }


    public void loginWithFbOrGplus(String login_check, String name, String email, String social_id, String image_utl,
                                   String device_type, String device_token) {
        if (login_check.equals(FB_LOGIN)) {
            Disposable disposable = ApplicationClass.getRetrofitService()
                    .socialFBSignup(name, email, social_id, image_utl, device_type, device_token)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<NormalLoginResponse>() {
                        @Override
                        public void accept(NormalLoginResponse response) throws Exception {
                            if (response != null) {
                                getNavigator().onSuccess(response);
                                if (response.getDetails() != null) {
                                    getDataManager().setCurrentUserInfo(new Gson().toJson(response.getDetails()));
                                    getDataManager().deletePassword();
                                }

                            } else {
                                Log.d("check_response", ": null response");
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response", ": " + throwable.getMessage());
                            getNavigator().onError(throwable);
                        }
                    });

            getCompositeDisposable().add(disposable);
        } else {
            Disposable disposable = ApplicationClass.getRetrofitService()
                    .socialGOOGLESignup(name, email, social_id, image_utl, device_type, device_token)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<NormalLoginResponse>() {
                        @Override
                        public void accept(NormalLoginResponse response) throws Exception {
                            if (response != null) {
                                getNavigator().onSuccess(response);
                                if (response.getDetails() != null) {
                                    getDataManager().setCurrentUserInfo(new Gson().toJson(response.getDetails()));
                                    getDataManager().deletePassword();
                                }

                            } else {
                                Log.d("check_response", ": null response");
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response", ": " + throwable.getMessage());
                            getNavigator().onError(throwable);
                        }
                    });

            getCompositeDisposable().add(disposable);
        }
    }


}
