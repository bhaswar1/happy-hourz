package com.admin.happyhours.ui.privacytermsbusinesssignup;

import com.admin.happyhours.data.model.api.response.PrivacyPolicyAndTermsAndConditionResponse;

public interface PrivacyTermsBusinessNavigator {

    void onSuccess(PrivacyPolicyAndTermsAndConditionResponse response);

    void onError(Throwable throwable);

}
