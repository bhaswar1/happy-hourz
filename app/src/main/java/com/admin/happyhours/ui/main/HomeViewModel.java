package com.admin.happyhours.ui.main;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.CusineRespone;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.ReviewPhotoResponse;
import com.admin.happyhours.data.model.api.response.ServiceResponse;
import com.admin.happyhours.data.model.api.response.TagResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class HomeViewModel extends BaseViewModel<HomeNavigator> {



    public HomeViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
    public void setTagListing() {
        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

        //    Log.d("uid", ": " + uid);


            Disposable disposable = ApplicationClass.getRetrofitService()
                    .tagresponselIsting()
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<TagResponse>() {
                        @Override
                        public void accept(TagResponse response) throws Exception {
                            if (response != null) {
                                if(response.getStatus().toLowerCase().equals("success")) {
                                    Log.d("response", "--" + new Gson().toJson(response));
                                    getNavigator().success(response);
                                }else{

                                }

                            }




                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response","---"+throwable.toString());
                            getNavigator().onError(throwable);

                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }
    public void setNotificationBadge() {
        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("uid", ":p " + responseUser.getId());


            Disposable disposable = ApplicationClass.getRetrofitService()
                    .notificationBadge(responseUser.getId())
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<CommonSimpleResponse>() {
                        @Override
                        public void accept(CommonSimpleResponse response) throws Exception {
                            if (response != null) {
                                if (response.getStatus().toLowerCase().equals("success")) {
                                    Log.d("response", "--" + new Gson().toJson(response));

                                    getNavigator().badgeSuccess(response.getBadgecount());
                                } else {

                                }

                            }


                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response", "---" + throwable.toString());
                            getNavigator().onError(throwable);

                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }








    public void setServiceListing() {
        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            //    Log.d("uid", ": " + uid);


            Disposable disposable = ApplicationClass.getRetrofitService()
                    .serviceListing()
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<ServiceResponse>() {
                        @Override
                        public void accept(ServiceResponse response) throws Exception {
                            if (response != null) {
                                if(response.getStatus().toLowerCase().equals("success")) {
                                    Log.d("response", "--" + new Gson().toJson(response));
                                    getNavigator().serviceSuccess(response);
                                }else{

                                }

                            }




                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response","---"+throwable.toString());
                            getNavigator().onError(throwable);

                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }






    public void setCusinsListing() {
        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            //    Log.d("uid", ": " + uid);


            Disposable disposable = ApplicationClass.getRetrofitService()
                    .cusinslIsting()
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<CusineRespone>() {
                        @Override
                        public void accept(CusineRespone response) throws Exception {
                            if (response != null) {
                                if(response.getStatus().toLowerCase().equals("success")) {
                                    Log.d("response", "--" + new Gson().toJson(response));
                                    getNavigator().cusinsuccess(response);
                                }else{

                                }

                            }




                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response","---"+throwable.toString());
                            getNavigator().onError(throwable);

                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }


    public void userLogout(){
        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);

        Disposable disposable = ApplicationClass.getRetrofitService()
                .userLogout(responseUser.getId())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<CommonSimpleResponse>() {
                    @Override
                    public void accept(CommonSimpleResponse response) throws Exception {
                        if (response != null) {

                            getNavigator().responseAdterLogout(response);
                            if (response.getStatus() != null && response.getStatus().trim().toLowerCase().equals("success")) {
                                 getDataManager().deletePassword();
                                 getDataManager().deleteCurrentUser();
                            }

                        } else {
                            Log.d("check_response", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response", ": " + throwable.getMessage());
                        getNavigator().onError(throwable);
                    }
                });

        getCompositeDisposable().add(disposable);

    }
}
