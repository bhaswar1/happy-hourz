package com.admin.happyhours.ui.privacytermsbusinesssignup;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;


import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.PrivacyPolicyAndTermsAndConditionResponse;
import com.admin.happyhours.databinding.ActivityPrivacyPolicyBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.utils.AppConstants;
import com.github.pdfviewer.PDFView;
//import com.github.barteksc.pdfviewer.PDFView;
//import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
//import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
//import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
//import com.github.barteksc.pdfviewer.listener.OnRenderListener;
//import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;

import org.apache.commons.text.StringEscapeUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;

public class
PrivacyTermsBusinessActivity extends BaseActivity<ActivityPrivacyPolicyBinding, PrivacyTermsBusinessViewModel> implements PrivacyTermsBusinessNavigator
{


    private String s_title_toolbar;
    ActivityPrivacyPolicyBinding activityPrivacyPolicyBinding;
    @Inject
    PrivacyTermsBusinessViewModel privacyTermsBusinessViewModel;

    public static Intent newIntent(Context context, String s_title_toolbar){
        return new Intent(context, PrivacyTermsBusinessActivity.class).putExtra("s_title_toolbar",s_title_toolbar);
    }
   String SAMPLE_FILE="Terms_of_use.pdf";

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_privacy_policy;
    }

    @Override
    public PrivacyTermsBusinessViewModel getViewModel() {
        return privacyTermsBusinessViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityPrivacyPolicyBinding = getViewDataBinding();
        privacyTermsBusinessViewModel.setNavigator(this);
        setStatusBarHidden(PrivacyTermsBusinessActivity.this);

        activityPrivacyPolicyBinding.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_arrow_black));
        activityPrivacyPolicyBinding.toolbar.setTitle("");
        setSupportActionBar(activityPrivacyPolicyBinding.toolbar);
        if (activityPrivacyPolicyBinding.toolbar != null) {
            activityPrivacyPolicyBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    finish();
                }
            });
        }

        if (getIntent()!=null && getIntent().getExtras()!=null){
            s_title_toolbar = getIntent().getExtras().getString("s_title_toolbar");
            activityPrivacyPolicyBinding.toolbarTitleTxt.setText(s_title_toolbar);


            if (s_title_toolbar.trim().equals(getResources().getString(R.string.privacy_policy))){
                activityPrivacyPolicyBinding.webView.setVisibility(View.VISIBLE);
              //  activityPrivacyPolicyBinding.pdfView.setVisibility(View.GONE);
privacypolicywebView();
//                showLoading();
//                privacyTermsBusinessViewModel.fetchDataPrivacyPolyAndTermsAndCondition("2");
            }else if (s_title_toolbar.trim().equals(getResources().getString(R.string.term_and_confition))){
                activityPrivacyPolicyBinding.webView.setVisibility(View.VISIBLE);



                termsofusewebView();



//

                // privacyTermsBusinessViewModel.fetchDataPrivacyPolyAndTermsAndCondition("1");
            }else if (s_title_toolbar.trim().equals(getResources().getString(R.string.sign_up_for_buisness_profile))){
                activityPrivacyPolicyBinding.webView.setVisibility(View.VISIBLE);
             //   activityPrivacyPolicyBinding.pdfView.setVisibility(View.GONE);
                signupwebView();

            }
        }


//        activityPrivacyPolicyBinding.webView.loadUrl(AppConstants.BASE_URL.concat("app/termscondition_control?mode=1"));
    }

    private void termsofusewebView() {
        showLoading();
        activityPrivacyPolicyBinding.webView.setWebViewClient(new WebViewClient());
        activityPrivacyPolicyBinding.webView.getSettings().setJavaScriptEnabled(true);
        activityPrivacyPolicyBinding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        activityPrivacyPolicyBinding.webView.loadUrl(AppConstants.BASE_URL.concat("app/Privacy_policy/terms"));
        activityPrivacyPolicyBinding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                System.out.println("hello");
                Log.d("hello", ""+url);


                if (url.contains("mailto:")){
                    MailTo mailTo = MailTo.parse(url);
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:"));
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{mailTo.getTo()});
                    //   intent.putExtra(Intent.EXTRA_SUBJECT, "dmkmd");
                    // intent.putExtra(Intent.EXTRA_TEXT, "dmdm");
                    startActivity(intent);
                    // make sure you have a context set somewhere in your activity, other wise use YOUR_ACTIVITY_NAME.this.
                    // For this example I am using mContext because that is my context variable
//                    Intent mailIntent = sendEmail(getApplicationContext(), mailTo.getTo(), mailTo.getSubject(), mailTo.getBody()); // I added these extra parameters just incase you need to send those
//                  startActivity(mailIntent);
                    view.reload(); // reload your webview using view.reload()

                    return true;
                }else{
                    // Handle what t do if the link doesn't contain or start with mailto:
                    view.loadUrl(url); // you want to use this otherwise the links in the webview won't work
                }
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // showLoading();
                activityPrivacyPolicyBinding.webView.loadUrl("javascript:HtmlViewer.showHTML" +
                        "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");


                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {


                hideLoading();
            }
        });

    }



    private void privacypolicywebView() {
        showLoading();
        activityPrivacyPolicyBinding.webView.setWebViewClient(new WebViewClient());
        activityPrivacyPolicyBinding.webView.getSettings().setJavaScriptEnabled(true);
        activityPrivacyPolicyBinding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        activityPrivacyPolicyBinding.webView.loadUrl(AppConstants.BASE_URL.concat("app/Privacy_policy"));
        activityPrivacyPolicyBinding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                System.out.println("hello");
                Log.d("hello", ""+url);


                if (url.contains("mailto:")){
                    MailTo mailTo = MailTo.parse(url);
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:"));
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{mailTo.getTo()});
                    //   intent.putExtra(Intent.EXTRA_SUBJECT, "dmkmd");
                    // intent.putExtra(Intent.EXTRA_TEXT, "dmdm");
                    startActivity(intent);
                    // make sure you have a context set somewhere in your activity, other wise use YOUR_ACTIVITY_NAME.this.
                    // For this example I am using mContext because that is my context variable
//                    Intent mailIntent = sendEmail(getApplicationContext(), mailTo.getTo(), mailTo.getSubject(), mailTo.getBody()); // I added these extra parameters just incase you need to send those
//                  startActivity(mailIntent);
                    view.reload(); // reload your webview using view.reload()

                    return true;
                }else{
                    // Handle what t do if the link doesn't contain or start with mailto:
                    view.loadUrl(url); // you want to use this otherwise the links in the webview won't work
                }
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // showLoading();
                activityPrivacyPolicyBinding.webView.loadUrl("javascript:HtmlViewer.showHTML" +
                        "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");


                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {


                hideLoading();
            }
        });

    }

    private Intent sendEmail(Context context, String email, String subject, String body){

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.putExtra(Intent.EXTRA_EMAIL, email); // if you wanted to send multiple emails then you would use intent.putExtra(Intent.EXTRA_EMAIL, new String[] { email })
        intent.putExtra(Intent.EXTRA_TEXT, body); // optional if you have the body in your mailto: link
        intent.putExtra(Intent.EXTRA_SUBJECT, subject); // optional if you have the subject in your mailto: link
        intent.setType("text/plain");

        return intent;
    }

    private void signupwebView() {
        showLoading();
        activityPrivacyPolicyBinding.webView.setWebViewClient(new WebViewClient());
        activityPrivacyPolicyBinding.webView.getSettings().setJavaScriptEnabled(true);
        activityPrivacyPolicyBinding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        activityPrivacyPolicyBinding.webView.loadUrl(AppConstants.BASE_URL.concat("app/addbusiness/add/0/0"));
        activityPrivacyPolicyBinding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                System.out.println("hello");
                Log.d("hello", ""+url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // showLoading();
                activityPrivacyPolicyBinding.webView.loadUrl("javascript:HtmlViewer.showHTML" +
                        "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");


                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {


                hideLoading();
            }
        });

    }

    @Override
    public void onSuccess(PrivacyPolicyAndTermsAndConditionResponse response) {
       // hideLoading();
        if (response!=null){
            if (response.getStatus().trim().toLowerCase().equals("success")){
                if (response.getDetails()!=null){
                    hideLoading();
                    //activityPrivacyPolicyBinding.webTv.setText(StringEscapeUtils.unescapeHtml4(response.getDetails()));
                    activityPrivacyPolicyBinding.webView.loadData(response.getDetails(), "text/html", "UTF-8");
                }
            }
        }
    }

    @Override
    public void onError(Throwable throwable) {
        hideLoading();
    }


}
