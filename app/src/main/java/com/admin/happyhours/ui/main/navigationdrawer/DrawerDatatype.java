package com.admin.happyhours.ui.main.navigationdrawer;

public class DrawerDatatype {

    String string_item;

    int position, notification_badge_count;

    public DrawerDatatype(String string_item, int position, int notification_badge_count) {
        this.string_item = string_item;
        this.position = position;
        this.notification_badge_count = notification_badge_count;
    }

    public String getString_item() {
        return string_item;
    }

    public void setString_item(String string_item) {
        this.string_item = string_item;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getNotification_badge_count() {
        return notification_badge_count;
    }

    public void setNotification_badge_count(int notification_badge_count) {
        this.notification_badge_count = notification_badge_count;
    }
}
