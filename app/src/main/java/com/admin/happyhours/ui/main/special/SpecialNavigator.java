package com.admin.happyhours.ui.main.special;

import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.google.gson.JsonArray;

import java.util.LinkedList;
import java.util.List;

public interface SpecialNavigator {

    void onSuccess(SpecialPageRestuarantListingResponse specialPageRestuarantListingResponse, List<SpecialPageRestuarantListingResponse.Detail > detail, JsonArray jsonArray);

    void onError(Throwable throwable);

    void responseAfterBookmarkOrLike(String check_for_button, int position);

    void failed();
}
