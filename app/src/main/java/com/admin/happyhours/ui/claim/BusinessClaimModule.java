package com.admin.happyhours.ui.claim;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class BusinessClaimModule {


    @Provides
    BusinessClaimViewModel provideSettingsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new BusinessClaimViewModel(dataManager, schedulerProvider);
    }


}
