package com.admin.happyhours.ui.feedback;

import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;

public interface FeedbackNavigator {

    void onSuccess(CommonSimpleResponse commonSimpleResponse);

    void onError(Throwable throwable);

}
