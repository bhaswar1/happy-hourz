package com.admin.happyhours.ui.reviewphoto;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.ReviewListResponse;
import com.admin.happyhours.data.model.api.response.ReviewPhotoResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class ReviewPhotoViewModel extends BaseViewModel<ReviewPhotoNavigator> {


    public ReviewPhotoViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
    public void setReviewPhotoLIst(String uid, String restaurant_id, int limit, int page , final boolean check) {
        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("uid", ": " + uid);


            Disposable disposable = ApplicationClass.getRetrofitService()
                    .reviewphotoListing(uid,restaurant_id,limit,page)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<ReviewPhotoResponse>() {
                        @Override
                        public void accept(ReviewPhotoResponse response) throws Exception {
                            if (response != null) {
                                if(response.getStatus().toLowerCase().equals("success")) {
                                    Log.d("response", "--" + new Gson().toJson(response));
                                    getNavigator().success(response);
                                }else{
                                    if(!check){
                                        getNavigator().noData();
                                    }else{
                                        getNavigator().noLoad();
                                    }
                                }

                            }




                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response","---"+throwable.toString());
                            getNavigator().onError(throwable);

                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }

}
