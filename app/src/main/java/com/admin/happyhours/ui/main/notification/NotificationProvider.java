package com.admin.happyhours.ui.main.notification;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class NotificationProvider {

    @ContributesAndroidInjector(modules = NotificationModule.class)
    abstract NotificationFragment provideNotificationFragmentFactory();


}
