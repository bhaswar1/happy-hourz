package com.admin.happyhours.ui.settings;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class SettingsModule {


    @Provides
    SettingsViewModel provideSettingsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new SettingsViewModel(dataManager, schedulerProvider);
    }


}
