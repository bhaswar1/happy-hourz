package com.admin.happyhours.ui.transparentrestaurantaddtocollection;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;

public class TransparentViewModel extends BaseViewModel<TransparentNavigator> {


    public TransparentViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
}
