package com.admin.happyhours.ui.main.events;

import com.admin.happyhours.data.model.api.response.EventListingResponse;

public interface EventNavigator {

    void onSuccess(EventListingResponse eventListingResponse);

    void onError(Throwable throwable);

    void likeUnlike(String check_for_button, int position);

    void noData();
}
