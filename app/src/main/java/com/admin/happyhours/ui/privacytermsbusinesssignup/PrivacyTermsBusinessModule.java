package com.admin.happyhours.ui.privacytermsbusinesssignup;


import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class PrivacyTermsBusinessModule {

    @Provides
    PrivacyTermsBusinessViewModel providePrivacyPolicyViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new PrivacyTermsBusinessViewModel(dataManager, schedulerProvider);
    }

}
