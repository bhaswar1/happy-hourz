package com.admin.happyhours.ui.main.search;

import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.SearchDataResponse;

public interface SearchNavigator {
    void searchSuccess(SearchDataResponse response);
    void searchHistorySuccess(SearchDataResponse response);
    void onError(Throwable throwable);

    void noData();
}
