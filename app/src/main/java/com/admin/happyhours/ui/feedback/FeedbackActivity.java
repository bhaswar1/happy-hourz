package com.admin.happyhours.ui.feedback;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.databinding.ActivityFeedbackBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.inject.Inject;

import static com.facebook.FacebookSdk.getCacheDir;

public class FeedbackActivity extends BaseActivity<ActivityFeedbackBinding, FeedbackViewModel> implements FeedbackNavigator {


    private static final String TAG = "FeedbackActivity";
    ActivityFeedbackBinding activityFeedbackBinding;
    @Inject
    FeedbackViewModel feedbackViewModel;
    private File file_cache_dir;


    public static Intent newIntent(Context context) {
        return new Intent(context, FeedbackActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_feedback;
    }

    @Override
    public FeedbackViewModel getViewModel() {
        return feedbackViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityFeedbackBinding = getViewDataBinding();
        feedbackViewModel.setNavigator(this);
        setStatusBarHidden(FeedbackActivity.this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        activityFeedbackBinding.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_arrow_black));
        activityFeedbackBinding.toolbar.setTitle("");
        setSupportActionBar(activityFeedbackBinding.toolbar);
        if (activityFeedbackBinding.toolbar != null) {
            activityFeedbackBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    finish();
                }
            });
        }
        hideSoftKeyboardWithTouchOutsideEdittext(activityFeedbackBinding.layoutParent);

        activityFeedbackBinding.feedbackIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });

        activityFeedbackBinding.tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationChecking()){
                    hideKeyboard();
                    if(activityFeedbackBinding.edittextFeedback.getText().toString()!=null) {
                        showLoading();

                        feedbackViewModel.sendFeedback(activityFeedbackBinding.edittextFeedback.getText().toString().trim(), file_cache_dir);
                    }else{
                        activityFeedbackBinding.edittextFeedback.setError("field can't be blank");
                    }
                }
            }
        });

        activityFeedbackBinding.edittextFeedback.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s.toString())){
                    activityFeedbackBinding.edittextFeedback.setError(null);
                }
            }
        });
    }

    private void chooseImage() {

        /*CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setMultiTouchEnabled(true)
              //  .setAspectRatio(1,1)
                .setActivityTitle("Crop")
                .start(this);*/

        ImagePicker.with(this)
                .crop()	    			//Crop image(Optional), Check Customization for more option
                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                .start();

    }


    private boolean validationChecking(){
        if (TextUtils.isEmpty(activityFeedbackBinding.edittextFeedback.getText().toString().trim())){
            activityFeedbackBinding.edittextFeedback.setError(getResources().getString(R.string.can_not_be_blank));
            return false;
        }
        return true;
    }


    @Override
    public void onSuccess(CommonSimpleResponse commonSimpleResponse) {
        hideLoading();
        if (commonSimpleResponse!=null){
            if (commonSimpleResponse.getMessage()!=null){
                Toast.makeText(this, commonSimpleResponse.getMessage(), Toast.LENGTH_LONG).show();
            }
            if (commonSimpleResponse.getStatus()!=null){
                if (commonSimpleResponse.getStatus().trim().toLowerCase().endsWith("success")){
                    finish();
                }
            }

        }
    }

    @Override
    public void onError(Throwable throwable) {
        hideLoading();
        if (throwable!=null){
            Log.d(TAG, "onError: "+throwable.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            Uri uri= data.getData();

            saveFrrdbackImage(uri);

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }

        /*if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                if (resultUri != null) {
                    // Get file from cache directory



                    file_cache_dir = new File(getCacheDir(), resultUri.getLastPathSegment());
                    Bitmap bitmap = BitmapFactory.decodeFile(file_cache_dir.toString());
                    //   Bitmap.createScaledBitmap(bitmap, 200, 200, true);
                    OutputStream os;
                    try {
                        os = new FileOutputStream(file_cache_dir);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 18, os);
                        os.flush();
                        os.close();
                    } catch (Exception e) {
                        Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
                    }
                    // file_cache_dir=  bitmap.compress(Bitmap.CompressFormat.PNG, 200, outputStream);
                    if (file_cache_dir.exists()) {
                        Glide.with(this).load(file_cache_dir).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(activityFeedbackBinding.feedbackIv);


                        if (feedbackViewModel.getDataManager().getCurrentUserInfo() != null) {
                            //showLoading();
                           // NormalLoginResponse.Details details = new Gson().fromJson(myAccountViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);

                        }

                        Log.d("check_path", ": " + resultUri.toString());

                        Log.d("check_file_get", ": " + file_cache_dir.toString());

                    } else {
                        Log.d("file_does_not_exists", ": " + true);
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.d("check_error", ": " + error.getMessage());
            }
        }*/
    }

    private void saveFrrdbackImage(Uri resultUri){

        if (resultUri != null) {
            // Get file from cache directory



            file_cache_dir = new File(getCacheDir(), resultUri.getLastPathSegment());
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(this.getContentResolver().openInputStream(resultUri));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            //   Bitmap.createScaledBitmap(bitmap, 200, 200, true);
            OutputStream os;
            try {
                os = new FileOutputStream(file_cache_dir);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 18, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            }
            // file_cache_dir=  bitmap.compress(Bitmap.CompressFormat.PNG, 200, outputStream);
            if (file_cache_dir.exists()) {
                Glide.with(this).load(file_cache_dir).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(activityFeedbackBinding.feedbackIv);


                if (feedbackViewModel.getDataManager().getCurrentUserInfo() != null) {
                    //showLoading();
                    // NormalLoginResponse.Details details = new Gson().fromJson(myAccountViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);

                }

                Log.d("check_path", ": " + resultUri.toString());

                Log.d("check_file_get", ": " + file_cache_dir.toString());

            } else {
                Log.d("file_does_not_exists", ": " + true);
            }
        }

    }

}
