package com.admin.happyhours.ui.main.collections;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.admin.happyhours.BR;
import com.admin.happyhours.data.model.api.response.ResturantsAllData;
import com.admin.happyhours.ui.main.HomeActivity;
import com.admin.happyhours.ui.main.favourites.FavouritesFragment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CollectionListingResponse;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.admin.happyhours.databinding.FragmentCollectionBinding;
import com.admin.happyhours.interfaces.ItemLongClickListener;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.admin.happyhours.ui.base.BaseFragment;
import com.admin.happyhours.ui.createcollection.CreateCollectionActivity;
import com.admin.happyhours.ui.main.collections.recycleradapter.CollectioRecyclerAdapter;
import com.admin.happyhours.ui.transparentrestaurantlist.TransparentRestaurantListingActivity;
import com.admin.happyhours.utils.AppConstants;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.LinkedList;

import javax.inject.Inject;


public class CollectionFragment extends BaseFragment<FragmentCollectionBinding, CollectionViewModel> implements CollectionNavigator {

    private int adapter_item_long_press_position = -1;
    private BottomSheetBehavior bottomSheetBehavior;
    private boolean check_for_listing_reload = false;
    private static final int REQUEST_CODE = 007;
    private static final int RESULT_CODE = 007;
    private boolean control_redirection_from_restaurant_listing_to_collection_listing = false;
    //    LinkedList<CollectionListingResponse.Details> details;
    private ResturantsAllData passed_detail_data = null;
    FragmentCollectionBinding fragmentCollectionBinding;
    @Inject
    CollectionViewModel collectionViewModel;

    private  String collectionName=null;
    private String collectionid=null;
//    @Inject
   // ViewModelProvider.Factory mViewModelFactory;
    //    CollectionAdapter collectionAdapter;

    CollectioRecyclerAdapter collectioRecyclerAdapter;
    private int pagination = 1;
    private boolean hasLoadMore = false;
    android.app.AlertDialog alertDialog;
    public FirebaseAnalytics mFirebaseAnalytics;

    public  int favouriteStatus=0;



//    int check = 0;


    public static CollectionFragment newInstance() {
        Bundle args = new Bundle();
        CollectionFragment fragment = new CollectionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static CollectionFragment newInstance(String special_to_collection_restaurent_data) {
        Bundle args = new Bundle();
        args.putString("special_to_collection_restaurent_data", special_to_collection_restaurent_data);
        CollectionFragment fragment = new CollectionFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_collection;
    }

    @Override
    public CollectionViewModel getViewModel() {
        //collectionViewModel = ViewModelProviders.of(getActivity()).get(CollectionViewModel.class);
//      collectionViewModel = ViewModelProviders.of(this, mViewModelFactory).get(CollectionViewModel.class);
        return collectionViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        collectionViewModel.setNavigator(this);
    }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentCollectionBinding = getViewDataBinding();

        check_for_listing_reload = false;

        if (getArguments() != null && getArguments().getString("special_to_collection_restaurent_data") != null) {

            Toast.makeText(getActivity(),"please choose a collection to store this restaurant",Toast.LENGTH_LONG).show();
            Log.d("datacollection","--"+new Gson().toJson(getArguments().
                    getString("special_to_collection_restaurent_data")));
            passed_detail_data = new Gson().fromJson(getArguments().
                    getString("special_to_collection_restaurent_data"), ResturantsAllData.class);
            Log.d("datacollection==","--"+new Gson().toJson(passed_detail_data));
            control_redirection_from_restaurant_listing_to_collection_listing = true;
        } else {
            control_redirection_from_restaurant_listing_to_collection_listing = false;
        }


        setupRecyclerview();

        initializeBottomSheetBehavior();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        loadData();
//        reloadOrLoadData();

    }


    @Override
    public void onResume() {
        super.onResume();
        if (!check_for_listing_reload) {
            reloadOrLoadData();
            check_for_listing_reload = true;
        }

    }

    private void reloadOrLoadData() {
        if (collectioRecyclerAdapter != null) {
            collectioRecyclerAdapter.clearData();
            hasLoadMore = false;
            pagination = 1;
            collectioRecyclerAdapter.setData(new CollectionListingResponse.Details());
            loadData();
        }
    }

    private void loadData() {
        if (collectionViewModel.getDataManager().getCurrentUserInfo() != null) {
            getBaseActivity().showLoading();
            NormalLoginResponse.Details details = new Gson().fromJson(collectionViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
//            specialViewModel.fetchCollectionListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
            collectionViewModel.fetchCollectionListing(details.getId(), pagination, AppConstants.LISTING_FETCH_LIMIT);
        }
    }

    private void setupRecyclerview() {

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        fragmentCollectionBinding.recyclerView.setLayoutManager(gridLayoutManager);
        LinkedList<CollectionListingResponse.Details> details = new LinkedList<>();
        collectioRecyclerAdapter = new CollectioRecyclerAdapter(getActivity(), details,
                fragmentCollectionBinding.recyclerView, gridLayoutManager,favouriteStatus);
        fragmentCollectionBinding.recyclerView.setAdapter(collectioRecyclerAdapter);
        collectioRecyclerAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
//                Log.d("checking_steps_loadmore",": "+(++check));
                Log.d("check_bools", ": " + hasLoadMore);
                if (hasLoadMore) {
                    getBaseActivity().showLoading();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadData();
                        }
                    }, 1000);
                }
            }
        });
        collectioRecyclerAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(final int position, View view1) {
                NormalLoginResponse.Details details_current_user = new Gson().fromJson(collectionViewModel.getDataManager().
                        getCurrentUserInfo(), NormalLoginResponse.Details.class);
              //  Log.d("clicked","--"+details_current_user.getId());

                if (!control_redirection_from_restaurant_listing_to_collection_listing) {
                    if (position == 0) {
                     //  Toast.makeText(getActivity(), "Create new in next page", Toast.LENGTH_SHORT).show();
                        getActivity().startActivityForResult(CreateCollectionActivity.newIntent(getActivity(), AppConstants.create_collection_page_for_create), REQUEST_CODE);
                    } else {
                  //     Toast.makeText(getActivity(), "Restaurant Listing will be shown"+collectioRecyclerAdapter.getData().get(position).getCollection_name(),Toast.LENGTH_LONG).show();

                        if(favouriteStatus==1 && position==1){
                          //  getActivity().checkFragmentInBackstackAndOpen
                            FavouritesFragment nextFrag= new FavouritesFragment();
                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.layout_container, nextFrag, "FavouritesFragment")
                                    .addToBackStack(null)
                                    .commit();
//                            getActivity().
                        }else {
                            collectionName = collectioRecyclerAdapter.getData().get(position).getCollection_name();
                            collectionid = collectioRecyclerAdapter.getData().get(position).getCollection_name();
                            getActivity().startActivityForResult(TransparentRestaurantListingActivity.newIntent(getActivity(), collectioRecyclerAdapter.getData().get(position).getCollection_id()).putExtra("collectionName", collectioRecyclerAdapter.getData().get(position).getCollection_name()), REQUEST_CODE);
                        }
                        }
                }
                else {
                    if (position == 0) {
                       Toast.makeText(getActivity(), "Create and Add new in next page", Toast.LENGTH_SHORT).show();
                        String special_page_restuarant_details_to_add_in_collection = new Gson().toJson(passed_detail_data);
                        getActivity().startActivityForResult(CreateCollectionActivity.newIntent(getActivity(),
                                special_page_restuarant_details_to_add_in_collection,
                                AppConstants.create_collection_page_for_create_and_add), REQUEST_CODE);
                    }

                    else {

//                        NormalLoginResponse.Details details_current_user1 = new Gson().fromJson(collectionViewModel.getDataManager().
//                                getCurrentUserInfo(), NormalLoginResponse.Details.class);
                        //Log.d("check_click", ": bbbbbbbbbbbb"+passed_detail_data.getRestaurentId());


//                        Log.d("check_click_adapter_2", ": " + new Gson().toJson(collectioRecyclerAdapter.getData().get(position))+passed_detail_data.getRestaurentId());

//                        if(favouriteStatus==1 && position==1){
//                          //  getActivity().checkFragmentInBackstackAndOpen
//                            FavouritesFragment nextFrag= new FavouritesFragment();
//                            getActivity().getSupportFragmentManager().beginTransaction()
//                                    .replace(R.id.layout_container, nextFrag, "FavouritesFragment")
//                                    .addToBackStack(null)
//                                    .commit();
////                            getActivity().
//                        }else{
                            Toast.makeText(getContext(),"hii"+position,Toast.LENGTH_LONG).show();
                            android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getActivity()).setCancelable(false);
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.custom_addresturant_dialog, null);
                            dialogBuilder.setView(dialogView);
                            final Button cancelicon=(Button) dialogView.findViewById(R.id.cancel);
                            collectioRecyclerAdapter.showHideSelector(position);
                            Button submit=(Button) dialogView.findViewById(R.id.proceed);
                            //  Toast.makeText(BusinessClaimActivity.this,"click",Toast.LENGTH_LONG).show();
                            alertDialog = dialogBuilder.create();
                            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            alertDialog.show();
                            cancelicon.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    //    Toast.makeText(getActivity(), "Popup for collection Add", Toast.LENGTH_SHORT).show();
                                    if (collectionViewModel.getDataManager().getCurrentUserInfo() != null) {
                                        getBaseActivity(). showLoading();
                                        NormalLoginResponse.Details details = new Gson().fromJson(collectionViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
                                        collectioRecyclerAdapter.showHideSelector(1000);
//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                                        collectionViewModel.addSpecialtoCollection(details.getId(),passed_detail_data.getRestaurent_id(),collectioRecyclerAdapter.getData().get(position).getCollection_id());

//                                        Bundle bundle = new Bundle();
//                                        bundle.putString("userId", details.getId());
//                                        bundle.putString("userName", details.getUserName());
//                                        bundle.putString("restaurantId",passed_detail_data.getRestaurentId());
//                                        bundle.putString("restaurantName",passed_detail_data.getRestaurentName());
//                                        bundle.putString("restaurantRating",passed_detail_data.getRating());
////            bundle.putString("restaurantRating",eventbusForSpecialListingOptionClick.getDetail().getRating());
//
//                                        // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
//                                        //  bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, resturantName);
//
//                                        mFirebaseAnalytics.logEvent("restaurant_added_to_collections_android", bundle);



                                    }
                                }
                            });


                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    collectioRecyclerAdapter.showHideSelector(1000);
                                    alertDialog.dismiss();

                                }
                            });





                    }
                }
            }
        });
        collectioRecyclerAdapter.setCallbackLong(new ItemLongClickListener() {
            @Override
            public void onItemLongClick(int position, View view) {
//                Toast.makeText(getActivity(), "Delete", Toast.LENGTH_SHORT).show();
                adapter_item_long_press_position = position;
                openCloseBottomSheet();
//                NormalLoginResponse.Details details_current_user = new Gson().fromJson(collectionViewModel.getDataManager().
//                        getCurrentUserInfo(), NormalLoginResponse.Details.class);
//                showDialogForCollectionDelete(details_current_user.getId(), collectioRecyclerAdapter.getData().get(position), position);

            }
        });

    }

    private void showDialogForCollectionDelete(final String userid, final CollectionListingResponse.Details details_current_user, final int position) {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Confirmation");
        builder.setMessage(R.string.delete_collection_alert);
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getBaseActivity().showLoading();
                collectionViewModel.collectionDelete(userid, details_current_user, position);
                NormalLoginResponse.Details responseUser = new Gson().fromJson(collectionViewModel.getDataManager().getCurrentUserInfo(),
                        NormalLoginResponse.Details.class);

                Bundle bundle = new Bundle();

                if (control_redirection_from_restaurant_listing_to_collection_listing){
                    bundle.putString("resturantid",passed_detail_data.getRestaurent_id());
                    bundle.putString("resturantName",passed_detail_data.getRestaurent_id());
                }
                bundle.putString("userName",responseUser.getUserName());
                bundle.putString("userid",responseUser.getId());

                bundle.putString("collectionName",collectioRecyclerAdapter.getData().get(adapter_item_long_press_position).getCollection_name());
                bundle.putString("collectionId",collectioRecyclerAdapter.getData().get(adapter_item_long_press_position).getCollection_id());
//        bundle.putString("resturantrating",passed_detail_data.getRating());

                mFirebaseAnalytics.logEvent("collection_deleted_android", bundle);

            }
        });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showDialogForRestaurantAdditionToExistingCollection(final String userid,
                                                                     final SpecialPageRestuarantListingResponse.Detail passed_detail_data, final CollectionListingResponse.Details collection_details) {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Confirmation");
        builder.setMessage("Would you like to add the restaurant to this collection?");
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {








                getBaseActivity().showLoading();


                collectionViewModel.addRestaurantToExistingCollection(userid, passed_detail_data, collection_details);
            }
        });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @Override
    public void onSuccess(CollectionListingResponse collectionListingResponse) {
//        Log.d("checking_steps_response",": "+(++check));

        getBaseActivity().hideLoading();
        if (collectionListingResponse != null) {
            if (collectionListingResponse.getDetails() != null && collectionListingResponse.getDetails().size() > 0) {
                if (collectionListingResponse.getDetails().size() == AppConstants.LISTING_FETCH_LIMIT) {
                    pagination = pagination + 1;
                    hasLoadMore = true;
                } else {
                    hasLoadMore = false;
                }
                favouriteStatus=collectionListingResponse.getFavorite_status();
              //  detilCollect=collectionListingResponse.getDetails();
                collectioRecyclerAdapter.setAllData(collectionListingResponse.getDetails(),favouriteStatus);
                collectioRecyclerAdapter.setLoaded();
//                collectionAdapter.setData(collectionListingResponse.getDetails());
            } else {
                hasLoadMore = false;
            }
        }
        Log.d("check_response_frag", ": " + hasLoadMore);
//        collectionAdapter.showLoading(false);
    }

    @Override
    public void onSuccessAfterAdditonRestaurantToCollection(CommonSimpleResponse commonSimpleResponse) {
        control_redirection_from_restaurant_listing_to_collection_listing = false;
        getBaseActivity().hideLoading();
        if (commonSimpleResponse != null) {

            Toast.makeText(getActivity(), commonSimpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
            if (commonSimpleResponse.getStatus().trim().toLowerCase().equals("success")) {

            }
        }
    }

    @Override
    public void onSuccessAfterCollectionDelete(CommonSimpleResponse commonSimpleResponse, int position) {
        openCloseBottomSheet();
        getBaseActivity().hideLoading();
        if (commonSimpleResponse != null) {
            Toast.makeText(getActivity(), commonSimpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
            if (commonSimpleResponse.getStatus().trim().toLowerCase().equals("success")) {
                collectioRecyclerAdapter.clearParticularData(position);
            }
        }
    }

    @Override
    public void onError(Throwable throwable) {
        getBaseActivity().hideLoading();
        openCloseBottomSheet();
//        collectionAdapter.showLoading(false);
        Log.d("check_error", ": " + throwable.getMessage());
        Toast.makeText(getActivity(),throwable.getMessage(),Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailed(String message) {

        getBaseActivity().hideLoading();
        Toast.makeText(getActivity(),message,Toast.LENGTH_LONG).show();

    }

    @Override
    public void onAddSuccess() {
        getBaseActivity().hideLoading();
        Toast.makeText(getActivity(),"Successfully Added",Toast.LENGTH_LONG).show();

        NormalLoginResponse.Details responseUser = new Gson().fromJson(collectionViewModel.getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        Log.d("test","---------------------------");
        Bundle bundle = new Bundle();
        bundle.putString("userName",responseUser.getUserName());
        bundle.putString("userid",responseUser.getId());
        bundle.putString("resturantid",passed_detail_data.getRestaurent_id());
//        bundle.putString("collectionName",collectionName);
//        bundle.putString("collectionId",collectionid);
//        bundle.putString("resturantrating",passed_detail_data.getRating());
        bundle.putString("resturantName",passed_detail_data.getRestaurent_id());

        mFirebaseAnalytics.logEvent("restaurant_added_to_collections_android", bundle);


        control_redirection_from_restaurant_listing_to_collection_listing=false;
        alertDialog.dismiss();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_CODE) {
            check_for_listing_reload = false;
            control_redirection_from_restaurant_listing_to_collection_listing = false;
        }
    }


    private void initializeBottomSheetBehavior() {
        bottomSheetBehavior = BottomSheetBehavior.from(fragmentCollectionBinding.bottomSheetLayout);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        fragmentCollectionBinding.coverLayout.setVisibility(View.VISIBLE);
                        collectioRecyclerAdapter.showHideSelector(adapter_item_long_press_position);
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        fragmentCollectionBinding.coverLayout.setVisibility(View.GONE);
                        collectioRecyclerAdapter.showHideSelector(-1);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        fragmentCollectionBinding.tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCloseBottomSheet();
            }
        });

        fragmentCollectionBinding.coverLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCloseBottomSheet();
            }
        });

        fragmentCollectionBinding.tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCloseBottomSheet();
                getActivity().startActivityForResult(CreateCollectionActivity.newIntent(getActivity(),
                        new Gson().toJson(collectioRecyclerAdapter.getData().get(adapter_item_long_press_position)),
                        AppConstants.create_collection_page_for_update), REQUEST_CODE);
            }
        });

        fragmentCollectionBinding.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NormalLoginResponse.Details details_current_user = new Gson().fromJson(collectionViewModel.getDataManager().
                        getCurrentUserInfo(), NormalLoginResponse.Details.class);
                showDialogForCollectionDelete(details_current_user.getId(), collectioRecyclerAdapter.getData().get(adapter_item_long_press_position), adapter_item_long_press_position);

            }
        });

    }

    public void openCloseBottomSheet() {
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }


}
