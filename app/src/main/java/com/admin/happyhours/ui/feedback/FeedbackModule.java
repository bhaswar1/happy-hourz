package com.admin.happyhours.ui.feedback;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class FeedbackModule {

    @Provides
    FeedbackViewModel provideFeedbackViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new FeedbackViewModel(dataManager, schedulerProvider);
    }


}
