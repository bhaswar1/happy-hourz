package com.admin.happyhours.ui.main.favourites;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FavouritesProvider {

    @ContributesAndroidInjector(modules = FavouritesModule.class)
    abstract FavouritesFragment provideFavouritesFragmentFactory();


}
