package com.admin.happyhours.ui.resturantlocation;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class RasturantLocationModule {


    @Provides
    RasturantLocationViewModel provideSignupViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new RasturantLocationViewModel(dataManager, schedulerProvider);
    }

}
