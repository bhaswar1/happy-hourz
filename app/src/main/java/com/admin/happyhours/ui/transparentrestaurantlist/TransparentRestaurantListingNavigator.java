package com.admin.happyhours.ui.transparentrestaurantlist;

import com.admin.happyhours.data.model.api.response.RestaurantListingCollectionDetailsResponse;

public interface TransparentRestaurantListingNavigator {

    void onSuccess(RestaurantListingCollectionDetailsResponse response);

    void onError(Throwable throwable);

}
