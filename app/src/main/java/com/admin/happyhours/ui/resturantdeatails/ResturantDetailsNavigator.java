package com.admin.happyhours.ui.resturantdeatails;

import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.RestaurantDetailsData;
import com.admin.happyhours.data.model.api.response.ResturantsAllData;
import com.google.gson.JsonObject;

import org.json.JSONObject;
import org.json.JSONTokener;

public interface ResturantDetailsNavigator {
    void onError(Throwable throwable);

   // void onSuccess(RestaurantDetailsData restaurantDetailsData);

    void onSuccess(ResturantsAllData response,JsonObject jsonObject);

    void likeSucess(CommonSimpleResponse commonSimpleResponse);

    void noData();

    void responseAfterBookmarkOrLike();
}
