package com.admin.happyhours.ui.base.recyclerviewbaseadapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.admin.happyhours.R;


public class LoaderViewHolder extends RecyclerView.ViewHolder {

    ProgressBar mProgressBar;

    public LoaderViewHolder(View itemView) {
        super(itemView);
        mProgressBar = itemView.findViewById(R.id.progressbar);
    }
}
