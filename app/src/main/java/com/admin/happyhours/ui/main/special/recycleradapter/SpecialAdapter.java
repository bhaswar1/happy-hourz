package com.admin.happyhours.ui.main.special.recycleradapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.admin.happyhours.datatypecommon.EventbusForSpecialListingOptionClick;
import com.admin.happyhours.ui.resturantdeatails.ResturantDetailsActivity;
import com.admin.happyhours.utils.AppConstants;
import com.admin.happyhours.utils.GoogleMapHelper.HelperClassGoogleMap;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.kodmap.app.library.PopopDialogBuilder;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static android.widget.LinearLayout.VERTICAL;

public class SpecialAdapter extends RecyclerView.Adapter<SpecialAdapter.ViewHolder> {

    private Activity activity;
    public LinkedList<SpecialPageRestuarantListingResponse.Detail> details = new LinkedList<>();
    private int deals=0;
    public JsonArray jsonArray;
    public SpecialAdapter(Activity activity) {
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.special_list_row_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public void changeBoomarkValueOfParticularPosition(int position, int collectionstatus) {

        this.details.get(position).setCollectionStatus(collectionstatus);
        notifyItemChanged(position);
    }

    public void setRatingUpdate(int position, int rating) {
        this.details.get(position).setRating(Integer.toString(rating));
        notifyItemChanged(position);
    }

    public void setReviewUpdate(int position, int reveiew) {
        this.details.get(position).setTotalReview(reveiew);
        notifyItemChanged(position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView tv_hotelname,service_tv, tv_distance, tv_offerdata, tv_no_offer_available,tvCusion,tvTagFood,tvTagDrink,openTimeTv,totalReview,noPhotoTv;
        ImageView imv_share, imv_favourite, imv_bookmarks,firstImage,secondImage,thirdImage;
        RatingBar spaclist_ratingbar;
        RecyclerView offerrecyclerview;
        Offeradapter offeradapter;
        LinearLayout linearLayout;
      //  TextView ,tv

        LinearLayout llCusionList,llFoodList,llDrinkList;
        RecyclerView rvFoodList,rvCusionList,rvDrinkList;


        public ViewHolder(View itemView) {
            super(itemView);
            service_tv=itemView.findViewById(R.id.tag_service_tv);
            linearLayout =  itemView.findViewById(R.id.resturanRow);
            tv_hotelname =   itemView.findViewById(R.id.tv_hotelname);
            imv_share =   itemView.findViewById(R.id.imv_share);
            imv_bookmarks =  itemView.findViewById(R.id.imv_bookmarks);
            imv_favourite =  itemView.findViewById(R.id.imv_favourite);
            spaclist_ratingbar =  itemView.findViewById(R.id.spaclist_ratingbar);
            tv_distance =  itemView.findViewById(R.id.tv_distance);
            tv_offerdata =   itemView.findViewById(R.id.tv_offerdata);
            offerrecyclerview =   itemView.findViewById(R.id.offerrecyclerview);
            tv_no_offer_available = itemView.findViewById(R.id.tv_no_offer_available);
            offerrecyclerview.setLayoutManager(new LinearLayoutManager(activity));
            offerrecyclerview.setHasFixedSize(true);
            offerrecyclerview.setNestedScrollingEnabled(true);

            tvCusion=itemView.findViewById(R.id.cusion_tv);
            tvTagDrink=itemView.findViewById(R.id.tag_drink_tv);
            tvTagFood=itemView.findViewById(R.id.tag_food_tv);
            openTimeTv=itemView.findViewById(R.id.time_tv);
            totalReview=itemView.findViewById(R.id.totalReview_tv);
            firstImage=itemView.findViewById(R.id.firstimage);
            secondImage=itemView.findViewById(R.id.secondimage);
            thirdImage=itemView.findViewById(R.id.thirdimage);
            noPhotoTv=itemView.findViewById(R.id.noPhoto_tv);

            llCusionList=itemView.findViewById(R.id.llCusionList);
            llFoodList=itemView.findViewById(R.id.llFoodList);
            llDrinkList=itemView.findViewById(R.id.llDrinkList);

            rvFoodList=itemView.findViewById(R.id.rvFoodList);
            rvCusionList=itemView.findViewById(R.id.rvCusionList);
            rvDrinkList=itemView.findViewById(R.id.rvDrinkList);

            FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(activity);
            flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
            flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);
            //flexboxLayoutManager.setAlignItems(AlignItems.CENTER);

            //StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(5, LinearLayoutManager.VERTICAL);
            rvFoodList.setLayoutManager(flexboxLayoutManager);

            FlexboxLayoutManager flexboxLayoutManager2 = new FlexboxLayoutManager(activity);
            flexboxLayoutManager2.setFlexDirection(FlexDirection.ROW);
            flexboxLayoutManager2.setJustifyContent(JustifyContent.FLEX_START);
            rvCusionList.setLayoutManager(flexboxLayoutManager2);

            FlexboxLayoutManager flexboxLayoutManager3 = new FlexboxLayoutManager(activity);
            flexboxLayoutManager3.setFlexDirection(FlexDirection.ROW);
            flexboxLayoutManager3.setJustifyContent(JustifyContent.FLEX_START);
            rvDrinkList.setLayoutManager(flexboxLayoutManager3);

            offeradapter = new Offeradapter(activity);
            DividerItemDecoration decoration = new DividerItemDecoration(activity, VERTICAL);
            offerrecyclerview.addItemDecoration(decoration);

            offerrecyclerview.setAdapter(offeradapter);

            offerrecyclerview.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        offerrecyclerview.getParent().requestDisallowInterceptTouchEvent(false);
                    }
                    return false;

                }
            });
        }

        public void bind(final int position) {
            if (!details.get(position).getRestaurentName().isEmpty()) {
                String temp = details.get(position).getRestaurentName().trim();
                Character ch = temp.charAt(0);
                String rest_name = ch.toString().toUpperCase() + temp.substring(1);
                tv_hotelname.setText(rest_name);

            }
            LinkedList<String> images=new LinkedList<>();
            if(details.get(position).getOtherimages().size()>0){
                for(int i=0;i<details.get(position).getOtherimages().size();i++){
                    images.add(details.get(position).getOtherimages().get(i));
                }

            }
            if(details.get(position).getOtherimages().size()>0){
                Glide.with(activity)
                        .load(details.get(position).getOtherimages().get(0)).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image))
                        .into(firstImage);
                noPhotoTv.setVisibility(View.GONE);
             //   images.add(details.get(position).getCoverimage());


                if(details.get(position).getOtherimages().size()>1){
                    Glide.with(activity)
                            .load(details.get(position).getOtherimages().get(1)).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image))
                            .into(secondImage);
                   // images.add(details.get(position).getOtherimages().get(0));


                }else{
                    secondImage.setImageResource(android.R.color.transparent);
                    thirdImage.setImageResource(android.R.color.transparent);

                }

                    // thirdImage.setVisibility(View.GONE);


                if(details.get(position).getOtherimages().size()>2){
                    Glide.with(activity)
                            .load(details.get(position).getOtherimages().get(2)).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image))
                            .into(thirdImage);
                    details.get(position).getOtherimages().get(1);
                    //images.add(details.get(position).getOtherimages().get(1));


                }else{
                    thirdImage.setImageResource(android.R.color.transparent);

                }

                    // thirdImage.setVisibility(View.GONE);



            }else{

                noPhotoTv.setVisibility(View.VISIBLE);
                firstImage.setImageResource(android.R.color.transparent);
                secondImage.setImageResource(android.R.color.transparent);
                thirdImage.setImageResource(android.R.color.transparent);


            }



            secondImage.setOnClickListener(v -> {

                if(images.size()>1) {
                    Dialog dialog = new PopopDialogBuilder(activity)
                            // Set list like as option1 or option2 or option3
                            .setList(images, 1)
                            // or setList with initial position that like .setList(list,position)
                            // Set dialog header color
                            .setHeaderBackgroundColor(android.R.color.black)
                            // Set dialog background color
                            .setDialogBackgroundColor(R.color.black)
                            // Set close icon drawable

                            .setCloseDrawable(R.drawable.ic_close_white_24dp)
                            // Set loading view for pager image and preview image
                            .setLoadingView(R.layout.loader_item_layout)
                            // Set dialog style
                            // .setDialogStyle(R.style.DialogStyle)
                            // Choose selector type, indicator or thumbnail
                            .showThumbSlider(false)
                            // Set image scale type for slider image
                            .setSliderImageScaleType(ImageView.ScaleType.FIT_CENTER)
                            // Set indicator drawable
                            //  .setSelectorIndicator(R.drawable.selected_indicato)
                            // Enable or disable zoomable
                            .setIsZoomable(true)
                            // Build Km Slider Popup Dialog
                            .build();
//
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));

                    dialog.show();
                }

            });

            firstImage.setOnClickListener(v -> {





                if(images.size()>0) {

                    Dialog dialog = new PopopDialogBuilder(activity)
                            // Set list like as option1 or option2 or option3
                            .setList(images, 0)
                            // or setList with initial position that like .setList(list,position)
                            // Set dialog header color
                            .setHeaderBackgroundColor(android.R.color.black)
                            // Set dialog background color
                            .setDialogBackgroundColor(R.color.black)
                            // Set close icon drawable

                            .setCloseDrawable(R.drawable.ic_close_white_24dp)
                            // Set loading view for pager image and preview image
                            .setLoadingView(R.layout.loader_item_layout)
                            // Set dialog style
                            // .setDialogStyle(R.style.DialogStyle)
                            // Choose selector type, indicator or thumbnail
                            .showThumbSlider(false)
                            // Set image scale type for slider image
                            .setSliderImageScaleType(ImageView.ScaleType.FIT_CENTER)
                            // Set indicator drawable
                            //  .setSelectorIndicator(R.drawable.selected_indicato)
                            // Enable or disable zoomable
                            .setIsZoomable(true)
                            // Build Km Slider Popup Dialog
                            .build();

                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));

                    dialog.show();
                }

            });


            thirdImage.setOnClickListener(v -> {
                             if(images.size()>2) {

                                    Dialog dialog = new PopopDialogBuilder(activity)
                                            // Set list like as option1 or option2 or option3
                                            .setList(images, 2)
                                            // or setList with initial position that like .setList(list,position)
                                            // Set dialog header color
                                            .setHeaderBackgroundColor(android.R.color.black)
                                            // Set dialog background color
                                            .setDialogBackgroundColor(R.color.black)
                                            // Set close icon drawable

                                            .setCloseDrawable(R.drawable.ic_close_white_24dp)
                                            // Set loading view for pager image and preview image
                                            .setLoadingView(R.layout.loader_item_layout)
                                            // Set dialog style
                                            // .setDialogStyle(R.style.DialogStyle)
                                            // Choose selector type, indicator or thumbnail
                                            .showThumbSlider(false)
                                            // Set image scale type for slider image
                                            .setSliderImageScaleType(ImageView.ScaleType.FIT_CENTER)
                                            // Set indicator drawable
                                            //  .setSelectorIndicator(R.drawable.selected_indicato)
                                            // Enable or disable zoomable
                                            .setIsZoomable(true)
                                            // Build Km Slider Popup Dialog
                                            .build();

                                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));

                                    dialog.show();
                                }
            });

            if (deals == 1) {
                tv_offerdata.setText("Daily Special".toUpperCase());
            } else if (deals == 3) {
                tv_offerdata.setText("Social Hour".toUpperCase());
            } else {

                tv_offerdata.setText("Happy Hour".toUpperCase());
            }

            if (details.get(position).getCollectionStatus() == 1) {
//                Glide.with(imv_bookmarks.getContext()).load(activity.getResources().
//                        getDrawable(R.drawable.bookmark_logo_checked)).into(imv_bookmarks);
                imv_bookmarks.setImageResource(R.drawable.bookmark_logo_checked);
            } else {
//                Glide.with(imv_bookmarks.getContext()).load(activity.getResources().
//                        getDrawable(R.drawable.bookmark_logo)).into(imv_bookmarks);
                imv_bookmarks.setImageResource(R.drawable.bookmark_logo);
            }
            if (details.get(position).getFavouriteStatus() == 1) {
//                Glide.with(imv_favourite.getContext()).load(activity.getResources().
//                        getDrawable(R.drawable.like_logo_checked)).into(imv_favourite);
                imv_favourite.setImageResource(R.drawable.like_logo_checked);
            } else {
//                Glide.with(imv_favourite.getContext()).load(activity.getResources().
//                        getDrawable(R.drawable.like_logo)).into(imv_favourite);
                imv_favourite.setImageResource(R.drawable.like_logo);

            }
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String shareBody = new Gson().toJson(details.get(position));


                    Log.d("resturantid", "--" + details.get(position).getRestaurentId());
                    activity.startActivityForResult(new Intent(activity, ResturantDetailsActivity.class).
                            putExtra("lattitude", details.get(position).getLatitude()).
                            putExtra("longitude", details.get(position).getLongitude()).
                            putExtra("item_position", position).
                            putExtra("checklike", "notok").
                            putExtra("resturantid", details.get(position).getRestaurentId()).
                            putExtra("details", shareBody), 7);
                }
            });
            imv_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = new Gson().toJson(details.get(position));
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Restaurants");
                    //sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Check out offers from Daily Special, Happy Hour & Social Hour at "+details.get(position).getRestaurentName()+".  Click on the following link to download Happy Hourz - https://happyhourzapp.com/app/Restaurant_event_deeplink?restaurant_id=" + details.get(position).getRestaurentId());
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Check out this Happy Hour special at "+details.get(position).getRestaurentName()+"! Click on the following link:  https://happyhourzapp.com/app/Restaurant_event_deeplink?restaurant_id=" + details.get(position).getRestaurentId());
                    activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });
            imv_bookmarks.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new EventbusForSpecialListingOptionClick(AppConstants.adapter_img_bookmark, position, details.get(position)));
                }
            });
            imv_favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("fev","--"+ details.get(position).getRestaurentName());

                    EventBus.getDefault().post(new EventbusForSpecialListingOptionClick(AppConstants.adapter_img_like, position, details.get(position)));
                }
            });
            if (details.get(position).getRating() != null) {
                spaclist_ratingbar.setRating(Integer.parseInt(details.get(position).getRating()));
            }
            if (details.get(position).getDistance() != null) {

                if (details.get(position).getDistance() == 0 || details.get(position).getDistance() == 1) {
                    tv_distance.setText(String.valueOf(details.get(position).getDistance()) + " Mile Away");
                } else {
                    tv_distance.setText(String.valueOf(details.get(position).getDistance()) + " Miles Away");
                }
                strikeThroughText(tv_distance);
                tv_distance.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HelperClassGoogleMap.
                                showDirectionFromCurrentToDestinationPlaceInGoogleMapUsingLatiLong(activity,
                                        details.get(position).getLatitude(),
                                        details.get(position).getLongitude());
                    }
                });
            }
            //details.get(position).getExtra().getFood()!= null &&
//            details.get(position).getExtra().getDrinks()!= null
            JSONObject jo = new JSONObject();
            try {
                jo.put("extra", details.get(position));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObject j1 = jsonArray.get(position).getAsJsonObject();

            Log.d("testjson", "-----d--------" + new Gson().toJson(j1.get("extra")));
            JsonObject jsonObject1 = j1.getAsJsonObject("extra");

            JsonArray food = jsonObject1.getAsJsonArray("food");
            JsonArray drinks = jsonObject1.getAsJsonArray("gdrinks");

//            Details details1 = new Gson().fromJson(details.get(position).get("extra"),
//                    Details.class);
            Log.d("response_check", "--" + new Gson().toJson(details));
            //  Log.d("food","-------------"+new Gson().toJson(  details.get(position)));
//            (details.get(position).getExtra().getFood().size() > 0 )||( details.get(position).getExtra().getDrinks().size() > 0 )
            if(details.get(position).getCuisine()!=null && !details.get(position).getCuisine().isEmpty()){
                tvCusion.setVisibility(View.VISIBLE);



              //  wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLACK),0,8 ,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                String fooddata = details.get(position).getCuisine().substring(1, details.get(position).getCuisine().length() - 1);
                Character ch = fooddata.charAt(0);
                String fdata = ch.toString().toUpperCase() + fooddata.substring(1);

                Spannable wordtoSpan = new SpannableString(fdata);

//                wordtoSpan.setSpan(new StyleSpan(Typeface.BOLD),0,8,0);
//
//                wordtoSpan.setSpan(new RelativeSizeSpan(1f), 0,8, 0); // set size
//                wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 8, 0);


                Log.d("testdata","----------"+details.get(position).getCuisine());


                tvCusion.setText(details.get(position).getCuisine());

                /*ArrayList<String> list = new ArrayList<String>(Arrays.asList(details.get(position).getCuisine().split(",")));

                TagListAdapter tagListAdapter = new TagListAdapter(activity,list);
                rvCusionList.setAdapter(tagListAdapter);*/

                /*for (int i = 0; i < list.size(); i++) {
                    TextView tv = new TextView(activity); // Prepare textview object programmatically
                    tv.setTextColor(activity.getResources().getColor(R.color.black));
                    tv.setTextSize(16);
                    Typeface typeface = ResourcesCompat.getFont(activity, R.font.avenir);
                    tv.setTypeface(typeface);
                    tv.setBackground(activity.getDrawable(R.drawable.textview_tag_background));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.setMargins(5,0,5,0);
                    tv.setLayoutParams(params);
                    tv.setPadding(5,0,10,0);
                    tv.setText(list.get(i));
                    tv.setId(i + 5);
                    llCusionList.addView(tv); // Add to your ViewGroup using this method
                }*/

            }else{
                tvCusion.setVisibility(View.GONE);

            }

            if(details.get(position).getService()!=null && !details.get(position).getService().isEmpty()){
                service_tv.setVisibility(View.VISIBLE);
                service_tv.setText(details.get(position).getService());
            }else{
                service_tv.setVisibility(View.GONE);
            }
            if(details.get(position).getTotalReview()>1)
                 totalReview.setText(details.get(position).getTotalReview()+" Reviews");
            else
                totalReview.setText(details.get(position).getTotalReview()+" Review");

            if(!details.get(position).getOpenTime().isEmpty())
                    openTimeTv.setText(details.get(position).getOpenTime()+" to "+details.get(position).getCloseTime());

            if (food != null && drinks != null) {
                if (food.size() > 0 || drinks.size() > 0) {


//                    offerrecyclerview.setVisibility(View.VISIBLE);
//                    tv_no_offer_available.setVisibility(View.GONE);
//                    ArrayList<String> extradata = new ArrayList<>();
//                    //  Log.d("food","---"+details.get(position).getExtra().getFood().size());
//                    tv_offerdata.setVisibility(View.VISIBLE);
//
                    if (food.size() > 0) {
                        for (int i = 0; i < food.size(); i++) {
                            tvTagFood.setVisibility(View.GONE);

                         //   Spannable wordtoSpan = new SpannableString("Foods: ");


                      //      wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLACK),0,7 ,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                            String fooddata = food.get(i).toString().substring(1, food.get(i).toString().length() - 1);
                            Character ch = fooddata.charAt(0);
                            String fdata =ch.toString().toUpperCase() + fooddata.substring(1);

                            Spannable wordtoSpan = new SpannableString(fdata);
//
//                            wordtoSpan.setSpan(new StyleSpan(Typeface.BOLD),0,6,0);
//
//                            wordtoSpan.setSpan(new RelativeSizeSpan(1f), 0,6, 0); // set size
//                            wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 6, 0);
                            tvTagFood.setText(fdata);
                           // extradata.add(fdata);

                            ArrayList<String> list = new ArrayList<String>(Arrays.asList(food.get(i).toString().split(",")));

                            TagListAdapter tagListAdapter = new TagListAdapter(activity,list);
                            rvFoodList.setAdapter(tagListAdapter);

                            /*for (int j = 0; j < list.size(); j++) {
                                TextView tv = new TextView(activity); // Prepare textview object programmatically
                                tv.setTextColor(activity.getResources().getColor(R.color.black));
                                tv.setTextSize(16);
                                Typeface typeface = ResourcesCompat.getFont(activity, R.font.avenir);
                                tv.setTypeface(typeface);
                                tv.setBackground(activity.getDrawable(R.drawable.textview_tag_background));
                                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                params.setMargins(5,0,5,0);
                                tv.setLayoutParams(params);
                                tv.setPadding(5,0,10,0);
                                tv.setText(list.get(j).replace("\"",""));
                                tv.setId(j + 50);
                                llFoodList.addView(tv); // Add to your ViewGroup using this method
                            }*/
                        }
                    }else{
                        tvTagFood.setVisibility(View.GONE);

                    }
                    if (drinks.size() > 0) {

                    //    Spannable wordtoSpan = new SpannableString("Drinks: ");


               //         wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLACK),0,7 ,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        for (int i = 0; i < drinks.size(); i++) {
                            tvTagDrink.setVisibility(View.GONE);

                            String drinksdata = drinks.get(i).toString().substring(1, drinks.get(i).toString().length() - 1);
                            Character ch = drinksdata.charAt(0);
                            String fdata = ch.toString().toUpperCase() + drinksdata.substring(1);

                            Spannable wordtoSpan = new SpannableString(fdata);

//                            wordtoSpan.setSpan(new StyleSpan(Typeface.BOLD),0,7,0);
//
//                            wordtoSpan.setSpan(new RelativeSizeSpan(1f), 0,7, 0); // set size
//                            wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 7, 0);
                            tvTagDrink.setText(fdata);


                            //  extradata.add(fdata);


                            ArrayList<String> list = new ArrayList<String>(Arrays.asList(drinks.get(i).toString().split(",")));

                            TagListAdapter tagListAdapter = new TagListAdapter(activity,list);
                            rvDrinkList.setAdapter(tagListAdapter);

                            /*for (int j = 0; j < list.size(); j++) {
                                TextView tv = new TextView(activity); // Prepare textview object programmatically
                                tv.setTextColor(activity.getResources().getColor(R.color.black));
                                tv.setTextSize(16);
                                Typeface typeface = ResourcesCompat.getFont(activity, R.font.avenir);
                                tv.setTypeface(typeface);
                                tv.setBackground(activity.getDrawable(R.drawable.textview_tag_background));
                                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                params.setMargins(5,0,5,0);
                                tv.setLayoutParams(params);
                                tv.setPadding(5,0,10,0);
                                tv.setText(list.get(j).replace("\"",""));
                                tv.setId(j + 50);
                                llDrinkList.addView(tv); // Add to your ViewGroup using this method
                            }*/

                        }
                    }else {
                        tvTagDrink.setVisibility(View.GONE);
                    }
//
//                    offeradapter.addOfferData(extradata, food.size());
                } else {
                 //   tv_offerdata.setVisibility(View.GONE);
                    tvTagFood.setVisibility(View.GONE);
                    tvTagDrink.setVisibility(View.GONE);
                    if(details.get(position).getCuisine().isEmpty()){
                        tvCusion.setVisibility(View.VISIBLE);
                        tvCusion.setText("Refer to Menu.");

                    }

                    offerrecyclerview.setVisibility(View.GONE);
                //    tv_no_offer_available.setVisibility(View.VISIBLE);
//                tv_offerdata.setText("No Offer Available");
                }


//                tv_offerdata.setText("No offer available");
//            if (details.get(position).getOffers().size()==0){
//            }
            }
        }
    }

    public void changeValueOfParticularPosition(int position, int isChecked) {
        this.details.get(position).setFavouriteStatus(isChecked);
        notifyItemChanged(position);
    }

    public void addItems(List<SpecialPageRestuarantListingResponse.Detail> details, int deals1, JsonArray jsonArray) {
        this.details.clear();
        this.details.addAll(details);
        this.deals=deals1;
        this.jsonArray=jsonArray;

        Log.d("jsonarra","--"+new Gson().toJson(jsonArray));
        notifyDataSetChanged();


        Log.d("check_adap", ": " + details.size());
    }

    public void changeRowDataAfterBookmarkOrLike(String check_for_button, int position) {
        if (check_for_button.equals(AppConstants.adapter_img_bookmark)) {
            if (details.get(position).getCollectionStatus() == 0) {
                details.get(position).setCollectionStatus(1);
            } else {
                details.get(position).setCollectionStatus(0);
            }
        } else if (check_for_button.equals(AppConstants.adapter_img_like)) {
            if (details.get(position).getFavouriteStatus() == 0) {
                Toast.makeText(activity,activity.getResources().getString(R.string.add_to_favourite),Toast.LENGTH_LONG).show();

                details.get(position).setFavouriteStatus(1);
            } else {
                Toast.makeText(activity,activity.getResources().getString(R.string.removed_success_favourite),Toast.LENGTH_LONG).show();

                details.get(position).setFavouriteStatus(0);
            }
        }
        notifyItemChanged(position);
    }

    private void strikeThroughText(TextView price) {
        price.setPaintFlags(price.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }
}
