package com.admin.happyhours.ui.main.collections;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.CollectionListingResponse;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class CollectionViewModel extends BaseViewModel<CollectionNavigator> {


    public CollectionViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    void fetchCollectionListing(String user_id, int pageination, int limit_of_data_for_single_call) {

        Log.d("user_id", ": " + user_id);
        Log.d("user_pageination", ": " + pageination);
        Log.d("user_limit_of_data_for_single_call", ": " + limit_of_data_for_single_call);

        Disposable disposable = ApplicationClass.getRetrofitService()
                .collectionListing(user_id, pageination, limit_of_data_for_single_call)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<CollectionListingResponse>() {
                    @Override
                    public void accept(CollectionListingResponse response) throws Exception {
                        getNavigator().onSuccess(response);
                        if (response != null) {

                            Log.d("check_response", ": " + new Gson().toJson(response));

                        } else {
                            Log.d("check_response", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response", ": " + throwable.getMessage());
                        getNavigator().onError(throwable);
                    }
                });

        getCompositeDisposable().add(disposable);
    }

    void addRestaurantToExistingCollection(String user_id, SpecialPageRestuarantListingResponse.Detail passed_detail_data,
                                           CollectionListingResponse.Details collection_details) {

        Log.d("check", ": "+"addRestaurantToExistingCollection");


        Disposable disposable = ApplicationClass.getRetrofitService()
                .addRestaurantToCollection(user_id, passed_detail_data.getRestaurentId(), collection_details.getCollection_id())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<CommonSimpleResponse>() {
                    @Override
                    public void accept(CommonSimpleResponse response) throws Exception {

                        if (response != null) {
                            getNavigator().onSuccessAfterAdditonRestaurantToCollection(response);

                            Log.d("check_response", ": " + new Gson().toJson(response));

                        } else {
                            Log.d("check_response", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response", ": " + throwable.getMessage());
                        getNavigator().onError(throwable);
                    }
                });

        getCompositeDisposable().add(disposable);
    }

    void collectionDelete(String user_id, CollectionListingResponse.Details collection_details, final int position) {

        Log.e("===check===","1");

        if (ApplicationClass.getRetrofitService() == null){
            Log.e("===check===","6");
        }else {
            Log.e("===check===","7");
        }

        Disposable disposable = ApplicationClass.getRetrofitService()
                .deleteCollection(user_id, collection_details.getCollection_id())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<CommonSimpleResponse>() {
                    @Override
                    public void accept(CommonSimpleResponse response) throws Exception {
                        Log.e("===check===","2");

                        if (response != null) {
                            getNavigator().onSuccessAfterCollectionDelete(response, position);

                            Log.d("check_response", ": " + new Gson().toJson(response));

                        } else {
                            Log.d("check_response", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e("===check===","3");
                        Log.d("check_response", ": " + throwable.getMessage());
                        getNavigator().onError(throwable);
                    }
                });

        getCompositeDisposable().add(disposable);
    }
    public void addSpecialtoCollection(String id,String r_id,String c_id) {

        Log.d("check", ": "+"addSpecialtoCollection");
        Log.d("checkdata","uid--"+id);
        Log.d("checkdata","rid--"+r_id);
        Log.d("checkdata","cid-"+c_id);

        Disposable disposable = ApplicationClass.getRetrofitService()
                .addRestaurantToCollection(id,r_id,c_id)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<CommonSimpleResponse>() {
                    @Override
                    public void accept(CommonSimpleResponse response) throws Exception {
                        if (response != null && response.getStatus().toLowerCase().equals("success") ) {
                            if(response.getStatus().toLowerCase().equals("success"));
                          //  getNavigator().changepasswordSucccces();
                            getNavigator().onAddSuccess();
                        }else{
                            // getNavigator().noData();
                         //   getNavigator().unSuccess();

                            getNavigator().onFailed(response.getMessage());

                        }




                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response","---"+throwable.toString());
                        getNavigator().onError(throwable);

                    }
                });

        getCompositeDisposable().add(disposable);

    }

}
