package com.admin.happyhours.ui.feedback;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import java.io.File;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class FeedbackViewModel extends BaseViewModel<FeedbackNavigator> {



    public FeedbackViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public void sendFeedback(String message, File collectionimage) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);

        MultipartBody.Part body = null;
        if (collectionimage != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), collectionimage);
            body = MultipartBody.Part.createFormData("otherimage", collectionimage.getName(), reqFile);
        }
        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), responseUser.getId());

        RequestBody messageData = RequestBody.create(MediaType.parse("text/plain"), message);






        Disposable disposable = ApplicationClass.getRetrofitService()
                .feedbackResponse(user_id, messageData,body)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<CommonSimpleResponse>() {
                    @Override
                    public void accept(CommonSimpleResponse response) throws Exception {
                        if (response != null) {
                            Log.d("check_response", ": " + response.getMessage());
                            getNavigator().onSuccess(response);

                        } else {
                            Log.d("check_response", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response", ": " + throwable.getMessage());
                        getNavigator().onError(throwable);
                    }
                });

        getCompositeDisposable().add(disposable);
    }
}
