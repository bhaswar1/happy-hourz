package com.admin.happyhours.ui.main.notification.recycleradapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.NotificationListingResponse;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.admin.happyhours.ui.eventdetails.EventDetailActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.LinkedList;

public class RecyclerViewAdapter extends RecyclerView.Adapter {

    public boolean isLoading;
    private final int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private Context context;
    private Activity activity;
    private OnLoadMoreListener mOnLoadMoreListener;
    private OnItemClickListener callback = null;
    private LinkedList<NotificationListingResponse.Details> details = new LinkedList<>();

    public RecyclerViewAdapter(Context context, LinkedList<NotificationListingResponse.Details> details,
                               RecyclerView recyclerView, final LinearLayoutManager linearLayoutManager,Activity activity1) {
        this.context = context;
        this.details = details;
        this.activity=activity1;
//        this.details.add(new CollectionListingResponse.Details());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.single_item_row_notification, parent, false));


        return new Viewholder(LayoutInflater.from(context).inflate(R.layout.single_item_row_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        // Loader ViewHolder
        if (holder instanceof Viewholder) {
            Viewholder viewholder = (Viewholder) holder;

            viewholder.onBind(position);
        }
    }

    @Override
    public int getItemCount() {

        return details.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void clearData() {
        details.clear();
        notifyDataSetChanged();
    }

    public LinkedList<NotificationListingResponse.Details> getData() {
        return details;
    }

    public void setAllData(LinkedList<NotificationListingResponse.Details> data) {
        details.addAll(data);
        notifyDataSetChanged();
    }

    public void setData(NotificationListingResponse.Details detail) {
        details.add(detail);
        notifyDataSetChanged();
    }

    public void setLoaded() {
        isLoading = false;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView tv_title, tv_description;
        ImageView resturantimage;
        ConstraintLayout constraintLayout;

        public Viewholder(View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_title);
            tv_description = itemView.findViewById(R.id.tv_description);
            resturantimage = itemView.findViewById(R.id.resturantimage);
            constraintLayout =itemView.findViewById(R.id.notificatinConstraint);
        }

        public void onBind(final int position) {
            String temp=details.get(position).getEvent_name();
            Character ch=temp.charAt(0);
            String title=ch.toString().toUpperCase()+temp.substring(1);

            tv_title.setText(title);
            tv_description.setText(details.get(position).getEvent_desc());
            Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.placeholder_image).
                    error(R.drawable.placeholder_image)).load(details.get(position).getEvent_image()).
                    into(resturantimage);
            constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.startActivity(new Intent(activity, EventDetailActivity.class).
                            putExtra("eventId",details.get(position).getEvent_id()).
                            putExtra("favourite_status","0").
                            putExtra("position",Integer.toString(position)));

                }
            });
        }
    }

    /*public class LoaderViewHolder extends RecyclerView.ViewHolder {

        ProgressBar mProgressBar;

        public LoaderViewHolder(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.progressbar);
        }
    }*/


}
