package com.admin.happyhours.ui.reviewlist;

import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.ReviewListDetail;
import com.admin.happyhours.data.model.api.response.ReviewListResponse;

public interface ReviewListNavigator {
    void success(ReviewListResponse detail);

    void updatePushSuccess(CommonSimpleResponse response);

    void onError(Throwable throwable);

    void updatePasswordSuccess(CommonSimpleResponse response);

    void noLoad();

    void noData();
}
