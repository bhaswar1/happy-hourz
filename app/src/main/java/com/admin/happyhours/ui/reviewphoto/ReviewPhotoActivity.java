package com.admin.happyhours.ui.reviewphoto;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CollectionListingResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.ReviewPhotoDetail;
import com.admin.happyhours.data.model.api.response.ReviewPhotoResponse;
import com.admin.happyhours.databinding.ActivityReviewPhotoBinding;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.admin.happyhours.ui.base.BaseActivity;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.gson.Gson;

import java.util.LinkedList;

import javax.inject.Inject;

public class ReviewPhotoActivity extends BaseActivity<ActivityReviewPhotoBinding, ReviewPhotoViewModel> implements ReviewPhotoNavigator {

    private String TAG = ReviewPhotoActivity.this.getClass().getSimpleName();
    ActivityReviewPhotoBinding activityReviewPhotoBinding;
    @Inject
    ReviewPhotoViewModel reviewPhotoViewModel;
    String gender = "";
    DatePickerDialog datePickerDialog;
    SupportMapFragment mapFragment;
    ReviewPhotoAdapter reviewPhotoAdapter;
    private boolean hasLoadMore = false;
    int pagination=1;
    public static Intent newIntent(Context context) {
        return new Intent(context, ReviewPhotoActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_review_photo;
    }

    @Override
    public ReviewPhotoViewModel getViewModel() {
        return reviewPhotoViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStatusBarHidden(ReviewPhotoActivity.this);
        reviewPhotoViewModel.setNavigator(this);
        activityReviewPhotoBinding = getViewDataBinding();
        Toolbar mtoolbar=activityReviewPhotoBinding.toolbar;
        mtoolbar.setNavigationIcon(R.drawable.back_arrow_black);
        mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        if(getIntent().hasExtra("restaurantid")) {

        //    getSupportActionBar().setDisplayShowTitleEnabled(false);
            if (reviewPhotoViewModel.getDataManager().getCurrentUserInfo() != null) {
                showLoading();
                NormalLoginResponse.Details details = new Gson().fromJson(reviewPhotoViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                reviewPhotoViewModel.setReviewPhotoLIst(details.getId(), getIntent().getStringExtra("restaurantid"), 15, pagination, false);
            }
        }


        setRecyclerView();


    }

    private void setRecyclerView() {
        LinkedList<ReviewPhotoDetail> list=new LinkedList<>();

        GridLayoutManager gridLayoutManager = new GridLayoutManager(ReviewPhotoActivity.this, 3);
        activityReviewPhotoBinding.recyclerView.setLayoutManager(gridLayoutManager);
        LinkedList<CollectionListingResponse.Details> details = new LinkedList<>();
        reviewPhotoAdapter = new ReviewPhotoAdapter(ReviewPhotoActivity.this, list,
                activityReviewPhotoBinding.recyclerView, gridLayoutManager);
        activityReviewPhotoBinding.recyclerView.setAdapter(reviewPhotoAdapter);
        reviewPhotoAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
//                Log.d("checking_steps_loadmore",": "+(++check));
                Log.d("check_bools", ": " + hasLoadMore);
                if (hasLoadMore) {
                    showLoading();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadrestaurantData();
                        }
                    }, 1000);
                }
            }
        });

    }
public void   loadrestaurantData(){
    if (reviewPhotoViewModel.getDataManager().getCurrentUserInfo() != null) {
        showLoading();
        NormalLoginResponse.Details details = new Gson().fromJson(reviewPhotoViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
        reviewPhotoViewModel.setReviewPhotoLIst(details.getId(),getIntent().getStringExtra("restaurantid"),15,pagination,true);
    }

}

    @Override
    public void onSuccess(NormalLoginResponse normalLoginResponse) {
    }

    @Override
    public void onError(Throwable throwable) {
        hideLoading();
        Log.d(TAG,":-ERROR:-  "+throwable.getMessage());
    }

    @Override
    public void success(ReviewPhotoResponse response) {
        hideLoading();
        {
            hideLoading();
            if(response!=null){
                if (response.getDetails().size() == 15) {
                    pagination = pagination + 1;
                    hasLoadMore = true;
                } else {
                    hasLoadMore = false;
                }

                //fevouriteListAdapter.clearData();
                //
                LinkedList<ReviewPhotoDetail> reviewPhotoDetails=new LinkedList<>();
                reviewPhotoDetails.addAll( response.getDetails());
                reviewPhotoAdapter.setAllData(reviewPhotoDetails);

                reviewPhotoAdapter.setLoaded();
//                collectionAdapter.setData(collectionListingResponse.getDetails());
            } else {
                hasLoadMore = false;
            }
            //  LinkedList<ReviewListDetail> detail1=new LinkedList<>();
            // reviewListAdapter.setAllData();

        }

    }

    @Override
    public void noData() {
hideLoading();
    }

    @Override
    public void noLoad() {
hideLoading();
    }
}
