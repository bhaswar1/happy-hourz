package com.admin.happyhours.ui.splash;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;

public class SplashViewModel extends BaseViewModel<SplashNavigator> {


    public SplashViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

}
