package com.admin.happyhours.ui.reviewlist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.ReviewListDetail;
import com.admin.happyhours.data.model.api.response.ReviewListResponse;
import com.admin.happyhours.databinding.ActivityReviewListBinding;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.utils.AppConstants;
import com.google.gson.Gson;

import java.util.LinkedList;

import javax.inject.Inject;

import static android.widget.LinearLayout.VERTICAL;

public class ReviewListActivity extends BaseActivity<ActivityReviewListBinding, ReviewListViewModel> implements ReviewListNavigator {


    ActivityReviewListBinding activityReviewListBinding;
    @Inject
    ReviewListViewModel reviewListViewModel;
   private String password;
    AlertDialog alertDialog;
    private int pagination = 1;
    private boolean hasLoadMore = false;
    String newpass;
    ReviewListAdapter reviewListAdapter;

    public static Intent newIntent(Context context) {
        return new Intent(context, ReviewListActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_review_list;
    }

    @Override
    public ReviewListViewModel getViewModel() {
        return reviewListViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityReviewListBinding = getViewDataBinding();
        reviewListViewModel.setNavigator(this);
        setStatusBarHidden(ReviewListActivity.this);
        Toolbar mtoolbar=activityReviewListBinding.toolbar;
        setSupportActionBar(mtoolbar);
        mtoolbar.setNavigationIcon(R.drawable.back_arrow_black);
        mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if(getIntent().hasExtra("restaurantid")) {

            getSupportActionBar().setDisplayShowTitleEnabled(false);
            if (reviewListViewModel.getDataManager().getCurrentUserInfo() != null) {
                showLoading();
                NormalLoginResponse.Details details = new Gson().fromJson(reviewListViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                reviewListViewModel.setReviewLIst(details.getId(), getIntent().getStringExtra("restaurantid"), AppConstants.LISTING_FETCH_LIMIT, pagination, false);
            }
        }

setRecyclerView();


    }
    private void setRecyclerView() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ReviewListActivity.this);
        activityReviewListBinding.reviewlist.setLayoutManager(linearLayoutManager);
        //  fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(new SpacesItemDecoration(ViewUtils.dpToPx(2)));
        LinkedList<ReviewListDetail> details = new LinkedList<>();
        reviewListAdapter = new ReviewListAdapter(ReviewListActivity.this, details, activityReviewListBinding.reviewlist, linearLayoutManager);
        DividerItemDecoration decoration = new DividerItemDecoration(ReviewListActivity.this, VERTICAL);
        activityReviewListBinding.reviewlist.addItemDecoration(decoration);
        // fragmentSearchBinding.searchRecycle.setAdapter(fevouriteListAdapter);
        activityReviewListBinding.reviewlist.setAdapter(reviewListAdapter);
        reviewListAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
//                Log.d("checking_steps_loadmore",": "+(++check));
                Log.d("check_bools", ": " + hasLoadMore);
                if (hasLoadMore) {
                   showLoading();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadrestaurantData();
                        }
                    }, 1000);
                }
            }
        });
    }
    public  void  loadrestaurantData(){
        if (reviewListViewModel.getDataManager().getCurrentUserInfo() != null) {
            showLoading();
            NormalLoginResponse.Details details = new Gson().fromJson(reviewListViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
            reviewListViewModel.setReviewLIst(details.getId(),getIntent().getStringExtra("restaurantid"),AppConstants.LISTING_FETCH_LIMIT,pagination,true);
        }

    }

    @Override
    public void success(ReviewListResponse reviewListResponse) {
        hideLoading();
    if(reviewListResponse!=null){
        if (reviewListResponse.getDetails().size() == AppConstants.LISTING_FETCH_LIMIT) {
            pagination = pagination + 1;
            hasLoadMore = true;
        } else {
            hasLoadMore = false;
        }

        //fevouriteListAdapter.clearData();
        //
        reviewListAdapter.setAllData(reviewListResponse.getDetails());
        reviewListAdapter.setLoaded();
//                collectionAdapter.setData(collectionListingResponse.getDetails());
    } else {
        hasLoadMore = false;
    }
      //  LinkedList<ReviewListDetail> detail1=new LinkedList<>();
    // reviewListAdapter.setAllData();

    }


    @Override
    public void updatePushSuccess(CommonSimpleResponse response) {

    }

    @Override
    public void onError(Throwable throwable) {
hideLoading();
    }

    @Override
    public void updatePasswordSuccess(CommonSimpleResponse response) {

    }

    @Override
    public void noLoad() {

        hideLoading();
    }

    @Override
    public void noData() {
        hideLoading();
        activityReviewListBinding.reviewlist.setVisibility(View.GONE);
        activityReviewListBinding.nodataTv.setVisibility(View.VISIBLE);
    }
}
