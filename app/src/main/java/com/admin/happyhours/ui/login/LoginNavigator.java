package com.admin.happyhours.ui.login;

import com.admin.happyhours.data.model.api.response.NormalLoginResponse;

public interface LoginNavigator {

    void onSuccess(NormalLoginResponse normalLoginResponse);

    void onError(Throwable throwable);

    /*void fbSignupRequired();

    void googleSignupRequired();*/

}
