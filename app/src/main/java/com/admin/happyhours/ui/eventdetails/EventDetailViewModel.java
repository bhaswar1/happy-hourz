package com.admin.happyhours.ui.eventdetails;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.Details;
import com.admin.happyhours.data.model.api.response.EventDetail;
import com.admin.happyhours.data.model.api.response.EventDetailResponse;
import com.admin.happyhours.data.model.api.response.EventDetails;
import com.admin.happyhours.data.model.api.response.EventListingResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class EventDetailViewModel extends BaseViewModel<EventDetailNavigator> {


    public EventDetailViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
    public void eventDetails(String uid,String eventId) {
        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("uid", ": " + uid);


            Disposable disposable = ApplicationClass.getRetrofitService()
                    .eventDetails(uid,eventId)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<JsonObject>() {
                        @Override
                        public void accept(JsonObject response) throws Exception {
//                            if (response != null && response.getStatus().toLowerCase().equals("success") && response.getDetails()!=null ) {
//                                Log.d("check_response","---"+new Gson().toJson(response.getDetails()));
//                                getNavigator().onSuccess(response.getDetails());
//
//                            }else{
//                                    getNavigator().noData();
//                            }
                        if(response !=null ) {
                            EventDetails details = new Gson().fromJson(response.get("Details"),
                                    EventDetails.class);
                            Log.d("response_check", "--" + new Gson().toJson(details));
                            getNavigator().onSuccess(details);
                        }


                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response","---"+throwable.toString());
                            getNavigator().onError(throwable);

                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }
    void apiHitLikeUnlike(String uid,String eventId) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("check_button", ": " + uid);
            Log.d("check_adapter_uid", ": " + responseUser.getId());
            Log.d("eventId", ": " + eventId);

            Disposable disposable = ApplicationClass.getRetrofitService()
                    .eventLikeUnlike(uid,eventId)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<CommonSimpleResponse>() {
                        @Override
                        public void accept(CommonSimpleResponse response) throws Exception {
                            if (response != null) {

                                if (response.getStatus() != null && response.getStatus().trim().toLowerCase().equals("success")) {
                                    getNavigator().likeUnlike(response);
                                }

                            } else {
                                Log.d("check_response", ": null response");
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response", ": " + throwable.getMessage());
                            getNavigator().onError(throwable);
                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }


}
