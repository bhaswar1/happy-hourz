package com.admin.happyhours.ui.main.myaccount;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.DrawableCompat;

import android.os.StrictMode;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.AccountResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.ProfileImageUpdateResponse;
import com.admin.happyhours.databinding.FragmentAccountBinding;
import com.admin.happyhours.interfaces.HomeActivityEditSaveOptionMenuClick;
import com.admin.happyhours.ui.base.BaseFragment;
import com.admin.happyhours.ui.main.HomeActivity;
import com.admin.happyhours.utils.PermissionUtils;
import com.admin.happyhours.utils.RealPath;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;


public class MyAccountFragment extends BaseFragment<FragmentAccountBinding, MyAccountViewModel> implements MyAccountNavigator, HomeActivityEditSaveOptionMenuClick {

    FragmentAccountBinding fragmentAccountBinding;
    MyAccountViewModel myAccountViewModel;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    String userName;
    PermissionUtils permissionUtils;
    ArrayList<String> permissions=new ArrayList();
    Uri mCropImageUrifront;
    String customer_image;
    RealPath realPath;
    private String selectedImagePath="";
    public int i=0;
    String membername;
    boolean imageclick=false;
    private File file_cache_dir;
    String generValue;
   public int keyDel;
    public  String mobileNo=null;
    String dob,date,year,month;
    private boolean isFormatting;
    private boolean deletingHyphen;
    private int hyphenStart;
    private boolean deletingBackward;
    public final String[] MONTHS = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private static final int PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 0;
    private static final int PERMISSIONS_REQUEST_CAMERA = 1;
    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 2;

    DatePickerDialog datePickerDialog;

    public static MyAccountFragment newInstance() {
        Bundle args = new Bundle();
        MyAccountFragment fragment = new MyAccountFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_account;
    }

    @Override
    public MyAccountViewModel getViewModel() {
        myAccountViewModel = ViewModelProviders.of(this, mViewModelFactory).get(MyAccountViewModel.class);
        return myAccountViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myAccountViewModel.setNavigator(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

       // fragmentAccountBinding.phoneNoData.addTextChangedListener(new PhoneNumberFormattingTextWatcher("ISO 3166-2:US"));





  //
      }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentAccountBinding = getViewDataBinding();
        getBaseActivity().getViewModel();
        if (myAccountViewModel.getDataManager().getCurrentUserInfo() != null) {
            getBaseActivity().showLoading();
            NormalLoginResponse.Details details = new Gson().fromJson(myAccountViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
            myAccountViewModel.setAcountDetail(details.getId());
            ((HomeActivity)getActivity()).changeOptionMenuTextToEditAgain();

        }
//        PhoneNumberUtils.formatNumber(fragmentAccountBinding.phoneNoData.getText().toString(),
//                "ISO 3166-2:US");
       // DrawableCompat.setTint(batter)

        fragmentAccountBinding.phoneNoData.addTextChangedListener(new PhoneNumberFormattingTextWatcher("ISO 3166-2:US"));

        //DrawableComp;
        fragmentAccountBinding.addressData.setEnabled(false);
            fragmentAccountBinding.unameData.setEnabled(false);
            fragmentAccountBinding.phoneNoData.setEnabled(false);
            fragmentAccountBinding.genderValue.setEnabled(false);
            fragmentAccountBinding.dateofbirthData.setEnabled(false);
            if(myAccountViewModel.getDataManager().getPassword()==null){
                fragmentAccountBinding.userImage.setEnabled(false);
            }

          //  permissionUtils=new PermissionUtils(getActivity(),get);;
            realPath=new RealPath();

            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissions.add(Manifest.permission.CAMERA);

            //text view scrolling
     //  fragmentAccountBinding.emailid.setSelected(true);
        //end
      //  fragmentAccountBinding.emailid.setMovementMethod(new ScrollingMovementMethod());
        fragmentAccountBinding.userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("Entering","1");

                Dexter.withContext(requireActivity())
                        .withPermissions(
                                Manifest.permission.CAMERA
                                ,Manifest.permission.READ_EXTERNAL_STORAGE
                                ,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {

                                chooseImage();

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {

                                permissionToken.continuePermissionRequest();

                            }
                        }).check();



                if(file_cache_dir!=null){

                }
                   //permissionUtils.check_permission(permissions,"permisiion required",1);

            }
        });
            setMaleFemale();
            datePicker();

        ((HomeActivity)getActivity()).setListenerForHomeActivityEditSaveOptionMenuClick(this);

    }

    private void datePicker() {
        fragmentAccountBinding.calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                     // fragmentAccountBinding .dateofbirthData.setText( MONTHS[ (monthOfYear)] +" " + dayOfMonth+" "+year);
                                if ((monthOfYear + 1)<10){
                                            dob=year+"-0"+(monthOfYear+1)+"-"+dayOfMonth;

                                    fragmentAccountBinding .dateofbirthData.setText(dayOfMonth + "/0" +(monthOfYear + 1)+ "/" + year);
                                }else {
                                    dob=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
                                    fragmentAccountBinding.dateofbirthData.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                }
                            }
                        }, year, month, day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

                datePickerDialog.show();

            }
        });

    }

    private void setMaleFemale() {
        fragmentAccountBinding.male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentAccountBinding.genderValue.setText("Male");
                fragmentAccountBinding.male.setBackgroundResource(R.drawable.textbackgroundchoose);
                fragmentAccountBinding.female.setBackgroundResource(R.drawable.textviewbackground);
                fragmentAccountBinding.gender.setError(null);
            }
        });
        fragmentAccountBinding.female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentAccountBinding.genderValue.setText("Female");
                fragmentAccountBinding.female.setBackgroundResource(R.drawable.textbackgroundchoose)
                ;
                fragmentAccountBinding.gender.setError(null);
                fragmentAccountBinding.male.setBackgroundResource(R.drawable.textviewbackground);
            }
        });

    }


    private void chooseImage() {

        Log.d("Entering","2");

        /*CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setMultiTouchEnabled(true)
                .setAspectRatio(1,1)
                .setActivityTitle("Crop")
                .start(getBaseActivity());*/

        ImagePicker.with(this)
                .crop()	    			//Crop image(Optional), Check Customization for more option
                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                .start();
    }

    @Override
    public void onSuccess(AccountResponse response) {
            getBaseActivity().hideLoading();
            onResume();
            fragmentAccountBinding.unameData.setText(response.getName().trim());
            fragmentAccountBinding.addressData.setText(response.getAddress());
            if(!response.getGender().isEmpty())
            fragmentAccountBinding.genderValue.setText(response.getGender());
            else
                generValue="Male";
        userName=response.getName().trim();
        mobileNo=response.getPhone().trim();
           // fragmentAccountBinding.dateofbirthData.setText(response.);
        if(response.getPhone()!=null&& !response.getPhone().isEmpty()) {
           // String number = response.getPhone().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");

            fragmentAccountBinding.phoneNoData.setText(response.getPhone());
        }
        fragmentAccountBinding.emailid.setText(response.getEmail());
//        Glide.with(getBaseActivity())
//                .load(Uri.parse(response.getProfileImage()))
//
//                 .apply(RequestOptions.circleCropTransform())
//
//                .into(fragmentAccountBinding.userImage);
        if(response.getProfileImage()!=null && response.getProfileImage().length()!=0) {

//            Bitmap bitmap=StringToBitMap(response.getProfileImage());
//            Bitmap.createScaledBitmap(bitmap, 200, 200, true);

            Glide.with(getActivity()).load(response.getProfileImage()).apply(RequestOptions.circleCropTransform().placeholder(R.drawable.profile_image).error(R.drawable.profile_image)).into(fragmentAccountBinding.userImage);
        }else{
            NormalLoginResponse.Details details = new Gson().fromJson(myAccountViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

            Glide.with(getActivity()).load(details.getProfileImage()).apply(RequestOptions.circleCropTransform().placeholder(R.drawable.profile_image).error(R.drawable.profile_image)).into(fragmentAccountBinding.userImage);

        }
        dob=response.getEventYear()+"-"+response.getEventMonth()+"-"+response.getEventDay();
        if(response.getEventMonth()!=null&&!response.getEventMonth().isEmpty())
        fragmentAccountBinding.dateofbirthData.setText(response.getEventDay()+"/"+response.getEventMonth()+"/"+response.getEventYear());



        NormalLoginResponse.Details details = new Gson().fromJson(myAccountViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);


//        Bundle bundle = new Bundle();
//        bundle.putString("userName",details.getUserName());
//        bundle.putString("userid",details.getId());
//
//        mFirebaseAnalytics.logEvent("restaurant_added_to_collections_android", bundle);



    }

    @Override
    public void success(ProfileImageUpdateResponse profileImageUpdateResponse) {

        getBaseActivity().hideLoading();
     //  Glide.with(getActivity()).load(file_cache_dir).apply(RequestOptions.circleCropTransform()).into(fragmentAccountBinding.userImage);

       // Bitmap bitmap = BitmapFactory.decodeFile(file_cache_dir.toString());
      //  Bitmap.createScaledBitmap(bitmap, 200, 200, true);


        Glide.with(getActivity()).load(file_cache_dir).apply(RequestOptions.circleCropTransform().placeholder(R.drawable.profile_image).error(R.drawable.profile_image)).into(fragmentAccountBinding.userImage);

        myAccountViewModel.getDataManager().getCurrentUserId();
        NormalLoginResponse.Details responseUser = new Gson().fromJson(myAccountViewModel.
                getDataManager().getCurrentUserInfo(),NormalLoginResponse.Details.class);
        responseUser.setProfileImage(profileImageUpdateResponse.getProfile_image());
       // Log.d("username","--"+response.getName());
      //  responseUser.setAddress (response.getAddress());
        myAccountViewModel.getDataManager().setCurrentUserInfo(new Gson().toJson(responseUser));
        ( (HomeActivity)getActivity()).updateNavigationDrawerprofile();
    }

    @Override
    public void updateProfileSuccess(AccountResponse response) {
            getBaseActivity().hideLoading();
        fragmentAccountBinding.phoneNoData.setEnabled(false);
            Toast.makeText(getActivity(),getString(R.string.updated),Toast.LENGTH_LONG).show();
        fragmentAccountBinding.male.setVisibility(View.GONE);
        fragmentAccountBinding.female.setVisibility(View.GONE);
        fragmentAccountBinding.genderValue.setVisibility(View.VISIBLE);
        fragmentAccountBinding.genderValue.setText(generValue);
        fragmentAccountBinding.calender.setVisibility(View.GONE);
        myAccountViewModel.getDataManager().getCurrentUserId();
        NormalLoginResponse.Details responseUser = new Gson().fromJson(myAccountViewModel.
                        getDataManager().getCurrentUserInfo(),NormalLoginResponse.Details.class);
        responseUser.setName(response.getName());
        Log.d("username","--"+response.getName());
        responseUser.setAddress (response.getAddress());
       // String number = response.getPhone().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");

        fragmentAccountBinding.phoneNoData.setText(response.getPhone());
        myAccountViewModel.getDataManager().setCurrentUserInfo(new Gson().toJson(responseUser));
        ( (HomeActivity)getActivity()).updateNavigationDrawerprofile();
        ((HomeActivity)getActivity()).changeOptionMenuTextToEditAgain();
    }

    @Override
    public void noData() {
            getBaseActivity().hideLoading();
    }

    @Override
    public void phoneExist() {
            Toast.makeText(getActivity(),getString(R.string.already_exist_phone),Toast.LENGTH_LONG).show();
    }


    @Override
    public void onSave() {

        String user=fragmentAccountBinding.unameData.getText().toString().trim();
        String useraddres=fragmentAccountBinding.addressData.getText().toString().trim();
         generValue=fragmentAccountBinding.genderValue.getText().toString().trim();
        String dateOfBirth=fragmentAccountBinding.dateofbirthData.getText().toString().trim();
        String phoneNo=fragmentAccountBinding.phoneNoData.getText().toString().trim();

        Log.d("phoneno","--"+phoneNo.length());
        if(phoneNo.length()!=14) {
            phoneNo="12233";

        }else{
            phoneNo = phoneNo.substring(1, 4) + phoneNo.substring(6, 9) + phoneNo.substring(10, 14);
        }

        Log.d("phoneno","--"+phoneNo);

//    if(!dateOfBirth.isEmpty()&& dateOfBirth!=null ) {
//        int temp = dateOfBirth.indexOf("/");
//        String temp1 = dateOfBirth.substring(temp).trim();
//        int p = temp1.indexOf("/");
//        date = temp1.substring(0, p);
//        year = temp1.substring(p);
//        month = null;
//
//
//        for (i = 0; i <= 11; i++) {
//            if (dateOfBirth.substring(0, temp).equals(MONTHS[i])) {
//                Log.d("date", "--" + MONTHS[i] + "--" + (i + 1));
//                if (i < 9) {
//                    month = "0" + Integer.toString(i + 1);
//                } else {
//                    month = Integer.toString(i + 1);
//                }
//                break;
//            }
//        }
//    }

        if(user.length()==0 ){
            fragmentAccountBinding.unameData.requestFocus();
            fragmentAccountBinding.unameData.setError("name should not be blank");
        }else{
            if(generValue.length()==0){
                fragmentAccountBinding.gender.setError("");
                Toast.makeText(getActivity(),"please select gender",Toast.LENGTH_LONG).show();

            }else{
                if(dateOfBirth.length()==0){
                    fragmentAccountBinding.dateofbirth.setError("");

                }else{
                    fragmentAccountBinding.dateofbirth.setError(null);
                    if(phoneNo.length()==0){
                        fragmentAccountBinding.phoneNoData.setError(getString(R.string.phone_number_should_not_blank));
                        fragmentAccountBinding.phoneNoData.requestFocus();
                    }else{
                        if(phoneNo.length()!=10){
                            fragmentAccountBinding.phoneNoData.setError(getString(R.string.invaild_phone));
                            fragmentAccountBinding.phoneNoData.requestFocus();
                            fragmentAccountBinding.phoneNoData.setSelection(phoneNo.length());
                        }else{

                            if(useraddres.length()==0){
                                fragmentAccountBinding.addressData.requestFocus();
                                fragmentAccountBinding.addressData.setSelection(0);
                                fragmentAccountBinding.addressData.setError(getString(R.string.addres_should_not));

                            }else{
                                fragmentAccountBinding.addressData.setEnabled(false);
                                fragmentAccountBinding.unameData.setEnabled(false);

                                if (myAccountViewModel.getDataManager().getCurrentUserInfo() != null) {
                                    getBaseActivity().showLoading();
                                    Log.d("birth","--"+year+"-"+month+"-"+date);
                                   // dob=(year+"-"+month+"-"+date);

                                   // Toast.makeText(getActivity(),"okkkkkkk",Toast.LENGTH_LONG).show();
                                    NormalLoginResponse.Details details = new Gson().fromJson(myAccountViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//           eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                                    myAccountViewModel.updateProfile(details.getId(),user,useraddres,"","",dob,generValue,phoneNo);
                                }


                            }

                        }

                    }
                }
            }

        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        Log.e("code: ",+requestCode+" "+ resultCode);

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            Uri uri= data.getData();

            saveProfile(uri);

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(getBaseActivity(), ImagePicker.getError(data), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getBaseActivity(), "Task Cancelled", Toast.LENGTH_SHORT).show();
        }

        /*if (resultCode == getActivity().RESULT_CANCELED) {
            Log.d("Entering","7");
            return;
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            Log.d("Entering","3");

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                Log.d("Entering","4");

                if (resultUri != null) {
                    // Get file from cache directory

                    Log.d("Entering","5");

                    file_cache_dir = new File(getCacheDir(), resultUri.getLastPathSegment());
                    Bitmap bitmap = BitmapFactory.decodeFile(file_cache_dir.toString());
                //   Bitmap.createScaledBitmap(bitmap, 200, 200, true);
                    OutputStream os;
                    try {
                        os = new FileOutputStream(file_cache_dir);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 18, os);
                        os.flush();
                        os.close();
                    } catch (Exception e) {
                        Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
                    }
                    // file_cache_dir=  bitmap.compress(Bitmap.CompressFormat.PNG, 200, outputStream);
                    if (file_cache_dir.exists()) {
                        getBaseActivity().showLoading();


                        if (myAccountViewModel.getDataManager().getCurrentUserInfo() != null) {
                            getBaseActivity().showLoading();
                            NormalLoginResponse.Details details = new Gson().fromJson(myAccountViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                            myAccountViewModel.updateCollection(details.getId(),file_cache_dir);
                        }

                        Log.d("check_path", ": " + resultUri.toString());

                        Log.d("check_file_get", ": " + file_cache_dir.toString());

                    } else {
                        Log.d("file_does_not_exists", ": " + true);
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Log.d("Entering","6");

                Exception error = result.getError();
                Log.d("check_error", ": " + error.getMessage());
            }
        }*/
    }

    private void saveProfile(Uri resultUri){

        Log.d("Entering","4");

        if (resultUri != null) {
            // Get file from cache directory

            Log.d("Entering","5");

            file_cache_dir = new File(getCacheDir(), resultUri.getLastPathSegment());
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getBaseActivity().getContentResolver().openInputStream(resultUri));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            //   Bitmap.createScaledBitmap(bitmap, 200, 200, true);
            OutputStream os;
            try {
                os = new FileOutputStream(file_cache_dir);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 18, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            }
            // file_cache_dir=  bitmap.compress(Bitmap.CompressFormat.PNG, 200, outputStream);
            if (file_cache_dir.exists()) {
                getBaseActivity().showLoading();


                if (myAccountViewModel.getDataManager().getCurrentUserInfo() != null) {
                    getBaseActivity().showLoading();
                    NormalLoginResponse.Details details = new Gson().fromJson(myAccountViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                    myAccountViewModel.updateCollection(details.getId(),file_cache_dir);
                }

                Log.d("check_path", ": " + resultUri.toString());

                Log.d("check_file_get", ": " + file_cache_dir.toString());

            } else {
                Log.d("file_does_not_exists", ": " + true);
            }
        }
    }

    @Override
    public void onEdit() {
       // Toast.makeText(getActivity(), "EDit is done in Fragment", Toast.LENGTH_SHORT).show();
        fragmentAccountBinding.addressData.setEnabled(true);
        fragmentAccountBinding.unameData.setEnabled(true);
        Character ch=null;

        if(!fragmentAccountBinding.genderValue.getText().toString().trim().isEmpty())
         ch=fragmentAccountBinding.genderValue.getText().toString().trim().charAt(0);
        if(fragmentAccountBinding.genderValue.getText().toString().trim().isEmpty()) {
            fragmentAccountBinding.genderValue.setText("Male");
            fragmentAccountBinding.male.setBackgroundResource(R.drawable.textbackgroundchoose);
        }else if(ch.toString().equals("F"))
            fragmentAccountBinding.female.setBackgroundResource(R.drawable.textbackgroundchoose);
        else {
            fragmentAccountBinding.genderValue.setText("Male");
            fragmentAccountBinding.male.setBackgroundResource(R.drawable.textbackgroundchoose);
        }


            fragmentAccountBinding.unameData.setSelection(fragmentAccountBinding.unameData.getText().toString().length());
      //  if(myAccountViewModel.getDataManager().getPassword()==null && fragmentAccountBinding.genderValue.getText().toString().isEmpty()){
            fragmentAccountBinding.phoneNoData.setEnabled(true);
            fragmentAccountBinding.genderValue.setVisibility(View.GONE);
            fragmentAccountBinding.phoneNoData.setText(mobileNo);
            fragmentAccountBinding.male.setVisibility(View.VISIBLE);
            fragmentAccountBinding.calender.setVisibility(View.VISIBLE);
            fragmentAccountBinding.female.setVisibility(View.VISIBLE);



        fragmentAccountBinding.unameData.requestFocus();
        imageclick=true;
        onResume();

    }


}
