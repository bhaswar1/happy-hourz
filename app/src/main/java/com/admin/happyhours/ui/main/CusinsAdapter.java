package com.admin.happyhours.ui.main;


import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.admin.happyhours.R;
import com.admin.happyhours.customview.BookTextView;
import com.admin.happyhours.customview.HeavyTextView;
import com.admin.happyhours.data.model.api.response.CusineDetail;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.interfaces.OnLoadMoreListener;

import java.util.LinkedList;

public class CusinsAdapter extends RecyclerView.Adapter<CusinsAdapter.ViewHolder> {

    public boolean isLoading;
    private final int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private Context context;
    private OnLoadMoreListener mOnLoadMoreListener;
    private OnItemClickListener callback = null;
    private LinkedList<CusineDetail> details;
    public ItemClickListener searchItemClickListener;


    public CusinsAdapter(Context activity, LinkedList<CusineDetail> details) {
        this.context = activity;
        this.details = details;

//        this.details.add(new CollectionListingResponse.Details());
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                totalItemCount = linearLayoutManager.getItemCount();
//                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
//                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                    if (mOnLoadMoreListener != null) {
//                        mOnLoadMoreListener.onLoadMore();
//                    }
//                    isLoading = true;
//                }
//            }
//        });


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_tag_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.bind(position);

    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void clearData() {
        details.clear();
        notifyDataSetChanged();
    }

//    public void changeRowDataAfteLikeOrLike(String check_for_button, int position) {
//        if (check_for_button.equals(AppConstants.adapter_img_like_in_event_listing)) {
//            if (details.get(position).getFavourite_status() == 0) {
//                details.get(position).setFavourite_status(1);
//            } else {
//                details.get(position).setFavourite_status(0);
//            }
//        }
//        notifyItemChanged(position);
//    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        HeavyTextView textViewTag;
        ImageView checkBox;



        public ViewHolder(View itemView) {
            super(itemView);


        textViewTag=itemView.findViewById(R.id.textViewTag);
        checkBox=itemView.findViewById(R.id.radioButton);



        }

        public void bind(final int position) {

            String temp=details.get(position).getCusinsname();
            Character ch=temp.trim().charAt(0);
            String finaal=ch.toString().toUpperCase()+temp.substring(1);
                textViewTag.setText(finaal);
            if(details.get(position).isCheck()){
//                checkBox.setChecked(false);
                checkBox.setImageResource(R.drawable.checked);
            }else{
                //   checkBox.setChecked(true);
                checkBox.setImageResource(R.drawable.unchecked);

            }
//
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!details.get(position).isCheck()){
                        details.get(position).setChecked(true);
                        checkBox.setImageResource(R.drawable.checked);
                        Log.d("hii","--");
                    }else{
                        checkBox.setImageResource(R.drawable.unchecked);
                        details.get(position).setChecked(false);
                        Log.d("hii","----");
                    }
                    searchItemClickListener.onItemClick(details,position);

                }

            });

        }

//        @Override
//        public void onClick(View view) {
//          //  if (searchItemClickListener != null)
//                searchItemClickListener.onItemClick(view, getAdapterPosition());
//
//        }
    }

    public LinkedList<CusineDetail> getData() {
        return details;
    }

    public void setAllData(LinkedList<CusineDetail> data) {
        details.clear();
        details.addAll(data);
        notifyDataSetChanged();
    }

//    public void setData(LinkedList<TagDetail> detail) {
//        details.add(detail);
//        notifyDataSetChanged();
//    }

    public void setLoaded() {
        isLoading = false;
    }

    public interface ItemClickListener {
        void onItemClick(LinkedList<CusineDetail> view, int position);

    }
    public void setListener(ItemClickListener itemClickListener) {
        this.searchItemClickListener = itemClickListener;

    }
}
