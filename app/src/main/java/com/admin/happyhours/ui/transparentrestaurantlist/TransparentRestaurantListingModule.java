package com.admin.happyhours.ui.transparentrestaurantlist;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class TransparentRestaurantListingModule {


    @Provides
    TransparentRestaurantListingViewModel provideTransparentRestaurantListingViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new TransparentRestaurantListingViewModel(dataManager, schedulerProvider);
    }


}
