package com.admin.happyhours.ui.reviewlist;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.admin.happyhours.R;
import com.admin.happyhours.customview.BookTextView;
import com.admin.happyhours.customview.HeavyTextView;
import com.admin.happyhours.data.model.api.response.ReviewListDetail;
import com.admin.happyhours.data.model.api.response.SearchDetail;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kodmap.app.library.PopopDialogBuilder;

import java.util.LinkedList;

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.ViewHolder> {

    public boolean isLoading;
    private final int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private Context context;
    private OnLoadMoreListener mOnLoadMoreListener;
    private OnItemClickListener callback = null;
    private LinkedList<ReviewListDetail> details;
    public  ItemClickListener searchItemClickListener;


    public ReviewListAdapter(Context activity, LinkedList<ReviewListDetail> details,
                             RecyclerView recyclerView, final LinearLayoutManager linearLayoutManager) {
        this.context = activity;
        this.details = details;

//        this.details.add(new CollectionListingResponse.Details());
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_review_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.bind(position);

    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void clearData() {
        details.clear();
        notifyDataSetChanged();
    }

//    public void changeRowDataAfteLikeOrLike(String check_for_button, int position) {
//        if (check_for_button.equals(AppConstants.adapter_img_like_in_event_listing)) {
//            if (details.get(position).getFavourite_status() == 0) {
//                details.get(position).setFavourite_status(1);
//            } else {
//                details.get(position).setFavourite_status(0);
//            }
//        }
//        notifyItemChanged(position);
//    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView resturant_title,resturant_address;
        BookTextView expandableTextView;
        BookTextView date;
        HeavyTextView userName;
        RatingBar ratingBar;
        CardView first_card,second_card,third_card;
        ImageView first_iamge,seconnd_iameg,third_image;


        public ViewHolder(View itemView) {
            super(itemView);
           // resturant_title=itemView.findViewById(R.id.title);
            imageView=itemView.findViewById(R.id.imageView4);
            expandableTextView=itemView.findViewById(R.id.expand_tv);
            ratingBar=itemView.findViewById(R.id.rating);
            userName=itemView.findViewById(R.id.username);
            date=itemView.findViewById(R.id.date_tv);
            first_card=itemView.findViewById(R.id.first_card);
            second_card=itemView.findViewById(R.id.second_card);
            third_card=itemView.findViewById(R.id.third_card);
            first_iamge=itemView.findViewById(R.id.firstImage);
            seconnd_iameg=itemView.findViewById(R.id.secondImage);
            third_image=itemView.findViewById(R.id.third_image);





        }

        public void bind(final int position) {
         //   resturant_title.setText(details.get(position).getName());
            first_card.setVisibility(View.GONE);
            second_card.setVisibility(View.GONE);
            third_card.setVisibility(View.GONE);
            seconnd_iameg.setVisibility(View.GONE);
            third_image.setVisibility(View.GONE);

            final  LinkedList<String>  images=new LinkedList<>();





            String yourText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                    "Ut volutpat interdum interdum. Nulla laoreet lacus diam, vitae " +
                    "sodales sapien commodo faucibus. Vestibulum et feugiat enim. Donec " +
                    "semper mi et euismod tempor. Sed sodales eleifend mi id varius. Nam " +
                    "et ornare enim, sit amet gravida sapien. Quisque gravida et enim vel " +
                    "volutpat. Vivamus egestas ut felis a blandit. Vivamus fringilla " +
                    "dignissim mollis. Maecenas imperdiet interdum hendrerit. Aliquam" +
                    " dictum hendrerit ultrices. Ut vitae vestibulum dolor. Donec auctor ante" +
                    " eget libero molestie porta. Nam tempor fringilla ultricies. Nam sem " +
                    "lectus, feugiat eget ullamcorper vitae, ornare et sem. Fusce dapibus ipsum" +
                    " sed laoreet suscipit. ";
            if(!details.get(position).getComment().trim().isEmpty()) {
                expandableTextView.setVisibility(View.VISIBLE);

                String t = details.get(position).getComment().trim();
                Character c = t.charAt(0);
                String comment = c.toString().toUpperCase() + t.substring(1);

                    expandableTextView.setText(comment);


            }else{
                expandableTextView.setVisibility(View.GONE);
            }
            if(details.get(position).getRecentreviewimage().size()!=0) {
                images.clear();
                for ( int index = 0; index < details.get(position).getRecentreviewimage().size(); index++) {
                    images.add(details.get(position).getRecentreviewimage().get(index).getImage());

                    if(index==0){

                        first_card.setVisibility(View.VISIBLE);
                        second_card.setVisibility(View.VISIBLE);
                        third_card.setVisibility(View.VISIBLE);
                        Glide.with(context).load(details.get(position).getRecentreviewimage().get(index).getImage()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(first_iamge);

                        first_card.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                             //   Toast.makeText(context,"position"+position,Toast.LENGTH_SHORT).show();

                                Log.d("position","--"+details.get(position).getRecentreviewimage().get(0).getImage());
                                final  int pos=0;
                                Dialog dialog = new PopopDialogBuilder(context)
                                        // Set list like as option1 or option2 or option3

                                        .setList(images,pos)
                                        // or setList with initial position that like .setList(list,position)
                                        // Set dialog header color
                                        .setHeaderBackgroundColor(android.R.color.transparent)
                                        // Set dialog background color
                                        .setDialogBackgroundColor(R.color.black)
                                        // Set close icon drawable
                                        .setCloseDrawable(R.drawable.ic_close_white_24dp)
                                        // Set loading view for pager image and preview image
                                        .setLoadingView(R.layout.loader_item_layout)
                                        // Set dialog style
                                        // .setDialogStyle(R.style.DialogStyle)
                                        // Choose selector type, indicator or thumbnail
                                        .showThumbSlider(false)
                                        // Set image scale type for slider image
                                        .setSliderImageScaleType(ImageView.ScaleType.FIT_CENTER)
                                        // Set indicator drawable
                                     //   .setSelectorIndicator(R.drawable.selected_indicato)
                                        // Enable or disable zoomable
                                        .setIsZoomable(true)
                                        // Build Km Slider Popup Dialog
                                        .build();
                                dialog.show();


                            }

                        });

                    }
                    if(index==1){

                        seconnd_iameg.setVisibility(View.VISIBLE);
                        Glide.with(context).load(details.get(position).getRecentreviewimage().get(index).getImage()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(seconnd_iameg);
                        seconnd_iameg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Dialog dialog1 = new PopopDialogBuilder(context)
                                        // Set list like as option1 or option2 or option3
                                        .setList(images,1)
                                        // or setList with initial position that like .setList(list,position)
                                        // Set dialog header color
                                        .setHeaderBackgroundColor(android.R.color.transparent)
                                        // Set dialog background color
                                        .setDialogBackgroundColor(R.color.black)
                                        // Set close icon drawable
                                        .setCloseDrawable(R.drawable.ic_close_white_24dp)
                                        // Set loading view for pager image and preview image
                                        .setLoadingView(R.layout.loader_item_layout)
                                        // Set dialog style
                                        // .setDialogStyle(R.style.DialogStyle)
                                        // Choose selector type, indicator or thumbnail
                                        .showThumbSlider(false)
                                        // Set image scale type for slider image
                                        .setSliderImageScaleType(ImageView.ScaleType.FIT_CENTER)
                                        // Set indicator drawable
                                    //    .setSelectorIndicator(R.drawable.selected_indicato)
                                        // Enable or disable zoomable
                                        .setIsZoomable(true)
                                        // Build Km Slider Popup Dialog
                                        .build();
                                dialog1.show();

                            }
                        });
                    }
                    if(index==2){
                        third_image.setVisibility(View.VISIBLE);


                        Glide.with(context).load(details.get(position).getRecentreviewimage().get(index).getImage()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(third_image);

                        third_image.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Dialog dialog1 = new PopopDialogBuilder(context)
                                        // Set list like as option1 or option2 or option3
                                        .setList(images,2)
                                        // or setList with initial position that like .setList(list,position)
                                        // Set dialog header color
                                        .setHeaderBackgroundColor(android.R.color.transparent)
                                        // Set dialog background color
                                        .setDialogBackgroundColor(R.color.black)
                                        // Set close icon drawable
                                        .setCloseDrawable(R.drawable.ic_close_white_24dp)
                                        // Set loading view for pager image and preview image
                                        .setLoadingView(R.layout.loader_item_layout)
                                        // Set dialog style
                                        // .setDialogStyle(R.style.DialogStyle)
                                        // Choose selector type, indicator or thumbnail
                                        .showThumbSlider(false)
                                        // Set image scale type for slider image
                                        .setSliderImageScaleType(ImageView.ScaleType.FIT_CENTER)
                                        // Set indicator drawable
                                     //   .setSelectorIndicator(R.drawable.selected_indicato)
                                        // Enable or disable zoomable
                                        .setIsZoomable(true)
                                        // Build Km Slider Popup Dialog
                                        .build();
                                dialog1.show();

                            }
                        });

                    }




                }

            }


            String temp=details.get(position).getCommentername().trim();
            Character ch=temp.charAt(0);
            String uname=ch.toString().toUpperCase()+temp.substring(1);


        userName.setText(uname);
            ratingBar.setRating(Integer.parseInt(details.get(position).getStar()));
            date.setText(details.get(position).getEventMonth()+" "+details.get(position).getEventDay()+", "+details.get(position).getEventYear());
            Glide.with(context).load(details.get(position).getProfileImage()).apply(RequestOptions.circleCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(imageView);

        }

//        @Override
//        public void onClick(View view) {
//          //  if (searchItemClickListener != null)
//                searchItemClickListener.onItemClick(view, getAdapterPosition());
//
//        }
    }

    public LinkedList<ReviewListDetail> getData() {
        return details;
    }

    public void setAllData(LinkedList<ReviewListDetail> data) {
        details.addAll(data);
        notifyDataSetChanged();
    }

    public void setData(ReviewListDetail detail) {
        details.add(detail);
        notifyDataSetChanged();
    }

    public void setLoaded() {
        isLoading = false;
    }

    public interface ItemClickListener {
        void onItemClick(SearchDetail view, int position);

    }
    public void setListener(ReviewListAdapter.ItemClickListener itemClickListener) {
        this.searchItemClickListener = itemClickListener;

    }
}
