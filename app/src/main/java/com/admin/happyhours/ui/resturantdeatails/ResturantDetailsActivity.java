package com.admin.happyhours.ui.resturantdeatails;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import com.admin.happyhours.BR;
import com.admin.happyhours.utils.AppConstants;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;

import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextPaint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.Details;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.ResturantsAllData;
import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.admin.happyhours.databinding.ActivityResturantDeatilBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.ui.claim.BusinessClaimActivity;
import com.admin.happyhours.ui.resturantlocation.RasturantLocationActivity;
import com.admin.happyhours.ui.review.ReviewActivity;
import com.admin.happyhours.ui.reviewlist.ReviewListActivity;
import com.admin.happyhours.ui.reviewphoto.ReviewPhotoActivity;
import com.admin.happyhours.utils.GoogleMapHelper.HelperClassGoogleMap;
import com.admin.happyhours.utils.LinePagerIndicatorDecoration;
import com.admin.happyhours.utils.PermissionUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Locale;

import javax.inject.Inject;

import static android.widget.LinearLayout.VERTICAL;

public class ResturantDetailsActivity extends BaseActivity<ActivityResturantDeatilBinding, ResturantDetailsViewModel>
        implements ResturantDetailsNavigator, AppBarLayout.OnOffsetChangedListener {

    private static final String TAG = "ResturantDetailsActivit";
    private boolean check_for_collection_open_from_details = false;
    private int item_position = 0;
    private static final int CHECK_LOCATION_SETTINGS_REQUEST_CODE = 1;
    private FusedLocationProviderClient mFusedLocationClient;
    private PermissionUtils permissionUtils;
    private ArrayList<String> permissions = new ArrayList<>();
    LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    public LinkedList<String> resturantDetailData1;
    public LinkedList<String> extradata;
    public ResturantDetailsdapter resturantDetailsdapter;
    public String resturantId;
    LinkedList<String>  images;
    //    public  Extra extra;
    int likelogostatus;
    public String lattitude;
    public  String restaurantName=null;


    public  String restaurant_lat=null;
    public String resaurant_long=null;
    int collectionStatus=0;
    int rating=0;
    String totalReview=null;
    String restaurantAddres;
   String restaurantImage;
    public String longitude;
    String phoneNo=null;

    private FirebaseAnalytics mFirebaseAnalytics;
    ResturantSliderImageAdapter resturantSliderImageAdapter;
    private SpecialPageRestuarantListingResponse.Detail restaurantDetails;
    TabLayout tabLayout;
    @Inject
    ResturantDetailsViewModel mResturantDetailsViewModel;
    private ActivityResturantDeatilBinding activityResturantDeatilBinding;
    private String userid;
    public String resturantName;
    public boolean checkeFavourite = false;
   public String lat;
    public  String longt;
    ResturantsAllData resturantsAllData;


    public static Intent newIntent(Context context) {
        return new Intent(context, ResturantDetailsActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_resturant_deatil;
    }

    @Override
    public ResturantDetailsViewModel getViewModel() {
        return mResturantDetailsViewModel;
    }

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setStatusBarHidden(ResturantDetailsActivity.this);

        mResturantDetailsViewModel.setNavigator(this);
        activityResturantDeatilBinding = getViewDataBinding();

        Toolbar toolbar = activityResturantDeatilBinding.toolbar;
        setSupportActionBar(toolbar);
        activityResturantDeatilBinding.toolbar.setNavigationIcon(R.drawable.back_arrow_black);
        activityResturantDeatilBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Log.d("restaurantid","---"+ mResturantDetailsViewModel.getDataManager().getCurrentUserId().toString());




        images=new LinkedList<>();

        reviewClick();
        claimClick();
        activityResturantDeatilBinding.appBar.addOnOffsetChangedListener(this);
        //  CollapsingToolbarLayout toolbarLayout = (CollapsingToolbarLayout) activityResturantDeatilBinding.toolbarLayout;
        Log.d("phoneno","----------"+phoneNo);
        //  toolbarLayout.setTitle("Resturant Details");
//
            activityResturantDeatilBinding.phoneno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("phoneno","--"+phoneNo);
                    if(phoneNo!=null&& phoneNo.length()!=0) {
                      //  Toast.makeText(ResturantDetailsActivity.this, "clicked", Toast.LENGTH_LONG).show();
                         openDialer(phoneNo);
                    }
                }
            });
      //  }

activityResturantDeatilBinding.reviewEbt.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(ResturantDetailsActivity.this, ReviewActivity.class).putExtra("resturantName",resturantName)
                .putExtra("restaurantImage",restaurantImage)
                .putExtra("restaurantAddres",restaurantAddres)
                .putExtra("id",resturantId)
        );

    }
});

        RecyclerView recyclerViewPager = findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        //initCollapsingToolbar();
        tabLayout.addTab(tabLayout.newTab().setText("Daily Special"));
        tabLayout.addTab(tabLayout.newTab().setText("Happy Hour"));
        tabLayout.addTab(tabLayout.newTab().setText("Social Hour"));
        lattitude = mResturantDetailsViewModel.getDataManager().getLatitude();
        longitude =mResturantDetailsViewModel.getDataManager().getLongitude();
        tabLayout.getTabAt(1).select();
        //tabLayout.setTab
        resturantId = getIntent().getStringExtra("resturantid");
        String checkImage=getIntent().getStringExtra("checklike");
        item_position = getIntent().getExtras().getInt("item_position");

        if(getIntent().hasExtra("details")) {
            restaurantDetails = (new Gson().fromJson(getIntent().getStringExtra("details"),
                    new TypeToken<SpecialPageRestuarantListingResponse.Detail>() {
                    }.getType()));
            Log.d("test", "" + restaurantDetails.getFavouriteStatus());
        }
        //  restaurantDetails.add(new );
//
//        if(checkImage.equals("ok")){
//            checkeFavourite = true;
//            activityResturantDeatilBinding.like.setImageResource(R.drawable.like_logo_checked);
//        }else {
//            if (restaurantDetails.getFavouriteStatus() == 1) {
//                likelogostatus = 1;
//                checkeFavourite = true;
//                activityResturantDeatilBinding.like.setImageResource(R.drawable.like_logo_checked);
//
//            } else {
//                activityResturantDeatilBinding.like.setImageResource(R.drawable.like_logo);
//                likelogostatus = 0;
//            }
//        }
        //  showLoading();
        final NormalLoginResponse.Details details = new Gson().fromJson(mResturantDetailsViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
        userid = details.getId();

        showLoading();
        mResturantDetailsViewModel.setResturantDetail(userid, "2", lattitude, longitude,resturantId);

        Log.d("userid", "--" + mResturantDetailsViewModel.getDataManager().getCurrentUserId().toString());


        //   Toast.makeText(ResturantDetailsActivity.this,"userid"+mResturantDetailsViewModel.getDataManager().getCurrentUserId().toString(),Toast.LENGTH_LONG).show();
        resturantDetailData1 = new LinkedList<>();
        // resturantDetailData1.add(new ResturantImageData("1"));
        // resturantDetailData1.add(new ResturantImageData("2"));
        extradata = new LinkedList<>();
        tabSelset();
        setRecycleView();
        reviewlist();

        initCollapsingToolbar();
        setDirection();
        activityResturantDeatilBinding.dataRecycle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    activityResturantDeatilBinding.dataRecycle.getParent().requestDisallowInterceptTouchEvent(false);
                }
                return false;

            }
        });



                resturantSliderImageAdapter = new ResturantSliderImageAdapter(resturantDetailData1, ResturantDetailsActivity.this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewPager.setLayoutManager(linearLayoutManager);
        recyclerViewPager.setAdapter(resturantSliderImageAdapter);
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        recyclerViewPager.setOnFlingListener(null);
        recyclerViewPager.addItemDecoration(new LinePagerIndicatorDecoration());
        snapHelper.attachToRecyclerView(recyclerViewPager);

        activityResturantDeatilBinding.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  activityResturantDeatilBinding.like.setImageResource(R.drawable.like_logo_checked);


                if (mResturantDetailsViewModel.getDataManager().getCurrentUserInfo() != null) {
                    showLoading();
                    NormalLoginResponse.Details details = new Gson().fromJson(mResturantDetailsViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                    mResturantDetailsViewModel.setLikeUnlike(details.getId(), resturantId);
                }

            }
        });

        activityResturantDeatilBinding.bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(collectionStatus==0) {
                    resturantsAllData.setRestaurent_id(resturantId);
                    Log.d("THIS","--"+new Gson().toJson(resturantsAllData)+resturantId);
                    check_for_collection_open_from_details = true;
//                homeActivity.goFromSpecialToCollection(new Gson().toJson(restaurantDetails));

                    hideLoading();

                    onBackPressed();
                }else{
                   showLoading();
                    mResturantDetailsViewModel.bookmarkAndLikeApiHit(resturantId);
                }


            }
        });

        activityResturantDeatilBinding.shareimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = new Gson().toJson(restaurantDetails);
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "share_restaurant_details");
                //sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Check out offers from Daily Special, Happy Hour & Social Hour at "+restaurantName+".  Click on the following link to download Happy Hourz - https://happyhourzapp.com/app/Restaurant_event_deeplink?restaurant_id=" + resturantId);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Check out this Happy Hour special at "+restaurantName+"! Click on the following link:  https://happyhourzapp.com/app/Restaurant_event_deeplink?restaurant_id=" + resturantId);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

    }

    private void onReviewPhotosClick( LinkedList<String> images) {
        final  LinkedList<String>  images1=new LinkedList<>();
        if(images.size()!=0){
            images1.addAll(images);
        }


                activityResturantDeatilBinding.firstimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                 //      Toast.makeText(ResturantDetailsActivity.this,"clicked",Toast.LENGTH_LONG).show();
                      //  Log.d("position","--"+details.get(position).getRecentreviewimage().get(0).getImage());
//                        final  int pos=0;
//                        Dialog dialog = new PopopDialogBuilder(ResturantDetailsActivity.this)
//                                // Set list like as option1 or option2 or option3
//
//                                .setList(images1,pos)
//                                // or setList with initial position that like .setList(list,position)
//                                // Set dialog header color
//                                .setHeaderBackgroundColor(android.R.color.transparent)
//                                // Set dialog background color
//                                .setDialogBackgroundColor(R.color.black)
//                                // Set close icon drawable
//                                .setCloseDrawable(R.drawable.ic_close_white_24dp)
//                                // Set loading view for pager image and preview image
//                                .setLoadingView(R.layout.loader_item_layout)
//                                // Set dialog style
//                                // .setDialogStyle(R.style.DialogStyle)
//                                // Choose selector type, indicator or thumbnail
//                                .showThumbSlider(false)
//                                // Set image scale type for slider image
//                                .setSliderImageScaleType(ImageView.ScaleType.FIT_CENTER)
//                                // Set indicator drawable
//                                .setSelectorIndicator(R.drawable.selected_indicato)
//                                // Enable or disable zoomable
//                                .setIsZoomable(true)
//                                // Build Km Slider Popup Dialog
//                                .build();
//                        dialog.show();

                        startActivity(new Intent(ResturantDetailsActivity.this,ReviewPhotoActivity.class).putExtra("restaurantid",resturantId));


                    }

                });




        activityResturantDeatilBinding.secondimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // final  int pos=1;
              //  Toast.makeText(ResturantDetailsActivity.this,"clicked",Toast.LENGTH_LONG).show();
                startActivity(new Intent(ResturantDetailsActivity.this,ReviewPhotoActivity.class).putExtra("restaurantid",resturantId));

            }
        });
        activityResturantDeatilBinding.thirdimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ResturantDetailsActivity.this,ReviewPhotoActivity.class).putExtra("restaurantid",resturantId));

            }
        });
    }

    private void reviewlist() {
        activityResturantDeatilBinding.viewReviewTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ResturantDetailsActivity.this, ReviewListActivity.class).putExtra("restaurantid",resturantId));
            }
        });
    }

    private void claimClick() {
        activityResturantDeatilBinding.calimBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("userId", mResturantDetailsViewModel.getDataManager().getCurrentUserId().toString());
                bundle.putString("userName", resturantName);
                // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
//        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, resturantName);
                bundle.putString("resturantlocation",restaurantAddres);
                bundle.putString("resturantid",restaurantAddres);
                bundle.putString("resturantName",resturantName);
                bundle.putString("resturantPhoneNo",resturantName);
                mFirebaseAnalytics.logEvent("claim_business_clicked_android", bundle);

                //startActivity(new Intent(ResturantDetailsActivity.this, BusinessClaimActivity.class).putExtra("restaurantid",resturantId));

                openCustomBrowserTab(AppConstants.BASE_URL+"app/addbusiness/add/"+resturantId+"/1");

            }
        });
    }

    private void reviewClick() {
        activityResturantDeatilBinding.reviewBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ResturantDetailsActivity.this, ReviewActivity.class).putExtra("resturantName",resturantName)
                        .putExtra("restaurantImage",restaurantImage)
                        .putExtra("restaurantAddres",restaurantAddres)
                        .putExtra("id",resturantId)
                );
            }
        });

    }

    protected void openDialer(String phone_number){


        Intent intent = new Intent(Intent.ACTION_DIAL);
        // Send phone number to intent as data
        intent.setData(Uri.parse("tel:" + phone_number));
        // Start the dialer app activity with number
        startActivity(intent);
    }

    private void setDirection() {
        activityResturantDeatilBinding.tvGetDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( resaurant_long!=null && restaurant_lat!=null ) {
                    HelperClassGoogleMap.showDirectionFromCurrentToDestinationPlaceInGoogleMapUsingLatiLong(ResturantDetailsActivity.this,
                            restaurant_lat, resaurant_long);
                }
            }
        });
        activityResturantDeatilBinding.address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                  //  HelperClassGoogleMap.showDirectionFromCurrentToDestinationPlaceInGoogleMapUsingLatiLong(ResturantDetailsActivity.this,
                           // restaurantDetails.getLatitude(), restaurantDetails.getLongitude());
                 // HelperClassGoogleMap.showMarkerOnMap("","","","hii");

                //Open In App Google Map and add marker
                /*startActivity(new Intent(ResturantDetailsActivity.this, RasturantLocationActivity.class)
                        .putExtra("lattitude",lat)
                        .putExtra("longitude",longt)
                        .putExtra("rasturantName",resturantName));*/

                //Open Google Map and add marker
                //String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lat, longt);
                /*String uri = "http://maps.google.com/maps?q=loc:" + lat + "," + longt + " (" + resturantName + ")";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);*/

                //Open Google Map and Direction
                String my_data= String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=" + lat + "," + longt + " (" + resturantName + ")");
                Log.e("mapurl:",my_data);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(my_data));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);


            }
        });

    }

    private void initCollapsingToolbar() {

        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) activityResturantDeatilBinding.toolbarLayout;
        //   collapsingToolbar.setTitleEnabled(false);
        //  activityResturantDeatilBinding.toolbar.setTitle("Details");
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle("Details");
                    makeCollapsingToolbarLayoutLooksGood(collapsingToolbar);

                    //  activityResturantDeatilBinding.viewpager.setVisibility(View.GONE);
                    isShow = true;
                } else if (isShow) {
                    //  activityResturantDeatilBinding.viewpager.setVisibility(View.VISIBLE);

                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    private void tabSelset() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {


                if (tab.getPosition() == 0) {
                    // activityCartBinding.group.setVisibility(View.GONE);
                    //setCartList();
                    showLoading();
                    mResturantDetailsViewModel.setResturantDetail(userid, "1", lattitude, longitude, resturantId);


                } else if (tab.getPosition() == 1) {

                    //  setWishList();
                    showLoading();
                    mResturantDetailsViewModel.setResturantDetail(userid, "2", lattitude, longitude, resturantId);


                } else if (tab.getPosition() == 2) {
                    showLoading();
                    mResturantDetailsViewModel.setResturantDetail(userid, "3", lattitude, longitude, resturantId);

                }

            }


            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {


            }
        });

    }

    private void setRecycleView() {

        // fragmentFavouritesBinding.fevouriteRecycle.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        activityResturantDeatilBinding.dataRecycle.setLayoutManager(linearLayoutManager);
        //  fragmentFavouritesBinding.fevouriteRecycle.addItemDecoration(new SpacesItemDecoration(ViewUtils.dpToPx(2)));

        resturantDetailsdapter = new ResturantDetailsdapter(extradata, ResturantDetailsActivity.this,activityResturantDeatilBinding.dataRecycle);
      //  activityResturantDeatilBinding.dataRecycle.setClickable(true);

        //DividerItemDecoration decoration = new DividerItemDecoration(this, VERTICAL);
        //activityResturantDeatilBinding.dataRecycle.addItemDecoration(decoration);
        activityResturantDeatilBinding.dataRecycle.setAdapter(resturantDetailsdapter);


    }

    @Override
    public void onError(Throwable throwable) {

        hideLoading();
        if (throwable!=null){
            Log.d(TAG, "onError: "+throwable.getMessage());
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("rating",Integer.toString(rating));
        intent.putExtra("collection_status",collectionStatus);
        intent.putExtra("item_position",item_position);
        intent.putExtra("review",totalReview);
        intent.putExtra("checkeFavourite",checkeFavourite);
        intent.putExtra("check_for_collection_open_from_details",check_for_collection_open_from_details);
        intent.putExtra("restaurant_data", new Gson().toJson(resturantsAllData));
        setResult( 8, intent);
        super.onBackPressed();
//        Toast.makeText(ResturantDetailsActivity.this, "favourite like value" + checkeFavourite, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        activityResturantDeatilBinding.firstimage.setVisibility(View.GONE);
        activityResturantDeatilBinding.thirdimage.setVisibility(View.GONE);
        activityResturantDeatilBinding.secondimage.setVisibility(View.GONE);
        showLoading();
        mResturantDetailsViewModel.setResturantDetail(userid, "2", lattitude, longitude,resturantId);
    }

    @Override
    public void onSuccess(ResturantsAllData response, JsonObject jsonObject) {
        hideLoading();

        Log.e("response===",new Gson().toJson(response));
        Log.e("jsonObject===",new Gson().toJson(jsonObject));

        restaurantAddres=response.getAddress();
        restaurantImage=response.getCoverimage();
        resaurant_long=response.getLongitude();
        restaurant_lat=response.getLatitude();
        rating=response.getRating();
        totalReview=response.getTotal_review();
        resturantsAllData=response;

        Bundle bundle = new Bundle();
        bundle.putString("userId", mResturantDetailsViewModel.getDataManager().getCurrentUserId().toString());
        bundle.putString("userName", resturantName);
        // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
//        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, resturantName);
        bundle.putString("resturantlocation",restaurantAddres);
        bundle.putString("resturantid",restaurantAddres);
        bundle.putString("resturantName",resturantName);
        bundle.putString("resturantPhoneNo",resturantName);
        mFirebaseAnalytics.logEvent("restaurant_details_visited_android", bundle);




        if(!response.getRecentreview().trim().isEmpty()) {
            activityResturantDeatilBinding.mostRecent.setTextColor(getResources().getColor(R.color.black));

            activityResturantDeatilBinding.mostRecent.setText(response.getRecentreview().trim());
        }else{
            activityResturantDeatilBinding.mostRecent.setText("No Reviews Available");
            activityResturantDeatilBinding.mostRecent.setTextColor(getResources().getColor(R.color.colorGray));

        }

        if(response.getClaim_status().equals("1")){
            activityResturantDeatilBinding.reviewBt.setVisibility(View.VISIBLE);
            activityResturantDeatilBinding.calimBt.setVisibility(View.VISIBLE);
            activityResturantDeatilBinding.reviewEbt.setVisibility(View.GONE);
        }else{
            activityResturantDeatilBinding.reviewBt.setVisibility(View.GONE);
            activityResturantDeatilBinding.calimBt.setVisibility(View.GONE);
            activityResturantDeatilBinding.reviewEbt.setVisibility(View.VISIBLE);
        }

       // Toast.makeText(ResturantDetailsActivity.this,"outside image",Toast.LENGTH_LONG).show();

if(response.getRecentreviewimage().size()!=0) {
    //Toast.makeText(ResturantDetailsActivity.this,"inside image",Toast.LENGTH_LONG).show();

    activityResturantDeatilBinding.noPhotoTv.setVisibility(View.GONE);

      //   images=new LinkedList<>();
    images.clear();

    for(int i=0;i<response.getRecentreviewimage().size();i++) {
        images.add(response.getRecentreviewimage().get(i).getImage());

        Log.d("insideimage","---"+response.getRecentreviewimage().size());
        if (i==0) {
            activityResturantDeatilBinding.firstimage.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext()).load(response.getRecentreviewimage().get(i).getImage()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(activityResturantDeatilBinding.firstimage);

        }
        if (i==1) {
            activityResturantDeatilBinding.secondimage.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext()).load(response.getRecentreviewimage().get(i).getImage()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(activityResturantDeatilBinding.secondimage);
        }
        if (i==2) {
            activityResturantDeatilBinding.thirdimage.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext()).load(response.getRecentreviewimage().get(i).getImage()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(activityResturantDeatilBinding.thirdimage);
        }
    }

    onReviewPhotosClick(images);
}

else{
    activityResturantDeatilBinding.noPhotoTv.setVisibility(View.VISIBLE);
}




        activityResturantDeatilBinding.coordinatorLayout.setVisibility(View.VISIBLE);
        Log.d("miles", "---" + jsonObject.get("miles").toString());
        activityResturantDeatilBinding.nameresturant.setText(jsonObject.get("name").toString().substring(1, jsonObject.get("name").toString().length() - 1));
            if(jsonObject.get("miles").toString().equals("0") ||
                    jsonObject.get("miles").toString().equals("1") ){
                activityResturantDeatilBinding.tvGetDirection.setText(jsonObject.get("miles").toString() + " mile away");

            }else{
                activityResturantDeatilBinding.tvGetDirection.setText(jsonObject.get("miles").toString() + " miles away");

            }

        activityResturantDeatilBinding.address.setText(response.getAddress().toString());
        if (response.getOpenTime() != null && response.getOpenTime().length() != 0)
           /// if(response.getOpenTime()!=null || !TextUtils.isEmpty(response.getOpenTime()))
            activityResturantDeatilBinding.openingtime.setText(response.getOpenTime() + " to " + response.getCloseTime());
            else {
            activityResturantDeatilBinding.openingtime.setPadding(10,0,0,0);
            activityResturantDeatilBinding.openingtime.setText("--");
        }


            activityResturantDeatilBinding.viewReviewTv.setText("View all Reviews ("+response.getTotal_review()+")");

            if (Integer.parseInt(response.getTotal_review()) > 0){
                activityResturantDeatilBinding.tvLastReviewerName.setVisibility(View.VISIBLE);
                activityResturantDeatilBinding.rbLastReviewerRating.setVisibility(View.VISIBLE);
                activityResturantDeatilBinding.tvLastReviewerName.setText(response.getRecent_reviewer_name());
                activityResturantDeatilBinding.rbLastReviewerRating.setRating(response.getRecent_review_star());
            }else {
                activityResturantDeatilBinding.tvLastReviewerName.setVisibility(View.GONE);
                activityResturantDeatilBinding.rbLastReviewerRating.setVisibility(View.GONE);
            }

        String number = response.getPhone().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
        activityResturantDeatilBinding.phoneno.setText(number);
        phoneNo = response.getPhone();
        lat = response.getLatitude();
        restaurantName=response.getName();
        longt = response.getLongitude();
        if (response.getFavouriteStatus() == 1) {
            checkeFavourite = true;
            activityResturantDeatilBinding.like.setImageResource(R.drawable.like_logo_checked);

        }else{
            activityResturantDeatilBinding.like.setImageResource(R.drawable.like_logo);
        }
        if(response.getCollectionStatus()==1){
            collectionStatus=1;
            activityResturantDeatilBinding.bookmark.setImageResource(R.drawable.bookmark_logo_checked);
        }else{
            activityResturantDeatilBinding.bookmark.setImageResource(R.drawable.bookmark_logo);

        }

        Details detailsImage = new Gson().fromJson(jsonObject.get("Details"),
                Details.class);
        activityResturantDeatilBinding.ratingBar.setRating(Integer.parseInt(jsonObject.get("rating").toString()));
     //  Log.d("response_check","-a-"+"");
        resturantName = response.getName();
        JsonObject details = jsonObject.getAsJsonObject("Details");
        JsonObject extra = details.getAsJsonObject("extra");
        Log.d("extra","--"+new Gson().toJson(extra));
        JsonArray food = extra.getAsJsonArray("food");
        JsonArray drinks = extra.getAsJsonArray("gdrinks");
        JsonArray image = details.getAsJsonArray("otherimages");
        if(food!=null) {
            if (food.size() == 0 && drinks.size() == 0) {
                activityResturantDeatilBinding.dataRecycle.setVisibility(View.GONE);
                activityResturantDeatilBinding.nodata.setVisibility(View.VISIBLE);
                //   Log.d("testconst","insidethe");
                //  Toast.makeText(ResturantDetailsActivity.this,"inside",Toast.LENGTH_LONG).show();
            /*ConstraintLayout constraintLayout3 = activityResturantDeatilBinding.resturantDetailconstrain;

            activityResturantDeatilBinding.view11.setBackgroundColor(getResources().getColor(R.color.colorline));
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(constraintLayout3);

            constraintSet.connect(activityResturantDeatilBinding.view11.getId(), ConstraintSet.TOP, activityResturantDeatilBinding.nodata.getId(), ConstraintSet.BOTTOM, 0);
            constraintSet.connect(activityResturantDeatilBinding.view11.getId(), ConstraintSet.START, constraintLayout3.getId(), ConstraintSet.START, 0);
            constraintSet.applyTo(constraintLayout3);*/

            } else {
            /*ConstraintLayout constraintLayout3 = activityResturantDeatilBinding.resturantDetailconstrain;
            TextView nodata = activityResturantDeatilBinding.nodata;
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(constraintLayout3);

            constraintSet.connect(activityResturantDeatilBinding.view11.getId(), ConstraintSet.TOP, activityResturantDeatilBinding.dataRecycle.getId(), ConstraintSet.BOTTOM, 0);
            constraintSet.connect(activityResturantDeatilBinding.view11.getId(), ConstraintSet.LEFT, constraintLayout3.getId(), ConstraintSet.LEFT, 0);
            constraintSet.applyTo(constraintLayout3);*/
                activityResturantDeatilBinding.dataRecycle.setVisibility(View.VISIBLE);
                activityResturantDeatilBinding.nodata.setVisibility(View.GONE);
                extradata.clear();

                if (food != null) {
                    for (int i = 0; i < food.size(); i++) {

                        extradata.add(food.get(i).toString());
                    }
                }

                if (drinks != null) {
                    for (int i = 0; i < drinks.size(); i++) {

                        extradata.add(drinks.get(i).toString());
                    }
                }
                resturantDetailsdapter.addItems(extradata, food.size());
            }
        }

        if ((detailsImage != null && detailsImage.getOtherimages().size()!=0)||response.getCoverimage()!=null) {
            resturantSliderImageAdapter.clearItems();
            resturantDetailData1.add(response.getCoverimage());
            resturantDetailData1.addAll(detailsImage.getOtherimages());
            resturantSliderImageAdapter.addItem(resturantDetailData1);
        }else{
            resturantSliderImageAdapter.clearItems();
            resturantDetailData1.add("noimage");
            resturantSliderImageAdapter.addItem(resturantDetailData1);
        }

    }

    @Override
    public void likeSucess(CommonSimpleResponse commonSimpleResponse) {
        hideLoading();
        if (commonSimpleResponse.getMessage().equals("Successfully added to favourite")) {
            checkeFavourite = true;
            activityResturantDeatilBinding.like.setImageResource(R.drawable.like_logo_checked);
            Toast.makeText(ResturantDetailsActivity.this,getResources().getString(R.string.add_to_favourite),Toast.LENGTH_LONG).show();

            Bundle bundle = new Bundle();
            bundle.putString("userId", mResturantDetailsViewModel.getDataManager().getCurrentUserId().toString());
            bundle.putString("userName", resturantName);
            // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
//        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, resturantName);
            bundle.putString("resturantlocation",restaurantAddres);
            bundle.putString("resturantid",restaurantAddres);
            bundle.putString("resturantName",resturantName);
            bundle.putString("resturantPhoneNo",resturantName);
            mFirebaseAnalytics.logEvent("restaurant_added_to_favourites_android", bundle);


        } else {
            checkeFavourite = false;
            activityResturantDeatilBinding.like.setImageResource(R.drawable.like_logo);
            Toast.makeText(ResturantDetailsActivity.this,getResources().getString(R.string.removed_success_favourite),Toast.LENGTH_LONG).show();


            Bundle bundle = new Bundle();
            bundle.putString("userId", mResturantDetailsViewModel.getDataManager().getCurrentUserId().toString());
            bundle.putString("userName", resturantName);
            // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
//        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, resturantName);
            bundle.putString("resturantlocation",restaurantAddres);
            bundle.putString("resturantid",restaurantAddres);
            bundle.putString("resturantName",resturantName);
            bundle.putString("resturantPhoneNo",resturantName);
            mFirebaseAnalytics.logEvent("restaurant_removed_frm_fvrites_android", bundle);


        }
    }

    @Override
    public void noData() {
        hideLoading();
        Toast.makeText(ResturantDetailsActivity.this,"insufficient data",Toast.LENGTH_LONG).show();
        onBackPressed();

    }

    @Override
    public void responseAfterBookmarkOrLike() {
            collectionStatus=0;
            hideLoading();

            activityResturantDeatilBinding.bookmark.setImageResource(R.drawable.bookmark_logo);

    }

    private void makeCollapsingToolbarLayoutLooksGood(CollapsingToolbarLayout collapsingToolbarLayout) {
        try {
            final Field field = collapsingToolbarLayout.getClass().getDeclaredField("mCollapsingTextHelper");
            field.setAccessible(true);

            final Object object = field.get(collapsingToolbarLayout);
            final Field tpf = object.getClass().getDeclaredField("mTextPaint");
            tpf.setAccessible(true);

            ((TextPaint) tpf.get(object)).setTypeface(Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf"));
            ((TextPaint) tpf.get(object)).setColor(getResources().getColor(R.color.black));
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(i) / (float) maxScroll;

        Log.d("check_offset",": "+i);
        Log.d("check_maxscroll",": "+maxScroll);
        Log.d("check_percentage",": "+percentage);



        if (percentage>0.82){
            activityResturantDeatilBinding.toolbar.setNavigationIcon(R.drawable.back_arrow_white);
        }else {
            activityResturantDeatilBinding.toolbar.setNavigationIcon(R.drawable.back_arrow_black);
        }
    }

    public void openCustomBrowserTab(String url){
        // initializing object for custom chrome tabs.
        CustomTabsIntent.Builder customIntent = new CustomTabsIntent.Builder();

        // below line is setting toolbar color
        // for our custom chrome tab.
        customIntent.setToolbarColor(ContextCompat.getColor(ResturantDetailsActivity.this, R.color.bg_color));

        // we are calling below method after
        // setting our toolbar color.
        openCustomTab(ResturantDetailsActivity.this, customIntent.build(), Uri.parse(url));
    }

    public void openCustomTab(Activity activity, CustomTabsIntent customTabsIntent, Uri uri) {
        // package name is the default package
        // for our custom chrome tab
        String packageName = "com.android.chrome";
        if (packageName != null) {

            // we are checking if the package name is not null
            // if package name is not null then we are calling
            // that custom chrome tab with intent by passing its
            // package name.
            customTabsIntent.intent.setPackage(packageName);

            // in that custom tab intent we are passing
            // our url which we have to browse.
            customTabsIntent.launchUrl(activity, uri);
        } else {
            // if the custom tabs fails to load then we are simply
            // redirecting our user to users device default browser.
            activity.startActivity(new Intent(Intent.ACTION_VIEW, uri));
        }
    }
}
