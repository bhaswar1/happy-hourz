package com.admin.happyhours.ui.helpsupport;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class HelpSupportActivityModule {

    @Provides
    HelpSupportViewModel provideSplashViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new HelpSupportViewModel(dataManager, schedulerProvider);
    }

}
