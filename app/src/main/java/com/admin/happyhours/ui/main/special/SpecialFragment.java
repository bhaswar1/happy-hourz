package com.admin.happyhours.ui.main.special;


import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.admin.happyhours.BR;
import com.admin.happyhours.data.model.api.response.ServiceDetail;
import com.admin.happyhours.helper.OnSwipeTouchListener;
import com.facebook.ads.AdSettings;
import com.google.android.material.appbar.AppBarLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CusineDetail;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.admin.happyhours.data.model.api.response.TagDetail;
import com.admin.happyhours.databinding.FragmentSpecialBinding;
import com.admin.happyhours.datatypecommon.EventbusForSpecialListingOptionClick;
import com.admin.happyhours.ui.base.BaseFragment;
import com.admin.happyhours.ui.main.HomeActivity;
import com.admin.happyhours.ui.main.special.recycleradapter.SpecialAdapter;
import com.admin.happyhours.ui.resturantdeatails.ResturantDetailsActivity;
import com.admin.happyhours.utils.AppConstants;
import com.admin.happyhours.utils.GlobalBus;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.AudienceNetworkAds;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;

import java.util.List;

import javax.inject.Inject;

import static android.content.Context.MODE_PRIVATE;


public class SpecialFragment extends BaseFragment<FragmentSpecialBinding, SpecialViewModel> implements
        SpecialNavigator, AdListener {

    public static GoogleMap mMap;
    public static Marker myMarker;
    public int count=1;
    public  int mapcount=1;
    public  boolean filterCheck=false;

    public  JsonArray resaturantlist;

    List< SpecialPageRestuarantListingResponse.Detail> restuurantDetailList;

    ArrayList<Double> latitude = new ArrayList<>();
    ArrayList<Double> longitude = new ArrayList<>();
    ArrayList<String> arrayListTitle = new ArrayList<>();
    public static Marker myMarker1[]=new Marker[1000];

    private @Nullable AdView bannerAdView;

   public ArrayList<String> tagId,tagDrinksId,serviceId;
    ArrayList<String> cusinId;
   public String centerLat;
   public String centerLong;
    private SpecialAdapter specialAdapter;
    FragmentSpecialBinding fragmentSpecialBinding;
    SpecialViewModel specialViewModel;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private SupportMapFragment mapFragment;
    public  int deals=2;
    public  int progress_distance=-1;
    LinearLayoutManager linearLayoutManager;
    private FirebaseAnalytics mFirebaseAnalytics;

    Integer currentPage = -1;
    SnapHelper snapHelper = new LinearSnapHelper();
    private Boolean actionExecuted = false;
    private Integer oldScrollPosition = 0;
    private Boolean checkClickAndScroll = false;

    public static SpecialFragment newInstance() {
        Bundle args = new Bundle();
        SpecialFragment fragment = new SpecialFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_special;
    }

    @Override
    public SpecialViewModel getViewModel() {
        specialViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SpecialViewModel.class);
        return specialViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        specialViewModel.setNavigator(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        GlobalBus.getEventBus().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        GlobalBus.getEventBus().register(this);
      //  Log.d("hii","--");
      //  getBaseActivity().showLoading();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       // String strtext = getArguments().getString("edttext");




        return super.onCreateView(inflater, container, savedInstanceState);


    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentSpecialBinding = getViewDataBinding();
        setUpRecyclerViewAndLayoutManager();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());

   //     Crashlytics.getInstance().crash();

        SharedPreferences sharedpref1 = getContext().getSharedPreferences("MySharedPreference", MODE_PRIVATE);
        Log.d("sharedpref","--"+new Gson().toJson(sharedpref1));
         progress_distance = sharedpref1.getInt("progress", -1);
         deals=sharedpref1.getInt("deals",2);
        final String tag_id=sharedpref1.getString("tag_id",null);
        Gson gson = new Gson();
        String tagDrinkjson=sharedpref1.getString("tagDrinkDetails", null);
        String json = sharedpref1.getString("tagDetails", null);
        String cusinjson = sharedpref1.getString("cusinsDetails", null);
        String serviceJson = sharedpref1.getString("serviceDetails", null);

        Type type = new TypeToken<List<TagDetail>>(){}.getType();
        List<TagDetail> tagDetails = gson.fromJson(json, type);
        Log.d("cusinsarray","---"+new Gson().toJson(cusinjson));


       // Type typeDrinks = new TypeToken<List<TagDetail>>(){}.getType();
        List<TagDetail> tagDetailsDrink = gson.fromJson(tagDrinkjson, type);
        Log.d("tagdrinks","---"+new Gson().toJson(tagDetailsDrink));

        Type cusintype = new TypeToken<List<CusineDetail>>(){}.getType();
        List<CusineDetail> cusinDetails = gson.fromJson(cusinjson, cusintype);
        Log.d("tagdetails","----"+new Gson().toJson(json));
        Type servicetype = new TypeToken<List<ServiceDetail>>(){}.getType();
        List<ServiceDetail> serviceDetails = gson.fromJson(serviceJson, servicetype);

//        Type tagDrinktype = new TypeToken<List<CusineDetail>>(){}.getType();
//        List<CusineDetail> cusinDetails = gson.fromJson(cusinjson, cusintype);

       // List<String> listagem = obj.createQuery("Select t.dataNasc  from Users t", String.class).getResultList()''
         tagId =new ArrayList<>();
        tagDrinksId =new ArrayList<>();
        serviceId=new ArrayList<>();
        cusinId = new ArrayList<>();
        tagId.clear();
        cusinId.clear();
        serviceId.clear();
        tagDrinksId.clear();
       if(cusinjson!=null){
        //   cusinDetails.clear();
           for(int i=0;i<cusinDetails.size();i++){
               if(cusinDetails.get(i).isCheck()){
                   cusinId.add(cusinDetails.get(i).getCusinsid());
               }
           }
       }
            Log.d("cusinsarray","---"+new Gson().toJson(cusinId));
       if(json!=null){
           for(int i=0;i<tagDetails.size();i++){
               if(tagDetails.get(i).isCheck()){
                   tagId.add(tagDetails.get(i).getTagid());
               }
           }
       }
        Log.d("tagId","---"+new Gson().toJson(tagId));

        if(tagDrinkjson!=null){
            for(int i=0;i<tagDetailsDrink.size();i++){
                if(tagDetailsDrink.get(i).isCheck()){
                    tagDrinksId.add(tagDetailsDrink.get(i).getTagid());
                }
            }
        }

        if(serviceJson!=null){
            for(int i=0;i<serviceDetails.size();i++){
                if(serviceDetails.get(i).isCheck()){
                    serviceId.add(serviceDetails.get(i).getServiceid());
                }
            }
        }
        Log.d("serviceId","p--"+new Gson().toJson(serviceId));


        Log.d("tagdetailsDrink","p--"+new Gson().toJson(tagDrinksId));

    //  Toast.makeText(getActivity(),"--"+budgeted,Toast.LENGTH_LONG).show();
        NormalLoginResponse.Details responseUser = new Gson().fromJson(specialViewModel.getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser!=null && responseUser.getId() != null ) {

            getBaseActivity().showLoading();
           if( progress_distance!=-1||deals!=2|| tag_id!=null || tagDetails!=null || cusinDetails!=null ) {
               filterCheck=true;
               mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(specialViewModel.getDataManager().getCenterLat()),Double.parseDouble(specialViewModel.getDataManager().getCenterLong())), 14f));

               specialViewModel.fetchRestuarantListing(responseUser.getId(),
                       specialViewModel.getDataManager().getCenterLat(),
                       specialViewModel.getDataManager().getCenterLong(),Integer.toString(deals)
                       ,Integer.toString(progress_distance),tagId,cusinId,tagDrinksId,serviceId);
             //  Toast.makeText(getActivity(),"inside this",LENGTH_LONG).show();






            }else{
           //   Toast.makeText(getActivity(),"inside",LENGTH_LONG).show();

              specialViewModel.fetchRestuarantListing(responseUser.getId(),
                      specialViewModel.getDataManager().getLatitude(), specialViewModel.getDataManager().getLongitude(),Integer.toString(deals),"",null,null,null,null);


//               specialViewModel.fetchRestuarantListing(responseUser.getId(),
//                       specialViewModel.getDataManager().getLatitude(), specialViewModel.getDataManager().getLongitude(),"","",null,null);

            }
        }

        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        fragmentSpecialBinding.layoutMap.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                 if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    fragmentSpecialBinding.layoutMap.getParent().requestDisallowInterceptTouchEvent(true);
                }
                return false;

            }
        });



        //setAppBarDragging(fragmentSpecialBinding.appbarContainer, false);
        //


        fragmentSpecialBinding.textView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            mapcount=2;
                NormalLoginResponse.Details responseUser = new Gson().fromJson(specialViewModel.getDataManager().getCurrentUserInfo(),
                        NormalLoginResponse.Details.class);

                fragmentSpecialBinding.textView10.setVisibility(View.GONE);
                getBaseActivity().showLoading();
                if(progress_distance == -1) {

                    tagId.clear();
                    cusinId.clear();
                    specialViewModel.fetchRestuarantListing(responseUser.getId(),
                            centerLat, centerLong, Integer.toString(deals), "", null, null,null,null);

                    Bundle bundle = new Bundle();
                    bundle.putString("userId", responseUser.getId());
                    bundle.putString("userName", responseUser.getUserName());
                    // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
                    //  bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, resturantName);
                    bundle.putString("userAdress",responseUser.getAddress());
                    bundle.putString("userphoneno",responseUser.getPhone());
                    bundle.putString("lattitude",centerLat);
                    bundle.putString("longitude",centerLong);
                    mFirebaseAnalytics.logEvent("home_page_search_here_clicked_android", bundle);

                }else{

                    specialViewModel.fetchRestuarantListing(responseUser.getId(),
                            centerLat, centerLong, Integer.toString(deals), Integer.toString(progress_distance), tagId, cusinId,tagDrinksId,serviceId);



                    Bundle bundle = new Bundle();
                    bundle.putString("userId", responseUser.getId());
                    bundle.putString("userName", responseUser.getUserName());
                    // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
                    //  bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, resturantName);
                    bundle.putString("userAdress",responseUser.getAddress());
                    bundle.putString("userphoneno",responseUser.getPhone());
                    bundle.putString("lattitude",centerLat);
                    bundle.putString("longitude",centerLong);
                    mFirebaseAnalytics.logEvent("home_page_search_here_clicked_android", bundle);

                }


            }
        });


        ///////map
        latitude.clear();
        latitude.clear();
        ;latitude.clear();
        latitude.add(Double.parseDouble(specialViewModel.getDataManager().getLatitude()));
        longitude.add(Double.parseDouble(specialViewModel.getDataManager().getLongitude()));
        arrayListTitle.add("Your Location");
        showMarkerOnMapForNearby(getContext(), mapFragment, latitude, longitude, arrayListTitle);

            fragmentSpecialBinding.curretLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    latitude.clear();
                    latitude.clear();
                    ;latitude.clear();
                    latitude.add(Double.parseDouble(specialViewModel.getDataManager().getLatitude()));
                    longitude.add(Double.parseDouble(specialViewModel.getDataManager().getLongitude()));
                    arrayListTitle.add("Your Location");
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(specialViewModel.getDataManager().getLatitude()),Double.parseDouble(specialViewModel.getDataManager().getLongitude())), 14f));
                    fragmentSpecialBinding.textView10.setVisibility(View.GONE);
                    getBaseActivity().showLoading();
                    NormalLoginResponse.Details responseUser = new Gson().fromJson(specialViewModel.getDataManager().getCurrentUserInfo(),
                            NormalLoginResponse.Details.class);

                    if(progress_distance==-1) {
                        tagId.clear();
                        cusinId.clear();
                        specialViewModel.fetchRestuarantListing(responseUser.getId(),
                                specialViewModel.getDataManager().getLatitude(), specialViewModel.getDataManager().getLongitude(), Integer.toString(deals), "", null, null,null,null);
                    }else{

                        specialViewModel.fetchRestuarantListing(responseUser.getId(),
                                specialViewModel.getDataManager().getLatitude(), specialViewModel.getDataManager().getLongitude(), Integer.toString(deals), Integer.toString(progress_distance), tagId, cusinId,tagDrinksId,serviceId);

                    }

                }
            });
//    setAdView();

        fragmentSpecialBinding.llSwipe.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
                //Toast.makeText(getActivity(), "top", Toast.LENGTH_SHORT).show();
                Log.e("swipe","top");
                //toggle(false);
                //fragmentSpecialBinding.layoutMap.setVisibility(View.GONE);
                hideView(fragmentSpecialBinding.layoutMap);
            }
            public void onSwipeRight() {
                //Toast.makeText(getActivity(), "right", Toast.LENGTH_SHORT).show();
                Log.e("swipe","right");
            }
            public void onSwipeLeft() {
                //Toast.makeText(getActivity(), "left", Toast.LENGTH_SHORT).show();
                Log.e("swipe","left");
            }
            public void onSwipeBottom() {
                //Toast.makeText(getActivity(), "bottom", Toast.LENGTH_SHORT).show();
                Log.e("swipe","bottom");
                //toggle(true);
                //fragmentSpecialBinding.layoutMap.setVisibility(View.VISIBLE);
                showView(fragmentSpecialBinding.layoutMap);
            }

        });
    }

    private void showView(View view){
        // Prepare the View for the animation
        view.setVisibility(View.VISIBLE);
        view.setAlpha(0.0f);

        // Start the animation
        view.animate()
                .translationY(0)
                .setDuration(500)
                .alpha(1.0f)
                .setListener(null);
    }

    private void hideView(View view){
        view.animate()
                .translationY(0)
                .setDuration(200)
                .alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.setVisibility(View.GONE);
                    }
                });
    }

    private void setAdView() {

        AudienceNetworkAds.initialize(getActivity());
        boolean isTablet=false;
        if (bannerAdView != null) {
            bannerAdView.destroy();
            bannerAdView = null;
        }
//        AdSettings.addTestDevice("54QD9oQFd/wXlylLSf///hrrJ6I=");
//        2291339747630003_2465876356843007
        bannerAdView = new AdView(this.getActivity(), "IMG_16_9_APP_INSTALL#2291339747630003_2465876356843007",
                isTablet ? AdSize.BANNER_HEIGHT_90 : AdSize.BANNER_HEIGHT_50);

        // Reposition the ad and add it to the view hierarchy.
        fragmentSpecialBinding.bannerAdContainer.addView(bannerAdView);
        //  bannerAdView.setAdListener(this);

        // Set a listener to get notified on changes or when the user interact with the ad.




        AdListener adListener = new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                Toast.makeText(
                        getContext(),
                        "Error: " + adError.getErrorMessage(),
                        Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Ad loaded callback
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
            }
        };
        // Initiate a request to load an ad.
//        bannerAdView.loadAd();
        bannerAdView.loadAd(bannerAdView.buildLoadAdConfig().withAdListener(adListener).build());

//        bannerAdView.loadAd(bannerAdView.buildLoadAdConfig().withAdListener(this).build());
        fragmentSpecialBinding.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragmentSpecialBinding.bannerAdContainer.removeView(bannerAdView);
            }
        });
    }



    // Disable the drag on AppBar inside Coordinator Layout
    private void setAppBarDragging(AppBarLayout appBarLayout, final boolean newValue) {

        CoordinatorLayout.LayoutParams params =
                (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        AppBarLayout.Behavior behavior = new AppBarLayout.Behavior();
        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
            @Override
            public boolean canDrag(AppBarLayout appBarLayout) {
                return newValue;
            }
        });
        params.setBehavior(behavior);
    }


    private void setUpRecyclerViewAndLayoutManager() {
         linearLayoutManager = new LinearLayoutManager(getActivity());
        fragmentSpecialBinding.rcvList.setLayoutManager(linearLayoutManager);
        fragmentSpecialBinding.rcvList.setHasFixedSize(true);
        specialAdapter = new SpecialAdapter(getActivity());
        fragmentSpecialBinding.rcvList.setAdapter(specialAdapter);
        //snapHelper.attachToRecyclerView(fragmentSpecialBinding.rcvList);

        fragmentSpecialBinding.rcvList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                /*if(newState == RecyclerView.SCROLL_STATE_IDLE) {
                    View centerView = snapHelper.findSnapView(linearLayoutManager);
                    int pos = linearLayoutManager.getPosition(centerView);
                    Log.e("Snapped Item Position:",""+pos);
                }*/

                /*if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    // LinearLayoutManager
                    Integer pos = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                    Log.e("Snapped Item Position:",""+pos);
                }*/

                switch (newState){
                    case RecyclerView.SCROLL_STATE_DRAGGING : actionExecuted = false;
                    case RecyclerView.SCROLL_STATE_SETTLING : actionExecuted = false;

                    case RecyclerView.SCROLL_STATE_IDLE : {
                        Integer scrolledPosition = linearLayoutManager.findLastVisibleItemPosition();

                        if (scrolledPosition != RecyclerView.NO_POSITION && scrolledPosition != oldScrollPosition && !actionExecuted){
                            //action.invoke();
                            actionExecuted = true;
                            Log.e("oldScrollPosition", String.valueOf(oldScrollPosition));
                            oldScrollPosition = scrolledPosition;
                            Log.e("scrolledPosition", String.valueOf(oldScrollPosition));

                            if (checkClickAndScroll){
                                checkClickAndScroll = false;
                            }else {
                                //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(specialAdapter.details.get(oldScrollPosition).getLatitude()),Double.parseDouble(specialAdapter.details.get(oldScrollPosition).getLatitude())), 10f));
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(specialAdapter.details.get(oldScrollPosition).getLatitude()),Double.parseDouble(specialAdapter.details.get(oldScrollPosition).getLongitude())),mMap.getCameraPosition().zoom));
                            }


                            Log.e("latlong", " "+ specialAdapter.details.get(oldScrollPosition).getLatitude() + "/" + specialAdapter.details.get(oldScrollPosition).getLongitude());

                        }
                    };

                }
            }
        });
    }


//    @Override
//    public void onError(Ad ad, AdError error) {
//        if (ad == bannerAdView) {
//            Log.d("erroradd","--"+error.getErrorMessage());
//            setLabel("Ad failed to load: " + error.getErrorMessage());
//        }
//    }



    @Override
    public void onSuccess(SpecialPageRestuarantListingResponse specialPageRestuarantListingResponse, List< SpecialPageRestuarantListingResponse.Detail> detail, JsonArray jsonArray) {
        getBaseActivity().hideLoading();
        if (specialPageRestuarantListingResponse != null) {
            if (specialPageRestuarantListingResponse.getDetails() != null &&
                    specialPageRestuarantListingResponse.getDetails().size() > 0) {

                ((HomeActivity)getActivity()).showHideFilterOptionMenu(true);

                latitude.clear();
                longitude.clear();
                arrayListTitle.clear();
                fragmentSpecialBinding.rcvList.setVisibility(View.VISIBLE);
                fragmentSpecialBinding.tvNoData.setVisibility(View.GONE);


               // if(mapcount==1) {
                    latitude.add(Double.parseDouble(specialViewModel.getDataManager().getLatitude()));
                    longitude.add(Double.parseDouble(specialViewModel.getDataManager().getLongitude()));
                    arrayListTitle.add(getResources().getString(R.string.your_location));
             //   }
                for (int i = 0; i < specialPageRestuarantListingResponse.getDetails().size(); i++) {

                    latitude.add(Double.parseDouble(specialPageRestuarantListingResponse.getDetails().get(i).getLatitude()));
                    longitude.add(Double.parseDouble(specialPageRestuarantListingResponse.getDetails().get(i).getLongitude()));
                    arrayListTitle.add(specialPageRestuarantListingResponse.getDetails().get(i).getRestaurentName());
                }

            //   HelperClassGoogleMap.showMarkerOnMapwithuser(mapFragment, latitude, longitude, arrayListTitle);




         //   if(!filterCheck){

               // HelperClassGoogleMap.showMarkerOnMapwithuser(mapFragment, latitude, longitude, arrayListTitle);
                setMarkerOnMapForNearby(getContext(), mapFragment, latitude, longitude, arrayListTitle,false);


//                if(mapcount==1) {
//
//                    showMarkerOnMapForNearby(getContext(), mapFragment, latitude, longitude, arrayListTitle);
//                    mapcount=2;
//
//                }
//            }else{
//                HelperClassGoogleMap.showMarkerOnMapwithuser(mapFragment, latitude, longitude, arrayListTitle);
//
//            }
                resaturantlist =new JsonArray();
                restuurantDetailList=new ArrayList<>();
                restuurantDetailList=detail;
                resaturantlist=jsonArray;




                specialAdapter.addItems(detail,deals,jsonArray);
            }else {
                setMarkerOnMapForNearby(getContext(), mapFragment, latitude, longitude, arrayListTitle,true);
              //  HelperClassGoogleMap.showMarkerOnMapwithuser(mapFragment, latitude, longitude, arrayListTitle);

            //    latitude.clear();
                showingOfNoDataFound();

                //  ((HomeActivity)getActivity()).showHideFilterOptionMenu(false);
            }
        }
    }

    private void setMarkerOnMapForNearby(Context context, SupportMapFragment mapFragment, ArrayList<Double> latitude, ArrayList<Double> longitude, ArrayList<String> arrayListTitle,boolean errCheck) {
     //   mMap.clear();
      //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(specialViewModel.getDataManager().getLatitude()),Double.parseDouble(specialViewModel.getDataManager().getLongitude())), 14f));
        //myMarker.remove();
        Log.d("test","===="+"inside");
        myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(specialViewModel.getDataManager().getLatitude()), Double.parseDouble(specialViewModel.getDataManager().getLongitude()))).title(arrayListTitle.get(0)).
                icon(BitmapDescriptorFactory.fromResource(R.drawable.locetorcurrent)).draggable(false));

        if(myMarker1[1]!=null){
            // myMarker1.remove();
            // Log.d("inside","-sjs-"+circle[0].getCenter().latitude);
            //  check=false;
            //   distance_lat=distance_lat-1;
            for(int i=1;i<=count;i++) {

                //  Log.d("inside","-j-"+circle[0].getCenter().latitude);
                if(myMarker1[i]!=null) {
                    //    Log.d("inside","-j-"+circle[0].getCenter().latitude);
                    myMarker1[i].remove();
                }
//                                getBaseActivity().showLoading();
//                                specialViewModel.fetchRestuarantListing(specialViewModel.getDataManager().getCurrentUserId().toString(),
//                                       Double.toString(mMap.getCameraPosition().target.latitude), Double.toString(mMap.getCameraPosition().target.longitude),"","",null,null);


            }


            count=1;
        }
        if(errCheck) {
            for(int i=1;i<latitude.size();i++)
                latitude.remove(i);
        }
        int item=1;
        for (int i = 1; i < latitude.size(); i++) {

            //   Log.d("circlelat","--"+distance_lat);
            //  Double resturant_lat = latitudes.get(i) + circle[0].getRadius();

            // Log.d("circlelat1", "-----radious----" + resturant_lat+ "---" + distance_lat );
            //   Log.d("circlelat1", "-----normal----" + latitudes.get(i) + "---" + circle[0].getCenter().latitude);


            Log.d("testset","--");


            //   Double r=circle[0].getCenter().latitude-900;
            // Double l1=latitudes.get(i)+900;
            //  Double centerLat=circle[0].getCenter().latitude+circle[0].getRadius();
            // Double l=latitudes.get(i)+circle[0].getCenter().latitude;
            LatLng latLng1 =new LatLng(mMap.getCameraPosition().target.latitude,mMap.getCameraPosition().target.longitude);
            LatLng latLng2 =new LatLng(latitude.get(i),longitude.get(i));
            Log.d("dis",""+ arrayListTitle.get(i)) ;
            //Log.d("dis2",latLng1.d)
            float[] results = new float[1];
            Location.distanceBetween(latLng1.latitude, latLng1.longitude,
                    latLng2.latitude, latLng2.longitude,
                    results);

            //Log.d("dis",""+ CalculationByDistance(latLng1,latLng2)) ;
            //     if (Double.compare(CalculationByDistance(latLng1,latLng2),5000)<=0) {
            //Log.d("circlelat1", "----" + distance_lat);
            //  myMarker.remove();
            if(!errCheck) {
                myMarker1[item] = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude.get(i), longitude.get(i))).title(arrayListTitle.get(i)).
                        icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_icon)).draggable(false));
                count++;
                item++;
            }

            //  }
        }

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapFragment.onLowMemory();
    }

    private void showingOfNoDataFound(){
       // fragmentSpecialBinding.parentCoordinatorLayout.setVisibility(View.GONE);
        fragmentSpecialBinding.rcvList.setVisibility(View.GONE);
        fragmentSpecialBinding.tvNoData.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(Throwable throwable) {
        getBaseActivity().hideLoading();
        Log.d("inside","==");
    }

    @Override
    public void responseAfterBookmarkOrLike(String check_for_button, int position) {
        getBaseActivity().hideLoading();
        specialAdapter.changeRowDataAfterBookmarkOrLike(check_for_button, position);
    }

    @Override
    public void failed() {
            getBaseActivity().hideLoading();
        setMarkerOnMapForNearby(getContext(), mapFragment, latitude, longitude, arrayListTitle,true);

        showingOfNoDataFound();
    }


    @Subscribe
    public void onEvent(EventbusForSpecialListingOptionClick eventbusForSpecialListingOptionClick) {
        if (eventbusForSpecialListingOptionClick.getCheck_option_click().equals(AppConstants.adapter_img_like)) {
            getBaseActivity().showLoading();


            NormalLoginResponse.Details responseUser = new Gson().fromJson(specialViewModel.getDataManager().getCurrentUserInfo(),
                    NormalLoginResponse.Details.class);

            Bundle bundle = new Bundle();
            bundle.putString("userId", responseUser.getId());
            bundle.putString("userName", responseUser.getUserName());
            bundle.putString("restaurantId",eventbusForSpecialListingOptionClick.getDetail().getRestaurentId());
            bundle.putString("restaurantName",eventbusForSpecialListingOptionClick.getDetail().getRestaurentName());
//            bundle.putString("restaurantRating",eventbusForSpecialListingOptionClick.getDetail().getRating());
//            bundle.putString("restaurantRating",eventbusForSpecialListingOptionClick.getDetail().getRating());

            // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
            //  bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, resturantName);

            mFirebaseAnalytics.logEvent("restaurant_added_to_favourites_android", bundle);

            specialViewModel.bookmarkAndLikeApiHit(eventbusForSpecialListingOptionClick.getCheck_option_click(),
                    eventbusForSpecialListingOptionClick.getPosition(), eventbusForSpecialListingOptionClick.getDetail());
        }else {
            if (eventbusForSpecialListingOptionClick.getDetail().getCollectionStatus()==0){
                ((HomeActivity)getActivity()).goFromSpecialToCollection(new Gson().toJson(eventbusForSpecialListingOptionClick.getDetail()));
//                startActivity(TransparentRestaurantListingActivity.newIntent(getActivity()));
            }else {
                NormalLoginResponse.Details responseUser = new Gson().fromJson(specialViewModel.getDataManager().getCurrentUserInfo(),
                        NormalLoginResponse.Details.class);

                Bundle bundle = new Bundle();
                bundle.putString("userId", responseUser.getId());
                bundle.putString("userName", responseUser.getUserName());
                bundle.putString("restaurantId",eventbusForSpecialListingOptionClick.getDetail().getRestaurentId());
                bundle.putString("restaurantName",eventbusForSpecialListingOptionClick.getDetail().getRestaurentName());
//            bundle.putString("restaurantRating",eventbusForSpecialListingOptionClick.getDetail().getRating());
//            bundle.putString("restaurantRating",eventbusForSpecialListingOptionClick.getDetail().getRating());

                // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
                //  bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, resturantName);

                mFirebaseAnalytics.logEvent("restaurant_removed_collection_android", bundle);
                specialViewModel.bookmarkAndLikeApiHit(eventbusForSpecialListingOptionClick.getCheck_option_click(),
                        eventbusForSpecialListingOptionClick.getPosition(), eventbusForSpecialListingOptionClick.getDetail());
            }
        }

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==7){
            if (resultCode==8){
                int position = data.getExtras().getInt("item_position");
                boolean isChecked = data.getExtras().getBoolean("checkeFavourite");

                if(data.hasExtra("rating")){
                    int rating=Integer.parseInt(data.getStringExtra("rating"));
                    specialAdapter.setRatingUpdate(position,rating);
                }

                if(data.hasExtra("review")){
                    if(data.getStringExtra("review")!=null && !data.getStringExtra("review").isEmpty()) {
                        int reveiew = Integer.parseInt(data.getStringExtra("review"));

                        Log.d("review","-----------"+reveiew);
                        specialAdapter.setReviewUpdate(position, reveiew);
                    }
                }
                if(data.hasExtra("collection_status")) {
                    int collectionstatus = data.getExtras().getInt("collection_status");

                    if (collectionstatus == 1) {
                        specialAdapter.changeBoomarkValueOfParticularPosition(position, collectionstatus);
                    } else {
                        specialAdapter.changeBoomarkValueOfParticularPosition(position, 0);

                    }
                }
                if (specialAdapter!=null){
                    if (isChecked) {
                        specialAdapter.changeValueOfParticularPosition(position, 1);
                    }else {
                        specialAdapter.changeValueOfParticularPosition(position, 0);
                    }
                }
                boolean check_for_collection_open_from_details = data.getExtras().getBoolean("check_for_collection_open_from_details");
                final String restaurant_data = data.getExtras().getString("restaurant_data");
                Log.d("retaurantdata","=="+restaurant_data);
                if (check_for_collection_open_from_details){

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((HomeActivity)getActivity()).goFromSpecialToCollection(restaurant_data);
                        }
                    }, 200);
                }
            }
        }
    }

    public  void showMarkerOnMapForNearby(final Context mContext, final SupportMapFragment mapFragment, final ArrayList<Double> latitudes, final ArrayList<Double> longitudes,
                                                final ArrayList<String> mapAddressIds) {

       // selectedMarkerForNearby = null;

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;

                mMap.clear();
                if(progress_distance==-1)
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(specialViewModel.getDataManager().getLatitude()),Double.parseDouble(specialViewModel.getDataManager().getLongitude())), 14f));

                else
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(specialViewModel.getDataManager().getCenterLat()),Double.parseDouble(specialViewModel.getDataManager().getCenterLong())), 10f));

//                mMap.addCircle(new CircleOptions()
//                        .center(new LatLng(mMap.getCameraPosition().target.latitude,mMap.getCameraPosition().target.longitude))
//                        .radius(700)
//                        .strokeColor(Color.RED)
//                        .fillColor(R.color.light_blue));



//                myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(0), longitudes.get(0))).title(mapAddressIds.get(0)).
//                        icon(BitmapDescriptorFactory.fromResource(R.drawable.locetorcurrent)).draggable(false));



//                final Circle[] circle = { mMap.addCircle(new CircleOptions()
//                        .center(new LatLng(mMap.getCameraPosition().target.latitude,mMap.getCameraPosition().target.longitude))
//                        .radius(900)
//                        .strokeColor(R.color.transparent)
//                        .fillColor(R.color.light_blue))};

                mMap.setOnCameraIdleListener(() -> {
//                       circle[0].remove();
//                        circle[0] = mMap.addCircle(new CircleOptions()


//                               .center(new LatLng(mMap.getCameraPosition().target.latitude,mMap.getCameraPosition().target.longitude))
//                               .radius(900)
//                               .strokeColor(R.color.transparent)
//                               .fillColor(R.color.light_blue));
                    centerLat=Double.toString(mMap.getCameraPosition().target.latitude);
                    centerLong=Double.toString(mMap.getCameraPosition().target.longitude);

                    specialViewModel.getDataManager().setCenterLat(centerLat);
                    specialViewModel.getDataManager().setCenterLong(centerLong);

                    //  Double distance_lat = circle[0].getCenter().latitude + circle[0].getRadius();
                    if(mapcount>1)
                    fragmentSpecialBinding.textView10.setVisibility(View.VISIBLE);
                    mapcount++;
//                        specialViewModel.fetchRestuarantListing(specialViewModel.getDataManager().getCurrentUserId().toString(),
//                                Double.toString(mMap.getCameraPosition().target.latitude), Double.toString(mMap.getCameraPosition().target.longitude),"","",null,null);

                    //   Double distance_long = circle[0].getCenter().longitude + circle[0].getRadius();

                    //getBaseActivity().showLoading();
                    if(!filterCheck)
//                        specialViewModel.fetchRestuarantListing(specialViewModel.getDataManager().getCurrentUserId().toString(),
//                                Double.toString(mMap.getCameraPosition().target.latitude), Double.toString(mMap.getCameraPosition().target.longitude),"","",null,null);
                    // myMarker.remove();


                    Log.d("inside","--"+myMarker1[1]);





                });


                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

                    @Override
                    public void onInfoWindowClick(Marker marker) {
//                        Toast.makeText(getContext(), "clicked", Toast.LENGTH_SHORT).show();


                        int j=0;
                        JsonArray finalResturantlist=new JsonArray();
                        ArrayList<SpecialPageRestuarantListingResponse.Detail> finalresturantDetaillist=new ArrayList<>();


                        if(resaturantlist!=null  && restuurantDetailList!=null) {

                            for (int i = 0; i < resaturantlist.size(); i++) {
                                JsonObject jsonObject = resaturantlist.get(i).getAsJsonObject();
                                Log.d("itemsare", "--" + new Gson().toJson(jsonObject.get("restaurent_name")));

                                String restname = jsonObject.get("restaurent_name").toString();


                                if (restname.substring(1, restname.length() - 1).equals(marker.getTitle())) {
                                    //    Log.d("itemsare","-n-"+restname+"details"+new Gson().toJson(resaturantlist.get(i)));

                                    j = i;
                                    //  finalResturantlist=new JsonArray();
                                    // finalresturantDetaillist.add(restuurantDetailList.get(i));
                                    //  finalResturantlist.add(resaturantlist.get(i));
                                    break;

                                }
                            }


                            String shareBody = new Gson().toJson(restuurantDetailList.get(j));

                            startActivityForResult(new Intent(getContext(), ResturantDetailsActivity.class).
                                    putExtra("lattitude", restuurantDetailList.get(j).getLatitude()).
                                    putExtra("longitude", restuurantDetailList.get(j).getLongitude()).
                                    putExtra("item_position", j).
                                    putExtra("checklike", "notok").
                                    putExtra("resturantid", restuurantDetailList.get(j).getRestaurentId()).
                                    putExtra("details", shareBody), 7);


                        }

                    }
                });



                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        int j=0;
                        JsonArray finalResturantlist=new JsonArray();
                        ArrayList<SpecialPageRestuarantListingResponse.Detail> finalresturantDetaillist=new ArrayList<>();


                        if(resaturantlist!=null) {

                            for (int i = 0; i < resaturantlist.size(); i++) {
                                JsonObject jsonObject = resaturantlist.get(i).getAsJsonObject();
                                Log.d("itemsare", "--" + new Gson().toJson(jsonObject.get("restaurent_name")));

                                String restname = jsonObject.get("restaurent_name").toString();


                                if (restname.substring(1, restname.length() - 1).equals(marker.getTitle())) {
                                    Log.d("itemsare", "-n-" + restname);

                                    j = i;
                                    //  finalResturantlist=new JsonArray();
                                    // finalresturantDetaillist.add(restuurantDetailList.get(i));
                                    //  finalResturantlist.add(resaturantlist.get(i));
                                    break;

                                }
                            }

                        }
                        Log.d("position","--"+j);
//dont needed
//
//                        for(int i=0;i<resaturantlist.size();i++){
//                            JsonObject jsonObject=resaturantlist.get(i).getAsJsonObject();
//                            Log.d("itemsare","--"+new Gson().toJson(jsonObject.get("restaurent_name")));
//
//                            String restname=jsonObject.get("restaurent_name").toString();
//
//
//                            if(!(restname.substring(1,restname.length()-1).equals(marker.getTitle()))){
//                                Log.d("itemsare","-n-"+restname);
//                               // finalResturantlist=new JsonArray();
//                                finalResturantlist.add(resaturantlist.get(i));
//                                finalresturantDetaillist.add(restuurantDetailList.get(i));
//
//                                //  break;
//
//                            }
//                        }
//                        specialAdapter.addItems(finalresturantDetaillist,deals,finalResturantlist);

                        Log.d("itemsare","----------------"+new Gson().toJson(finalResturantlist));
                        Log.d("itemsare","----------------"+new Gson().toJson(finalresturantDetaillist));

                       // linearLayoutManager.scrollToPositionWithOffset(j, resaturantlist.size());


//                        fragmentSpecialBinding.rcvList.smoothScrollToPosition(j);


                        checkClickAndScroll = true;
                        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(getContext()) {
                            @Override protected int getVerticalSnapPreference() {
                                return LinearSmoothScroller.SNAP_TO_START;
                            }
                        };
                        smoothScroller.setTargetPosition(j);
                        linearLayoutManager.startSmoothScroll(smoothScroller);
                    //    Toast.makeText(getActivity(),"clicked"+marker.getPosition(),LENGTH_LONG).show();

                        return false;
                    }
                });
                mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {


                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
//                        latitude=String.valueOf(marker.getPosition().latitude);
//                        longitude= String.valueOf(marker.getPosition().longitude);
//
//                        Log.v("latitude", String.valueOf(marker.getPosition().latitude));
//                        Log.v("longitude", String.valueOf(marker.getPosition().longitude));
//                        getAddressFromLocation((marker.getPosition().latitude), marker.getPosition().longitude);
                    }
                });

//                for (int i = 0; i < latitudes.size(); i++) {
//                    myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(i), longitudes.get(i))).title(mapAddressIds.get(i)).
//                            icon(BitmapDescriptorFactory.fromResource(R.drawable.locator)).draggable(false));
//                }

//                if (latitudes.size() != 0) {
//                  //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudes.get(0), longitudes.get(0)), 13f));
//
//                    try {
//                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//                            @Override
//                            public boolean onMarkerClick(Marker marker) {
//
//                                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.loc));
//                                if (selectedMarkerForNearby != null) {
//                                    selectedMarkerForNearby.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.locator));
//                                }
//                                selectedMarkerForNearby = marker;
//                                String markerId = marker.getTitle();
//                               // onItemClickReturnString.onItemClick(markerId);
//                                return true;
//                            }
//                        });
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
                //mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(mContext, nearbyProductJsonArr));

            }
        });
    }

    public  double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));

        double meter = (valueResult % 1000)+(kmInDec*1000);
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return meter;
    }


    @Override
    public void onError(Ad ad, AdError adError) {
        Log.d("erroradd","--"+adError.getErrorMessage());


    }

    @Override
    public void onAdLoaded(Ad ad) {

    }

    @Override
    public void onAdClicked(Ad ad) {

    }

    @Override
    public void onLoggingImpression(Ad ad) {

    }
}
