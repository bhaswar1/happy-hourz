package com.admin.happyhours.ui.main.events;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class EventProvider {

    @ContributesAndroidInjector(modules = EventModule.class)
    abstract EventFragment provideEventFragmentFactory();


}
