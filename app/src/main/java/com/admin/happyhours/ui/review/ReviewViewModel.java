package com.admin.happyhours.ui.review;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.ProfileImageUpdateResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import java.io.File;
import java.util.ArrayList;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Multipart;

public class ReviewViewModel extends BaseViewModel<ReviewNavigator> {
    ReviewViewModel reviewViewModel;


    public ReviewViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
    public void setReviewData(String userid, ArrayList<String> collectionimage,String star,String restaurantId,String comment
    ) {
       ArrayList< MultipartBody.Part> list=new ArrayList<>();


//        MultipartBody.Part body = null;
//        for(int index=0;index<collectionimage.size();index++) {
//            if (collectionimage != null) {
//                File file = collectionimage.get(index);
//                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
//                body = MultipartBody.Part.createFormData("otherimage", file.getName(), reqFile);
//
//            }
//            list.add(body);
//        }

        //hggs
        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[collectionimage.size()];

        for (int index = 0; index < collectionimage.size(); index++) {
            String path=collectionimage.get(index);
           Log.d("imagespath", "requestUploadSurvey: survey image " + index + "  " + collectionimage.get(index));
            File file = new File(path);


            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
            //surveyImagesParts[index]=MultipartBody.Part.create(surveyBody)


            surveyImagesParts[index] = MultipartBody.Part.createFormData("otherimage["+index+"]", file.getName(), surveyBody);
        }
        //nsnns
        Log.d("array","=="+String.valueOf(surveyImagesParts.length));

        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), userid);
        RequestBody star_value = RequestBody.create(MediaType.parse("text/plain"), star);

        RequestBody comment_data = RequestBody.create(MediaType.parse("text/plain"), comment);

        RequestBody restaurant_id = RequestBody.create(MediaType.parse("text/plain"), restaurantId);

        Log.e("userid",userid);

        //  RequestBody collec_name = RequestBody.create(MediaType.parse("text/plain"), collection_name);
        // RequestBody TYPE = RequestBody.create(MediaType.parse("text/plain"), type);
        //  RequestBody COLLECTION_ID = RequestBody.create(MediaType.parse("text/plain"), collection_id);

        Disposable disposable = ApplicationClass.getRetrofitService()
                .setReview(userid, star,comment,restaurantId,surveyImagesParts)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<CommonSimpleResponse>() {
                    @Override
                    public void accept(CommonSimpleResponse response) throws Exception {
                        if (response != null && response.getStatus().toLowerCase().equals("success")) {

                            getNavigator().success(response);

                        } else {
                            Log.d("check_response_update", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getNavigator().error();
                        Log.d("check_response_update", ": " + throwable.getMessage());


                    }
                });

        getCompositeDisposable().add(disposable);
    }

}
