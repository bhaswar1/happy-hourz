package com.admin.happyhours.ui.main.myaccount;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MyAccountProvider {

    @ContributesAndroidInjector(modules = MyAccountModule.class)
    abstract MyAccountFragment provideAccountFragmentFactory();


}
