package com.admin.happyhours.ui.signup;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class SignupModule {


    @Provides
    SignupViewModel provideSignupViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new SignupViewModel(dataManager, schedulerProvider);
    }

}
