package com.admin.happyhours.ui.forgotpassword;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.databinding.ActivityForgotPasswordBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.utils.CommonUtils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import javax.inject.Inject;

public class ForgotPasswordActivity extends BaseActivity<ActivityForgotPasswordBinding, ForgotPasswordViewModel> implements ForgotPasswordNavigator {

    private String TAG = ForgotPasswordActivity.this.getClass().getSimpleName();
    ActivityForgotPasswordBinding activityForgotPasswordBinding;
    @Inject
    ForgotPasswordViewModel forgotPasswordViewModel;
    String gender = "";
    DatePickerDialog datePickerDialog;

    boolean forgtpasscheck=false;
    String uid;
    private FirebaseAnalytics mFirebaseAnalytics;

    public static Intent newIntent(Context context) {
        return new Intent(context, ForgotPasswordActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_forgot_password;
    }

    @Override
    public ForgotPasswordViewModel getViewModel() {
        return forgotPasswordViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStatusBarHidden(ForgotPasswordActivity.this);
        forgotPasswordViewModel.setNavigator(this);
        activityForgotPasswordBinding = getViewDataBinding();
        Toolbar toolbar=activityForgotPasswordBinding.toolbar;
        toolbar.setNavigationIcon(R.drawable.back_arrow_black);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

     //   mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);



            activityForgotPasswordBinding.btEnter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("insidethe ","--first");

                    String email = activityForgotPasswordBinding.emailIdTv.getText().toString().trim();
                    if (email == null || email.length() == 0) {
                        activityForgotPasswordBinding.emailIdTv.requestFocus();
                        activityForgotPasswordBinding.emailIdTv.setError(getString(R.string.email_can_not_be_blank));
                    } else {
                        if (!CommonUtils.isEmailValid(email.trim())) {
//            activityLoginBinding.textInputLayoutEmail.setErrorEnabled(true);
//            activityLoginBinding.textInputLayoutEmail.setError(getString(R.string.invalid_email));
                            activityForgotPasswordBinding.emailIdTv.requestFocus();
                            activityForgotPasswordBinding.emailIdTv.setError(getString(R.string.invalid_email));
                            // return false;
                        } else {
                            showLoading();
                            forgotPasswordViewModel.sendEmail(email);


                        }

                    }


                }
            });


            activityForgotPasswordBinding.btFenter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("insidethe ","--2nd");

                    String email = activityForgotPasswordBinding.emailIdTv.getText().toString().trim();
                    String newPass=activityForgotPasswordBinding.etNewpass.getText().toString().trim();
                    String conPass=activityForgotPasswordBinding.etConfirmpass.getText().toString().trim();

                    if (email == null || email.length() == 0) {
                        activityForgotPasswordBinding.emailIdTv.requestFocus();
                        activityForgotPasswordBinding.emailIdTv.setError("verification code  can not be blank");
                       // activityForgotPasswordBinding.etConfirmpass.setText("");
                       // activityForgotPasswordBinding.etNewpass.setText("");

                    } else {

                        if(newPass.length()==0 || newPass==null){
                            activityForgotPasswordBinding.etNewpass.setError("new password can not be blank");
                            activityForgotPasswordBinding.etNewpass.requestFocus();
                            activityForgotPasswordBinding.etConfirmpass.setText("");

                        }else {

                            if (newPass.trim().length() < 6) {
                                activityForgotPasswordBinding.etNewpass.setError(getResources().getString(R.string.password_atleast));
                                activityForgotPasswordBinding.etNewpass.requestFocus();
                                activityForgotPasswordBinding.etConfirmpass.setText(null);
                            } else {


                                if (conPass.length() == 0 || conPass == null) {
                                    activityForgotPasswordBinding.etConfirmpass.setError("confirm password can not be blank");
                                    activityForgotPasswordBinding.etConfirmpass.requestFocus();
                                    //activityForgotPasswordBinding.etConfirmpass.setText("");
                                } else {
                                    if (!conPass.equals(newPass)) {
                                        activityForgotPasswordBinding.etConfirmpass.setError(getString(R.string.confrm_and_new_pass));

                                    } else {
                                        showLoading();
                                        //   Toast.makeText(ForgotPasswordActivity.this,"okkkkk",Toast.LENGTH_LONG).show();
                                        forgotPasswordViewModel.changePassword(uid, newPass, email);
//                                        NormalLoginResponse.Details responseUser = new Gson().fromJson(forgotPasswordViewModel.getDataManager().getCurrentUserInfo(),
//                                                NormalLoginResponse.Details.class);
//                                        Bundle bundle = new Bundle();
//                                        bundle.putString("userId", responseUser.getId());
//                                        bundle.putString("userName", responseUser.getUserName());
//                                        // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
//                                        //  bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, resturantName);
//
//                                        mFirebaseAnalytics.logEvent("forgot_password_android", bundle);



                                    }
                                }
                            }
                        }
                    }


                }
            });

        }









    @Override
    public void onSuccess(String id) {
        hideLoading();
        Toast.makeText(ForgotPasswordActivity.this,"verification code has been sent to your email",Toast.LENGTH_LONG).show();
        activityForgotPasswordBinding.emailIdTv.setText("");

        activityForgotPasswordBinding.btEnter.setVisibility(View.GONE);
        activityForgotPasswordBinding.btFenter.setVisibility(View.VISIBLE);
        activityForgotPasswordBinding.tvEnteremail.setText("Enter the verification code");
        uid=id;

        activityForgotPasswordBinding.group.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(Throwable throwable) {
    }

    @Override
    public void noData() {
            hideLoading();
            Toast.makeText(ForgotPasswordActivity.this,"email id is not registered",Toast.LENGTH_LONG).show();
    }

    @Override
    public void changepasswordSucccces() {
        hideLoading();
        Toast.makeText(ForgotPasswordActivity.this,"succesfully set a new password",Toast.LENGTH_LONG).show();
        onBackPressed();
    }

    @Override
    public void unSuccess() {
        hideLoading();
        Toast.makeText(ForgotPasswordActivity.this,"invalid code",Toast.LENGTH_LONG).show();
    }
}
