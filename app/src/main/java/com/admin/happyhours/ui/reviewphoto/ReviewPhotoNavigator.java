package com.admin.happyhours.ui.reviewphoto;

import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.ReviewPhotoResponse;

public interface ReviewPhotoNavigator {

    void onSuccess(NormalLoginResponse normalLoginResponse);

    void onError(Throwable throwable);

    void success(ReviewPhotoResponse response);

    void noData();

    void noLoad();
}
