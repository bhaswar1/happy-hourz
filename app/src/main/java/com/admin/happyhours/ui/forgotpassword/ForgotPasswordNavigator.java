package com.admin.happyhours.ui.forgotpassword;

import com.admin.happyhours.data.model.api.response.NormalLoginResponse;

public interface ForgotPasswordNavigator {

    void onSuccess(String id);

    void onError(Throwable throwable);

    void noData();

    void changepasswordSucccces();

    void unSuccess();
}
