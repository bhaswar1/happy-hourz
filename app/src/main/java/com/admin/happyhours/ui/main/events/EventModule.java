package com.admin.happyhours.ui.main.events;

import androidx.lifecycle.ViewModelProvider;

import com.admin.happyhours.ViewModelProviderFactory;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class EventModule {


    @Provides
    EventViewModel eventViewModel(DataManager dataManager,
                                  SchedulerProvider schedulerProvider) {
        return new EventViewModel(dataManager, schedulerProvider);
    }


    @Provides
    ViewModelProvider.Factory provideEventViewModel(EventViewModel profilePageViewModel) {
        return new ViewModelProviderFactory<>(profilePageViewModel);
    }

}
