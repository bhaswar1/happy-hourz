package com.admin.happyhours.ui.main.events;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.EventListingResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class EventViewModel extends BaseViewModel<EventNavigator> {


    public EventViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    void apiHitLikeUnlike(final String check_for_button, final int position, EventListingResponse.Details detail) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("check_button", ": " + check_for_button);
            Log.d("check_adapter_uid", ": " + responseUser.getId());
            Log.d("check_adapter_restid", ": " + detail.getEventid());

            Disposable disposable = ApplicationClass.getRetrofitService()
                    .eventLikeUnlike(responseUser.getId(), detail.getEventid())
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<CommonSimpleResponse>() {
                        @Override
                        public void accept(CommonSimpleResponse response) throws Exception {
                            if (response != null) {

                                if (response.getStatus() != null && response.getStatus().trim().toLowerCase().equals("success")) {
                                    getNavigator().likeUnlike(check_for_button, position);
                                }

                            } else {
                                getNavigator().noData();
                                Log.d("check_response", ": null response");
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response", ": " + throwable.getMessage());
                            getNavigator().onError(throwable);
                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }

    void fetchEventListing(String user_id, int pageination, int limit_of_data_for_single_call, final boolean check) {

        Log.d("user_id", ": " + user_id);
        Log.d("user_pageination", ": " + pageination);
        Log.d("user_limit_of_data_for_single_call", ": " + limit_of_data_for_single_call);

        Disposable disposable = ApplicationClass.getRetrofitService()
                .eventListing(user_id, pageination, limit_of_data_for_single_call)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<EventListingResponse>() {
                    @Override
                    public void accept(EventListingResponse response) throws Exception {


                        if (response != null && response.getStatus().toLowerCase().equals("success")) {
                            getNavigator().onSuccess(response);
                            Log.d("check_response", ": " + new Gson().toJson(response));

                        } else {
                            if(!check)
                             getNavigator().noData();


                            Log.d("check_response", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getNavigator().onError(throwable);
                        Log.d("check_response", ": " + throwable.getMessage());

                    }
                });

        getCompositeDisposable().add(disposable);
    }
}
