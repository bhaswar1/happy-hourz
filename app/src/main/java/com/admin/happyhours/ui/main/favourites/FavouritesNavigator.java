package com.admin.happyhours.ui.main.favourites;

import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.FevouriteEventData;
import com.admin.happyhours.data.model.api.response.FevouriteListData;

public interface FavouritesNavigator {
    void onSuccess(FevouriteListData detail);

    void onEventSuccess(FevouriteEventData response);

    void noData();

    void noload();

    void onSuccessEventDelete(CommonSimpleResponse commonSimpleResponse, int position);

    void onError(Throwable throwable);

    void onSuccessSpecialDelete(CommonSimpleResponse commonSimpleResponse, int position);
}
