package com.admin.happyhours.ui.eventdetails;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.EventDetails;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.databinding.ActivityEventDetailBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.ui.resturantlocation.RasturantLocationActivity;
import com.admin.happyhours.utils.GoogleMapHelper.HelperClassGoogleMap;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.kodmap.app.library.PopopDialogBuilder;

import java.util.LinkedList;
import java.util.Locale;

import javax.inject.Inject;

public class EventDetailActivity extends BaseActivity<ActivityEventDetailBinding, EventDetailViewModel> implements EventDetailNavigator {

    private EventDetails event_detail = null;
    private String TAG = EventDetailActivity.this.getClass().getSimpleName();
    ActivityEventDetailBinding activityEventDetailBinding;
    @Inject
    EventDetailViewModel eventDetailViewModel;
    String gender = "";
    DatePickerDialog datePickerDialog;
    String eventId;
    boolean checkeFavourite = false;
    int item_position;
    String eventImage=null;
    private FirebaseAnalytics mFirebaseAnalytics;


    public static Intent newIntent(Context context) {
        return new Intent(context, EventDetailActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_event_detail;
    }

    @Override
    public EventDetailViewModel getViewModel() {
        return eventDetailViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStatusBarHidden(EventDetailActivity.this);
        eventDetailViewModel.setNavigator(this);
        activityEventDetailBinding = getViewDataBinding();
        Toolbar toolbar = activityEventDetailBinding.toolbar5;
        setSupportActionBar(toolbar);
        activityEventDetailBinding.toolbar5.setNavigationIcon(R.drawable.back_arrow_black);

        activityEventDetailBinding.toolbar5.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        eventId = getIntent().getStringExtra("eventId");

        item_position = Integer.parseInt(getIntent().getStringExtra("position"));
        int fav_stat = Integer.parseInt(getIntent().getStringExtra("favourite_status"));
        if (fav_stat == 1) {
            activityEventDetailBinding.like.setImageResource(R.drawable.like_logo_checked);
            checkeFavourite = true;
        }else{
            activityEventDetailBinding.like.setImageResource(R.drawable.like_logo);

        }
        LinkedList<String> linkedList=new LinkedList<>();

        activityEventDetailBinding.eventimage.setOnClickListener(v -> {

            if(eventImage!=null) {
                linkedList.clear();
                linkedList.add(eventImage);


                Dialog dialog1 = new PopopDialogBuilder(this)
                        // Set list like as option1 or option2 or option3
                        .setList(linkedList, 0)
                        // or setList with initial position that like .setList(list,position)
                        // Set dialog header color
                        .setHeaderBackgroundColor(android.R.color.transparent)
                        // Set dialog background color
                        .setDialogBackgroundColor(R.color.black)
                        // Set close icon drawable
                        .setCloseDrawable(R.drawable.ic_close_white_24dp)
                        // Set loading view for pager image and preview image
                        .setLoadingView(R.layout.loader_item_layout)
                        // Set dialog style
                        // .setDialogStyle(R.style.DialogStyle)
                        // Choose selector type, indicator or thumbnail
                        .showThumbSlider(false)

                        // Set image scale type for slider image
                        .setSliderImageScaleType(ImageView.ScaleType.FIT_CENTER)
                        // Set indicator drawable
                        //  .setSelectorIndicator(R.drawable.selected_indicato)
                        // Enable or disable zoomable
                        .setIsZoomable(false)

                        // Build Km Slider Popup Dialog
                        .build();
                dialog1.show();

            }
        });


        activityEventDetailBinding.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eventDetailViewModel.getDataManager().getCurrentUserInfo() != null) {
                    showLoading();
                    NormalLoginResponse.Details details = new Gson().fromJson(eventDetailViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);



//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                    eventDetailViewModel.apiHitLikeUnlike(details.getId(), eventId);
                }

            }
        });

        if (eventDetailViewModel.getDataManager().getCurrentUserInfo() != null) {
showLoading();
            NormalLoginResponse.Details details = new Gson().fromJson(eventDetailViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
            eventDetailViewModel.eventDetails(details.getId(), eventId);
        }

        activityEventDetailBinding.tvGetDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (event_detail!=null) {
                    HelperClassGoogleMap.showDirectionFromCurrentToDestinationPlaceInGoogleMapUsingLatiLong(EventDetailActivity.this,
                            event_detail.getVenueLat(), event_detail.getVenueLong());
                }
            }
        });

        activityEventDetailBinding.phoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (event_detail!=null){
                    openDialer(event_detail.getPhoneNumber());
                }
            }
        });
activityEventDetailBinding.address.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        /*startActivity(new Intent(EventDetailActivity.this, RasturantLocationActivity.class)
                .putExtra("lattitude",event_detail.getVenueLat())
                .putExtra("longitude",event_detail.getVenueLong())
                .putExtra("rasturantName",event_detail.getEventName()));*/

        //Open Google Map and Direction
        String my_data= String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=" + event_detail.getVenueLat() + "," + event_detail.getVenueLong() + " (" + event_detail.getEventName() + ")");
        Log.e("mapurl:",my_data);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(my_data));
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }
});


        activityEventDetailBinding.shareIv.setOnClickListener(v -> {
            //  startActivity(Intent(thi));
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = new Gson().toJson(event_detail);
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Event");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,

                    "Join event "+ event_detail.getEventName()+" at "+ event_detail.getVenueName()+ " on "+ event_detail.getEventDate()+". Click on the following link to download Happy Hourz - https://happyhourzapp.com/app/Restaurant_event_deeplink?event_id=" + eventId);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        });


    }


    @Override
    public void onSuccess(EventDetails eventDetails) {
        if (eventDetails != null) {
            event_detail = eventDetails;
            activityEventDetailBinding.parentLl.setVisibility(View.VISIBLE);
            NormalLoginResponse.Details responseUser = new Gson().fromJson(eventDetailViewModel.getDataManager().getCurrentUserInfo(),
                    NormalLoginResponse.Details.class);

            Bundle bundle = new Bundle();
           // bundle.putString(FirebaseAnalytics.Param.ITEM_ID, mResturantDetailsViewModel.getDataManager().getCurrentUserId().toString());
            // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
//            bundle.putString("eventImage", eventDetails.getEventImage());
            bundle.putString("eventadress",eventDetails.getVenueAddress());
          //  bundle.putString("userid",eventDetailViewModel.getDataManager().);
            bundle.putString("eventname",eventDetails.getEventName());
            bundle.putString("venueaddress",eventDetails.getVenueAddress());
            bundle.putString("eventDate",eventDetails.getEventDate()+" "+eventDetails.getEventDay()+" "+eventDetails.getEventYear());
            bundle.putString("venueName",eventDetails.getVenueName());
            bundle.putString("eventstartTime",eventDetails.getEventStartTime());
            bundle.putString("userName",responseUser.getUserName());
            bundle.putString("userId",responseUser.getAddress());
            mFirebaseAnalytics.logEvent("event_details_visited_android", bundle);
            hideLoading();

            eventImage=eventDetails.getEventImage();
//            Glide.with(EventDetailActivity.this)
//                    .load(Uri.parse(eventDetails.getEventImage()))
//                    .into(activityEventDetailBinding.eventimage);

            if(eventDetails.getEventName().trim().length()!=0) {
                String temp = eventDetails.getEventName().trim();


                Character ch = temp.charAt(0);
                String uname = ch.toString().toUpperCase() + temp.substring(1);
                activityEventDetailBinding.eventName.setText(uname);

            }
            Glide.with(EventDetailActivity.this).load(Uri.parse(eventDetails.getEventImage())).apply(RequestOptions.fitCenterTransform().placeholder(R.drawable.no_image_3x).error(R.drawable.no_image_3x)).into(activityEventDetailBinding.eventimage);

            activityEventDetailBinding.address.setText(eventDetails.getVenueAddress());
            activityEventDetailBinding.eventStartTime.setText(eventDetails.getEventStartTime());
            activityEventDetailBinding.eventEndTime.setText("to " + eventDetails.getEventEndTime());

            String number = eventDetails.getPhoneNumber().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
          //  activityResturantDeatilBinding.phoneno.setText(number);
            activityEventDetailBinding.phoneNumber.setText(number);
            activityEventDetailBinding.eventDate.setText(eventDetails.getEventDay());
            activityEventDetailBinding.eventdatelast.setText(eventDetails.getEventMonth() + " " + eventDetails.getEventYear());
            activityEventDetailBinding.description.setText(eventDetails.getEventDescription());
            if (eventDetails.getFavouriteStatus().equals(1))
                activityEventDetailBinding.like.setImageResource(R.drawable.like_logo_checked);

//            Toast.makeText(this, eventDetails.getEventDay(), Toast.LENGTH_SHORT).show();
//            startActivity(LoginActivity.newIntent(EventDetailActivity.this));
//            finish();

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("item_position", item_position);

        intent.putExtra("checkeFavourite", checkeFavourite);
        //  intent.putExtra("check_for_collection_open_from_details",check_for_collection_open_from_details);
        //  intent.putExtra("restaurant_data", new Gson().toJson(restaurantDetails));
        setResult(8, intent);
        super.onBackPressed();
//        Toast.makeText(ResturantDetailsActivity.this, "favourite like value" + checkeFavourite, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onError(Throwable throwable) {
        hideLoading();
        Log.d(TAG, ":-ERROR:-  " + throwable.getMessage());
    }

    @Override
    public void likeUnlike(CommonSimpleResponse response) {
        hideLoading();
        NormalLoginResponse.Details details = new Gson().fromJson(eventDetailViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

        if (response.getMessage().equals("Successfully added to favourite")) {
            activityEventDetailBinding.like.setImageResource(R.drawable.like_logo_checked);
            Toast.makeText(EventDetailActivity.this,getResources().getString(R.string.add_to_favourite),Toast.LENGTH_LONG).show();
            checkeFavourite = true;

            Bundle bundle = new Bundle();
            // bundle.putString(FirebaseAnalytics.Param.ITEM_ID, mResturantDetailsViewModel.getDataManager().getCurrentUserId().toString());
            // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
//            bundle.putString("eventImage", eventDetails.getEventImage());
            bundle.putString("eventadress",event_detail.getVenueAddress());
            //  bundle.putString("userid",eventDetailViewModel.getDataManager().);
            bundle.putString("eventname",event_detail.getEventName());
            bundle.putString("venueaddress",event_detail.getVenueAddress());
            bundle.putString("eventDate",event_detail.getEventDate()+" "+event_detail.getEventDay()+" "+event_detail.getEventYear());
            bundle.putString("venueName",event_detail.getVenueName());
            bundle.putString("eventstartTime",event_detail.getEventStartTime());
            bundle.putString("userName",details.getUserName());
            bundle.putString("userId",details.getAddress());
            mFirebaseAnalytics.logEvent("event_added_to_favourite_android", bundle);


        } else {
            activityEventDetailBinding.like.setImageResource(R.drawable.like_logo);
            checkeFavourite = false;
            Toast.makeText(EventDetailActivity.this,getResources().getString(R.string.removed_success_favourite),Toast.LENGTH_LONG).show();

            Bundle bundle = new Bundle();
            // bundle.putString(FirebaseAnalytics.Param.ITEM_ID, mResturantDetailsViewModel.getDataManager().getCurrentUserId().toString());
            // bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mResturantDetailsViewModel.getDataManager().getCurrentUserInfo());
//            bundle.putString("eventImage", eventDetails.getEventImage());
            bundle.putString("eventadress",event_detail.getVenueAddress());
            //  bundle.putString("userid",eventDetailViewModel.getDataManager().);
            bundle.putString("eventname",event_detail.getEventName());
            bundle.putString("venueaddress",event_detail.getVenueAddress());
            bundle.putString("eventDate",event_detail.getEventDate()+" "+event_detail.getEventDay()+" "+event_detail.getEventYear());
            bundle.putString("venueName",event_detail.getVenueName());
            bundle.putString("eventstartTime",event_detail.getEventStartTime());
            bundle.putString("userName",details.getUserName());
            bundle.putString("userId",details.getAddress());
            mFirebaseAnalytics.logEvent("event_removed_from_favourite_android", bundle);


        }

    }

    @Override
    public void noData() {
hideLoading();
    }



    // Custom method to open dialer app
    protected void openDialer(String phone_number){

        Intent intent = new Intent(Intent.ACTION_DIAL);
        // Send phone number to intent as data
        intent.setData(Uri.parse("tel:" + phone_number));
        // Start the dialer app activity with number
        startActivity(intent);
    }
}
