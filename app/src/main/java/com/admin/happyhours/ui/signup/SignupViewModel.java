package com.admin.happyhours.ui.signup;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class SignupViewModel extends BaseViewModel<SignupNavigator> {


    public SignupViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public void apiHitForSignup(String name, String phone, String email,final String password, String gender,
                                String dob, String device_token, String device_type) {


        Disposable disposable = ApplicationClass.getRetrofitService()
                .normalSignup(name, email, password, gender, dob, phone, device_type, device_token)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<NormalLoginResponse>() {
                    @Override
                    public void accept(NormalLoginResponse response) throws Exception {
                        if (response != null) {
                            // Store last login time
                            Log.d("check_response", ": " + response.getMessage());
                            getNavigator().onSuccess(response);
                            if (response.getDetails() != null && response.getStatus().equals("success")) {
//                                getDataManager().setCurrentUserInfo(new Gson().toJson(response.getDetails()));
                                getDataManager().setCurrentUserInfo(new Gson().toJson(response.getDetails()));
                                getDataManager().setPassword(password);
                            }
                        } else {
                            Log.d("check_response", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response", ": " + throwable.getMessage());
                        getNavigator().onError(throwable);
                    }
                });

        getCompositeDisposable().add(disposable);
    }
}
