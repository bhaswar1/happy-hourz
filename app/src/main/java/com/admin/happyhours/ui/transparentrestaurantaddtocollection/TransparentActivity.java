package com.admin.happyhours.ui.transparentrestaurantaddtocollection;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.databinding.ActivityTransparentBinding;
import com.admin.happyhours.ui.base.BaseActivity;

import javax.inject.Inject;

public class TransparentActivity extends BaseActivity<ActivityTransparentBinding, TransparentViewModel> implements TransparentNavigator {


    ActivityTransparentBinding activityTransparentBinding;
    @Inject
    TransparentViewModel transparentViewModel;

    public static Intent newIntent(Context context) {
        return new Intent(context, TransparentActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_transparent;
    }

    @Override
    public TransparentViewModel getViewModel() {
        return transparentViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityTransparentBinding = getViewDataBinding();
        transparentViewModel.setNavigator(this);
        setStatusBarHidden(TransparentActivity.this);


    }
}
