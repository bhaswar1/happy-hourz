package com.admin.happyhours.ui.main.favourites;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.FevouriteEventData;
import com.admin.happyhours.data.model.api.response.FevouriteListData;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class FavouritesViewModel extends BaseViewModel<FavouritesNavigator> {

    public FavouritesViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    void setResturantDetail(String userId, int page, int limit,final  boolean check) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("check_uid", ": " + userId);
            Log.d("check_limit", ": " + limit);

            Disposable disposable = ApplicationClass.getRetrofitService()
                    .fevouriteDetail(responseUser.getId(), page, limit)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<FevouriteListData>() {
                        @Override
                        public void accept(FevouriteListData response) throws Exception {

                            if (response != null && response.getStatus().equals("success")) {
                                Log.d("check_response", "--" + new Gson().toJson(response));
                                Log.d("check", "---" + response.getDetails().get(0).getAddress());
                                getNavigator().onSuccess(response);
                            } else {
                                if (!check) {
                                    getNavigator().noData();
                                } else {
                                    getNavigator().noload();
                                }                            }

                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response", ": " + throwable.getMessage());
                            getNavigator().onError(throwable);
                        }
                    });
            getCompositeDisposable().add(disposable);
        }
    }

    void setEventDetail(String userId, int page, int limit, final boolean check) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("check_uid", ": " + userId);
            Log.d("check_limit", ": " + limit);

            Disposable disposable = ApplicationClass.getRetrofitService()
                    .fevouriteEventDetail(userId, page, limit)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<FevouriteEventData>() {
                        @Override
                        public void accept(FevouriteEventData response) throws Exception {

                            if (response != null && response.getStatus().equals("success")) {
                                Log.d("check_response", "--" + new Gson().toJson(response));
                                Log.d("check", "---" + response.getDetails().get(0).getAddress());
                                getNavigator().onEventSuccess(response);
                            } else {
                                if (!check) {
                                    getNavigator().noData();
                                } else {
                                    getNavigator().noload();
                                }

                            }

                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response", ": " + throwable.getMessage());
                            getNavigator().onError(throwable);
                        }
                    });
            getCompositeDisposable().add(disposable);

        }
    }





    void apiHitEventLikeUnlike(final String event_id, final int position) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("check_adapter_uid", ": " + responseUser.getId());
            Log.d("check_adapter_event_id", ": " + event_id);

            Disposable disposable = ApplicationClass.getRetrofitService()
                    .eventLikeUnlike(responseUser.getId(), event_id)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<CommonSimpleResponse>() {
                        @Override
                        public void accept(CommonSimpleResponse response) throws Exception {
                            if (response != null) {
                                getNavigator().onSuccessEventDelete(response, position);

                            } else {
                                Log.d("check_response", ": null response");
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response", ": " + throwable.getMessage());
                            getNavigator().onError(throwable);
                        }
                    });

            getCompositeDisposable().add(disposable);

        }
    }

    void apiHitSpecialLikeUnlike(String restaurant_id, final int position) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {


            Disposable disposable = ApplicationClass.getRetrofitService()
                    .speciallikeAndUnlike(responseUser.getId(), restaurant_id)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<CommonSimpleResponse>() {
                        @Override
                        public void accept(CommonSimpleResponse response) throws Exception {
                            if (response != null) {

                                    getNavigator().onSuccessSpecialDelete(response, position);

                            }
                            Log.d("account", "--" + new Gson().toJson(response) + response.getMessage());

                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("check_response", ": " + throwable.getMessage());
                            getNavigator().onError(throwable);
                        }
                    });

//
            getCompositeDisposable().add(disposable);

        }
    }

}
