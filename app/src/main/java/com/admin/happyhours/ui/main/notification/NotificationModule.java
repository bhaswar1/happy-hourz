package com.admin.happyhours.ui.main.notification;

import androidx.lifecycle.ViewModelProvider;

import com.admin.happyhours.ViewModelProviderFactory;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class NotificationModule {


    @Provides
    NotificationViewModel notificationViewModel(DataManager dataManager,
                                                SchedulerProvider schedulerProvider) {
        return new NotificationViewModel(dataManager, schedulerProvider);
    }


    @Provides
    ViewModelProvider.Factory provideNotificationViewModel(NotificationViewModel notificationViewModel) {
        return new ViewModelProviderFactory<>(notificationViewModel);
    }

}
