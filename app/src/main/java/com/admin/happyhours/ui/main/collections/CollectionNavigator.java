package com.admin.happyhours.ui.main.collections;

import com.admin.happyhours.data.model.api.response.CollectionListingResponse;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;

public interface CollectionNavigator {

    void onSuccess(CollectionListingResponse collectionListingResponse);

    void onSuccessAfterAdditonRestaurantToCollection(CommonSimpleResponse commonSimpleResponse);

    void onSuccessAfterCollectionDelete(CommonSimpleResponse commonSimpleResponse, int position);

    void onError(Throwable throwable);

    void onAddSuccess();

    void onFailed(String message);
}
