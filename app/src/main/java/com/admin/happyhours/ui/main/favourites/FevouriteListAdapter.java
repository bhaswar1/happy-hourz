package com.admin.happyhours.ui.main.favourites;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.Detail;
import com.admin.happyhours.datatypecommon.EventBusEventSpecialDeleteInFavouritePage;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.admin.happyhours.ui.resturantdeatails.ResturantDetailsActivity;
import com.admin.happyhours.utils.swipereveallayout.SwipeRevealLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.LinkedList;

public class FevouriteListAdapter extends RecyclerView.Adapter<FevouriteListAdapter.ViewHolder> {

    private final int UNSELECTED = -1;
    private int selectedItem = UNSELECTED;

    public boolean isLoading;
    private final int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private Context context;
    private OnLoadMoreListener mOnLoadMoreListener;
    private OnItemClickListener callback = null;
    private LinkedList<Detail> details;
    public Activity activity;
    FavouritesFragment favouritesFragment;

    public FevouriteListAdapter(FragmentActivity activity, LinkedList<Detail> details,
                                RecyclerView recyclerView, final LinearLayoutManager linearLayoutManager) {
        this.context = activity;
        this.details = details;
        this.activity=activity;
        favouritesFragment=new FavouritesFragment();
//        this.details.add(new CollectionListingResponse.Details());
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_fevourite_special, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {


        return details.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void clearData() {
        details.clear();
        notifyDataSetChanged();
    }

    public void changeValueOfParticularPosition(int position) {
        details.remove(position);
        notifyDataSetChanged();
    }


//    public void changeRowDataAfteLikeOrLike(String check_for_button, int position) {
//        if (check_for_button.equals(AppConstants.adapter_img_like_in_event_listing)) {
//            if (details.get(position).getFavourite_status() == 0) {
//                details.get(position).setFavourite_status(1);
//            } else {
//                details.get(position).setFavourite_status(0);
//            }
//        }
//        notifyItemChanged(position);
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView resturant_title, resturant_address;
        SwipeRevealLayout swipe_layout;
        CardView card_view_delete;
        private int position;
        ConstraintLayout constraintLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            resturant_title = itemView.findViewById(R.id.resturantName);
            imageView = itemView.findViewById(R.id.resturantimage);
            resturant_address = itemView.findViewById(R.id.address);
            constraintLayout= itemView.findViewById(R.id.special_constraint);

            /* Sutanu -- Swipe (start)*/
            card_view_delete = itemView.findViewById(R.id.card_view_delete);
            swipe_layout = itemView.findViewById(R.id.swipe_layout);
            swipe_layout.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
                @Override
                public void onClosed(SwipeRevealLayout view) {
                    Log.d("check_closed", ": " + true);
                }

                @Override
                public void onOpened(SwipeRevealLayout view) {
                    Log.d("check_opened", ": " + true);
                    if (selectedItem >= 0) {
                        int prev = selectedItem;
                        notifyItemChanged(prev);
                    }

                    if (position == selectedItem) {
                        /*selectedItem = -1;
                        notifyItemChanged(selectedItem);*/
                    } else {
                        selectedItem = position;
                        notifyItemChanged(selectedItem);

                    }
                }

                @Override
                public void onSlide(SwipeRevealLayout view, float slideOffset) {
                    Log.d("check_slide", ": " + slideOffset);

                }
            });
            /* Sutanu -- Swipe (end)*/
        }

        public void bind(final int position) {
            /* Sutanu -- Swipe (start)*/
            this.position = position;

            if (position == selectedItem) {
                swipe_layout.open(false);
            } else {
                swipe_layout.close(false);
            }
            /* Sutanu -- Swipe (end)*/

            String s = details.get(position).getRestaurentName().trim();
            if(s.length()!=0 && s!=null) {
                Character ch = details.get(position).getRestaurentName().charAt(0);
                String title = ch.toString().toUpperCase() + s.substring(1);

                resturant_title.setText(title);
            }
            resturant_address.setText(details.get(position).getAddress());

            Glide.with(context).load(details.get(position).getCoverimage()).apply(RequestOptions.centerInsideTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(imageView);

//            Glide.with(context)
//                    .load(Uri.parse(details.get(position).getCoverimage()))
//                    .into(imageView);

            card_view_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new EventBusEventSpecialDeleteInFavouritePage(true, position));
                }
            });
            constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  Toast.makeText(context,"clicked"+details.get(position).getId(),Toast.LENGTH_LONG).show();
                    String shareBody = new Gson().toJson(details.get(position));


                    Log.d("resturantid", "--" + details.get(position).getId());
                    activity.startActivityForResult(new Intent(context, ResturantDetailsActivity.class).
//                           putExtra("lattitude", details.get(position).getLatitude()).
//                            putExtra("longitude", details.get(position).getLongitude()).
                                  putExtra("lattitude",details.get(position).getLatitude()).
                                    putExtra("longitude", details.get(position).getLongitude()
                                    ).
                                 putExtra("checklike","ok").

//
                            putExtra("item_position", position).
                            putExtra("resturantid", details.get(position).getId()).
                            putExtra("details", shareBody),9);
                }
            });

        }
    }

    public LinkedList<Detail> getData() {
        return details;
    }

    public void setAllData(LinkedList<Detail> data) {
        details.addAll(data);
        notifyDataSetChanged();
    }

    public void setData(Detail detail) {
        details.add(detail);
        notifyDataSetChanged();
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void clearParticularData(int position) {
        selectedItem = UNSELECTED;
        details.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, details.size());
    }
}
