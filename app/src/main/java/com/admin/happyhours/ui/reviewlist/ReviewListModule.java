package com.admin.happyhours.ui.reviewlist;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class ReviewListModule {


    @Provides
    ReviewListViewModel provideSettingsViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new ReviewListViewModel(dataManager, schedulerProvider);
    }


}
