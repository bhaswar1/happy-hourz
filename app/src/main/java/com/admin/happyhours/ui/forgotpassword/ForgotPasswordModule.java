package com.admin.happyhours.ui.forgotpassword;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class ForgotPasswordModule {


    @Provides
    ForgotPasswordViewModel provideSignupViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new ForgotPasswordViewModel(dataManager, schedulerProvider);
    }

}
