package com.admin.happyhours.ui.createcollection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CollectionCreateResponse;
import com.admin.happyhours.data.model.api.response.CollectionListingResponse;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.admin.happyhours.databinding.ActivityCreateCollectionBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.utils.AppConstants;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.inject.Inject;

public class CreateCollectionActivity extends BaseActivity<ActivityCreateCollectionBinding, CreateCollectionViewModel> implements CreateCollectionNavigator {

    private static final int RESULT_CODE = 007;
    private SpecialPageRestuarantListingResponse.Detail passed_detail_data_to_add = null ;
    private CollectionListingResponse.Details details_update = null ;
    private File file_cache_dir;
    ActivityCreateCollectionBinding activityCreateCollectionBinding;
    @Inject
    CreateCollectionViewModel createCollectionViewModel;
    private int check_for_api_hitting;
    public FirebaseAnalytics mFirebaseAnalytics;
    public static Intent newIntent(Context context, int check_for_api_hitting) {
        return new Intent(context, CreateCollectionActivity.class).
                putExtra("check_for_api_hitting", check_for_api_hitting);
    }

    public static Intent newIntent(Context context, String deatils_of_listing_to_update, int check_for_api_hitting) {
        return new Intent(context, CreateCollectionActivity.class).
                putExtra("deatils_of_listing_to_update", deatils_of_listing_to_update).
                putExtra("check_for_api_hitting", check_for_api_hitting);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_create_collection;
    }

    @Override
    public CreateCollectionViewModel getViewModel() {
        return createCollectionViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStatusBarHidden(CreateCollectionActivity.this);
        mFirebaseAnalytics=FirebaseAnalytics.getInstance(this);
        activityCreateCollectionBinding = getViewDataBinding();
        createCollectionViewModel.setNavigator(this);

        hideSoftKeyboardWithTouchOutsideEdittext(activityCreateCollectionBinding.parentLayout);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                check_for_api_hitting = getIntent().getExtras().getInt("check_for_api_hitting");

                if (check_for_api_hitting == AppConstants.create_collection_page_for_create) {
//                    Toast.makeText(this, "New Collection will be created", Toast.LENGTH_LONG).show();
                } else if (check_for_api_hitting == AppConstants.create_collection_page_for_update) {
                    details_update = new Gson().fromJson(getIntent().getExtras().getString("deatils_of_listing_to_update"),
                            CollectionListingResponse.Details.class);
                    activityCreateCollectionBinding.etTitle.setText(details_update.getCollection_name());
                    if (details_update.getCollection_image()!=null && !TextUtils.isEmpty(details_update.getCollection_image())){
                        Glide.with(CreateCollectionActivity.this).load(details_update.getCollection_image()).
                                into(activityCreateCollectionBinding.imgImageUpload);
                    }
//                    Toast.makeText(this, "Collection will be update", Toast.LENGTH_LONG).show();
                } else if (check_for_api_hitting == AppConstants.create_collection_page_for_create_and_add) {
                    passed_detail_data_to_add = new Gson().fromJson(getIntent().getExtras().getString("deatils_of_listing_to_update"),
                            SpecialPageRestuarantListingResponse.Detail.class);
//                    Toast.makeText(this, "Add will be done for:- "+passed_detail_data_to_add.getRestaurentId(), Toast.LENGTH_SHORT).show();
//                    Toast.makeText(this, "New Collection will be created and restaurant will be added into it", Toast.LENGTH_LONG).show();
                }
            }
        }
        activityCreateCollectionBinding.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_arrow_black));
        activityCreateCollectionBinding.toolbar.setTitle("");
        setSupportActionBar(activityCreateCollectionBinding.toolbar);
        if (activityCreateCollectionBinding.toolbar != null) {
            activityCreateCollectionBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    finish();
                }
            });
        }

        activityCreateCollectionBinding.imgImageUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });

        activityCreateCollectionBinding.tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkForEmptyFieldAndShowError()){
                    NormalLoginResponse.Details details_current_user = new Gson().fromJson(createCollectionViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

                    Bundle bundle = new Bundle();
                    bundle.putString("userName",details_current_user.getUserName());
                    bundle.putString("userid",details_current_user.getId());
                    bundle.putString("collectionName",activityCreateCollectionBinding.etTitle.getText().toString().trim());
//        bundle.putString("collectionName",collectionName);
//        bundle.putString("collectionId",collectionid);
//        bundle.putString("resturantrating",passed_detail_data.getRating());
//                    bundle.putString("resturantName",passed_detail_data.getRestaurentName());

                    mFirebaseAnalytics.logEvent("new_collection_created_android", bundle);
                     if (check_for_api_hitting == AppConstants.create_collection_page_for_create) {
                         showLoading();
                        createCollectionViewModel.createCollection(true, null,
                                details_current_user.getId(), activityCreateCollectionBinding.etTitle.getText().toString().trim(),
                                file_cache_dir, "1");
                    } else if (check_for_api_hitting == AppConstants.create_collection_page_for_update) {
                         if (file_cache_dir!=null) {
                             showLoading();
                             createCollectionViewModel.updateCollection(details_current_user.getId(),
                                     activityCreateCollectionBinding.etTitle.getText().toString().trim(),
                                     file_cache_dir, details_update.getCollection_id(), "2");

                         //    NormalLoginResponse.Details details_current_user = new Gson().fromJson(createCollectionViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);

                             Bundle bundle1 = new Bundle();
                             bundle1.putString("userName",details_current_user.getUserName());
                             bundle1.putString("userid",details_current_user.getId());
                             bundle1.putString("collectionName",activityCreateCollectionBinding.etTitle.getText().toString().trim());
//        bundle.putString("collectionName",collectionName);
//        bundle.putString("collectionId",collectionid);
//        bundle.putString("resturantrating",passed_detail_data.getRating());
//                    bundle.putString("resturantName",passed_detail_data.getRestaurentName());

                             mFirebaseAnalytics.logEvent("collection_edited", bundle1);
                         }else {
                             showLoading();
                             createCollectionViewModel.updateCollection(details_current_user.getId(),
                                     activityCreateCollectionBinding.etTitle.getText().toString().trim(),
                                     null, details_update.getCollection_id(),  "2");
                         }
                    }else if (check_for_api_hitting == AppConstants.create_collection_page_for_create_and_add){
                         showLoading();
                         createCollectionViewModel.createCollection(false, passed_detail_data_to_add.getRestaurentId(),
                                 details_current_user.getId(), activityCreateCollectionBinding.etTitle.getText().toString().trim(),
                                 file_cache_dir, "1");
                     }
                }
            }
        });

    }

    private boolean checkForEmptyFieldAndShowError(){
        if (TextUtils.isEmpty(activityCreateCollectionBinding.etTitle.getText().toString().trim())){
            activityCreateCollectionBinding.etTitle.requestFocus();
            activityCreateCollectionBinding.etTitle.setError(getResources().getString(R.string.can_not_be_blank));
            return false;
        }
        if (details_update==null && file_cache_dir==null){
            Toast.makeText(this, "Please Select an image", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }



    private void chooseImage() {
        /*CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle("Crop")
                .start(CreateCollectionActivity.this);*/

        ImagePicker.with(this)
                .crop()	    			//Crop image(Optional), Check Customization for more option
                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                .start();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            Uri uri= data.getData();

            saveCollectionImage(uri);

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }

        /*if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                if (resultUri != null) {
                    // Get file from cache directory
                    file_cache_dir = new File(getCacheDir(), resultUri.getLastPathSegment());
                    if (file_cache_dir.exists()) {
                        Glide.with(CreateCollectionActivity.this).load(resultUri.toString()).into(activityCreateCollectionBinding.imgImageUpload);
                        Log.d("check_path", ": " + resultUri.toString());

                        Log.d("check_file_get", ": " + file_cache_dir.toString());

                    } else {
                        Log.d("file_does_not_exists", ": " + true);
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.d("check_error", ": " + error.getMessage());
            }
        }*/
    }

    private void saveCollectionImage(Uri resultUri){
        if (resultUri != null) {
            // Get file from cache directory
            file_cache_dir = new File(getCacheDir(), resultUri.getLastPathSegment());
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(resultUri));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            //   Bitmap.createScaledBitmap(bitmap, 200, 200, true);
            OutputStream os;
            try {
                os = new FileOutputStream(file_cache_dir);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 18, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            }
            if (file_cache_dir.exists()) {
                Glide.with(CreateCollectionActivity.this).load(resultUri.toString()).into(activityCreateCollectionBinding.imgImageUpload);
                Log.d("check_path", ": " + resultUri.toString());

                Log.d("check_file_get", ": " + file_cache_dir.toString());

            } else {
                Log.d("file_does_not_exists", ": " + true);
            }
        }
    }


    @Override
    public void onSuccessAfterCreation(CollectionCreateResponse collectionCreateResponse, String type) {

        hideLoading();
        if (collectionCreateResponse!=null){
            Toast.makeText(this,"Collection Successfully Added", Toast.LENGTH_SHORT).show();
            if (collectionCreateResponse.getStatus().trim().toLowerCase().equals("success")){
                setResult(RESULT_CODE);
                CreateCollectionActivity.this.finish();
            }
        }
    }

    @Override
    public void onSuccessAfterUpdation(CommonSimpleResponse commonSimpleResponse, String type) {
        hideLoading();
        if (commonSimpleResponse!=null){
            Toast.makeText(this, commonSimpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
            if (commonSimpleResponse.getStatus().trim().toLowerCase().equals("success")){
                setResult(RESULT_CODE);
                CreateCollectionActivity.this.finish();
            }
        }
    }

    @Override
    public void onSuccessAfterCreationAndAdditonRestaurantToCollection(CommonSimpleResponse commonSimpleResponse, String type) {
        hideLoading();
        if (commonSimpleResponse!=null){
            Toast.makeText(this, commonSimpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
            if (commonSimpleResponse.getStatus().trim().toLowerCase().equals("success")){
                setResult(RESULT_CODE);
                CreateCollectionActivity.this.finish();
            }
        }
    }

    @Override
    public void onError(Throwable throwable) {
        hideLoading();
    }
}
