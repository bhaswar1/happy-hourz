package com.admin.happyhours.ui.resturantdeatails;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.admin.happyhours.databinding.ItemResturantDetailBinding;
import com.admin.happyhours.ui.base.BaseViewHolder;
import com.admin.happyhours.ui.main.special.recycleradapter.TagListAdapter;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class ResturantDetailsdapter extends RecyclerView.Adapter<BaseViewHolder>{
    @NonNull
private LinkedList<String> resturantdetail;

//private ProductLikeClickListner productLikeClickListner;
//Extra extra;
Context ctx;

public  CartItemClickListener cartItemClickListener;
ItemResturantDetailBinding itemCartViewBinding;
    private int fposition;
    RecyclerView offerrecyclerview;


    public ResturantDetailsdapter( LinkedList<String> carts,Context context,RecyclerView recyclerView) {
    this.resturantdetail=carts;
    this.ctx=context;
    this.offerrecyclerview=recyclerView;
//    this.itemCartViewBinding=itemCartViewBinding;
//        this.products = products;
        // this.context = context;
        }

      // CartActivity cartActivity =new CartActivity();
@NotNull
@Override
public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemResturantDetailBinding itemResturantDetailBinding = ItemResturantDetailBinding
        .inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        return new CustomCartHolder(itemResturantDetailBinding);
        }

@Override
public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        baseViewHolder.onBind(i);
        //bitemCartViewBinding.productTv.setText("hi");;


}

    @Override
    public int getItemCount() {


   Log.d("resturantdetailsize","--"+resturantdetail.size());
    return resturantdetail.size();
        }

    public void setContext(Context context) {
    this.ctx=context;
    }

    public class CustomCartHolder extends BaseViewHolder {


    private final ItemResturantDetailBinding mBinding;

    public CustomCartHolder(ItemResturantDetailBinding binding) {
        super(binding.getRoot());
        this.mBinding = binding;
//        if(ch==1) {
//            binding.group.setVisibility(View.GONE);
//
//        }else{
//            binding.group.setVisibility(View.VISIBLE);
//        }

    }

    @Override
    public void onBind(int position) {

    //    final CartProductDtl carts1 =carts.get(position);
        Log.d("foodposition",""+fposition+ new Gson().toJson(resturantdetail));

        if(position==0 && fposition!=0){
            mBinding.food.setVisibility(View.VISIBLE);

            mBinding.food.setText("Food");
        }else if(position==fposition){
            mBinding.food.setVisibility(View.VISIBLE);

            mBinding.food.setText("Drinks");
        }else{
            mBinding.food.setVisibility(View.GONE);
        }

        int p=resturantdetail.get(position).length();
        String temp=resturantdetail.get(position).trim().substring(1,p-1);
        Character ch=temp.charAt(0);

        mBinding.item.setText(ch.toString().toUpperCase()+temp.substring(1));

        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(ctx);
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);

        mBinding.rvItem.setLayoutManager(flexboxLayoutManager);

        /*ArrayList<String> list = new ArrayList<>();
        for (Object i : resturantdetail) {
            list.add(i.toString());
        }*/

        ArrayList<String> list = new ArrayList<String>(Arrays.asList(resturantdetail.get(position).split(",")));

        TagListAdapter tagListAdapter = new TagListAdapter(ctx,list);
        mBinding.rvItem.setAdapter(tagListAdapter);

        ResturantDetailItemViewModel resturantDetailItemViewModel = new ResturantDetailItemViewModel(resturantdetail.get(position),position);
        mBinding.setViewModel(resturantDetailItemViewModel);
      //  ViewCompat.setNestedScrollingEnabled(offerrecyclerview, true);
    }

}
    public void addItems(LinkedList<String> repoList,int position) {
        Log.d("repoList",""+repoList.size());

      //  resturantdetail.addAll(repoList);
        Log.d("repoList",""+resturantdetail.size());

        this.fposition=position;
        notifyDataSetChanged();
    }
    public void clearItems() {
        resturantdetail.clear();
        notifyDataSetChanged();

    }

public interface CartItemClickListener{
//
//    void onRemove(CartProductDtl cartProductDtl, int position);
//    void onHeartClick(CartProductDtl cartProductDtl, int Position);
//    void onCarttemClick(CartProductDtl cartProductDtl, int position);
//    void onProductClick(CartProductDtl cartProductDtl, int position);
 }
    public void setListener() {
       // this.cartItemClickListener = listener;

    }


}
