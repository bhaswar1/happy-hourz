package com.admin.happyhours.ui.main.favourites;

import androidx.lifecycle.ViewModelProvider;

import com.admin.happyhours.ViewModelProviderFactory;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class FavouritesModule {


    @Provides
    FavouritesViewModel favouritesViewModel(DataManager dataManager,
                                            SchedulerProvider schedulerProvider) {
        return new FavouritesViewModel(dataManager, schedulerProvider);
    }


    @Provides
    ViewModelProvider.Factory provideFavouritesViewModel(FavouritesViewModel profilePageViewModel) {
        return new ViewModelProviderFactory<>(profilePageViewModel);
    }

}
