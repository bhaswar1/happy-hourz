package com.admin.happyhours.ui.main.special;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.R;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.CusineDetail;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.ResturantsAllData;
import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.admin.happyhours.data.model.api.response.TagDetail;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.AppConstants;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class SpecialViewModel extends BaseViewModel<SpecialNavigator> {


    public SpecialViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }


    void fetchRestuarantListing(String user_id, String latitude, String longitude, String deals, String distance, ArrayList<String> tag_id, ArrayList<String> cusinarray,ArrayList<String> tagDrinksId,ArrayList<String> serViceId) {
          //  user_id=getDataManager().getCurrentUserId().toString();
        Log.d("user_id", ": " + user_id);
        Log.d("user_latitude", ": " + latitude);
        Log.d("user_longitude", ": " + longitude);
        Log.d("food", ": " + new Gson().toJson(tag_id)+"========"+new Gson().toJson(tagDrinksId));

     //   ArrayList<String> tagsDrinks=new ArrayList();

        Disposable disposable = ApplicationClass.getRetrofitService()
                .specialPageRestuarantListing(user_id, latitude, longitude,tag_id,distance,deals,cusinarray,tagDrinksId,serViceId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<JsonObject>() {
                    @Override
                    public void accept(JsonObject response) throws Exception {
                        if (response != null  ) {
                            SpecialPageRestuarantListingResponse specialPageRestuarantListingResponse = new Gson().fromJson(response.toString(),
                                    SpecialPageRestuarantListingResponse.class);

                            if(specialPageRestuarantListingResponse.getStatus().equals("success")) {
                                Log.d("responserun", "--t" + specialPageRestuarantListingResponse.getStatus());

                                JsonArray jsonDetailsarray = response.getAsJsonArray("Details");


                                // Log.d("array","--t"+new Gson().toJson(details.get(0)));


                                Log.d("responserun", "--t" + new Gson().toJson(specialPageRestuarantListingResponse));

//                            SpecialPageRestuarantListingResponse.Detail detail = new Gson().fromJson(response.toString(),
//                                    SpecialPageRestuarantListingResponse.Detail.class);
                                JsonArray jsonObject = response.get("Details").getAsJsonArray();
                                Log.d("responserun", "----t" + new Gson().toJson(jsonObject));


                                //              Log.d("responserun","----t"+new Gson().toJson(jsonObject.get(0).get()));


                                Type type = new TypeToken<List<SpecialPageRestuarantListingResponse.Detail>>() {
                                }.getType();
                                List<SpecialPageRestuarantListingResponse.Detail> detail = new Gson().fromJson(response.get("Details"), type);


                                Log.d("responserun", "--t" + new Gson().toJson(detail.get(0).getExtra()));

                                getNavigator().onSuccess(specialPageRestuarantListingResponse, detail, jsonDetailsarray);
                            }else{
                                getNavigator().failed();
                            }

                        } else {

                            Log.d("check_response", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response", ": " + throwable.getMessage());
                        getNavigator().onError(throwable);
                    }
                });

        getCompositeDisposable().add(disposable);
    }

    void bookmarkAndLikeApiHit(final String check_for_button, final int position, SpecialPageRestuarantListingResponse.Detail detail) {

        NormalLoginResponse.Details responseUser = new Gson().fromJson(getDataManager().getCurrentUserInfo(),
                NormalLoginResponse.Details.class);
        if (responseUser.getId() != null) {

            Log.d("check_button", ": " + check_for_button);
            Log.d("check_adapter_uid", ": " + responseUser.getId());
            Log.d("check_adapter_restid", ": " + detail.getRestaurentId());
            if (check_for_button.equals(AppConstants.adapter_img_bookmark)) {

                Disposable disposable = ApplicationClass.getRetrofitService()
                        .bookmarkAndUnbookmark(responseUser.getId(), detail.getRestaurentId())
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(new Consumer<CommonSimpleResponse>() {
                            @Override
                            public void accept(CommonSimpleResponse response) throws Exception {
                                if (response != null) {

                                    if (response.getStatus() != null && response.getStatus().trim().toLowerCase().equals("success")) {
                                        getNavigator().responseAfterBookmarkOrLike(check_for_button, position);
                                    }

                                } else {
                                    Log.d("check_response", ": null response");
                                }
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Log.d("check_response", ": " + throwable.getMessage());
                                getNavigator().onError(throwable);
                            }
                        });

                getCompositeDisposable().add(disposable);
                    } else {
                Disposable disposable = ApplicationClass.getRetrofitService()
                        .speciallikeAndUnlike(responseUser.getId(), detail.getRestaurentId())
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(new Consumer<CommonSimpleResponse>() {
                            @Override
                            public void accept(CommonSimpleResponse response) throws Exception {
                                if (response != null) {

                                    if (response.getStatus() != null && response.getStatus().trim().toLowerCase().equals("success")) {
                                        getNavigator().responseAfterBookmarkOrLike(check_for_button, position);
                                    }

                                } else {
                                    Log.d("check_response", ": null response");
                                }
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Log.d("check_response", ": " + throwable.getMessage());
                                getNavigator().onError(throwable);
                            }
                        });

                getCompositeDisposable().add(disposable);
            }

        }
    }




}
