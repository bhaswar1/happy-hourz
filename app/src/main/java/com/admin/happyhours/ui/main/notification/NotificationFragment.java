package com.admin.happyhours.ui.main.notification;


import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.NotificationListingResponse;
import com.admin.happyhours.databinding.FragmentNotificationBinding;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.admin.happyhours.ui.base.BaseFragment;
import com.admin.happyhours.ui.main.HomeActivity;
import com.admin.happyhours.ui.main.notification.recycleradapter.RecyclerViewAdapter;
import com.admin.happyhours.utils.AppConstants;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.LinkedList;

import javax.inject.Inject;


public class NotificationFragment extends BaseFragment<FragmentNotificationBinding, NotificationViewModel> implements NotificationNavigator {

    private final int DATA_LIMIT_PER_PAGE = AppConstants.LISTING_FETCH_LIMIT;
    private int PAGE_NO = 1;
    RecyclerViewAdapter recyclerViewAdapter;
    FragmentNotificationBinding fragmentNotificationBinding;
    NotificationViewModel notificationViewModel;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private boolean hasLoadMore = false;
    public  NotificaionBadge notificaionBadge;
    public FirebaseAnalytics mFirebaseAnalytics;
    public static NotificationFragment newInstance() {
        Bundle args = new Bundle();
        NotificationFragment fragment = new NotificationFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_notification;
    }

    @Override
    public NotificationViewModel getViewModel() {
        notificationViewModel = ViewModelProviders.of(this, mViewModelFactory).get(NotificationViewModel.class);
        return notificationViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notificationViewModel.setNavigator(this);
      //  HomeActivity homeActivity=new HomeActivity();
    //    homeActivity.showHideFilterOptionMenu(false);
    }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentNotificationBinding = getViewDataBinding();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());

        setUpRecyclerView();
        PAGE_NO = 1;
        getBaseActivity().showLoading();

        notificationViewModel.getNotificationListing(DATA_LIMIT_PER_PAGE, PAGE_NO,false);
    }

    private void setUpRecyclerView() {
        fragmentNotificationBinding.recyclerview.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        fragmentNotificationBinding.recyclerview.setLayoutManager(linearLayoutManager);
        LinkedList<NotificationListingResponse.Details> detail = new LinkedList<>();
        recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), detail, fragmentNotificationBinding.recyclerview,
                linearLayoutManager,getActivity());
        fragmentNotificationBinding.recyclerview.setAdapter(recyclerViewAdapter);

        recyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.d("check_bools", ": " + hasLoadMore);
                if (hasLoadMore) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadData();
                        }
                    }, 1000);
                }
            }
        });

        /*fragmentNotificationBinding.recyclerview.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                Log.d("check_bools", ": " + hasLoadMore);
                if (hasLoadMore) {
//                    getBaseActivity().showLoading();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadData();
                        }
                    }, 1000);
                }
            }
        });*/
    }

    private void loadData() {
        getBaseActivity().showLoading();
//        recyclerViewAdapter.showLoading(true);
        notificationViewModel.getNotificationListing(DATA_LIMIT_PER_PAGE, PAGE_NO,true);
    }

    @Override
    public void onSuccessAfterGettingListing(NotificationListingResponse notificationListingResponse) {
        getBaseActivity().hideLoading();
        if (notificationListingResponse != null) {
            if (notificationListingResponse.getDetails() != null && notificationListingResponse.getDetails().size() > 0) {
                NormalLoginResponse.Details responseUser = new Gson().fromJson(notificationViewModel.getDataManager().getCurrentUserInfo(),
                        NormalLoginResponse.Details.class);

                Bundle bundle = new Bundle();
                bundle.putString("userName",responseUser.getUserName());
                bundle.putString("userid",responseUser.getId());

//                bundle.putString("resturantName",passed_detail_data.getRestaurentName());


                mFirebaseAnalytics.logEvent("notification_clicked_android", bundle);


                if (notificationListingResponse.getDetails().size() == AppConstants.LISTING_FETCH_LIMIT) {
                    PAGE_NO = PAGE_NO + 1;
                    hasLoadMore = true;
                } else {
                    hasLoadMore = false;
                }

                recyclerViewAdapter.setAllData(notificationListingResponse.getDetails());
                recyclerViewAdapter.setLoaded();
            } else {
                hasLoadMore = false;
            }
        }
        Log.d("check_response_frag", ": " + hasLoadMore);
    }

    @Override
    public void onError(Throwable throwable) {
        getBaseActivity().hideLoading();
        Log.d("check_error", ": " + throwable.getMessage());
    }

    public class NotificaionBadge extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

        }
    }


    @Override
    public void onSuccessAfterClearBadgeCount(CommonSimpleResponse commonSimpleResponse) {
        if (commonSimpleResponse!=null && commonSimpleResponse.getStatus().trim().toLowerCase().equals("success")){
            ((HomeActivity)getActivity()).clearBadgeCountInAdapter();
        }
    }

    @Override
    public void noData() {
            fragmentNotificationBinding.recyclerview.setVisibility(View.GONE);
            fragmentNotificationBinding.tvNoData.setVisibility(View.VISIBLE);

    }

    @Override
    public void noload() {
            getBaseActivity().hideLoading();
    }
}
