package com.admin.happyhours.ui.reviewphoto;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class ReviewPhotoModule {


    @Provides
    ReviewPhotoViewModel provideSignupViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new ReviewPhotoViewModel(dataManager, schedulerProvider);
    }

}
