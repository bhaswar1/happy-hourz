package com.admin.happyhours.ui.createcollection;

import android.util.Log;

import com.admin.happyhours.ApplicationClass;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.data.model.api.response.CollectionCreateResponse;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.ui.base.BaseViewModel;
import com.admin.happyhours.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import java.io.File;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CreateCollectionViewModel extends BaseViewModel<CreateCollectionNavigator> {


    public CreateCollectionViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public void createCollection(final boolean check_for_only_create_or_create_and_add, final String restaurant_id,
                                 final String userid, String collection_name, File collectionimage, final String type) {

        MultipartBody.Part body = null;
        if (collectionimage != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), collectionimage);
            body = MultipartBody.Part.createFormData("collectionimage", collectionimage.getName(), reqFile);
        }
        final RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), userid);
        RequestBody collec_name = RequestBody.create(MediaType.parse("text/plain"), collection_name);
        RequestBody TYPE = RequestBody.create(MediaType.parse("text/plain"), type);

        final Disposable disposable = ApplicationClass.getRetrofitService()
                .createCollection(user_id, collec_name, TYPE, body)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<CollectionCreateResponse>() {
                    @Override
                    public void accept(CollectionCreateResponse response) throws Exception {
                        if (response != null) {

                            if (check_for_only_create_or_create_and_add) {
                                getNavigator().onSuccessAfterCreation(response, type);

                            }else {
                                createColectionAndAddRestaurantToIt(userid, restaurant_id, response, type);
                            }
                            Log.d("check_response_create", ": " + new Gson().toJson(response));

                        } else {
                            Log.d("check_response_create", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response_create", ": " + throwable.getMessage());

                        getNavigator().onError(throwable);

                    }
                });

        getCompositeDisposable().add(disposable);
    }

    public void updateCollection(String userid, String collection_name, File collectionimage, String collection_id,
                                 final String type) {

        MultipartBody.Part body = null;
        if (collectionimage != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), collectionimage);
            body = MultipartBody.Part.createFormData("collectionimage", collectionimage.getName(), reqFile);
        }
        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), userid);
        RequestBody collec_name = RequestBody.create(MediaType.parse("text/plain"), collection_name);
        RequestBody TYPE = RequestBody.create(MediaType.parse("text/plain"), type);
        RequestBody COLLECTION_ID = RequestBody.create(MediaType.parse("text/plain"), collection_id);

        Disposable disposable = ApplicationClass.getRetrofitService()
                .updateCollection(user_id, collec_name, TYPE, COLLECTION_ID, body)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<CommonSimpleResponse>() {
                    @Override
                    public void accept(CommonSimpleResponse response) throws Exception {
                        if (response != null) {

                            getNavigator().onSuccessAfterUpdation(response, type);

                            Log.d("check_response_update", ": " + new Gson().toJson(response));

                        } else {
                            Log.d("check_response_update", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response_update", ": " + throwable.getMessage());

                        getNavigator().onError(throwable);

                    }
                });

        getCompositeDisposable().add(disposable);
    }


    public void createColectionAndAddRestaurantToIt(String userid, String restaurant_id, CollectionCreateResponse collectionCreateResponse, final String type) {
        Disposable disposable = ApplicationClass.getRetrofitService()
                .addRestaurantToCollection(userid, restaurant_id, collectionCreateResponse.getCollection_id())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<CommonSimpleResponse>() {
                    @Override
                    public void accept(CommonSimpleResponse response) throws Exception {
                        if (response != null) {

                            getNavigator().onSuccessAfterCreationAndAdditonRestaurantToCollection(response, type);

                            Log.d("check_response_create_update", ": " + new Gson().toJson(response));

                        } else {
                            Log.d("check_response_create_update", ": null response");
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("check_response_create_update", ": " + throwable.getMessage());

                        getNavigator().onError(throwable);

                    }
                });

        getCompositeDisposable().add(disposable);
    }
}
