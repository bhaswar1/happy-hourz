package com.admin.happyhours.ui.resturantdeatails;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.admin.happyhours.R;
import com.admin.happyhours.databinding.ItemResturantdeatailImageBinding;
import com.admin.happyhours.ui.base.BaseViewHolder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kodmap.app.library.PopopDialogBuilder;


import java.util.LinkedList;

import static android.content.Context.MODE_PRIVATE;

public class ResturantSliderImageAdapter extends RecyclerView.Adapter<BaseViewHolder> {


    Context context;
   private LinkedList<String> deatilData;
    SharedPreferences shareprefTutorial;
    public ResturantSliderImageAdapter(LinkedList<String> tutorialData, Context context) {
        this.deatilData=tutorialData;
        this.context = context;
        shareprefTutorial =context.getSharedPreferences("tutorial",MODE_PRIVATE);
    }



    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemResturantdeatailImageBinding resturantdeatailImageBinding = ItemResturantdeatailImageBinding
                .inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        return new CustomCategoryHolder(resturantdeatailImageBinding);
    }
    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
        viewHolder.onBind(i);


    }

    @Override
    public int getItemCount() {
        if (!deatilData.isEmpty()) {
            Log.d("size","-"+deatilData.size());
            return deatilData.size();
        } else {
            return 0;
        }
    }

    public class CustomCategoryHolder extends BaseViewHolder{


        private final ItemResturantdeatailImageBinding mBinding;


        public CustomCategoryHolder(ItemResturantdeatailImageBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;

        }

        @Override
        public void onBind(final int position) {
            final String resturantDetailData = deatilData.get(position);
            Log.d("dataimage","--"+deatilData.get(position).toString());
if(deatilData.get(position)!=null && !deatilData.get(position).equals("noimage")) {
   // Log.d("checkkkkkkkk","--");
//    Glide.with(context)
//            .load(Uri.parse(deatilData.get(position)))
//
//            .into(mBinding.imageSlider);
    Glide.with(context).load(deatilData.get(position)).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(mBinding.imageSlider);

    mBinding.imageSlider.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            Dialog dialog = new PopopDialogBuilder(context)
                    // Set list like as option1 or option2 or option3
                    .setList(deatilData,position)
                    // or setList with initial position that like .setList(list,position)
                    // Set dialog header color
                    .setHeaderBackgroundColor(android.R.color.black)
                    // Set dialog background color
                    .setDialogBackgroundColor(R.color.black)
                    // Set close icon drawable

                    .setCloseDrawable(R.drawable.ic_close_white_24dp)
                    // Set loading view for pager image and preview image
                    .setLoadingView(R.layout.loader_item_layout)
                    // Set dialog style
                     .setDialogStyle(R.style.DialogStyle)
                    // Choose selector type, indicator or thumbnail
                    .showThumbSlider(false)
                    // Set image scale type for slider image
                    .setSliderImageScaleType(ImageView.ScaleType.FIT_CENTER)
                    // Set indicator drawable
                  //  .setSelectorIndicator(R.drawable.selected_indicato)
                    // Enable or disable zoomable
                    .setIsZoomable(true)
                    // Build Km Slider Popup Dialog
                    .build();

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));

            dialog.show();


        }
    });

}else{
    Glide.with(context)
            .load(R.drawable.placeholder_image)

            .into(mBinding.imageSlider);
}
            ResturantImageViewModel itemTutorialSliderImageViewModel = new ResturantImageViewModel(resturantDetailData,position);
            mBinding.setViewModel(itemTutorialSliderImageViewModel);


        }



    }
    public  void addItem(LinkedList<String> resturantImageData){
       // deatilData.addAll(resturantImageData);
        notifyDataSetChanged();
    }

    public void clearItems() {
        deatilData.clear();
    }



}
