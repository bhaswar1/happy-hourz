package com.admin.happyhours.ui.review;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import android.os.StrictMode;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.admin.happyhours.databinding.ActivityCreateReviewBinding;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.utils.PermissionUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.inject.Inject;

import static com.facebook.FacebookSdk.getCacheDir;

public class ReviewActivity extends BaseActivity<ActivityCreateReviewBinding, ReviewViewModel>
        implements ReviewNavigator {

    private static final int CHECK_LOCATION_SETTINGS_REQUEST_CODE = 1;
    private FusedLocationProviderClient mFusedLocationClient;
    private PermissionUtils permissionUtils;
    private ArrayList<String> permissions = new ArrayList<>();
    LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private SpecialPageRestuarantListingResponse.Detail restaurantDetails;
    private File file_cache_dir;
    ArrayList<String> fileList;

    public String arr[]={"","",""};
    int pos=0;
    String restaurantId;
    String restName=null;
    String restImage=null;
    String address=null;
    public FirebaseAnalytics mFirebaseAnalytics;

    @Inject
    ReviewViewModel mReviewViewModel;
    private ActivityCreateReviewBinding activityCreateReviewBinding;


//    public static Intent newIntent(Context context) {
//        return new Intent(context, HelpSupportActivity.class);
//    }
//
//    public static Intent newIntent(Context context, String notification) {
//        Log.d("check_noti_1", ": " + notification);
//        return new Intent(context, HelpSupportActivity.class).putExtra("notification", notification);
//    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_create_review;
    }

    @Override
    public ReviewViewModel getViewModel() {
        return mReviewViewModel;
    }

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setStatusBarHidden(ReviewActivity.this);
        mReviewViewModel.setNavigator(this);
        activityCreateReviewBinding = getViewDataBinding();
        Toolbar toolbar = activityCreateReviewBinding.toolbar5;
        setSupportActionBar(toolbar);
        activityCreateReviewBinding.toolbar5.setNavigationIcon(R.drawable.back_arrow_black);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        activityCreateReviewBinding.toolbar5.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
         restName=getIntent().getStringExtra("resturantName");
         restImage=getIntent().getStringExtra("restaurantImage");
         address=getIntent().getStringExtra("restaurantAddres");
        restaurantId=getIntent().getStringExtra("id");

        fileList=new ArrayList<>();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Glide.with(ReviewActivity.this).load(restImage).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(activityCreateReviewBinding.restImage);
        activityCreateReviewBinding.specialTv.setText(restName);
        activityCreateReviewBinding.specialAddresTv.setText(address);
//        activityCreateReviewBinding.reviewEt.setKeyListener(null);

        activityCreateReviewBinding.reviewEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.review_et) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
//        activityCreateReviewBinding.reviewEt.setScroller(new Scroller(HelpSupportActivity.this));
//        activityCreateReviewBinding.reviewEt.setMaxLines(30);
//        activityCreateReviewBinding.reviewEt.setVerticalScrollBarEnabled(true);
//        activityCreateReviewBinding.reviewEt.setMovementMethod(new ScrollingMovementMethod());
        imageSet();
        submit();

    }

    private void submit() {
        activityCreateReviewBinding.submitBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(activityCreateReviewBinding.ratingBar2.getRating()>0) {
                    fileList.clear();
                    for(int i=0;i<arr.length;i++){
                        if(!(arr[i].length()==0)){

                            fileList.add(arr[i]);
                        }
                    }

                    if (mReviewViewModel.getDataManager().getCurrentUserInfo() != null) {
                        String rating = String.valueOf(activityCreateReviewBinding.ratingBar2.getRating());
                        showLoading();
                        NormalLoginResponse.Details details = new Gson().fromJson(mReviewViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
                        Log.d("rating", "---" + rating);
                        Log.e("details", "---" + details);




//
//
                        Bundle bundle = new Bundle();
                        bundle.putString("userName",details.getUserName());
                        bundle.putString("userid",details.getId());
                        bundle.putString("resturantid",restaurantId);
                        bundle.putString("resturantName",restName);
                        bundle.putString("resturantAddress",address);
                        bundle.putString("resturantrating",rating);
                        bundle.putString("resturantratingtext",activityCreateReviewBinding.reviewEt.getText().toString());
//                        bundle.putString("collectionId",collectionid);
//        bundle.putString("resturantrating",passed_detail_data.getRating());
//                        bundle.putString("resturantName",passed_detail_data.getRestaurentName());

                        mFirebaseAnalytics.logEvent("restaurant_rated_and_reviewed_android", bundle);



//            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
                        mReviewViewModel.setReviewData(details.getId(), fileList, rating, restaurantId, activityCreateReviewBinding.reviewEt.getText().toString());
                    }
                }else{
                    Toast.makeText(ReviewActivity.this,getString(R.string.rating_mandatory),Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void imageSet() {
        activityCreateReviewBinding.firstImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos=1;
                chooseImage();

            }
        });
        activityCreateReviewBinding.secondImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos=2;
                chooseImage();
            }
        });
        activityCreateReviewBinding.thirdImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos=3;
                chooseImage();
            }
        });
    }
    private void chooseImage() {
        /*CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setMultiTouchEnabled(true)
              //  .setAspectRatio(1,1)
                .setActivityTitle("Crop")
                .start(ReviewActivity.this);*/

        ImagePicker.with(this)
                .crop()	    			//Crop image(Optional), Check Customization for more option
                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                .start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            Uri uri= data.getData();

            saveReviewImage(uri);

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }

        /*if (resultCode == ReviewActivity.RESULT_CANCELED) {
            return;
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                if (resultUri != null) {
                    // Get file from cache directory
                    file_cache_dir = new File(getCacheDir(), resultUri.getLastPathSegment());
                    if (file_cache_dir.exists()) {
                       // getBaseActivity().showLoading();

//                        if (mReviewViewModel.getDataManager().getCurrentUserInfo() != null) {
//                            showLoading();
//                            NormalLoginResponse.Details details = new Gson().fromJson(mReviewViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
//
////            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
//                            mReviewViewModel.setReviewData(details.getId(),file_cache_dir,"","","");
//                        }

                        Log.d("check_path", ": " + resultUri.toString());

                        Log.d("check_file_get", ": " + file_cache_dir.toString());
                        if(pos==1) {

                            arr[0]=file_cache_dir.toString();
                          //  fileList.add(0,file_cache_dir.toString());
                            Glide.with(ReviewActivity.this).load(resultUri.toString()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(activityCreateReviewBinding.firstImage);
                        }
                        if(pos==2) {
                            arr[1]=file_cache_dir.toString();
//                            if(fileList.size()>0)
//                            fileList.add(1,file_cache_dir.toString());
//                            else
//                                fileList.add(0,file_cache_dir.toString());
                            Glide.with(ReviewActivity.this).load(resultUri.toString()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(activityCreateReviewBinding.secondImage);
                        }
                        if(pos==3) {
                            arr[2]=file_cache_dir.toString();
//                            if(fileList.size()>0 && fileList.size()<=1 )
//                            fileList.add(1,file_cache_dir.toString());
//                            else if ( fileList.size()>1)
//                                fileList.add(0,file_cache_dir.toString());
//                            else

                           // fileList.add(0,file_cache_dir.toString());
                            Glide.with(ReviewActivity.this).load(resultUri.toString()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(activityCreateReviewBinding.thirdImage);
                        }


                    } else {
                        Log.d("file_does_not_exists", ": " + true);
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.d("check_error", ": " + error.getMessage());
            }
        }*/
    }

    private void saveReviewImage(Uri resultUri){
        if (resultUri != null) {
            // Get file from cache directory
            file_cache_dir = new File(getCacheDir(), resultUri.getLastPathSegment());
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(resultUri));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            //   Bitmap.createScaledBitmap(bitmap, 200, 200, true);
            OutputStream os;
            try {
                os = new FileOutputStream(file_cache_dir);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 18, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            }
            if (file_cache_dir.exists()) {
                // getBaseActivity().showLoading();

//                        if (mReviewViewModel.getDataManager().getCurrentUserInfo() != null) {
//                            showLoading();
//                            NormalLoginResponse.Details details = new Gson().fromJson(mReviewViewModel.getDataManager().getCurrentUserInfo(), NormalLoginResponse.Details.class);
//
////            eventViewModel.fetchEventListing("2", pagination, AppConstants.LISTING_FETCH_LIMIT);
//                            mReviewViewModel.setReviewData(details.getId(),file_cache_dir,"","","");
//                        }

                Log.d("check_path", ": " + resultUri.toString());

                Log.d("check_file_get", ": " + file_cache_dir.toString());
                if(pos==1) {

                    arr[0]=file_cache_dir.toString();
                    //  fileList.add(0,file_cache_dir.toString());
                    Glide.with(ReviewActivity.this).load(resultUri.toString()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(activityCreateReviewBinding.firstImage);
                }
                if(pos==2) {
                    arr[1]=file_cache_dir.toString();
//                            if(fileList.size()>0)
//                            fileList.add(1,file_cache_dir.toString());
//                            else
//                                fileList.add(0,file_cache_dir.toString());
                    Glide.with(ReviewActivity.this).load(resultUri.toString()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(activityCreateReviewBinding.secondImage);
                }
                if(pos==3) {
                    arr[2]=file_cache_dir.toString();
//                            if(fileList.size()>0 && fileList.size()<=1 )
//                            fileList.add(1,file_cache_dir.toString());
//                            else if ( fileList.size()>1)
//                                fileList.add(0,file_cache_dir.toString());
//                            else

                    // fileList.add(0,file_cache_dir.toString());
                    Glide.with(ReviewActivity.this).load(resultUri.toString()).apply(RequestOptions.centerCropTransform().placeholder(R.drawable.placeholder_image).error(R.drawable.placeholder_image)).into(activityCreateReviewBinding.thirdImage);
                }


            } else {
                Log.d("file_does_not_exists", ": " + true);
            }
        }
    }

    @Override
    public void success(CommonSimpleResponse response) {
        hideLoading();
        Toast.makeText(ReviewActivity.this,getString(R.string.review_posted),Toast.LENGTH_LONG).show();
        onBackPressed();
    }

    @Override
    public void error() {
        hideLoading();
    }
}