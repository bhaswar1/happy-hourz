package com.admin.happyhours.ui.transparentrestaurantlist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.admin.happyhours.BR;
import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.RestaurantListingCollectionDetailsResponse;
import com.admin.happyhours.databinding.TransparentRestaurantListingBinding;
import com.admin.happyhours.interfaces.OnLoadMoreListener;
import com.admin.happyhours.ui.base.BaseActivity;
import com.admin.happyhours.ui.transparentrestaurantlist.recycleradapter.RecyclerviewAdapterForCollectionListing;
import com.admin.happyhours.utils.AppConstants;
import com.admin.happyhours.utils.SimpleDividerItemDecoration;

import java.util.LinkedList;

import javax.inject.Inject;

public class TransparentRestaurantListingActivity extends BaseActivity<TransparentRestaurantListingBinding, TransparentRestaurantListingViewModel> implements TransparentRestaurantListingNavigator {

    private String collection_id;
    RecyclerviewAdapterForCollectionListing recyclerViewAdapter;
    private int pagination = 1;
    private boolean hasLoadMore = false;
    TransparentRestaurantListingBinding transparentRestaurantListingBinding;
    @Inject
    TransparentRestaurantListingViewModel transparentRestaurantListingViewModel;

    public static Intent newIntent(Context context, String collection_id) {
        return new Intent(context, TransparentRestaurantListingActivity.class).putExtra("collection_id", collection_id);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.transparent_restaurant_listing;
    }

    @Override
    public TransparentRestaurantListingViewModel getViewModel() {
        return transparentRestaurantListingViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        transparentRestaurantListingBinding = getViewDataBinding();
        transparentRestaurantListingViewModel.setNavigator(this);

        setStatusBarHidden(TransparentRestaurantListingActivity.this);

        transparentRestaurantListingBinding.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (getIntent() != null && getIntent().getExtras() != null) {
            collection_id = getIntent().getExtras().getString("collection_id");
        }
if(getIntent().hasExtra("collectionName")){
    transparentRestaurantListingBinding.tvTitle.setText(getIntent().getStringExtra("collectionName"));
}

        setUpRecyclerViewAndLayoutManager();

        reloadOrLoadData();

    }

    private void setUpRecyclerViewAndLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TransparentRestaurantListingActivity.this);
        transparentRestaurantListingBinding.recyclerView.setLayoutManager(linearLayoutManager);
        transparentRestaurantListingBinding.recyclerView.setHasFixedSize(true);
     //   transparentRestaurantListingBinding.recyclerView.addItemDecoration(new SpacesItemDecoration(ViewUtils.dpToPx(2)));
        transparentRestaurantListingBinding.recyclerView.addItemDecoration(new SimpleDividerItemDecoration(TransparentRestaurantListingActivity.this));
        LinkedList<RestaurantListingCollectionDetailsResponse.Details> details = new LinkedList<>();

        recyclerViewAdapter = new RecyclerviewAdapterForCollectionListing(TransparentRestaurantListingActivity.this,
                details, transparentRestaurantListingBinding.recyclerView, linearLayoutManager);
        transparentRestaurantListingBinding.recyclerView.setAdapter(recyclerViewAdapter);

        recyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
//                Log.d("checking_steps_loadmore",": "+(++check));
                Log.d("check_bools", ": " + hasLoadMore);
                if (hasLoadMore) {
                    showLoading();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadData();
                        }
                    }, 1000);
                }
            }
        });
    }

    private void reloadOrLoadData() {
        if (recyclerViewAdapter != null) {
//            recyclerViewAdapter.clearData();
            hasLoadMore = false;
            pagination = 1;
//            eventAdapter.setData(new EventListingResponse.Details());
            loadData();
        }
    }

    private void loadData() {
        if (collection_id != null) {
            showLoading();
            transparentRestaurantListingViewModel.fetchRestuarantListing(collection_id, pagination, AppConstants.LISTING_FETCH_LIMIT);
        }
    }

    @Override
    public void onSuccess(RestaurantListingCollectionDetailsResponse response) {
        hideLoading();
        if (response != null) {
            if (response.getDetails() != null && response.getDetails().size() > 0) {
                if (response.getDetails().size() == AppConstants.LISTING_FETCH_LIMIT) {
                    pagination = pagination + 1;
                    hasLoadMore = true;
                } else {
                    hasLoadMore = false;
                }

                recyclerViewAdapter.setAllData(response.getDetails());
                recyclerViewAdapter.setLoaded();
//                collectionAdapter.setData(collectionListingResponse.getDetails());
            } else if (pagination == 1) {
                hasLoadMore = false;
                transparentRestaurantListingBinding.recyclerView.setVisibility(View.GONE);
                transparentRestaurantListingBinding.nodata.setVisibility(View.VISIBLE);
            } else {
                hasLoadMore = false;
            }
        }
        Log.d("check_response_frag", ": " + hasLoadMore);
    }

    @Override
    public void onError(Throwable throwable) {
        hideLoading();
        if (throwable != null) {
            Log.d("check_error", ": " + throwable.getMessage());
        }
    }
}
