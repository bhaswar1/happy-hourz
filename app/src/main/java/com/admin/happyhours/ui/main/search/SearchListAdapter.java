package com.admin.happyhours.ui.main.search;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.admin.happyhours.R;
import com.admin.happyhours.data.model.api.response.SearchDetail;
import com.admin.happyhours.interfaces.OnItemClickListener;
import com.admin.happyhours.interfaces.OnLoadMoreListener;

import java.util.LinkedList;

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.ViewHolder> {

    public boolean isLoading;
    private final int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private Context context;
    private OnLoadMoreListener mOnLoadMoreListener;
    private OnItemClickListener callback = null;
    private LinkedList<SearchDetail> details;
    public  ItemClickListener searchItemClickListener;

    public SearchListAdapter(FragmentActivity activity, LinkedList<SearchDetail> details,
                             RecyclerView recyclerView, final LinearLayoutManager linearLayoutManager) {
        this.context = activity;
        this.details = details;
//        this.details.add(new CollectionListingResponse.Details());
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_search, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.bind(position);

        holder.resturant_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchItemClickListener.onItemClick(details.get(position),position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void clearData() {
        details.clear();
        notifyDataSetChanged();
    }

//    public void changeRowDataAfteLikeOrLike(String check_for_button, int position) {
//        if (check_for_button.equals(AppConstants.adapter_img_like_in_event_listing)) {
//            if (details.get(position).getFavourite_status() == 0) {
//                details.get(position).setFavourite_status(1);
//            } else {
//                details.get(position).setFavourite_status(0);
//            }
//        }
//        notifyItemChanged(position);
//    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView resturant_title,resturant_address;

        public ViewHolder(View itemView) {
            super(itemView);
            resturant_title=itemView.findViewById(R.id.title);


        }

        public void bind(final int position) {
            resturant_title.setText(details.get(position).getName());


        }

//        @Override
//        public void onClick(View view) {
//          //  if (searchItemClickListener != null)
//                searchItemClickListener.onItemClick(view, getAdapterPosition());
//
//        }
    }

    public LinkedList<SearchDetail> getData() {
        return details;
    }

    public void setAllData(LinkedList<SearchDetail> data) {
        details.addAll(data);
        notifyDataSetChanged();
    }

    public void setData(SearchDetail detail) {
        details.add(detail);
        notifyDataSetChanged();
    }

    public void setLoaded() {
        isLoading = false;
    }

    public interface ItemClickListener {
        void onItemClick(SearchDetail view,int position);

    }
    public void setListener(SearchListAdapter.ItemClickListener itemClickListener) {
        this.searchItemClickListener = itemClickListener;

    }
}
