package com.admin.happyhours.ui.main.special;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SpecialProvider {

    @ContributesAndroidInjector(modules = SpecialModule.class)
    abstract SpecialFragment provideSpecialFragmentFactory();


}
