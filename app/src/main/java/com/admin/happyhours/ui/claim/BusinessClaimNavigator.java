package com.admin.happyhours.ui.claim;

import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;

public interface BusinessClaimNavigator {
    void success(String push);

    void updatePushSuccess(CommonSimpleResponse response);

    void onError(Throwable throwable);

    void updatePasswordSuccess(CommonSimpleResponse response);
}
