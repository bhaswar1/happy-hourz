package com.admin.happyhours.ui.nointernet;

import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class NoInternetModule {

    @Provides
    NoInternetViewModel provideNoInternetViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        return new NoInternetViewModel(dataManager, schedulerProvider);
    }


}
