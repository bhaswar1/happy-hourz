package com.admin.happyhours.ui.main.myaccount;

import androidx.lifecycle.ViewModelProvider;

import com.admin.happyhours.ViewModelProviderFactory;
import com.admin.happyhours.data.DataManager;
import com.admin.happyhours.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class MyAccountModule {


    @Provides
    MyAccountViewModel accountViewModel(DataManager dataManager,
                                       SchedulerProvider schedulerProvider) {
        return new MyAccountViewModel(dataManager, schedulerProvider);
    }


    @Provides
    ViewModelProvider.Factory provideAccountViewModel(MyAccountViewModel myAccountViewModel) {
        return new ViewModelProviderFactory<>(myAccountViewModel);
    }

}
