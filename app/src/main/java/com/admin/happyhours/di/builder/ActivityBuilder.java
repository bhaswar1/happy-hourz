package com.admin.happyhours.di.builder;

import com.admin.happyhours.ui.claim.BusinessClaimActivity;
import com.admin.happyhours.ui.claim.BusinessClaimModule;
import com.admin.happyhours.ui.createcollection.CreateCollectionActivity;
import com.admin.happyhours.ui.createcollection.CreateCollectionModule;
import com.admin.happyhours.ui.eventdetails.EventDetailActivity;
import com.admin.happyhours.ui.eventdetails.EventDetailModule;
import com.admin.happyhours.ui.feedback.FeedbackActivity;
import com.admin.happyhours.ui.feedback.FeedbackModule;
import com.admin.happyhours.ui.forgotpassword.ForgotPasswordActivity;
import com.admin.happyhours.ui.forgotpassword.ForgotPasswordModule;
import com.admin.happyhours.ui.helpsupport.HelpSupportActivity;
import com.admin.happyhours.ui.helpsupport.HelpSupportActivityModule;
import com.admin.happyhours.ui.login.LoginActivity;
import com.admin.happyhours.ui.login.LoginActivityModule;
import com.admin.happyhours.ui.main.HomeActivity;
import com.admin.happyhours.ui.main.HomeModule;
import com.admin.happyhours.ui.main.collections.CollectionProvider;
import com.admin.happyhours.ui.main.events.EventProvider;
import com.admin.happyhours.ui.main.favourites.FavouritesProvider;
import com.admin.happyhours.ui.main.myaccount.MyAccountProvider;
import com.admin.happyhours.ui.main.notification.NotificationProvider;
import com.admin.happyhours.ui.main.search.SearchProvider;
import com.admin.happyhours.ui.main.special.SpecialProvider;
import com.admin.happyhours.ui.nointernet.NoInternetActivity;
import com.admin.happyhours.ui.nointernet.NoInternetModule;
import com.admin.happyhours.ui.privacytermsbusinesssignup.PrivacyTermsBusinessActivity;
import com.admin.happyhours.ui.privacytermsbusinesssignup.PrivacyTermsBusinessModule;
import com.admin.happyhours.ui.resturantdeatails.ResturantDeatilsActivityModule;
import com.admin.happyhours.ui.resturantdeatails.ResturantDetailsActivity;
import com.admin.happyhours.ui.resturantlocation.RasturantLocationActivity;
import com.admin.happyhours.ui.resturantlocation.RasturantLocationModule;
import com.admin.happyhours.ui.review.ReviewActivity;
import com.admin.happyhours.ui.review.ReviewActivityModule;
import com.admin.happyhours.ui.reviewlist.ReviewListActivity;
import com.admin.happyhours.ui.reviewlist.ReviewListModule;
import com.admin.happyhours.ui.reviewphoto.ReviewPhotoActivity;
import com.admin.happyhours.ui.reviewphoto.ReviewPhotoModule;
import com.admin.happyhours.ui.settings.SettingsActivity;
import com.admin.happyhours.ui.settings.SettingsModule;
import com.admin.happyhours.ui.signup.SignupActivity;
import com.admin.happyhours.ui.signup.SignupModule;
import com.admin.happyhours.ui.splash.SplashActivity;
import com.admin.happyhours.ui.splash.SplashActivityModule;
import com.admin.happyhours.ui.transparentrestaurantaddtocollection.TransparentActivity;
import com.admin.happyhours.ui.transparentrestaurantaddtocollection.TransparentModule;
import com.admin.happyhours.ui.transparentrestaurantlist.TransparentRestaurantListingActivity;
import com.admin.happyhours.ui.transparentrestaurantlist.TransparentRestaurantListingModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {


    @ContributesAndroidInjector(modules = SplashActivityModule.class)
    abstract SplashActivity bindSplashActivity();

    @ContributesAndroidInjector(modules = LoginActivityModule.class)
    abstract LoginActivity bindLoginActivity();

    @ContributesAndroidInjector(modules = ResturantDeatilsActivityModule.class)
    abstract ResturantDetailsActivity resturantDetailsActivity();

    @ContributesAndroidInjector(modules = SignupModule.class)
    abstract SignupActivity bindSignupActivity();

    @ContributesAndroidInjector(modules = {
            HomeModule.class,
            SpecialProvider.class,
            CollectionProvider.class,
            SearchProvider.class,
            EventProvider.class,
            FavouritesProvider.class,

            MyAccountProvider.class,
            NotificationProvider.class
    })
    abstract HomeActivity bindHomeActivity();

    @ContributesAndroidInjector(modules = CreateCollectionModule.class)
    abstract CreateCollectionActivity bindCreateCollectionActivity();

    @ContributesAndroidInjector(modules = PrivacyTermsBusinessModule.class)
    abstract PrivacyTermsBusinessActivity bindPrivacyPolicyActivity();

    @ContributesAndroidInjector(modules = FeedbackModule.class)
    abstract FeedbackActivity bindFeedbackActivity();

    @ContributesAndroidInjector(modules = TransparentModule.class)
    abstract TransparentActivity bindTransparentActivity();

    @ContributesAndroidInjector(modules = NoInternetModule.class)
    abstract NoInternetActivity bindNoInternetActivity();

    @ContributesAndroidInjector(modules = SettingsModule.class)
    abstract SettingsActivity bindNoSettingsActivity();

    @ContributesAndroidInjector(modules = EventDetailModule.class)
    abstract EventDetailActivity eventDetailActivity();

    @ContributesAndroidInjector(modules = ForgotPasswordModule.class)
    abstract ForgotPasswordActivity forgotPasswordActivity();

    @ContributesAndroidInjector(modules = TransparentRestaurantListingModule.class)
    abstract TransparentRestaurantListingActivity transparentRestaurantListingActivity();
    @ContributesAndroidInjector(modules = RasturantLocationModule.class)
    abstract RasturantLocationActivity rasturantLocationActivity();

    @ContributesAndroidInjector(modules = ReviewActivityModule.class)
    abstract ReviewActivity reviewActivity();
    @ContributesAndroidInjector(modules = BusinessClaimModule.class)
    abstract BusinessClaimActivity businessClaimActivity();
    @ContributesAndroidInjector(modules = ReviewListModule.class)
    abstract ReviewListActivity reviewListActivity();
    @ContributesAndroidInjector(modules = ReviewPhotoModule.class)
    abstract ReviewPhotoActivity reviewPhotoActivity();
    @ContributesAndroidInjector(modules = HelpSupportActivityModule.class)
    abstract HelpSupportActivity helpSupportActivity();
}
