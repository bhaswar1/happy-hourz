package com.admin.happyhours.data.local.db;

import android.util.Log;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import com.admin.happyhours.data.model.db.UserDataModel;

@Singleton
public class AppDbHelper implements DbHelper {

    private final AppDataBase mAppDatabase;

    @Inject
    public AppDbHelper(AppDataBase mAppDatabase) {
        this.mAppDatabase = mAppDatabase;
    }


    @Override
    public Observable<List<UserDataModel>> getAllUsers() {
        return Observable.fromCallable(new Callable<List<UserDataModel>>() {
            @Override
            public List<UserDataModel> call() throws Exception {
                Log.d("check_inserted_data_size",": "+mAppDatabase.userDao().loadAll().size());
                return mAppDatabase.userDao().loadAll();
            }
        });
    }

    @Override
    public Observable<Boolean> insertUser(final UserDataModel user) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Log.d("check_inserted_data",": "+user.name);
                mAppDatabase.userDao().insert(user);
                return true;
            }
        });
    }
}
