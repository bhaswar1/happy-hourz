
package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

//@TypeConverters(DataTypeConverters.class)
public class SpecialPageRestuarantListingResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Details")
    @Expose
    private LinkedList<Detail> details;




    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(LinkedList<Detail> details) {
        this.details = details;
    }

    public  class Detail {

        @SerializedName("restaurent_id")
        @Expose
        private String restaurentId;


        @SerializedName("service")
        @Expose
        private String service;
        @SerializedName("restaurent_name")
        @Expose
        private String restaurentName;

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        @SerializedName("coverimage")
        @Expose
        private String coverimage;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("distance")
        @Expose
        private Integer distance;
        @SerializedName("rating")
        @Expose
        private String rating;
        @SerializedName("collection_status")
        @Expose
        private Integer collectionStatus;
        @SerializedName("favourite_status")
        @Expose
        private Integer favouriteStatus;

        @SerializedName("extra")
        @Expose
        private Extra extra;

        @SerializedName("cuisine")
        @Expose
        private String cuisine;

        @SerializedName("open_time")
        @Expose
        private String openTime;
        public int getTotalReview() {
            return totalReview;
        }

        public void setTotalReview(int totalReview) {
            this.totalReview = totalReview;
        }

        @SerializedName("total_review")
        @Expose
        public int totalReview;


        public String getOpenTime() {
            return openTime;
        }

        public void setOpenTime(String openTime) {
            this.openTime = openTime;
        }

        public String getCloseTime() {
            return closeTime;
        }

        public void setCloseTime(String closeTime) {
            this.closeTime = closeTime;
        }

        @SerializedName("close_time")
        @Expose
        private String closeTime;

        public ArrayList<String> getOtherimages() {
            return otherimages;
        }

        public void setOtherimages(ArrayList<String> otherimages) {
            this.otherimages = otherimages;
        }

        @SerializedName("otherimages")
        @Expose
        private ArrayList<String> otherimages = new ArrayList<>();





        public String getCuisine() {
            return cuisine;
        }

        public void setCuisine(String cuisine) {
            this.cuisine = cuisine;
        }

        public Extra getExtra() {
            return extra;
        }

        public void setExtra(Extra extra) {
            this.extra = extra;
        }

        public String getRestaurentId() {
            return restaurentId;
        }

        public void setRestaurentId(String restaurentId) {
            this.restaurentId = restaurentId;
        }

        public String getRestaurentName() {
            return restaurentName;
        }

        public void setRestaurentName(String restaurentName) {
            this.restaurentName = restaurentName;
        }

        public String getCoverimage() {
            return coverimage;
        }

        public void setCoverimage(String coverimage) {
            this.coverimage = coverimage;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public Integer getDistance() {
            return distance;
        }

        public void setDistance(Integer distance) {
            this.distance = distance;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public Integer getCollectionStatus() {
            return collectionStatus;
        }

        public void setCollectionStatus(Integer collectionStatus) {
            this.collectionStatus = collectionStatus;
        }

        public Integer getFavouriteStatus() {
            return favouriteStatus;
        }

        public void setFavouriteStatus(Integer favouriteStatus) {
            this.favouriteStatus = favouriteStatus;
        }



    }

    public class Offer {

        @SerializedName("dealdetails")
        @Expose
        private String dealdetails;

        public String getDealdetails() {
            return dealdetails;
        }

        public void setDealdetails(String dealdetails) {
            this.dealdetails = dealdetails;
        }

    }

}
