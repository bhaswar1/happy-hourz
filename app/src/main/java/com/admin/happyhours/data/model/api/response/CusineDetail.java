
package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CusineDetail {

    @SerializedName("cusinsid")
    @Expose
    private String cusinsid;
    @SerializedName("cusinsname")
    @Expose
    private String cusinsname;
    public boolean isCheck() {
        return check;
    }

    private  boolean check=false;
    public String getCusinsid() {
        return cusinsid;
    }

    public void setCusinsid(String cusinsid) {
        this.cusinsid = cusinsid;
    }

    public String getCusinsname() {
        return cusinsname;
    }

    public void setCusinsname(String cusinsname) {
        this.cusinsname = cusinsname;
    }

    public void setChecked(boolean isChecked) {
        this.check=isChecked;
    }
}
