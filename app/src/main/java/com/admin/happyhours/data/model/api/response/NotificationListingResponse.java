package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class NotificationListingResponse {

    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("Details")
    @Expose
    LinkedList<Details> details;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public LinkedList<Details> getDetails() {
        return details;
    }

    public static class Details {

        @SerializedName("event_id")
        @Expose
        String event_id;
        @SerializedName("event_name")
        @Expose
        String event_name;
        @SerializedName("event_image")
        @Expose
        String event_image;
        @SerializedName("event_desc")
        @Expose
        String event_desc;

        public String getEvent_id() {
            return event_id;
        }

        public String getEvent_name() {
            return event_name;
        }

        public String getEvent_image() {
            return event_image;
        }

        public String getEvent_desc() {
            return event_desc;
        }
    }

}
