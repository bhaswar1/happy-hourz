
package com.admin.happyhours.data.model.api.response;

import java.util.LinkedList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TagResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Food")
    @Expose
    private LinkedList<TagDetail> details = new LinkedList<>();
    @SerializedName("Drink")
    @Expose
    private LinkedList<TagDetail> detailsDrink = new LinkedList<>();


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LinkedList<TagDetail> getDetailsDrink() {
        return detailsDrink;
    }

    public void setDetailsDrink(LinkedList<TagDetail> detailsDrink) {
        this.detailsDrink = detailsDrink;
    }

    public LinkedList<TagDetail> getDetails() {
        return details;
    }

    public void setDetails(LinkedList<TagDetail> details) {
        this.details = details;
    }

}
