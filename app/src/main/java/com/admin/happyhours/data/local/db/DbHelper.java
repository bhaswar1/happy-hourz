package com.admin.happyhours.data.local.db;

import java.util.List;

import io.reactivex.Observable;
import com.admin.happyhours.data.model.db.UserDataModel;

public interface DbHelper {

    Observable<List<UserDataModel>> getAllUsers();

    Observable<Boolean> insertUser(UserDataModel user);
}
