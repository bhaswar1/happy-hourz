
package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventDetails {

    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("event_date")
    @Expose
    private String eventDate;
    @SerializedName("event_day")
    @Expose
    private String eventDay;
    @SerializedName("event_month")
    @Expose
    private String eventMonth;
    @SerializedName("event_year")
    @Expose
    private String eventYear;
    @SerializedName("eventDescription")
    @Expose
    private String eventDescription;
    @SerializedName("eventLink")
    @Expose
    private String eventLink;
    @SerializedName("eventStartTime")
    @Expose
    private String eventStartTime;
    @SerializedName("eventEndTime")
    @Expose
    private String eventEndTime;
    @SerializedName("event_image")
    @Expose
    private String eventImage;
    @SerializedName("favourite_status")
    @Expose
    private Integer favouriteStatus;
    @SerializedName("venue_id")
    @Expose
    private String venueId;
    @SerializedName("venue_name")
    @Expose
    private String venueName;
    @SerializedName("venue_address")
    @Expose
    private String venueAddress;
    @SerializedName("venue_lat")
    @Expose
    private String venueLat;
    @SerializedName("venue_long")
    @Expose
    private String venueLong;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventDay() {
        return eventDay;
    }

    public void setEventDay(String eventDay) {
        this.eventDay = eventDay;
    }

    public String getEventMonth() {
        return eventMonth;
    }

    public void setEventMonth(String eventMonth) {
        this.eventMonth = eventMonth;
    }

    public String getEventYear() {
        return eventYear;
    }

    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventLink() {
        return eventLink;
    }

    public void setEventLink(String eventLink) {
        this.eventLink = eventLink;
    }

    public String getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(String eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public Integer getFavouriteStatus() {
        return favouriteStatus;
    }

    public void setFavouriteStatus(Integer favouriteStatus) {
        this.favouriteStatus = favouriteStatus;
    }

    public String getVenueId() {
        return venueId;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getVenueAddress() {
        return venueAddress;
    }

    public void setVenueAddress(String venueAddress) {
        this.venueAddress = venueAddress;
    }

    public String getVenueLat() {
        return venueLat;
    }

    public void setVenueLat(String venueLat) {
        this.venueLat = venueLat;
    }

    public String getVenueLong() {
        return venueLong;
    }

    public void setVenueLong(String venueLong) {
        this.venueLong = venueLong;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
