
package com.admin.happyhours.data.model.api.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Extra implements Serializable
{

    @SerializedName("food")
    @Expose
    private ArrayList<String> food =new ArrayList<>();
    @SerializedName("drinks")
    @Expose
    private ArrayList<String> drinks =new  ArrayList<>();
    private final static long serialVersionUID = 1623422316264215266L;

    public ArrayList<String> getFood() {
        return food;
    }

    public void setFood(ArrayList<String> food) {
        this.food = food;
    }

    public ArrayList<String> getDrinks() {
        return drinks;
    }

    public void setDrinks(ArrayList<String> drinks) {
        this.drinks = drinks;
    }

}
