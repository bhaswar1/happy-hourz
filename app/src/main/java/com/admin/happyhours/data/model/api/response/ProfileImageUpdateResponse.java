package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileImageUpdateResponse {
    @SerializedName("status")
    @Expose
    String status;


    @SerializedName("message")
    @Expose
    String message;

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    @SerializedName("profile_image")
    @Expose
    String profile_image;
    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
