
package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TagDetail {

    @SerializedName("tagid")
    @Expose
    private String tagid;
    @SerializedName("tagname")
    @Expose
    private String tagname;

    public boolean isCheck() {
        return check;
    }

    private  boolean check=false;

    public String getTagid() {
        return tagid;
    }

    public void setTagid(String tagid) {
        this.tagid = tagid;
    }

    public String getTagname() {
        return tagname;
    }

    public void setTagname(String tagname) {
        this.tagname = tagname;
    }

    public void setChecked(boolean isChecked) {
        this.check=isChecked;
    }
}
