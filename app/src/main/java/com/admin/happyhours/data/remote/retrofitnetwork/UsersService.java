package com.admin.happyhours.data.remote.retrofitnetwork;

import com.admin.happyhours.data.model.api.response.AccountResponse;
import com.admin.happyhours.data.model.api.response.CollectionCreateResponse;
import com.admin.happyhours.data.model.api.response.CollectionListingResponse;
import com.admin.happyhours.data.model.api.response.CommonSimpleResponse;
import com.admin.happyhours.data.model.api.response.CusineRespone;
import com.admin.happyhours.data.model.api.response.EventDetailResponse;
import com.admin.happyhours.data.model.api.response.EventListingResponse;
import com.admin.happyhours.data.model.api.response.FevouriteEventData;
import com.admin.happyhours.data.model.api.response.FevouriteListData;
import com.admin.happyhours.data.model.api.response.NormalLoginResponse;
import com.admin.happyhours.data.model.api.response.NotificationListingResponse;
import com.admin.happyhours.data.model.api.response.PrivacyPolicyAndTermsAndConditionResponse;
import com.admin.happyhours.data.model.api.response.ProfileImageUpdateResponse;
import com.admin.happyhours.data.model.api.response.PushNotificationResponse;
import com.admin.happyhours.data.model.api.response.RestaurantListingCollectionDetailsResponse;
import com.admin.happyhours.data.model.api.response.ReviewListResponse;
import com.admin.happyhours.data.model.api.response.ReviewPhotoResponse;
import com.admin.happyhours.data.model.api.response.SearchDataResponse;
import com.admin.happyhours.data.model.api.response.ServiceResponse;
import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.admin.happyhours.data.model.api.response.TagResponse;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;


public interface UsersService {


    @Headers({
            "x-api-key: 123456"
    })

    // normal signup
    @GET("app/user/signup")
    Single<NormalLoginResponse> normalSignup(@Query("name") String name,
                                             @Query("email") String email,
                                             @Query("password") String password,
                                             @Query("gender") String gender,
                                             @Query("dob") String dob,
                                             @Query("phone") String phone,
                                             @Query("device_type") String device_type,
                                             @Query("device_token") String device_token);


    // normal signin
    @GET("app/user/profile_login")
    Single<NormalLoginResponse> normalSignin(@Query("email") String email,
                                             @Query("password") String password,
                                             @Query("device_type") String device_type,
                                             @Query("device_token") String device_token);


    // social signin
    /*@GET("app/user/login")
    Single<NormalLoginResponse> socialSignin(@Query("email") String email,
                                             @Query("id") String id);*/


    // social signup (FB)
    @GET("app/user/fbsignup")
    Single<NormalLoginResponse> socialFBSignup(@Query("name") String name,
                                               @Query("email") String email,
                                               @Query("fbid") String id,
                                               @Query("fb_image") String profile_image,
                                               @Query("device_type") String device_type,
                                               @Query("device_token") String device_token);

    // social signup (GOOGLE)
    @GET("app/user/googlesignup")
    Single<NormalLoginResponse> socialGOOGLESignup(@Query("name") String name,
                                                   @Query("email") String email,
                                                   @Query("gid") String id,
                                                   @Query("gplus_image") String profile_image,
                                                   @Query("device_type") String device_type,
                                                   @Query("device_token") String device_token);

    // Restaurant Listing
    @GET("app/findrestaurentlast_new_control")
    Single<JsonObject> specialPageRestuarantListing(                          @Query("userid") String userid,
                                                                              @Query("latitude") String latitude,
                                                                              @Query("longitude") String longitude,
                                                                              @Query("tagsandroid[]") ArrayList<String> tags,
                                                                              @Query("distance") String distance,
                                                                              @Query("deals") String deals,
                                                                               @Query("cuisineandroid[]") ArrayList<String> cuisineandroid,
                                                                               @Query("drink_tag_android[]") ArrayList<String> tagsDrinks,
                                                                              @Query("service_android[]") ArrayList<String> serViceId


    );

    // Like Restaurant
    @GET("app/collectiondelete_control")
    Single<CommonSimpleResponse> bookmarkAndUnbookmark(@Query("userid") String userid,
                                                       @Query("restaurent_id") String restaurent_id);

    // Like Restaurant
    @GET("app/Restaurentfavourite_control")
    Single<CommonSimpleResponse> speciallikeAndUnlike(@Query("userid") String userid,
                                               @Query("restaurent_id") String restaurent_id);

    // Collection Listing
    @GET("app/Collectioncreate_control/listing")
    Single<CollectionListingResponse> collectionListing(@Query("userid") String userid,
                                                        @Query("page") int pageination,
                                                        @Query("limit") int limit_of_data_for_single_call);

    // Event Listing
    @GET("app/eventlisting")
    Single<EventListingResponse> eventListing(@Query("userid") String userid,
                                              @Query("page") int pageination,
                                              @Query("limit") int limit_of_data_for_single_call);

    // Event Like Unlike
    @GET("app/Eventfavourite_control")
    Single<CommonSimpleResponse> eventLikeUnlike(@Query("userid") String userid,
                                                 @Query("event_id") String event_id);

    //sudip resturant detail

    @GET("app/restaurentdetails_control")
    Single<JsonObject> resturantDeatail(@Query("userid") String userid,
                                        @Query("restaurentid") String restaurentid,
                                        @Query("mode") String mode,
                                        @Query("latitude") String latitude,
                                        @Query("longitude") String longitude
    );

    //sutanu create collection
    @Multipart
    @POST("app/Collectioncreate_control")
    Single<CollectionCreateResponse> createCollection(@Part("userid") RequestBody userid,
                                                      @Part("name") RequestBody name,
                                                      @Part("type") RequestBody type,
                                                      @Part MultipartBody.Part part);

    //sutanu create collection
    @Multipart
    @POST("app/Collectioncreate_control")
    Single<CommonSimpleResponse> updateCollection(@Part("userid") RequestBody userid,
                                                  @Part("name") RequestBody name,
                                                  @Part("type") RequestBody type,
                                                  @Part("collection_id") RequestBody collection_id,
                                                  @Part MultipartBody.Part part);

    //        sudip faveroute part
    @GET("app/Restaurentfavourite_control/getlistingrestaurent")
    Single<FevouriteListData> fevouriteDetail(@Query("userid") String userid,
                                              @Query("page") int page,
                                              @Query("limit") int limit);

    @GET("app/Eventfavourite_control/getlistingevent")
    Single<FevouriteEventData> fevouriteEventDetail(@Query("userid") String userid,
                                                    @Query("page") int page,
                                                    @Query("limit") int limit
    );

    // Sutanu restaurant add to collection
    @FormUrlEncoded
    @POST("app/collectionadd_control")
    Single<CommonSimpleResponse> addRestaurantToCollection(@Field("userid") String userid,
                                                           @Field("restaurent_id") String restaurent_id,
                                                           @Field("collection_id") String collection_id);

    // Sutanu deletion of collection
    @FormUrlEncoded
    @POST("app/Collectioncreate_control/collectiondelete")
    Single<CommonSimpleResponse> deleteCollection(@Field("userid") String userid,
                                                  @Field("collection_id") String collection_id);


    // Sutanu notification page listing
    @FormUrlEncoded
    @POST("app/notification_control")
    Single<NotificationListingResponse> notificationListing(@Field("userid") String userid,
                                                            @Field("limit") int limit,
                                                            @Field("page") int page);


    @GET("app/user")
    Single<AccountResponse> accountDetail(@Query("userid") String userid);


    // Sutanu User logout
    @FormUrlEncoded
    @POST("app/logout_control")
    Single<CommonSimpleResponse> userLogout(@Field("userid") String userid);




    @Multipart
    @POST("app/user/image_update")
    Single<ProfileImageUpdateResponse> updateProfileImage(@Part("userid") RequestBody userid,

                                                          @Part MultipartBody.Part part);


    @FormUrlEncoded
    @POST("app/user/profileedit")
    Single<AccountResponse> updateProfileData(@Field("userid") String id,
                                                            @Field("address") String address,
                                                            @Field("latitude") String latitude,
                                                             @Field("longitude") String longitude,
                                                          @Field("name") String name,
                                                             @Field("dob") String dob,
                                                         @Field("gender") String gender,
                                                          @Field("phone") String phone
                                                                 );


    // Sutanu notification badge count clear
    @FormUrlEncoded
    @POST("app/readnotification_control")
    Single<CommonSimpleResponse> clearNotificationBadgeCount(@Field("userid") String userid);


    // Sutanu privacy policy andTerms & Condition
    @FormUrlEncoded
    @POST("app/termscondition_control")
    Single<PrivacyPolicyAndTermsAndConditionResponse> privacyPolicyAndTermsCondition(@Field("mode") String mode);



    @FormUrlEncoded
    @POST("app/settings_control")
    Single<PushNotificationResponse> pushSwitch(@Field("userid") String id

    );

    @FormUrlEncoded
    @POST("app/settings_control/updatepush")
    Single<CommonSimpleResponse> updatePush(@Field("userid") String id,
                                            @Field("push") String push
    );

    @FormUrlEncoded
    @POST("app/settings_control/updatepassword")
    Single<CommonSimpleResponse> changePassword(@Field("userid") String id,
                                            @Field("oldpassword") String oldpassword,
                                                @Field("password") String password
    );
    //search lIst
    @GET("app/Searchhistory_control")
    Single<SearchDataResponse> searchList(@Query("userid") String id,
                                          @Query("search") String search,
                                          @Query("type") String type,
                                          @Query("limit") String limit,
                                           @Query("page") String page
    );
   // Searchhistory_detail
    @FormUrlEncoded
    @POST("app/Searchhistory_control/getsearch")
    Single<SearchDataResponse> searchHistoryList(@Field("userid") String id,

                                          @Field("type") String type

    );
    //clear search History
    @FormUrlEncoded
    @POST("app/Searchhistory_control/clearhistory")
    Single<CommonSimpleResponse> clearSearchHistoryList(@Field("userid") String id



    );
    @FormUrlEncoded
    @POST("app/Eventdetails_control")
    Single<JsonObject> eventDetails(@Field("userid") String id,
                                                @Field("eventid") String eventid


    );
//    @FormUrlEncoded
//    @POST("app/feedbackapi_control")
//    Single<CommonSimpleResponse> feedbackResponse(@Field("userid") String id,
//                                                  @Field("message") String message);


    @Multipart
    @POST("app/feedbackapi_control")
    Single<CommonSimpleResponse> feedbackResponse(@Part("userid") RequestBody userid,
                                                        @Part("message") RequestBody message,
                                                          @Part MultipartBody.Part part);


    @FormUrlEncoded
    @POST("app/forgotpass_control")
    Single<CommonSimpleResponse> forgotPassword(@Field("email") String id
                                                );
    @FormUrlEncoded
    @POST("app/forgotpass_control/changepassword")
    Single<CommonSimpleResponse> setNewpassword(@Field("id") String id,
                                                @Field("code") String code,
                                                @Field("password") String password
    );


    // Sutanu -- Collection Details listing for transparent page
    @FormUrlEncoded
    @POST("app/collectiondetails_control")
    Single<RestaurantListingCollectionDetailsResponse> fetchRestaurantCollection(@Field("userid") String userid,
                                                                                 @Field("collection_id") String collection_id,
                                                                                 @Field("page") int page,
                                                                                 @Field("limit") int limit);
    @FormUrlEncoded
    @POST("app/collectionadd_control")
    Single<CollectionListingResponse> addSpecialToCollection(@Field("userid") String userid,
                                                                                 @Field("collection_id") String collection_id,
                                                                                 @Field("restaurent_id") int restaurent_id
                                                                                          );



    @Multipart
    @POST("app/review_restaurent")
    Single<CommonSimpleResponse> setReview(@Query("userid") String userid,
                                           @Query("star") String star,
                                           @Query("comment") String comment,
                                           @Query("restaurent") String restaurent,
                                           @Part MultipartBody.Part[] part);

    @POST("app/review_restaurent/listing")
    Single<ReviewListResponse> reviewListing(@Query("userid") String userid,
                                             @Query("restaurent_id") String restaurent_id,
                                             @Query("limit") int limit,
                                             @Query("page") int page
    );
    @FormUrlEncoded
    @POST("app/restaurentreviewimage_control")
    Single<ReviewPhotoResponse> reviewphotoListing(@Field("userid") String userid,
                                                   @Field("restaurentid") String restaurent_id,
                                                   @Field("limit") int limit,
                                                   @Field("page") int page
    );


    @POST("app/alltags_control")
    Single<TagResponse> tagresponselIsting(
    );

//

    @POST("app/allcuisins_control")
    Single<CusineRespone> cusinslIsting(
    );
//help support


    @POST("app/allcuisins_control/all_service")
    Single<ServiceResponse> serviceListing(
    );




    @FormUrlEncoded
    @POST("app/Static_details_api")
    Single<CommonSimpleResponse> helpsupprtApiCal(@Field("static_id") String static_id

    );


    @FormUrlEncoded
    @POST("app/unreadnotification_control")
    Single<CommonSimpleResponse> notificationBadge(@Field("userid") String userid

    );


}
