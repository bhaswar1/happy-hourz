
package com.admin.happyhours.data.model.api.response;

import java.util.LinkedList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FevouriteListData
{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Details")
    @Expose
    private LinkedList<Detail> details = new LinkedList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LinkedList<Detail> getDetails() {
        return details;
    }

    public void setDetails(LinkedList<Detail> details) {
        this.details = details;
    }

}
