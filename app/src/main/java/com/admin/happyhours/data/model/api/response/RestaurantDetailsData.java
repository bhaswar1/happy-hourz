
package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestaurantDetailsData {

    @SerializedName("status")
    @Expose
    private String status;

    public String getStatus() {
        return status;
    }

    //    @SerializedName("message")
//    @Expose
//    private String message;
//    @SerializedName("coverimage")
//    @Expose
//    private String coverimage;
//    @SerializedName("name")
//    @Expose
//    private String name;
//    @SerializedName("address")
//    @Expose
//    private String address;
//    @SerializedName("phone")
//    @Expose
//    private String phone;
//    @SerializedName("open_time")
//    @Expose
//    private String openTime;
//    @SerializedName("close_time")
//    @Expose
//    private String closeTime;
//    @SerializedName("miles")
//    @Expose
//    private Integer miles;
//    @SerializedName("Details")
//    @Expose
//    private Details details;
//    @SerializedName("favourite_status")
//    @Expose
//    private Integer favouriteStatus;
//    @SerializedName("collection_status")
//    @Expose
//    private Integer collectionStatus;
//    @SerializedName("rating")
//    @Expose
//    private Integer rating;


    public static class Details {

        @SerializedName("otherimages")
        @Expose
        private List<String> otherimages = null;
        @SerializedName("extra")
        @Expose
        private Extra extra;

    }


    public static class Extra {

        @SerializedName("food")
        @Expose
        private List<String> food = null;
        @SerializedName("drinks")
        @Expose
        private List<String> drinks = null;
    }

}
