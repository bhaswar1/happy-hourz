
package com.admin.happyhours.data;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

import com.admin.happyhours.data.local.db.DbHelper;
import com.admin.happyhours.data.local.prefs.PreferencesHelper;
import com.admin.happyhours.data.model.db.UserDataModel;


@Singleton
public class ApplicationDataManager implements DataManager {


    private final Context mContext;

    private final DbHelper mDbHelper;

    private final Gson mGson;

    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public ApplicationDataManager(Context context, DbHelper dbHelper, PreferencesHelper preferencesHelper, Gson gson) {
        mContext = context;
        mDbHelper = dbHelper;
        mPreferencesHelper = preferencesHelper;
        mGson = gson;


    }



    @Override
    public void setUserAsLoggedOut() {
        updateUserInfo(
                null);
    }



    @Override
    public void updateUserInfo(
            String user) {

        /*setUserDetails(user);
        if (user != null) {
            setUserDetailsForOnce(user);
        }*/

//        setAccessToken(accessToken);
//        setCurrentUserId(userId);
//        setCurrentUserLoggedInMode(loggedInMode);
//        setCurrentUserName(userName);
//        setCurrentUserEmail(email);
//        setCurrentUserProfilePicUrl(profilePicPath);
//
//        updateApiHeader(userId, accessToken);
    }


    @Override
    public Observable<List<UserDataModel>> getAllUsers() {
        return mDbHelper.getAllUsers();
    }

    @Override
    public Observable<Boolean> insertUser(UserDataModel user) {
        Log.d("check_inserted_data_2",": "+user.name);
        return mDbHelper.insertUser(user);
    }

    @Override
    public void setCurrentUserInfo(String email) {
        mPreferencesHelper.setCurrentUserInfo(email);
    }

    @Override
    public void deleteCurrentUser() {
        mPreferencesHelper.deleteCurrentUser();
    }

    @Override
    public String getCurrentUserInfo() {
        return mPreferencesHelper.getCurrentUserInfo();
    }

    @Override
    public Long getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(Long userId) {
        mPreferencesHelper.setCurrentUserId(userId);
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
    }

    @Override
    public String getLatitude() {
        return mPreferencesHelper.getLatitude();
    }

    @Override
    public void setLatitude(String latitude) {
        mPreferencesHelper.setLatitude(latitude);
    }

    @Override
    public String getLongitude() {
        return mPreferencesHelper.getLongitude();
    }

    @Override
    public void setLongitude(String longitude) {
        mPreferencesHelper.setLongitude(longitude);
    }

    @Override
    public String getPassword() {
        return mPreferencesHelper.getPassword();
    }

    @Override
    public void setPassword(String password) {
        mPreferencesHelper.setPassword(password);
    }

    @Override
    public void deletePassword() {
        mPreferencesHelper.deletePassword();
    }

    @Override
    public void setCenterLat(String lattitude) {
        mPreferencesHelper.setCenterLat(lattitude);
    }

    @Override
    public String getCenterLat() {
        return mPreferencesHelper.getCenterLat();
    }

    @Override
    public void setCenterLong(String longitude) {
        mPreferencesHelper.setCenterLong(longitude);
    }

    @Override
    public String getCenterLong() {
        return mPreferencesHelper.getCenterLong();
    }


}
