package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class CollectionListingResponse {

    @SerializedName("status")
    @Expose
    String status;

    @SerializedName("favorite_status")
    @Expose
    int favorite_status;

    public int getFavorite_status() {
        return favorite_status;
    }

    public void setFavorite_status(int favorite_status) {
        this.favorite_status = favorite_status;
    }

    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("Details")
    @Expose
    LinkedList<Details> details;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public LinkedList<Details> getDetails() {
        return details;
    }

    public static class Details {

        @SerializedName("collection_name")
        @Expose
        String collection_name;
        @SerializedName("collection_id")
        @Expose
        String collection_id;
        @SerializedName("collection_image")
        @Expose
        String collection_image;

        public String getCollection_name() {
            return collection_name;
        }

        public String getCollection_id() {
            return collection_id;
        }

        public String getCollection_image() {
            return collection_image;
        }
    }
}
