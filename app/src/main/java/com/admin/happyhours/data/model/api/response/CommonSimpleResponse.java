package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonSimpleResponse {




    @SerializedName("badgecount")
    @Expose
    String badgecount;

    public String getBadgecount() {
        return badgecount;
    }

    public void setBadgecount(String badgecount) {
        this.badgecount = badgecount;
    }

    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("id")
    @Expose
    String id;





    @SerializedName("results")
    @Expose
    String result;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
    public String getResult() {
        return result;
    }
}
