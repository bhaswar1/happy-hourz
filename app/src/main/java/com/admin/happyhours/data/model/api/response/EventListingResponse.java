package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class EventListingResponse {

    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("Details")
    @Expose
    LinkedList<Details> details;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public LinkedList<Details> getDetails() {
        return details;
    }

    public static class Details{

        @SerializedName("eventid")
        @Expose
        String eventid;
        @SerializedName("eventname")
        @Expose
        String eventname;
        @SerializedName("description")
        @Expose
        String description;
        @SerializedName("eventimage")
        @Expose
        String eventimage;
        @SerializedName("favourite_status")
        @Expose
        int favourite_status;
        @SerializedName("event_day")
        @Expose
        String event_day;
        @SerializedName("event_month")
        @Expose
        String event_month;
        @SerializedName("event_year")
        @Expose
        String event_year;
        @SerializedName("address")
        @Expose
        String address;
        @SerializedName("contactno")
        @Expose
        String contactno;


        public void setVenueName(String venueName) {
            this.venueName = venueName;
        }

        public String getVenueName() {
            return venueName;
        }

        @SerializedName("venue_name")
        @Expose
        String venueName;


        public String getEventid() {
            return eventid;
        }

        public String getEventname() {
            return eventname;
        }

        public String getDescription() {
            return description;
        }

        public String getEventimage() {
            return eventimage;
        }

        public int getFavourite_status() {
            return favourite_status;
        }

        public String getEvent_day() {
            return event_day;
        }

        public String getEvent_month() {
            return event_month;
        }

        public String getEvent_year() {
            return event_year;
        }

        public String getAddress() {
            return address;
        }

        public String getContactno() {
            return contactno;
        }

        public void setEventid(String eventid) {
            this.eventid = eventid;
        }

        public void setEventname(String eventname) {
            this.eventname = eventname;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setEventimage(String eventimage) {
            this.eventimage = eventimage;
        }

        public void setFavourite_status(int favourite_status) {
            this.favourite_status = favourite_status;
        }

        public void setEvent_day(String event_day) {
            this.event_day = event_day;
        }

        public void setEvent_month(String event_month) {
            this.event_month = event_month;
        }

        public void setEvent_year(String event_year) {
            this.event_year = event_year;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public void setContactno(String contactno) {
            this.contactno = contactno;
        }

    }

}
