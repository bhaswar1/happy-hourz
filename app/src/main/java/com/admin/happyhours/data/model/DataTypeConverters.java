package com.admin.happyhours.data.model;

import androidx.room.TypeConverter;

import com.admin.happyhours.data.model.api.response.SpecialPageRestuarantListingResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

public class DataTypeConverters {

    @TypeConverter
    public static String DetailListToString(List<SpecialPageRestuarantListingResponse.Detail> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<SpecialPageRestuarantListingResponse.Detail>>() {}.getType();
        String json = gson.toJson(list, type);
        return json;
    }

    @TypeConverter
    public static List<SpecialPageRestuarantListingResponse.Detail> stringToDetailLIst(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<LinkedList<SpecialPageRestuarantListingResponse.Detail>>() {}.getType();
        List<SpecialPageRestuarantListingResponse.Detail> measurements = gson.fromJson(json, type);
        return measurements;
    }
}
