package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceDetail {

    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("service_name")
    @Expose
    private String serviceName;

    public boolean isCheck() {
        return check;
    }

    private  boolean check=false;

    public String getServiceid() {
        return serviceId;
    }

    public void setServiceid(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String tagname) {
        this.serviceName = tagname;
    }

    public void setChecked(boolean isChecked) {
        this.check=isChecked;
    }
}
