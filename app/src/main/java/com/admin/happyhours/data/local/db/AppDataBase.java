package com.admin.happyhours.data.local.db;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.admin.happyhours.data.local.db.dao.UserDao;
import com.admin.happyhours.data.model.db.UserDataModel;

@Database(entities = {UserDataModel.class}, version = 2, exportSchema = false)
public abstract class AppDataBase extends RoomDatabase {

    public abstract UserDao userDao();

}
