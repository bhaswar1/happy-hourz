
package com.admin.happyhours.data.model.api.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details implements Serializable
{

    @SerializedName("otherimages")
    @Expose
    private ArrayList<String> otherimages = new ArrayList<>();
    @SerializedName("extra")
    @Expose
    private Extra extra;
    private final static long serialVersionUID = 3932385343201021104L;

    public ArrayList<String> getOtherimages() {
        return otherimages;
    }

    public void setOtherimages(ArrayList<String> otherimages) {
        this.otherimages = otherimages;
    }

    public Extra getExtra() {
        return extra;
    }

    public void setExtra(Extra extra) {
        this.extra = extra;
    }

}
