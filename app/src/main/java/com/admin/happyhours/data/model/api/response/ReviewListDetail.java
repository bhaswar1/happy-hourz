
package com.admin.happyhours.data.model.api.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReviewListDetail {

    @SerializedName("commentername")
    @Expose
    private String commentername;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("star")
    @Expose
    private String star;
    @SerializedName("event_day")
    @Expose
    private String eventDay;
    @SerializedName("event_month")
    @Expose
    private String eventMonth;
    @SerializedName("event_year")
    @Expose
    private String eventYear;
    @SerializedName("recentreviewimage")
    @Expose
    private List<Recentreviewimage> recentreviewimage = null;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;

    public String getCommentername() {
        return commentername;
    }

    public void setCommentername(String commentername) {
        this.commentername = commentername;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getEventDay() {
        return eventDay;
    }

    public void setEventDay(String eventDay) {
        this.eventDay = eventDay;
    }

    public String getEventMonth() {
        return eventMonth;
    }

    public void setEventMonth(String eventMonth) {
        this.eventMonth = eventMonth;
    }

    public String getEventYear() {
        return eventYear;
    }

    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    public List<Recentreviewimage> getRecentreviewimage() {
        return recentreviewimage;
    }

    public void setRecentreviewimage(List<Recentreviewimage> recentreviewimage) {
        this.recentreviewimage = recentreviewimage;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

}
