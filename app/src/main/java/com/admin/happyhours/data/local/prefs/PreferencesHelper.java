package com.admin.happyhours.data.local.prefs;

public interface PreferencesHelper {

    void setCurrentUserInfo(String email);

    void deleteCurrentUser();

    String getCurrentUserInfo();

    Long getCurrentUserId();

    void setCurrentUserId(Long userId);

    String getAccessToken();

    void setAccessToken(String accessToken);

    String getLatitude();

    void setLatitude(String latitude);

    String getLongitude();

    void setLongitude(String longitude);

    String getPassword();

    void setPassword(String password);

    void deletePassword();

    void setCenterLat(String lattitude);

    String getCenterLat();

    void  setCenterLong(String longitude);
    String  getCenterLong();



}
