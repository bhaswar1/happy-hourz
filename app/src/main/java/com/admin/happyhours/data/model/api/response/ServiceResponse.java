package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class ServiceResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Details")
    @Expose
    private LinkedList<ServiceDetail> details = new LinkedList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LinkedList<ServiceDetail> getDetails() {
        return details;
    }

    public void setDetails(LinkedList<ServiceDetail> details) {
        this.details = details;
    }
}
