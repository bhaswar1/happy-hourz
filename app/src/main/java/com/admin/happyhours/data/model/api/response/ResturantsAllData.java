
package com.admin.happyhours.data.model.api.response;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResturantsAllData
{

    public String getRestaurent_id() {
        return restaurent_id;
    }

    public void setRestaurent_id(String restaurent_id) {
        this.restaurent_id = restaurent_id;
    }

    @SerializedName("restaurent_id")
    @Expose
    private String restaurent_id;

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("coverimage")
    @Expose
    private String coverimage;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("open_time")
    @Expose
    private String openTime;
    @SerializedName("close_time")
    @Expose
    private String closeTime;
    @SerializedName("miles")
    @Expose
    private Integer miles;
    @SerializedName("Details")
    @Expose
    private Details details;
    @SerializedName("favourite_status")
    @Expose
    private Integer favouriteStatus;
    @SerializedName("collection_status")
    @Expose
    private Integer collectionStatus;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("latitude")
    @Expose
    private String latitude;

    public String getClaim_status() {
        return claim_status;
    }

    public void setClaim_status(String claim_status) {
        this.claim_status = claim_status;
    }

    @SerializedName("claim_status")
    @Expose
    private String claim_status;

    @SerializedName("total_review")
    @Expose
    private String total_review;

    public String getTotal_review() {
        return total_review;
    }

    public void setTotal_review(String total_review) {
        this.total_review = total_review;
    }

    @SerializedName("recentreviewimage")
    @Expose
    private ArrayList<Recentreviewimage> recentreviewimage = new ArrayList<>();
    @SerializedName("recentreview")
    @Expose
    private String recentreview;

    @SerializedName("recent_review_star")
    @Expose
    private Integer recent_review_star;

    @SerializedName("recent_reviewer_name")
    @Expose
    private String recent_reviewer_name;

    public String getRecentreview() {
        return recentreview;
    }

    public void setRecentreview(String recentreview) {
        this.recentreview = recentreview;
    }

    public ArrayList<Recentreviewimage> getRecentreviewimage() {
        return recentreviewimage;
    }

    public void setRecentreviewimage(ArrayList<Recentreviewimage> recentreviewimage) {
        this.recentreviewimage = recentreviewimage;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @SerializedName("longitude")
    @Expose
    private String longitude;
    private final static long serialVersionUID = -3315836205771626088L;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCoverimage() {
        return coverimage;
    }

    public void setCoverimage(String coverimage) {
        this.coverimage = coverimage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public Integer getMiles() {
        return miles;
    }

    public void setMiles(Integer miles) {
        this.miles = miles;
    }

//    public Details getDetails() {
//        return details;
//    }

//    public void setDetails(Details details) {
//        this.details = details;
//    }

    public Integer getFavouriteStatus() {
        return favouriteStatus;
    }

    public void setFavouriteStatus(Integer favouriteStatus) {
        this.favouriteStatus = favouriteStatus;
    }

    public Integer getCollectionStatus() {
        return collectionStatus;
    }

    public void setCollectionStatus(Integer collectionStatus) {
        this.collectionStatus = collectionStatus;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getRecent_review_star() {
        return recent_review_star;
    }

    public void setRecent_review_star(Integer recent_review_star) {
        this.recent_review_star = recent_review_star;
    }

    public String getRecent_reviewer_name() {
        return recent_reviewer_name;
    }

    public void setRecent_reviewer_name(String recent_reviewer_name) {
        this.recent_reviewer_name = recent_reviewer_name;
    }
}
