package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CollectionCreateResponse {


    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("collection_id")
    @Expose
    String collection_id;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getCollection_id() {
        return collection_id;
    }
}
