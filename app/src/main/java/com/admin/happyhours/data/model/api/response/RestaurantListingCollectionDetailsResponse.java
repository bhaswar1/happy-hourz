package com.admin.happyhours.data.model.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class RestaurantListingCollectionDetailsResponse {

    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("Details")
    @Expose
    LinkedList<Details> details;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public LinkedList<Details> getDetails() {
        return details;
    }

    public static class Details {

        @SerializedName("name")
        @Expose
        String name;
        @SerializedName("address")
        @Expose
        String address;
        @SerializedName("city_state")
        @Expose
        String city_state;
        @SerializedName("description")
        @Expose
        String description;
        @SerializedName("email")
        @Expose
        String email;
        @SerializedName("coverimage")
        @Expose
        String coverimage;
        @SerializedName("restaurent_id")
        @Expose
        String restaurent_id;

        public String getRestaurent_id() {
            return restaurent_id;
        }

        public void setRestaurent_id(String restaurent_id) {
            this.restaurent_id = restaurent_id;
        }

        public String getName() {
            return name;
        }

        public String getAddress() {
            return address;
        }

        public String getCity_state() {
            return city_state;
        }

        public String getDescription() {
            return description;
        }

        public String getEmail() {
            return email;
        }

        public String getCoverimage() {
            return coverimage;
        }
    }

}
