
package com.admin.happyhours.data;

import com.admin.happyhours.data.local.db.DbHelper;
import com.admin.happyhours.data.local.prefs.PreferencesHelper;

public interface DataManager extends PreferencesHelper, DbHelper {


    void setUserAsLoggedOut();


    void updateUserInfo(
            String user);

    enum LoggedInMode {

        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_GOOGLE(1),
        LOGGED_IN_MODE_FB(2),
        LOGGED_IN_MODE_SERVER(3);

        private final int mType;

        LoggedInMode(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }
}
