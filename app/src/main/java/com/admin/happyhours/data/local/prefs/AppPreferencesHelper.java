package com.admin.happyhours.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import com.admin.happyhours.di.PreferenceInfo;
import com.admin.happyhours.utils.AppConstants;

import java.security.acl.LastOwnerException;

public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_CURRENT_USER_INFO = "PREF_KEY_CURRENT_USER_INFO";

    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";

    private static  final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";

    private static  final String PREF_KEY_LATITUDE = "PREF_KEY_LATITUDE";

    private static  final String PREF_KEY_LONGITUDE = "PREF_KEY_LONGITUDE";

    private static  final String PREF_KEY_PASSWORD = "PREF_KEY_PASSWORD";

    private static  final String PREF_KEY_CLATTITUDE = "PREF_KEY_CLATTITUDE";
    private static  final String PREF_KEY_CLONGIITUDE = "PREF_KEY_CLONGIITUDE";


    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(Context context, @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public void setCurrentUserInfo(String email) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_INFO, email).apply();
    }

    @Override
    public void deleteCurrentUser() {
        mPrefs.edit().remove(PREF_KEY_CURRENT_USER_INFO).apply();
    }

    @Override
    public String getCurrentUserInfo() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_INFO, null);
    }

    @Override
    public Long getCurrentUserId() {
        return mPrefs.getLong(PREF_KEY_CURRENT_USER_ID,AppConstants.NULL_INDEX);
    }

    @Override
    public void setCurrentUserId(Long userId) {
        mPrefs.edit().putLong(PREF_KEY_CURRENT_USER_ID, userId);
    }

    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public String getLatitude() {
        return mPrefs.getString(PREF_KEY_LATITUDE, null);
    }

    @Override
    public void setLatitude(String latitude) {
        mPrefs.edit().putString(PREF_KEY_LATITUDE, latitude).apply();
    }

    @Override
    public String getLongitude() {
        return mPrefs.getString(PREF_KEY_LONGITUDE, null);
    }

    @Override
    public void setLongitude(String longitude) {
        mPrefs.edit().putString(PREF_KEY_LONGITUDE, longitude).apply();
    }

    @Override
    public String getPassword() {
        return mPrefs.getString(PREF_KEY_PASSWORD, null);
    }

    @Override
    public void setPassword(String password) {
        mPrefs.edit().putString(PREF_KEY_PASSWORD, password).apply();
    }

    @Override
    public void deletePassword() {
        mPrefs.edit().remove(PREF_KEY_PASSWORD).apply();
    }

    @Override
    public void setCenterLat(String lattitude) {
        mPrefs.edit().putString(PREF_KEY_CLATTITUDE,lattitude).apply();

    }

    @Override
    public String getCenterLat() {
        return mPrefs.getString(PREF_KEY_CLATTITUDE, null);
    }

    @Override
    public void setCenterLong(String longitude) {
        mPrefs.edit().putString(PREF_KEY_CLONGIITUDE, longitude).apply();
    }

    @Override
    public String getCenterLong() {
        return mPrefs.getString(PREF_KEY_CLONGIITUDE,null);
    }
}
